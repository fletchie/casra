# CASRA++

C++ Implementation of the CASRA simulation and reconstruction code. 


Related projects include: 
* [CASRA 2D](https://gitlab.com/fletchie/casra-2d)
* [CASRA 3D](https://gitlab.com/fletchie/casra-3d)


Relevant works include:
* [Fast modelling of field evaporation in atom probe tomography using level set methods](https://iopscience.iop.org/article/10.1088/1361-6463/ab3703)
* [Towards model-driven reconstruction in atom probe tomography](https://iopscience.iop.org/article/10.1088/1361-6463/abaaa6)
* [Model-driven reconstruction in atom probe tomography (Thesis)](https://ora.ox.ac.uk/objects/uuid:c37f5d70-4031-4d31-afdb-05cee63237c4)
* [Automated calibration of model-driven reconstructions in atom probe tomography](https://iopscience.iop.org/article/10.1088/1361-6463/ac7986)

The current code can be found within the directory `CASRA`. Example simulation configurations can be found within `models`.

# Licensing 

The majority of the code is licensed under LGPLv3 (see attached license). Included 3rd party libraries (modified or not) retain their original licensing.

# How to use

A how-to-use guide, along with more detailed build instructions, are coming soon. 

## Build Instructions

# Dependencies

No required dependencies are necessary. Optional dependencies include:
* OpenMP (versions >=2.0 supported) : For parallelisation
* Doxygen : For automated code documentation generation

### Ubuntu

Development has been performed in Ubuntu 20 (g++8 and CMAKE 3.5), and so this is the most tested system/compiler configuration.

Within the console:

1. enter CASRA directory within repository (cd CASRA)
2. create build directory and enter (mkdir build && cd build)
3. configure the make files
```{bash}
cmake ..
```
4. Perform program compilation and installation
```{bash}
make install
```

The final line will copy the the compiled program within a special installation folder and set its system path so it can be called from any location via the console. 

### Windows

Compilation can also be performed on Windows using Microsoft's MSVC compiler (tested with VS2019 and VS2021). Just select the project directory containing the CMAKE file (`CASRA`)
and visual studio should do the rest (although deployment mode might have to be selected). Linking to both OpenBLAS and MKL has been tested, although system path variables must be set correctly. A more detailed guide for this will be included in the future. 

### Unit Tests

On successful compilation a number of unit tests are also generated. These can be used to quickly confirm various program behaviours are as expected (alerting the user if any silent compilation issues or system specific bugs have arisen). On Ubuntu these can be easily run via the command. 

```{bash}
make test
```

Any failed tests will be flagged. 

## Installed Programs

On completing the installation (assuming it finished successfully), a number of command line programs will be created. These include:

* `LSEvaporationSimulator`: Runs the LS simulator for calculating specimen field evaporation 
* `trajectorySimulator`: The ion trajectory calculator (also used to generate synthetic data) 
* `trajectoryTrimmer`: For calculating the mapping data and simulated detector hitmaps following a uniform ion projection
* `dataReconstruct`: Program for performing both conventional Bas reconstruction, as well as model-driven reconstruction using simulation results
* `vacuumFieldSolver`: A simple program for calculating the electric field surrounding a generated surface mesh. Not required by the core model, instead serving as a visualisation and analysis aid. 
* `phaseExtractor`: Extracts specimen phase meshes as contours from a passed 3D TIFF image file (a standard, widely-used format for Electron Tomography datasets).

Help for the various progrsm inputs can be obtained by adding the -h flag when called from any location on the console. 

e.g.
```{bash}
phaseExtractor -h
```

While the specific configuration file details can be passed to these programs, the easiest approach is to run each of them from within the model directory containing the required `configuration.xml` (see below). All model outputs will then also be generated in this directory.

## Running examples

Several different example model configurations can be found within the `models` directory. Unlike previous implementations, only a single configuration.xml setup file is required. This file defines the specimen model (with initial phases meshes defined either via `tipBuilder` instructions, `phaseExtractor` instructions, or a passed vtk file), configures the evaporation simulation, and defines the ion trajectory calculation parameters. Examples for the various parameters that the model configuration can take can be found within these examples. 

A typical workflow is given within the included shell scripts. In short for...

* synthetic data simulation: `LSEvaporationSimulator` -> `trajectorySimulator`
* trajectory mapping and fast hitmap simulation: `LSEvaporationSimulator` -> `trajectorySimulator` -> `trajectoryTrimmer`

## Creating your own models

In order to create your own models, create a new directory and place the `configuration.xml` within it. This is most easily done by copying one from the examples, and modifying the various parameters within it. Assuming a valid configuration, the installed programs above can then be run from within this directory. 

## model outputs

Model outputs are generated within the same directory containing the model's `configuration.xml` file. A number of the output files are of vtk legacy format. I recommend using [Paraview](https://www.paraview.org) to aid visualisation and post-simulation data analysis. 

## Program Support

For additional help in setting up or using the program, or if you have any anomalous behaviour or bugs to report, do get in contact with me at via charles.ea.fletcher@outlook.com
