#!/bin/bash

#this example simulation pipeline should be run from within the model/void directory via ./run_example_void

#run the LS model
LSEvaporationSimulator

#run the trajectory simulator (projection mode set to uniform within configuration file e.g. one ion per panel corner)
trajectorySimulator

#run the trajectory trimmer (calculates the trajectory mapping and density hitmap from the uniform projection performed previously)
trajectoryTrimmer
