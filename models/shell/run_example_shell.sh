#!/bin/bash

#this example simulation pipeline should be run from within the model/shell directory via ./run_example_shell

#run the LS model
LSEvaporationSimulator

#run the trajectory simulator (projection mode set to realistic within configuration.xml file, e.g. synthetic data generated)
trajectorySimulator

reconstructData --aptdata synthetic_data/synthetic_data.epos --rangefile synthetic_data/synthetic_data.rrng --config reconstruction_configuration.xml --output recon/pprecon.pos


