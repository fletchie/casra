#!/bin/bash

#this example simulation pipeline should be run from within the model/finfet directory via ./run_example_finfet

#run the LS model
LSEvaporationSimulator

#run the trajectory simulator (projection mode set to realistic within configuration.xml file, e.g. synthetic data generated)
trajectorySimulator

projectionReconstructData --aptdata synthetic_data/synthetic_data.epos --rangefile synthetic_data/synthetic_data.rrng --config recconfiguration.xml

