#!/bin/bash

#this example simulation pipeline should be run from within the model/precipitate directory via ./run_example_precipitate

#run the LS model
LSEvaporationSimulator

#run the trajectory simulator (projection mode set to realistic within configuration.xml file, e.g. synthetic data generated)
trajectorySimulator

reconstructData --aptdata synthetic_data/synthetic_data.epos --rangefile synthetic_data/synthetic_data.rrng --config reconstruction_configuration.xml

