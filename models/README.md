# Models

A directory containing various example models. To run these, enter into the relevant model directory, and either run the desired program [see instructions](https://gitlab.com/fletchie/casra) or run the shell script pipeline (which executes a specific sequence of CASRA++ programs). 
