#!/usr/bin/env python3

"""

Python Module for creating bin species across the detector space. This can 
be used to determine the position of experimental landmarks e.g. the ion index
when particular phases appear or disappear from the detected ion sequence.

Required dependencies: pandas, numpy

Ideally, the optional dependency plotly should be installed to allow for 
interactive visualisations. However, matplotlib can be used as an alternative
dependency.    

Also outputs voltage curve for experimental data.

Author: Charles Fletcher
Atom Probe Tomography Group, Department of Materials, University of Oxford
Dec 2021

"""

import os
import math
import re
import struct

try:
    import numpy as np
except ModuleNotFoundError:
    raise ModuleNotFoundError("Cannot find Python package numpy. " +
                              "Please make sure numpy is installed.")

plotly_installed = True

try:
    import pandas as pd
except ModuleNotFoundError:
    print("Cannot find Python package pandas. Please make sure pandas " +
          "and plotly are both installed to use interactive plotly plots. " +
          "Falling back to matplotlib.")
    plotly_installed = False

try:
    import plotly.express as px
except ModuleNotFoundError:
    print("Cannot find Python package plotly. Please install for " +
          "interactive histogram functionality. \n Falling back to " +
          "matplotlib functions.")
    plotly_installed = False
    try:
        import matplotlib.pyplot as plt
    except ModuleNotFoundError:
        raise ModuleNotFoundError("Cannot find Python package matplotlib. " +
                                  "Please make sure either plotly or " +
                                  "matplotlib is installed.")

#return filesize of fileobject
def getSize(fileobject):
    """

    Returns the binary file size

    :param1 fileobject: The read file object
    :returns:
        size - The file object size (e.g. number of lines)

    """
    fileobject.seek(0,2) # move the cursor to the end of the file
    size = fileobject.tell()
    fileobject.seek(0) # move the cursor to the beginning of the file
    return size

def read_epos(filename, sample = 10):

    """

    A python reader for the .epos APT data file.
    Chunks reading of file to handle large file sizes
    Also samples ions (sample rate defined by `sample`)
    Once unpacked the 1D array d contains
    the following elements:

    'x': d[0::11], #reconstructed x-coordinate of ion within sample
    'y': d[1::11], #reconstructed y-coordinate of ion within sample
    'z': d[2::11], #reconstructed z-coordinate of ion within sample
    'Da': d[3::11],
    'ns': d[4::11],
    'DC_kV': d[5::11],
    'pulse_kV': d[6::11],
    'det_x': d[7::11],
    'det_y': d[8::11]})
    'pslep': d[9::11], # pulses since last event pulse
    'ipp': d[10::11]}) # ions per pulse

    The method also calculates and prints the instrument flightpath, 
    variance in the ion flight path, and the detector radius.

    :param1 filename: The .epos filename
    :param2 sample: The sample rate (defaults to one in every 10)
    :returns:
        det_x
        det_y
        Da
        pulse voltage
        pslep
        x
        y
        z

    """

    if filename.split(".")[-1] != "epos":
        raise TypeError("Incorrect filetype detected (based off name). Method only supports .epos file types. Please call appropriate reader.")

    # read in the data
    try:
        f = open(filename, "rb")
    except:
        raise FileNotFoundError("Could not open or read file: " + filename + "\n Please make sure the correct file (.epos) and location has been passed.")

    n = int(getSize(f)/4) #length of integers (bytes)
    rs = int(n / 11) #number of entries/rows

    d = []

    #chunk data reading to 44 bytes i.e. row by row (while sampling rows)
    for a in np.arange(0, (n * 4)/(11 * 4), sample, dtype = int):
        f.seek(a * 11 * 4)
        byte = f.read(44)
        unpacked = struct.unpack('>'+'fffffffffII'*1, byte)
        d.extend(unpacked)
                    # '>' denotes 'big-endian' byte order

    #calculate instrument flightpath:
    voltage = np.array(d[5::11]) + np.array(d[6::11])
    print("estimated instrument flightpath: " + str(np.mean(((2.0 * voltage * 1.6e-19/(np.array(d[3::11])* 1.67e-27) * (1e-9 * np.array(d[4::11]))**2.0))**0.5)))
    est_det_rad = np.mean([abs(np.min(np.array(d[7::11]) * 1e-3)), abs(np.max(np.array(d[7::11]) * 1e-3)), abs(np.min(np.array(d[8::11]) * 1e-3)), abs(np.max(np.array(d[8::11])) * 1e-3)])
    print("estimated detector radius: " + str(est_det_rad))

    VP = np.array(d[5::11]) + np.array(d[6::11])
    return np.array(d[7::11]) * 1e-3, np.array(d[8::11]) * 1e-3, np.array(d[3::11]), VP, d[9::11], np.array(d[0::11])*1e-9, np.array(d[1::11])*1e-9, np.array(d[2::11])*1e-9

def read_ato(filename):

    """

    A python reader for the .ato APT data file. Once unpacked the 1D array d contains
    the following elements:

    'x': d[8::14], #reconstructed x-coordinate of ion within sample
    'y': d[9::14], #reconstructed y-coordinate of ion within sample
    'z': d[10::14], #reconstructed z-coordinate of ion within sample
    'Da': d[11::14]
    'pn': d[13::14]
    'DC_kV': d[14::14]
    'TOF': d[15::14], ###micro seconds
    'det_x': d[16::14]
    'det_y': d[17::14]
    'pulse_kV': d[19::14]

    The method also calculates and prints the instrument flightpath, variance 
    in the ion flight path, and the detector radius.

    :param1 filename: The .ato filename
    :returns:
        det_x
        det_y
        Da
        pulse voltage
        pslep
        x
        y
        z

    """

    if filename.split(".")[-1] != "ato":
        raise TypeError("Incorrect filetype detected (based off name). " + 
                        "Method only supports .ato file types. Please call appropriate reader.")

    # read in the data
    try:
        f = open(filename, "rb")
    except:
        raise FileNotFoundError("Could not open or read file: " + filename + 
                                "\n Please make sure the correct file (.ato) and location has been passed.")

    n = int(getSize(f)/4) #length of integers (bytes)
    rs = int(n / 14) #number of entries/rows
    byte = f.read()

    ##note constant bytes (head) at start of binary file
    d = struct.unpack('<'+'cccccccc' + 'ffffIIffffffff'*rs, byte)
    # '<' denotes 'little-endian' byte order

    # unpack data
    #x,y,z,mass,clusID,pIndex,Vdc,TOF,dx,dy,dz,Vp,shank,FouR,FouI

    voltage = 1e3 * (np.array(d[19::14]) + np.array(d[14::14]))
    
    print("estimated instrument flightpath: " + 
          str(np.mean(((2 * voltage * 1.6e-19/(np.array(d[11::14])* 1.67e-27) * (1e-6 * np.array(d[15::14]))**2.0))**0.5)))
    est_det_rad = np.mean([abs(np.min(np.array(d[16::14]) * 1e-2)), 
                           abs(np.max(np.array(d[16::14]) * 1e-2)), 
                           abs(np.min(np.array(d[16::14]) * 1e-2)), 
                           abs(np.max(np.array(d[16::14])) * 1e-2)])
    print("estimated detector radius: " + str(est_det_rad))
    
    VP = np.array(d[19::14]) + np.array(d[14::14])
    
    return (np.array(d[16::14]) * 1e-2, np.array(d[17::14]) * 1e-2, 
            np.array(d[11::14]), VP, np.array(d[13::14]), np.array(d[8::14]), 
            np.array(d[9::14]), np.array(d[10::14]))

def read_rrng(f):

    """

    Method for reading range files

    :param1 f: The .range filename
    :returns:
        ions - Pandas dataframe of containing the ion number and its corresponding name (ionic compound)
        rrngs - Pandas dataframe with following structure:
                'ionic number','lower Da range','upper Da range','vol fraction','composition','colour'

    """

    try:
        rf = open(f,'r').readlines()
    except:
        raise FileNotFoundError("Could not open or read file: " + f + 
                                "\n Please make sure the correct file (.range) " + 
                                "and location has been passed.")

    print("called range reader")
    rions = []
    rrrngs = []
    ion_header = False
    rrng_header = False

    for line in rf:
        line = line.strip("\n")
        line = line.strip(" ")
        idx = line.find("[Ions]")
        if (idx != -1):
            ion_header = True
            continue
        if (ion_header == True):
            idx = line.find("Ion")
            if (idx != -1):
                rions.append(line.split("="))
                continue
            idx = line.find("[Ranges]")
            if (idx != -1):
                ion_header = False
                rrng_header = True
                continue
        if (rrng_header == True):
            idx = line.find("Range")
            if (idx != -1):
                rline = line.split("=")[1]
                rrline = rline.split(" ")
                if (len(rrline) >= 5):
                    nline = [rline]
                    nline.extend(rrline[:5]) 
                    rrrngs.append(nline)
            
    ions = {}
    for a in range(len(rions)):
        ions[rions[a][0]] = rions[a][1]

    rrngs = {}
    for a in range(len(rrrngs)):
        rrngs[rrrngs[a][0]] = {}
        rrngs[rrrngs[a][0]]["lower"] = float(rrrngs[a][1])
        rrngs[rrrngs[a][0]]["upper"] = float(rrrngs[a][2])
        rrngs[rrrngs[a][0]]["vol"] = float(rrrngs[a][3].split(":")[1])
        rrngs[rrrngs[a][0]]["comp"] = rrrngs[a][4]
        rrngs[rrrngs[a][0]]["colour"] = rrrngs[a][5]

    if len(ions) == 0:
        raise ValueError("Warning: no ions have been read! Please ensure that the " + 
                         "correct file has been past.")

    return ions,rrngs

#given range file and mass to charge, classify ion
def range_APT_data(da, range_dict):

    """

    Function for classifying ions based off their Da (mass-to-charge) given a range file

    :param1 da: The ionic mass-to-charge ratio
    :param2 range_dict: Python dictionary containing ranges

    :returns:
        chem - 1*len(da) array of ion numbers
        ions - list of corresponding string names for ion numbers in chem (by index)
        ic - The number of atoms for the particular ion (e.g. number of atoms in compound ion)

    """

    chem = np.zeros((len(da)), dtype = int)

    ions = []

    for num in range_dict:
        ions.append(range_dict[num]["comp"])

    ions = list(set(ions))

    da = np.array(da)

    for num in range_dict:
        lower = range_dict[num]["lower"]
        upper = range_dict[num]["upper"]

        #format N:2 -> N2 ion, separate id from N1
        ion = range_dict[num]["comp"]

        ###non-ranged data left as zero
        chem[(lower < da) * (upper > da)] = ions.index(ion) + 1

    #calculate the number of species in a detection event (e.g. number of atoms within a compound ion)
    ic = []
    for a in ions:
        ic.append(0)
        for v in re.split(":| ", a):
            if v.isdigit():
                ic[-1] += int(v[-1])

    #unidentified ions in chem labelled as 0
    return chem, ions, ic

def load_apt_data(filename, rangefile="", sample_rate=1.0):
    """

    Function for reading in APT data. Method detects whether file type is
    .tsv, .epos, or .ato and runs appropriate subroutine. Therefore,
    the file extension is critical for normal functionality of the method.

    :param1 filename (str): The APT data file
    :param2 rangefile (str): The range file (required for .epos and .ato files)
    :param3 sample_rate (float): The ion sampling rate
    :returns:
        :dx (list): detector x coordinate (m)
        :dy (list): detector y coordinate (m)
        :sx (list): sample x coordinate (m). Returns empty array if unapplicable.
        :sy (list): sample y coordinate (m). Returns empty array if unapplicable.
        :sz (list): sample z coordinate (m). Returns empty array if unapplicable.
        :chem (list): The ion chemistry (integer)
        :ions (list): The corresponding ion label to the chem integer values (by index)
        :ic (list): The number of nuclei making up each ion
        :VP (list): The pulse voltage data (only returned for experimental APT data
                    - obtainable from model .vtks for simulated data)

    :raises:
        FileNotFoundError: could not open/read from parsed APT data file (param1 filename)
        FileNotFoundError: could not open/read from parsed rangefile (param2 rangefile)
        TypeError: Input file (param1 filename) has wrong file extension
                   (not .tsv, .ato, or .epos)

    """

    sample_num = int(1.0/sample_rate)

    #case of .epos file
    if filename.split(".")[-1] == "epos":
        if not rangefile:
            raise FileNotFoundError("Please make sure a range file has been passed to " +
                                    "the method (required for .epos file types).")

        ions, rfile = read_rrng(rangefile)
        dx, dy, da, VP, pslep, sx, sy, sz = read_epos(filename,
                                                      sample_num)
        sx = sx * 1e-9
        sy = sy * 1e-9
        sz = sz * 1e-9
        chem, ions, ic = range_APT_data(da, rfile)
        da_p = True

    #case of .ato file
    elif filename.split(".")[-1] == "ato":
        if len(rangefile) == 0:
            raise FileNotFoundError("Please make sure a range file has been passed " +
                                    "to the method (required for .epos file types).")

        ions, rfile = read_rrng(rangefile)
        dx, dy, da, VP, tsld, sx, sy, sz = read_ato(filename)

        sx = sx * 1e-10
        sy = sy * 1e-10
        sz = sz * 1e-10
        chem, ions, ic = range_APT_data(da, rfile)
        da_p = True

    else:
        raise TypeError("input file not .tsv, .ato, or .epos. Please input file with " +
                        "correct suffex.")

    print("dataset loaded successfully")
    return dx, dy, sx, sy, sz, chem, ions, ic, VP, da, da_p

def ion_histogram(filename, out_loc, rangefile, bins,
                  det_rad=math.inf, autoopen=False):

    """

    Function for generating the ion histogram from the APT data
    If plotly is installed, an interactive plot is outputted as a .html
    Also outputs interactive plotly voltage curve for experimental data

    :param1 filename (str): The input APT data
    :param2 out_loc (str): The output file location
    :param3 rangefile (str): The range file for the APT data
    :param4 bins (int): The number of histogram bins
    :param5 det_rad (float): The detector radius (default is
                             infinity i.e. all ions included)

    :returns:
        None

    :raises:
        :ValueError: No input APT data file found

    """

    #create output directory if not found
    out_dir = out_loc.split("/")
    out_filename = out_dir[-1] 

    if (len(out_dir) == 1):
        out_dir = "."
    if not os.path.exists(out_dir):
        print("creating output directory: " + out_dir)
        os.makedirs(out_dir, exist_ok=True)

    #load APT data - inc range data
    print("Loading dataset...")
    (dx, dy, sx, sy, sz, chem,
     ions, ic, VP, da, da_p) = load_apt_data(filename, rangefile)
    print("dataset loaded")

    chem = chem.astype(int)
    chem = np.array(chem)
    VP = np.array(VP)

    #filter out ions outside of the detector radius
    rad = dx**2.0 + dy**2.0
    dx = dx[rad < det_rad**2.0]
    dy = dy[rad < det_rad**2.0]
    chem = chem[rad < det_rad**2.0]

    if len(VP) == len(rad):
        VP = VP[rad < det_rad**2.0]

    real_data = False
    #remove unranged ions
    if len(ions) != 0:
        fchem = chem[chem != 0]
        VP = VP[chem != 0]
        real_data = True
    else:
        fchem = chem
        real_data = False

    ion_seq = np.arange(0, len(fchem), 1)
    if len(ions) == 0:
        ions = np.unique(chem)

    #create histogram arrays
    hist_val = np.zeros((len(ions), bins))

    #construct histogram
    if real_data is True:
        for j in range(0, len(ions)):
            hist, bin_edges = np.histogram(ion_seq[fchem == j + 1],
                                           bins=bins, range=(0, len(fchem)))
            hist_val[j, :] += hist
            c = (bin_edges[:-1] + bin_edges[1:])/2.0
    else:
        for j in range(0, len(ions)):
            hist, bin_edges = np.histogram(ion_seq[fchem == ions[j]],
                                           bins=bins, range=(0, len(fchem)))
            hist_val[j, :] += hist
            c = (bin_edges[:-1] + bin_edges[1:])/2.0

    #output interative plotly visualisations if plotly installed
    if plotly_installed is True:
        #create dataframe and output plotly figure
        df = pd.DataFrame(data=np.vstack((c, hist_val)).T,
                          columns=np.append(["detected ion number"],
                          ions))
        pdf = pd.melt(df,
                      id_vars="detected ion number",
                      var_name="species",
                      value_name="total counts")
        fig = px.line(pdf,
                      x="detected ion number",
                      y="total counts",
                      color='species')
        fig.write_html(out_dir + '/' + out_filename + '.html',
                      auto_open=autoopen)
        print("Successfully outputed plotly interactive species " +
              "histogram to: " + out_dir + '/' + out_filename + '.html')

        if len(VP) == len(ion_seq):
            #output voltage curve
            vdf = pd.DataFrame(data=np.vstack((ion_seq[::10], VP[::10])).T,
                               columns=["detected ion number", "Voltage Curve (kV)"])
            fig = px.line(vdf,
                          x="detected ion number",
                          y="Voltage Curve (kV)")
            fig.write_html(out_dir + '/' + out_filename + '.html',
                           auto_open=False)
            print("Successfully outputed plotly interactive " +
                  "voltage curve to: " + out_dir + '/' + out_filename + '.html')

    else:
        #output matplotlib visualisations (plotly installation not detected)
        for a in range(0, len(hist_val)):
            plt.plot(c, np.log(hist_val[a]), label=ions[a])
        plt.legend()
        plt.xlabel("detected event index")
        plt.ylabel("species count")
        plt.savefig(out_dir + '/' + out_filename + ".png")
        print("Successfully outputed matplotlib species histogram " +
              "to: " + out_dir + '/' + out_filename + ".png")
        plt.close()

        plt.plot(VP)
        plt.ylim(0, max(VP))
        plt.xlabel("detected event count")
        plt.ylabel("evaporation voltage")
        plt.savefig(out_dir + '/' + out_filename + ".png")
        print("Successfully outputed matplotlib voltage curve " +
              "to: " + out_dir + '/' + out_filename + '.png')
        plt.close()

#argument parser for if called from command line
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--aptdata', metavar=': input APT event data (REQUIRED)',
                        type=str, default="",
                        help='The input APT data (either .tsv, .epos or .ato).')
    parser.add_argument('--output', metavar=': output file location (OPTIONAL)', type=str, default="species_hist",
                        help='The output file location where the species histogram ' +
                        'will be saved. Note that the appropriate file ending will be appended.')
    parser.add_argument('--rangefile', metavar=': input range file (REQUIRED)', type=str,
                        default = "",
                        help='The range file required for APT experimental ' +
                        'data for labelling ions')
    parser.add_argument('--bins', metavar=': species histogram bin number (OPTIONAL)', type=int,
                        default=120, help='Number of histogram bins')
    parser.add_argument('--detector_radius', metavar=': detector radius (OPTIONAL)',
                        type=float, default=math.inf,
                        help='The detector radius. Default is infinity ' +
                        '(all ions included).')

    args = parser.parse_args()

    #simulation parameter definition
    filename = args.aptdata
    if not filename:
        raise ValueError("No input APT data filename found. " +
                         "Please pass in filename. Possible input arguments " + 
                         "can be checked via './species_distribution.py -h'")

    out_loc = args.output
    rangefile = args.rangefile
    bins = args.bins
    det_rad = args.detector_radius

    ion_histogram(filename, out_loc, rangefile, bins, det_rad)
