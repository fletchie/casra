# Additional Analysis Tools

This directory contains non-core CASRA related tools to aid with modelling, reconstruction, and data analysis. 

Currently this includes: 

* ```experimentalLandmarkReviewer.py``` : Experimental Landmark Reviewer is a script for visualising and interpreting chemical changes in the detector event sequence of a particular apt data and range file. In particular, this script allows for positions in the sequence to be identified where particular phases are entering or exiting the experiment FOV (based off a composition change). These experimental landmark positions can be matched to particular model phases, and together these can define the landmarks used during the depth calibration stage in the [model-driven reconstruction protocol](../CASRA/src/reconstruction/modelDrivenReconstruction)

## Dependencies

Required:
* numpy
* pandas
* matplotlib/plotly (one of these is required, preferably plotly)
