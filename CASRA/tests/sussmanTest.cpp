/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/testMeshes.h"
#include "CASRA/mesh/marchingCubes.h"

#include "CASRA/field/testFields.h"

#include "CASRA/evaporationModels/levelSetModel/lscore.h"
#include "CASRA/io/modelDataio.h"

#define SMALL 1.0e-30

/*!
 * Template function for returning the sign of a variable
 * @param T Template input variable
 * @return sign of T
 */
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

using namespace std;

typedef vector<vector<double>> pointsType;
typedef vector<PointData> pointDataType;
typedef vector<vector<int>> cellsType;
typedef vector<CellData> cellDataType;

int sussmanReinit(int xDim, int yDim, int zDim);

int main(int argc, char* argv[])
{
  int sussRet = sussmanReinit(60, 60, 60);
  return sussRet;

}

int sussmanReinit(int xDim, int yDim, int zDim){

  cerr << "Performing Sussman Reinitialisation unit test" << endl;

  int pass = 0;

  Point PLC(0.0, 0.0, 0.0);
  double cellWidth = 1.0;
  scalarField3D rField = buildRectangleSignField(xDim, yDim, zDim);

  vtkObject out;
  out.setWithField(rField);
  writeVtk(out, "test_results/sussman_test/initial_field.vtk");

  LevelSetDomain lSDom(xDim, yDim, zDim, PLC, cellWidth);
  lSDom.lSField = rField;

  vector<vector<vector<double>>> cmesho;
  vector<double> vfo;

  for (int i=0;i<(lSDom.lSField.size());i++){
    for (int j=0;j<(lSDom.lSField[0].size());j++){

      Point p00(i, j, 0);
      Point p01(i+1, j, 0);
      Point p10(i, j+1, 0);
      Point p11(i+1, j+1, 0);

      vector<vector<double>> triangle1 = {{p00.px, p00.py, p00.pz}, {p01.px, p01.py, p01.pz}, {p10.px, p10.py, p10.pz}};
      vector<vector<double>> triangle2 = {{p01.px, p01.py, p01.pz}, {p10.px, p10.py, p10.pz}, {p11.px, p11.py, p11.pz}};
      vfo.push_back(lSDom.lSField[i][j][(int)zDim/2]);
      vfo.push_back(lSDom.lSField[i][j][(int)zDim/2]);

      cmesho.push_back(triangle1);
      cmesho.push_back(triangle2);

    }
  }

  TriangularMesh outMesh0;
  unpackUniqueVertices(cmesho, &outMesh0);
  outMesh0.assignCellData(vfo, "field");

  vtkObject om;
  om.setWithTriangularMesh(outMesh0);
  writeVtk(om, "test_results/sussman_test/inFieldCross.vtk");

  cerr << "Perform Sussman Reinitialisation...";
  int rval = lSDom.sussmanReinitialisation(0.25, 400);

  out.setWithField(lSDom.lSField);
  writeVtk(out, "test_results/sussman_test/final_field.vtk");

  vector<vector<vector<double>>> cmesh;
  vector<double> vf;

  for (int i=0;i<(lSDom.lSField.size());i++){
    for (int j=0;j<(lSDom.lSField[0].size());j++){

      Point p00(i, j, 0);
      Point p01(i+1, j, 0);
      Point p10(i, j+1, 0);
      Point p11(i+1, j+1, 0);

      vector<vector<double>> triangle1 = {{p00.px, p00.py, p00.pz}, {p01.px, p01.py, p01.pz}, {p10.px, p10.py, p10.pz}};
      vector<vector<double>> triangle2 = {{p01.px, p01.py, p01.pz}, {p10.px, p10.py, p10.pz}, {p11.px, p11.py, p11.pz}};
      vf.push_back(lSDom.lSField[i][j][(int)zDim/2]);
      vf.push_back(lSDom.lSField[i][j][(int)zDim/2]);

      cmesh.push_back(triangle1);
      cmesh.push_back(triangle2);

    }
  }

  TriangularMesh outMesh;
  unpackUniqueVertices(cmesh, &outMesh);
  outMesh.assignCellData(vf, "field");

  vtkObject om2;
  om2.setWithTriangularMesh(outMesh);
  writeVtk(om2, "test_results/sussman_test/outFieldCross.vtk");

  cerr << "complete." << endl;
  cerr << "Iterations: " << rval << endl;

  //check signed distance form restored
  if (rval > 0){
    pass = 0;
    cerr << "SUCCESS: convergence achieved as expected." << endl;
  }
  else {
    pass = 1;
    cerr << "FAIL: convergence not achieved as expected." << endl;
  }

  return pass;

}
