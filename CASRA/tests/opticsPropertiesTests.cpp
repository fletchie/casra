/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>
#include <tuple>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/chargedOptics/chargedOptics.h"
#include "CASRA/chargedOptics/hitmapCalculation.h"

#define SMALL 1e-10

using namespace std;

int calculateTestMagnifications();
int performTestLinearProjection();
int performTriangleRectangleIntersection();

int main(int argc, char* argv[])
{
  cout << "Performing magnification calculation test" << endl;
  int testRes1 = calculateTestMagnifications();
  if (testRes1 == 0){
    cerr << "\tSUCCESS: Test passed" << endl;
  }
  else {
    cerr << "\tFAILURE: Test failed" << endl;
  }

  cout << "Performing liner projection test" << endl;
  int testRes2 = performTestLinearProjection();
  if (testRes2 == 0){
    cerr << "\tSUCCESS: Test passed" << endl;
  }
  else {
    cerr << "\tFAILURE: Test failed" << endl;
  }

  cout << "Performing triangle-rectangle intersection test" << endl;
  int testRes3 = performTriangleRectangleIntersection();
  if (testRes3 == 0){
    cerr << "\tSUCCESS: Test passed" << endl;
  }
  else {
    cerr << "\tFAILURE: Test failed" << endl;
  }
  return testRes1 | testRes2 | testRes3;
}

int performTestLinearProjection(){

  Point ionPosition(5.0, -4.0, 0.0);
  Vec ionLaunchNormal(0, 0.0, 1);

  Point dPos(0.0, 0.0, 10.0);
  Vec normal(0.0, 0.0, 1.0);
  double radius = 100;

  Detector detector(dPos, normal, radius);
  double kappa = 1.0;

  Point intersectionPoint = ionPosition;
  bool intersects = detector.linearProjection(intersectionPoint, ionLaunchNormal, kappa);
  int notPassed = 1;

  if (fabs(intersectionPoint.px - 5.0) < SMALL && fabs(intersectionPoint.py + 4.0) < SMALL && fabs(intersectionPoint.pz - 10.0) < SMALL){
    notPassed = 0;
  }
  return notPassed;
}

int calculateTestMagnifications(){

  Point P1L(0.3, 0.0, 1.5);
  Point P2L(1.0, -6.0, -0.3);
  Point P3L(2.0, 2.0, 1.0);

  Point P1D(0.0, 0.0, 0.0);
  Point P2D(1.0, 0.0, 0.0);
  Point P3D(1.0, 1.0, 0.0);

  tuple<double, double, double, double, double> testRes = calculateLocalMagnification(P1L, P2L, P3L, P1D, P2D, P3D);

  double J = get<0>(testRes);

  int notPassed = 0;

  //calculate original panel area via Heron's formula
  double ABl = norm(P1L, P2L);
  double ACl = norm(P1L, P3L);
  double BCl = norm(P2L, P3L);
  double s = (ABl + ACl + BCl)/2.0;
  double A1 = sqrt(s * (s - ABl) * (s - BCl) * (s - ACl));

  //calculate projected panel area via Heron's formula
  ABl = norm(P1D, P2D);
  ACl = norm(P1D, P3D);
  BCl = norm(P2D, P3D);
  s = (ABl + ACl + BCl)/2.0;
  double A2 = sqrt(s * (s - ABl) * (s - BCl) * (s - ACl));

  cout << J << endl;
  cout << A2/A1 << endl;

  //compare calculated Jacobian determinant to analytical solution for panel magnification (panel area ratio)
  if (fabs(fabs(J) - A2/A1) >  SMALL){
    notPassed = 1;
  }

  return notPassed;

}

int performTriangleRectangleIntersection(){

  //calculate panel area via Heron's formula
  Point detPos1(0.0, 0.0, 0.0);
  Point detPos2(0.0, 1.0, 0.0);
  Point detPos3(1.0, 1.0, 0.0);

  double sxmin = 0.0;
  double symin = 0.0;
  double sxmax = 0.5;
  double symax = 0.5;

  double ABl = norm(detPos2, detPos1);
  double ACl = norm(detPos3, detPos1);
  double BCl = norm(detPos3, detPos2);

  double s = (ABl + ACl + BCl)/2.0;
  double triangleArea = sqrt(s * (s - ABl) * (s - BCl) * (s - ACl));

  double intersectionArea = triangleRectangleIntersection(sxmin, symin, sxmax, symax, detPos1, detPos2, detPos3, triangleArea);
  cout << "triangle-cell intersection area: " << intersectionArea << endl;

  int notPassed = 0;
  if (fabs(intersectionArea - 0.5 * 0.5 * 0.5) >  SMALL){
    notPassed = 1;
  }

  return notPassed;

}
