/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <math.h>

#include "CASRA/electrostatics/constantBEM.h"
#include "mgmres/mgmres.h"
#include "CASRA/base/LUdecomposition.h"

#define SMALL 1e-6

using namespace std;

int gmressolverTest();

int main(int argc, char* argv[])
{

  cout << "GMRES solver test" << endl;
  int pass1 = gmressolverTest();
  int pass2 = 0;
  int retVal = 0;

  if (pass1 != 0 || pass2 != 0){
    retVal = 1;
  }
  return retVal;

}

/*
 * Perform GMRES linear system solution test
 */
int gmressolverTest(){

  matrix testMat = {{4, -3, 3},
                    {-1, -4, 3},
                    {-4, -1, -4}};

  double problemMatrix[9] = {4, -1, -4, -3, -4, -1, 3, 3, -4};

  vector<double> rhs = {14, 24, 5};

  long dx = testMat.size();
  long dy = testMat[0].size();

  int sp = 0;
  for (int i = 0; i < dx; i++){
    for (int j = 0; j < dy; j++){
      if (testMat[i][j] != 0.0){
        sp += 1;
      }
    }
  }

  double *Amat;
  int *ia;
  int *ja;
  double *x;
  double *w;

  Amat = new double[sp];
  //ia = new int[dx+1];
  ia = new int[sp];
  ja = new int[sp];

  for (int i = 0; i < dx; i++){
    ia[i] = -1;
  }

  x = new double[dx];
  w = new double[dx];

  int it = 0;
  for (int i = 0; i < dx; i++){
    for (int j = 0; j < dy; j++){
      if (testMat[i][j] != 0.0){
        Amat[it] = testMat[i][j];
        ia[it] = i;
        ja[it] = j;
        it += 1;
      }
    }
  }

  for (int j = 0; j < dx; j++){
    w[j] = rhs[j];
    x[j] = 0.0; //output array preset to initial guess
  }

  cout << "\nmat: " << Amat[0] << " " << Amat[1] << " " << Amat[2] << "\n    " << Amat[3] << " " <<
          Amat[4] << " " << Amat[5] << "\n    " << Amat[6] << " " << Amat[7] << " " << Amat[8] << endl;
  cout << "rhs: " << w[0] << " " << w[1] << " " << w[2] << endl;

  //print row and column index lists
  cout << "row: " << ia[0] << " " << ia[1] << " " << ia[2] << " " << ia[3] << endl;
  cout << "col: " << ja[0] << " " << ja[1] << " " << ja[2] << " " << ja[3] << " " <<
          ja[4] << " " << ja[5] << " " << ja[6] << " " << ja[7] << " " << ja[8] << endl;

  cout << Amat[0] << " " << Amat[1] << " " << Amat[2] << " " << Amat[3] << " " << Amat[4] << " " << Amat[5] << endl;
  cout << ia[0] << " " << ia[1] << " " << ia[2] << " " << ia[3] << " " << ia[4] << " " << ia[5] << endl;
  cout << ja[0] << " " << ja[1] << " " << ja[2] << " " << ja[3] << " " << ja[4] << " " << ja[5] << endl;

  cout << "solving field \n" << endl;
  gmres ( dx, problemMatrix, x, w, 4, 3, SMALL*0.1, SMALL*0.1);
  cout << "\nsolved: " << endl;


  cout << "numerical sol: " << x[0] << " " << x[1] << " " << x[2] << endl;
  cout << "analytical sol: " << -1.0 << " " << -5.0 << " " << 1.0 << endl;

  //check if test has passed
  int pass;
  if ((fabs(x[0] + 1.0) < SMALL) && (fabs(x[1] + 5.0) < SMALL) && (fabs(x[2] - 1.0) < SMALL)){
    pass = 0;
    cerr << "SUCCESS: Solver converged to analytical solution." << endl;
  }
  else{
    pass = 1;
    cerr << "FAILURE: Solver failed to converge to analytical solution." << endl;
  }

  return pass;

}
