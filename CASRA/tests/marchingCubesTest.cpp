/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/marchingCubes.h"
#include "CASRA/field/testFields.h"

using namespace std;

int testMarchingCubesAlgorithms();

int main(int argc, char* argv[])
{
  cerr << "Marching Cubes test" << endl;
  int testRes = testMarchingCubesAlgorithms();
  return testRes;

}

/*!
 * Function for containing tests for Marching cubes algorithm.
 * Generates spherical signed distance field, extracts mesh, and calculates radius
 * Compares radius to predicted radius up to a tolerance.
 */
int testMarchingCubesAlgorithms(){

  int pass = 0;
  double Rc = 16.0;

  cout << "\tBuilding spherical signed distance field" << endl;
  scalarField3D testField = buildSphericalField(40, 40, 40, Rc);
  TriangularMesh outMesh;
  cout << "\tBuilding complete." << endl;

  cout << "\tmarching cubes" << endl;
  marchingCubes(testField, 0.0, outMesh);
  cout << "\tmarching cubes complete" << endl;

  double predRad = 4.0/3.0 * 3.14159 * pow(Rc, 3.0);

  //calculate shape volume
  double vol = fabs(outMesh.calculateVolume());
  cout << "\tSphere radius: " << Rc << endl;
  cout << "\textracted volume: " << vol << endl;
  cout << "\tPredicted volume: " << predRad << endl;

  if (fabs((vol - predRad)/predRad) > 1e-2) {
    pass = 1;
    cout << "\tFAILURE: Extracted mesh has incorrect volume" << endl;
  }
  else {
    pass = 0;
    cout << "\tSUCCESS: Extracted mesh has correct volume" << endl;
  }

  return pass;

}
