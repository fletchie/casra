/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <math.h>
#include <tuple>
#include <string>
#include <iostream>

#include "CASRA/base/vectorMath.h"
#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/marchingCubes.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/geometries/generateGeometries.h"
#include "CASRA/tipBuild/tipBuild.h"

using namespace std;

int extractPointTest(string testPoint);

int main(int argc, char* argv[])
{

  tuple<TriangularMesh, int> outData = buildTipMesh(10.0, 20.0, 100.0, 8.0);
  int retCode = get<1>(outData);

  //output as mesh
  vtkObject out;
  out.setWithTriangularMesh(get<0>(outData));
  writeVtk(out, "test_results/geometry_test/generated_test_tip.vtk");

  //test to extract point from string
  int tExtRes = extractPointTest("(1.0, 2.0, 3.0)");

  if (retCode == 0)
    cerr << "SUCCESS: Tip mesh successfully constructed." << endl;
  else
    cerr << "FAILURE: Tip mesh construction failed." << endl;

  return retCode;

}

int extractPointTest(string testPoint){
  Point extractedTestPoint = extractPoint(testPoint);
  cout << "point read: " << extractedTestPoint.px << " " << extractedTestPoint.py << " " << extractedTestPoint.pz << endl;
  return 0;
}
