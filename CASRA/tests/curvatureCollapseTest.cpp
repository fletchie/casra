/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/testMeshes.h"
#include "CASRA/mesh/marchingCubes.h"

#include "CASRA/field/testFields.h"

#include "CASRA/evaporationModels/levelSetModel/lscore.h"
#include "CASRA/specimenModels/numericalModel.h"
#include "CASRA/io/ioBase.h"
#include "CASRA/io/modelDataio.h"

#define RELSOLTOL 0.05

/*!
 * Template function for returning the sign of a variable
 * @param T Template input variable
 * @return sign of T
 */
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

using namespace std;

typedef vector<vector<double>> pointsType;
typedef vector<PointData> pointDataType;
typedef vector<vector<int>> cellsType;
typedef vector<CellData> cellDataType;

int sphereCollapseTest(int xDim, int yDim, int zDim, double Rc, double preFactor, string filename = "", int testIts = 10, double endTime = 1e20);
int squareCollapseTest();

int main(int argc, char* argv[])
{

  int testSphere = sphereCollapseTest(50, 50, 50, 20, 1.0, "test_results/curvature_sphere_collapse/collapse.tsv", 10000, 50.0);

  return testSphere;

}

int sphereCollapseTest(int xDim, int yDim, int zDim, double Rc, double preFactor, string filename, int testIts, double endTime){

  cout << "Testing level set curvature collapse of a sphere with radius " << Rc << " for " << endTime << " s" << endl;

  int pass = 0;

  //if passed, create output directory
  createDirectoryTree(filename);

  ofstream myfile;
  myfile.open(filename);

  myfile << "time" << "\t" << "volume" << endl;

  //create VTK output instance
  vtkObject out;

  cerr << "\tCreating test spherical field...";
  scalarField3D sphere = buildSphericalField(xDim, yDim, zDim, Rc);
  cerr << "\tcomplete." << endl;

  //create level set domain
  Point PLC(0.0, 0.0, 0.0);
  double cellWidth = 1.0;

  cerr << "\tInitialising specimen model...";
  TMSpecimen specimenModel;
  TriangularMesh specimenSurface;
  marchingCubes(sphere, 0.0, specimenSurface);

  specimenModel.appendPhase(specimenSurface);
  specimenModel.phases[0].setEvapLawMode = 4;
  specimenModel.phases[0].CLI.materialModifier = preFactor;

  //Add an additional phase with quarter of the evaporation field
  bool asymmetric = true;
  if (asymmetric == true){
    TriangularMesh rectMesh = buildRectangularMesh((double)xDim/2.0, (double)xDim,
                                                   0.0, (double)yDim,
                                                   0.0, (double)zDim);
    specimenModel.appendPhase(rectMesh);
    specimenModel.phases[1].setEvapLawMode = 4;
    specimenModel.phases[1].CLI.materialModifier = preFactor/2.0;
  }

  cerr << "\tspecimen model initialisation complete." << endl;

  cerr << "\tInitialising test level set domain...";
  LevelSetDomain lSDom(xDim, yDim, zDim, PLC, cellWidth);
  lSDom.lSField = sphere;
  lSDom.assignSpecimenModel(specimenModel);
  cerr << "complete." << endl;

  //define evaporation iterator
  int terminate = 0;
  double vol;

  CurvatureIterator evaporate(&lSDom, 0.5);
  evaporate.setTerminationConditions(endTime, testIts);
  evaporate.initialiseParameterGrid();

  double pRad, sRad;

  myfile << "time" << "\t" << "predicted-radius" << "\t" << "model-radius" << endl;
  cerr << "\trun mean curvature flow for " << endTime << " s or " << testIts << " iterations (whatever is reached first): " << endl;
  while (terminate == 0){

    tuple<int, TriangularMesh> retVals = evaporate.iterate();

    //out.setWithField(lSDom.lSField);
    //writeVtk(out, "test_results/spherical_collapse/field/field" + to_string(evaporate.it) + ".vtk");

    terminate = get<0>(retVals);

    TriangularMesh outMesh = get<1>(retVals);
    outMesh.rescaleMesh(lSDom.bottomLeftDomainCorner, lSDom.cellWidthX, lSDom.cellWidthY, lSDom.cellWidthZ);
    out.setWithTriangularMesh(outMesh);
    writeVtk(out, "test_results/curvature_sphere_collapse/surface/surface" + to_string(evaporate.it) + ".vtk");

    cout << "\tIteration: " << evaporate.it << "\t time: " << evaporate.T << endl;

    //calculate shape volume
    vol = outMesh.calculateVolume();

    //definition of mean curvature has additional factor or two, hence additional *2.0 here
    pRad = sqrt(pow(Rc, 2.0) - 2.0 * 2.0 * preFactor * evaporate.T);
    sRad = pow(3.0/4.0 * vol/PI, 1.0/3.0);

    myfile << evaporate.T << "\t" << pRad << "\t" << sRad << endl;

    if (evaporate.it % 20 == 0){
      lSDom.sussmanReinitialisation(0.1, 20);
    }
  }

  cout << "\tPredicted radius: " << pRad << " simulated radius: " << sRad << endl;

  if (((pRad - sRad)/pRad > RELSOLTOL) && evaporate.T != endTime){
    cerr << "\tFAILURE: test sphere collapse radius compared to analytical solution outside of defined tolerance." << endl;
    pass = 1;
  }
  else {
    cerr << "\tSUCCESS: test sphere collapse radius compared to analytical solution within defined tolerance." << endl;
    pass = 0;
  }

  myfile.close();

  return pass;
}
