/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>
#include <tuple>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/testMeshes.h"
#include "CASRA/mesh/marchingCubes.h"

#include "CASRA/field/testFields.h"

#include "CASRA/evaporationModels/levelSetModel/lscore.h"
#include "CASRA/specimenModels/numericalModel.h"
#include "CASRA/io/ioBase.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/geometries/generateGeometries.h"
#include "CASRA/thermalModels/thermalModels.h"
#include "CASRA/evaporationLaws/evaporationLaws.h"

#define SMALL 1.0e-30
#define RELTOL 0.05

//whether to perform an asymmetric sphere collapse or not
#define ASYMMETRIC false

/*!
 * Template function for returning the sign of a variable
 * @param T Template input variable
 * @return sign of T
 */
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

using namespace std;

typedef vector<vector<double>> pointsType;
typedef vector<PointData> pointDataType;
typedef vector<vector<int>> cellsType;
typedef vector<CellData> cellDataType;

int sphereCollapseTest(int xDim, int yDim, int zDim, double Rc, double preFactor, double beta, string filename = "", int testIts = 10, double endTime = 1e20);
int tipCollapseTest(double R0, double R1, double length, double offset,
                    double preFactor, double F0, double beta, int testIts, double endTime);

int main(int argc, char* argv[])
{

  double preFactor = 1.0;
  double beta = 10.0;
  int retCode = 2;

  if (argc < 2){
    cout << "Defaulting to sphere collapse test case" << endl;
    retCode = sphereCollapseTest(70, 70, 70, 20, preFactor, beta, "test_results/electrostatic_sphere_collapse/collapse.tsv", 10, 1e100);
    cout << "electrostatic test collapse complete" << endl;
  }
  else{
    int inputModel = atoi(argv[1]);
    if (inputModel == 1){
      cout << "Running sphere collapse test" << endl;
      retCode = sphereCollapseTest(70, 70, 70, 20, preFactor, beta, "test_results/electrostatic_sphere_collapse/collapse.tsv", 10, 1e100);
      cout << "electrostatic test collapse complete" << endl;
    }
    else if (inputModel == 2){
      cout << "Running tip collapse test" << endl;
      int testSphere = tipCollapseTest(10, 20, 40, 0.0, preFactor, 1.0, beta, 145, 1e100);
    }
    else{
      cout << "neither input model 1 (sphere collapse) or model 2 (tip collapse) modes detected. Please ensure the input is one of these." << endl;
    }
  }

  return retCode;

}

int tipCollapseTest(double R0, double R1, double length, double offset,
                    double preFactor, double F0, double beta, int testIts, double endTime){

  cout << "Testing level set electrostatic collapse of a tip for 1 s" << endl;

  int pass = 0;

  cerr << "Creating test tip field...";
  tuple<TriangularMesh, int> constructMeshOut = buildTipMesh(R0, R1, length, offset);
  TriangularMesh surfaceMesh = get<0>(constructMeshOut);
  cerr << "complete." << endl;

  vtkObject out;
  out.setWithTriangularMesh(surfaceMesh);
  writeVtk(out, "test_results/electrostatic_tip_collapse/initial_surface.vtk");

  //define specimen surface
  cout << "define specimen model" << endl;
  TMSpecimen specimenModel;

  specimenModel.appendPhase(surfaceMesh);

  //assign bulk evaporation law
  specimenModel.phases[0].setEvapLawMode = 1;
  specimenModel.phases[0].ALI.preFactor = 1.0;
  specimenModel.phases[0].ALI.C = {BOLTZMANN * 10.0};
  specimenModel.phases[0].ALI.Cexp = {1.0};
  specimenModel.phases[0].ALI.evapField = 1.0;

  bool precipitate = true;
  if (precipitate == true){
    cerr << "Creating sphere...";
    tuple<TriangularMesh, int> constructPrecipitateMeshOut = buildSphere(R0/2.0, Point(0.0, 0.0, length + R0/3.0),  50);
    TriangularMesh precipitateMesh = get<0>(constructPrecipitateMeshOut);
    cerr << "complete." << endl;
    out.setWithTriangularMesh(precipitateMesh);
    writeVtk(out, "test_results/electrostatic_tip_collapse/precipitate.vtk");

    specimenModel.appendPhase(precipitateMesh);

    //assign precipitate evaporation law
    specimenModel.phases[1].setEvapLawMode = 1;
    specimenModel.phases[1].ALI.preFactor = 1.0;
    specimenModel.phases[1].ALI.C = {BOLTZMANN * 10.0};
    specimenModel.phases[1].ALI.Cexp = {1.0};
    specimenModel.phases[1].ALI.evapField = 1.5;

  }

  LevelSetDomain lSDom(10, 10, 10, Point(0.0, 0.0, 0.0), 1.0);
  lSDom.assignSpecimenModel(specimenModel, true, 120, 120, 140);
  cout << "\nSpecimen model definition complete." << endl;

  //define evaporation iterator
  int terminate = 0;
  double vol;

  ElectrostaticConstantBEMIterator evaporate(&lSDom, 0.5, 5e-3, 0.0, true);
  evaporate.setTerminationConditions(endTime, testIts);

  //set thermal model for iterator
  UniformTemperatureModel tempModel(1.0);
  evaporate.setThermalModel(tempModel);

  double pRad, sRad;

  int info;
  while (terminate == 0){

    cout << "***Iteration: " << evaporate.it << endl;
    tuple<int, TriangularMesh, vector<double>, vector<double>, int> retVals = evaporate.iterate();
    info = get<4>(retVals);


    if (info == 0){
      cout << "***Iteration " << evaporate.it - 1 << " complete \t time: " << evaporate.T << endl;

      terminate = get<0>(retVals);

      TriangularMesh outMesh = get<1>(retVals);
      out.setWithTriangularMesh(outMesh);
      writeVtk(out, "test_results/electrostatic_tip_collapse/surface/surface" + to_string(evaporate.it) + ".vtk");
    }
  }

  return 0;

}

int sphereCollapseTest(int xDim, int yDim, int zDim, double Rc, double preFactor, double beta, string filename, int testIts, double endTime){

  cout << "Testing level set electrostatic collapse of a sphere for 1 s" << endl;

  int pass = 0;

  //if passed, create output directory
  createDirectoryTree(filename);

  ofstream myfile;

  //ofstream myfile;
  myfile.open(filename);

  myfile << "time" << "\t" << "volume" << endl;

  //create VTK output instance
  vtkObject out;

  cout << "\tCreating test spherical field...";
  scalarField3D sphere = buildSphericalField(xDim, yDim, zDim, Rc);
  cout << "\tcomplete." << endl;

  //create level set domain
  Point PLC(0.0, 0.0, 0.0);
  double cellWidth = 1.0;

  cout << "\tInitialising specimen model...";
  TMSpecimen specimenModel;

  //define specimen surface
  TriangularMesh specimenSurface;
  marchingCubes(sphere, 0.0, specimenSurface);
  specimenModel.appendPhase(specimenSurface);

  //assign precipitate evaporation law
  specimenModel.phases[0].setEvapLawMode = 1;
  specimenModel.phases[0].ALI.preFactor = 1.0;
  specimenModel.phases[0].ALI.C = {BOLTZMANN * 2.0};
  specimenModel.phases[0].ALI.Cexp = {1.0};
  specimenModel.phases[0].ALI.evapField = 1.0;

  //Add an additional phase with quarter of the evaporation field
  if (ASYMMETRIC == true){
    TriangularMesh rectMesh = buildRectangularMesh((double)xDim/2.0, (double)xDim,
                                                   0.0, (double)yDim,
                                                   0.0, (double)zDim);

    specimenModel.appendPhase(rectMesh);

    //assign precipitate evaporation law
    specimenModel.phases[1].setEvapLawMode = 1;
    specimenModel.phases[1].ALI.preFactor = 1.0;
    specimenModel.phases[1].ALI.C = {BOLTZMANN * 1.0};
    specimenModel.phases[1].ALI.Cexp = {1.0};
    specimenModel.phases[1].ALI.evapField = 1.0;

    out.setWithTriangularMesh(specimenSurface);
    writeVtk(out, "test_results/electrostatic_sphere_collapse/initial_surface.vtk");
    //out.setWithTriangularMesh(rectMesh);
    //writeVtk(out, "test_results/electrostatic_sphere_collapse/box.vtk");

  }

  cout << "\tspecimen model initialisation complete." << endl;

  cout << "\tInitialising test level set domain...";
  LevelSetDomain lSDom(xDim, yDim, zDim, PLC, cellWidth);
  lSDom.lSField = sphere;
  lSDom.assignSpecimenModel(specimenModel);
  cout << "\tcomplete." << endl;

  //define evaporation iterator
  int terminate = 0;
  double vol;

  ElectrostaticConstantBEMIterator evaporate(&lSDom, 0.5, 0.1);
  evaporate.setTerminationConditions(endTime, testIts);

  //set thermal model for iterator
  UniformTemperatureModel tempModel(1.0);
  evaporate.setThermalModel(tempModel);

  double pRad, sRad;
  int info;
  double firstItTime = -1.0;

  cout << "run electrostatic flow for " << endTime << " s or " << testIts << " iterations (whatever is reached first): " << endl;
  while (terminate == 0){

    cout << "***Iteration: " << evaporate.it << endl;
    tuple<int, TriangularMesh, vector<double>, vector<double>, int> retVals = evaporate.iterate();

    terminate = get<0>(retVals);
    info = get<4>(retVals);

    if (firstItTime < 0.0)
      firstItTime = evaporate.T;

    cout << "iterator info: " << info << endl;

    if (info == 0){
      cout << "***Iteration " << evaporate.it << " complete \t time: " << evaporate.T << endl;

      terminate = get<0>(retVals);

      TriangularMesh outMesh = get<1>(retVals);
      out.setWithTriangularMesh(outMesh);
      writeVtk(out, "test_results/electrostatic_sphere_collapse/surface/surface" + to_string(evaporate.it) + ".vtk");

      //calculate shape volume
      vol = outMesh.calculateVolume();

      //definition of mean curvature has additional factor or two, hence additional *2.0 here
      pRad = Rc - preFactor * (evaporate.T - firstItTime);
      sRad = pow(3.0/4.0 * vol/PI, 1.0/3.0);
      myfile << evaporate.T << "\t" << pRad << "\t" << sRad << endl;
      cout << "Predicted radius: " << pRad << " simulated radius: " << sRad << endl;
    }
  }

  cerr << endl;
  if (fabs(pRad - sRad)/pRad < RELTOL){
    cerr << "SUCCESS: test sphere collapse radius compared to analytical solution within defined tolerance." << endl;
    pass = 0;
  }
  else {
    cerr << "FAILURE: test sphere collapse radius compared to analytical solution outside of defined tolerance." << endl;
    pass = 1;
  }

  myfile.close();

  return pass;
}
