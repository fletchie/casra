/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <math.h>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/testMeshes.h"
#include "CASRA/mesh/remeshSupport.h"
#include "CASRA/mesh/marchingCubes.h"
#include "CASRA/field/scalarField.h"
#include "CASRA/field/testFields.h"
#include "CASRA/electrostatics/constantBEM.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/electrostatics/solverParser.h"

#include "isotropicremesher/isotropicremesher.h"
#include "isotropicremesher/isotropichalfedgemesh.h"

#include "mgmres/mgmres.h"

#define RELTOL 0.005

using namespace std;

int sphereFieldTest(int xDim, int yDim, int zDim, double Rc);

int main(int argc, char* argv[])
{

  cerr << "Testing the constant BEM electrostatic solver" << endl;
  int testSphere = sphereFieldTest(60, 60, 60, 20);

  return testSphere;

}

int sphereFieldTest(int xDim, int yDim, int zDim, double Rc){

  cerr << "\tCreating test spherical field...";
  scalarField3D voxelField = buildSphericalField(xDim, yDim, zDim, Rc);
  cerr << "\tcomplete." << endl;

  TriangularMesh surfaceMesh;

  cout << "\tmarching cubes" << endl;
  marchingCubes(voxelField, 0.0, surfaceMesh);
  cout << "\tmarching cubes complete" << endl;

  cout << "\tPerform remesh" << endl;
  surfaceMesh = performRemesh(surfaceMesh, 3, 1.4);
  cout << "\tremesh complete" << endl;

  electrostaticBemSolver bemSolver(5e-3);
  bemSolver.u = vector<double>(surfaceMesh.faces.size(), 1.0);

  cout << "\tCalculate problem matrices" << endl;
  bemSolver.calculateElectrostaticMatrix(surfaceMesh);
  bemSolver.matrixVectorProduct(bemSolver.rhs, bemSolver.Fmat, bemSolver.u);
  cout << "\tMatrix calculation complete" << endl;

  //cout << bemSolver.Fmat[0][0] << " " << bemSolver.Fmat[0][1] << " " << bemSolver.Fmat[0][2] << endl;
  //cout << bemSolver.Fmat[1][0] << " " << bemSolver.Fmat[1][1] << " " << bemSolver.Fmat[1][2] << endl;
  //cout << bemSolver.Fmat[2][0] << " " << bemSolver.Fmat[2][1] << " " << bemSolver.Fmat[2][2] << endl;
  //cout << bemSolver.Pmat[0][0] << " " << bemSolver.Pmat[0][1] << " " << bemSolver.Pmat[0][2] << endl;
  //cout << bemSolver.Pmat[1][0] << " " << bemSolver.Pmat[1][1] << " " << bemSolver.Pmat[1][2] << endl;
  //cout << bemSolver.Pmat[2][0] << " " << bemSolver.Pmat[2][1] << " " << bemSolver.Pmat[2][2] << endl;

  cout << "\tSolve electrostatics" << endl;
  solveField(bemSolver.Pmat, bemSolver.rhs, bemSolver.q, bemSolver.Amat, bemSolver.AmatDimSize);
  cout << "\tElectrostatic solution complete" << endl;

  vector<double> avq = averageField(surfaceMesh.faces, bemSolver.q);
  avq = averageField(surfaceMesh.faces, avq);

  double dev = 0;
  double analSol = 1.0/Rc;
  for (int a=0;a<avq.size();a++){
    dev += fabs(fabs(avq[a]) - fabs(analSol));
  }
  double relError = (dev/analSol)/avq.size();

  int pass = 0;

  if (relError < RELTOL) {
    pass = 1;
    cerr << "SUCCESS: Electrostatic field converged to expected analytical solution." << endl;
  }
  else {
    pass = 0;
    cerr << "FAILURE: Electrostatic field failed to converge to the expected analytical solution." << endl;
  }

  vtkObject out;
  out.setWithTriangularMesh(surfaceMesh);

  CellData cell;
  cell.setValues(bemSolver.q, "field");
  out.cellData.push_back(cell);
  cell.setValues(avq, "avfield");
  out.cellData.push_back(cell);

  writeVtk(out, "test_results/electrostatic_test/surface.vtk");

  return 0;

}
