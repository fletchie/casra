/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>
#include <tuple>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/remeshSupport.h"
#include "CASRA/mesh/testMeshes.h"
#include "CASRA/mesh/marchingCubes.h"

#include "CASRA/field/testFields.h"

#include "CASRA/geometries/generateGeometries.h"
#include "CASRA/electrostatics/constantBEM.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/electrostatics/solverParser.h"

#define RELSOLTOL 0.01

using namespace std;

int octreeBuildTest();
int octreeBuildTestSphere();

int main(int argc, char* argv[])
{
  int testRes = octreeBuildTestSphere();
  return testRes;
}

int octreeBuildTest(){

  int pass = 0;

  //build test tip mesh for octree test
  tuple<TriangularMesh, int> outData = buildTipMesh(10.0, 20.0, 100.0, 8.0);
  TriangularMesh& surfaceMesh = get<0>(outData);

  surfaceMesh = performRemesh(surfaceMesh, 6, 0.15);

  surfaceMesh.setCentres();
  surfaceMesh.setAreas();
  surfaceMesh.setNormals();
  surfaceMesh.setLongestSideSqrd();
  surfaceMesh.setLongestSides();

  electrostaticBemSolver bemSolver(0.2);
  bemSolver.u = vector<double>(surfaceMesh.faces.size(), 1.0);
  bemSolver.calculateElectrostaticMatrix(surfaceMesh);
  bemSolver.matrixVectorProduct(bemSolver.rhs, bemSolver.Fmat, bemSolver.u);
  solveField(bemSolver.Pmat, bemSolver.rhs, bemSolver.q, bemSolver.Amat, bemSolver.AmatDimSize);
  surfaceMesh.assignCellData(bemSolver.q, "field");

  int retCode = get<1>(outData);

  double minCoords[3] = {pointDimMinimum(surfaceMesh.vertices, 0), pointDimMinimum(surfaceMesh.vertices, 1), pointDimMinimum(surfaceMesh.vertices, 2)};
  double maxCoords[3] = {pointDimMaximum(surfaceMesh.vertices, 0), pointDimMaximum(surfaceMesh.vertices, 1), pointDimMaximum(surfaceMesh.vertices, 2)};

  cout << "Building octree for accelerating electrostatic calculations" << endl;
  OctreeNode top(minCoords, maxCoords,
                 &surfaceMesh,
                 &bemSolver);

  int resX = 40;
  int resY = 40;
  int resZ = 40;

  double admissTol = 0.1;

  cout << "Evaluating surrounding field" << endl;
  vector<vector<vector<double>>> fieldGridX = initialiseField(resX+1, resY+1, resZ+1, 0.0);
  vector<vector<vector<double>>> fieldGridY = initialiseField(resX+1, resY+1, resZ+1, 0.0);
  vector<vector<vector<double>>> fieldGridZ = initialiseField(resX+1, resY+1, resZ+1, 0.0);

  for (int i=0;i<resX+1;i++){
    for (int j=0;j<resY+1;j++){
      for (int k=0;k<resZ+1;k++){
        double v1, v2, v3 = 0.0;
        double px = ((resX - i) * minCoords[0] + i * maxCoords[0])/(resX);
        double py = ((resY - j) * minCoords[1] + j * maxCoords[1])/(resY);
        double pz = ((resZ - k) * minCoords[0] + k * (maxCoords[2] + 40.0))/(resZ);
        Point evalPoint(px, py, pz);

        double cdist = surfaceMesh.shortestDistanceToPoint(evalPoint);
        int insideCheck = surfaceMesh.insideMesh(evalPoint);
        if ((cdist > 0.1) && (insideCheck == 1)){
          top.calculateField(v1, v2, v3, evalPoint, admissTol);
          fieldGridX[i][j][k] = v1;
          fieldGridY[i][j][k] = v2;
          fieldGridZ[i][j][k] = v3;

        }
      }
    }
  }

  vtkObject out;
  out.setWithTriangularMesh(surfaceMesh);
  writeVtk(out, "test_results/octree-test/test-tip-mesh.vtk");

  TriangularMesh octreeMesh;
  top.buildBoundingMesh(octreeMesh);

  out.setWithTriangularMesh(octreeMesh);
  writeVtk(out, "test_results/octree-test/octree.vtk");

  Point bottomLeftDomainCorner(minCoords[0], minCoords[1], minCoords[2]);
  double cellWidthX = (maxCoords[0] - minCoords[0])/resX;
  double cellWidthY = (maxCoords[1] - minCoords[1])/resY;
  double cellWidthZ = ((maxCoords[2] + 40.0) - minCoords[2])/resZ;
  out.setWithField(fieldGridX, bottomLeftDomainCorner, cellWidthX, cellWidthY, cellWidthZ);
  writeVtk(out, "test_results/octree-test/surroundingFieldX.vtk");
  out.setWithField(fieldGridY, bottomLeftDomainCorner, cellWidthX, cellWidthY, cellWidthZ);
  writeVtk(out, "test_results/octree-test/surroundingFieldY.vtk");
  out.setWithField(fieldGridZ, bottomLeftDomainCorner, cellWidthX, cellWidthY, cellWidthZ);
  writeVtk(out, "test_results/octree-test/surroundingFieldZ.vtk");

  cout << "octree calculation complete" << endl;

  return pass;

}


int octreeBuildTestSphere(){

  int pass = 0;

  int xDim = 50;
  int yDim = 50;
  int zDim = 50;
  double Rc = 10;

  cerr << "Generating test sphere mesh" << endl;
  scalarField3D sphere = buildSphericalField(xDim, yDim, zDim, Rc);

  //create level set domain
  Point PLC(0.0, 0.0, 0.0);
  double cellWidth = 1.0;

  TriangularMesh surfaceMesh;
  marchingCubes(sphere, 0.0, surfaceMesh);

  surfaceMesh = performRemesh(surfaceMesh, 6, 0.5);
  cerr << "Test sphere generated" << endl;

  surfaceMesh.setMeshProperties();

  cout << "Solving electrostatic surface field" << endl;
  electrostaticBemSolver bemSolver(0.2);
  bemSolver.u = vector<double>(surfaceMesh.faces.size(), 1.0);
  bemSolver.calculateElectrostaticMatrix(surfaceMesh);
  bemSolver.matrixVectorProduct(bemSolver.rhs, bemSolver.Fmat, bemSolver.u);
  solveField(bemSolver.Pmat, bemSolver.rhs, bemSolver.q, bemSolver.Amat, bemSolver.AmatDimSize);
  surfaceMesh.assignCellData(bemSolver.q, "field");
  cout << "field solution complete" << endl;

  double meanCoords[3] = {pointDimMean(surfaceMesh.vertices, 0), pointDimMean(surfaceMesh.vertices, 1), pointDimMean(surfaceMesh.vertices, 2)};
  double minCoords[3] = {pointDimMinimum(surfaceMesh.vertices, 0), pointDimMinimum(surfaceMesh.vertices, 1), pointDimMinimum(surfaceMesh.vertices, 2)};
  double maxCoords[3] = {pointDimMaximum(surfaceMesh.vertices, 0), pointDimMaximum(surfaceMesh.vertices, 1), pointDimMaximum(surfaceMesh.vertices, 2)};

  cout << "Building octree for accelerating electrostatic calculations" << endl;
  OctreeNode top(minCoords, maxCoords,
                 &surfaceMesh,
                 &bemSolver);
  cout << "octree calculation complete" << endl;

  int resX = 40;
  int resY = 40;
  int resZ = 40;

  cout << "Evaluating cross-sectional field profile" << endl;
  double crossFieldProfileError = 0.0;
  int testPoints = 0;

  double admissTol = 0.1;

  for (int i=0;i<resX+1;i++){
    double px = ((resX - i) * 0 + i * 50)/(resX);
    double py = meanCoords[1];
    double pz = meanCoords[2];
    Point evalPoint(px, py, pz);
    double v1 = 0.0;
    double v2 = 0.0;
    double v3 = 0.0;
    top.calculateField(v1, v2, v3, evalPoint, admissTol);
    double numericFieldSol = v1;
    double analyticFieldSol = Rc/((px - meanCoords[0]) * (px - meanCoords[0]));
    double cdist = surfaceMesh.shortestDistanceToPoint(evalPoint);
    int insideCheck = surfaceMesh.insideMesh(evalPoint);
    if ((cdist > 0.1) && (insideCheck == 1)){
      crossFieldProfileError += abs((abs(numericFieldSol) - abs(analyticFieldSol))/abs(analyticFieldSol));
      testPoints += 1;
    }
  }

  if (((crossFieldProfileError/testPoints) < RELSOLTOL) && (testPoints > 10)){
    cerr << "SUCCESS: Error compared to analytical solution within defined tolerance." << endl;
    return 0;
  }
  else{
    cerr << "FAIL: Error compared to analytical solution outside of defined tolerance." << endl;
    return 1;
  }

  /*
  vtkObject out;
  out.setWithTriangularMesh(surfaceMesh);
  writeVtk(out, "test_results/octree-test/test-sphere-mesh.vtk");

  TriangularMesh octreeMesh;
  top.buildBoundingMesh(octreeMesh);

  out.setWithTriangularMesh(octreeMesh);
  writeVtk(out, "test_results/octree-test/octree.vtk");

  Point bottomLeftDomainCorner(minCoords[0], minCoords[1], minCoords[2]);
  double cellWidthX = (maxCoords[0] - minCoords[0])/resX;
  double cellWidthY = (maxCoords[1] - minCoords[1])/resY;
  double cellWidthZ = (maxCoords[2] - minCoords[2])/resZ;
  out.setWithField(fieldGridX, bottomLeftDomainCorner, cellWidthX, cellWidthY, cellWidthZ);
  writeVtk(out, "test_results/octree-test/surroundingFieldX.vtk");
  out.setWithField(fieldGridY, bottomLeftDomainCorner, cellWidthX, cellWidthY, cellWidthZ);
  writeVtk(out, "test_results/octree-test/surroundingFieldY.vtk");
  out.setWithField(fieldGridZ, bottomLeftDomainCorner, cellWidthX, cellWidthY, cellWidthZ);
  writeVtk(out, "test_results/octree-test/surroundingFieldZ.vtk");
  */

  return pass;

}
