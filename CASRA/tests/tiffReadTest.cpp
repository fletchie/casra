/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>

#include "CASRA/tipBuild/extractTomoPhases.h"
#include "CASRA/io/ioBase.h"
#include "CASRA/io/TIFFReader.h"

#define SMALL 1.0e-10

using namespace std;

int tiffReadTest(string testTiffFile);
int tiffReadTest3D(string testTiffFile);

int main(int argc, char* argv[])
{


  cout << "Testing 2D TIFF load" << endl;
  string testTiffFile1 = "../tests/test-files/test-tiff.tiff";
  int testRes1 = tiffReadTest(testTiffFile1);

  cout << "Testing 3D TIFF load" << endl;
  string testTiffFile2 = "../tests/test-files/test-tiff-3d.tiff";
  int testRes2 = tiffReadTest3D(testTiffFile2);

  int retCode = testRes1 | testRes2;

  if (retCode == 0)
    cerr << "SUCCESS: Both 2D and 3D TIFFs read successfully" << endl;
  if (testRes1 != 0)
    cerr << "FAILURE: 2D TIFF failed to read" << endl;
  if (testRes2 != 0)
    cerr << "FAILURE: 3D TIFF failed to read" << endl;

  return testRes1 | testRes2;
}

int tiffReadTest(string testTiffFile) {

  int retCode = 0;

  vector<vector<vector<vector<double>>>> outField = readTiff(testTiffFile);

  int zDim = outField.size();
  int yDim = outField[0].size();
  int xDim = outField[0][0].size();

  cout << zDim << " " << yDim << " " << xDim << endl;

  if (xDim != 1500 || yDim != 1500){
    cerr << "Unexpected image dimension loaded (should be 1500x1500 for default test image)." << endl;
    retCode = 1;
  }

  if (outField[0][600][600][0] != 0 || outField[0][600][600][1] != 255 || outField[0][600][600][2] != 0){
    cerr << "Unexpected image pixel colour defining specimen loaded (should be (0,255,0) for default test image)" << endl;
    retCode = 1;
  }

  if (retCode == 0){
    cerr << "2D TIFF load test successful." << endl;
  }

  return retCode;
}



int tiffReadTest3D(string testTiffFile) {

  int retCode = 0;

  vector<vector<vector<vector<double>>>> outField = readTiff(testTiffFile);

  int zDim = outField.size();
  int yDim = outField[0].size();
  int xDim = outField[0][0].size();

  cout << zDim << " " << yDim << " " << xDim << endl;

  if (xDim != 5 || yDim != 5 || zDim != 5){
    cerr << "Unexpected image dimension loaded (should be 5x5x5 for default test image)." << endl;
    retCode = 1;
  }

  if (outField[2][2][2][0] != 21){
    cerr << "Unexpected image pixel colour defining specimen loaded (should be (21) for default test image)" << endl;
    retCode = 1;

  }
  if (retCode == 0){
    cerr << "3D TIFF load test successful." << endl;
  }

  return retCode;
}
