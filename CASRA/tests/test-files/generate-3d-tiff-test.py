import numpy as np
import imageio
from skimage.external import tifffile as tif

field = np.zeros((5, 5, 5))
field[2, 2, 2] = 21.0

imageio.mimwrite('test-3d.tiff', field)
