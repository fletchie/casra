/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <cmath>

#include "CASRA/electrostatics/constantBEM.h"
#include "CASRA/mesh/triangularMesh.h"

using namespace std;

#define TOL 1e-4

int main(int argc, char* argv[])
{

  Point cx(1.0, 2.0, 3.0);
  Point cy(-1.0, 0.0, 0.0);
  Point nv(1.0, 0.0, -1.0);

  electrostaticBemSolver bemSolver;
  double out1 = bemSolver.greensFunction(cx, cy);
  double out2 = bemSolver.normGreensFunction(cx, cy, nv);

  int pass = 0;

  if (fabs(out1 - 0.0386007) < TOL)
    pass = 0;
  else
    pass = 1;

  if (fabs(out2 - 0.00113532) < TOL)
    pass = 0;
  else
    pass = 1;

  if (pass == 0)
    cerr << "SUCCESS: Calculated BEM matrix elements as expected." << endl;
  else
    cerr << "FAILURE: Calculated BEM matrix elements invalid." << endl;


  return pass;

}
