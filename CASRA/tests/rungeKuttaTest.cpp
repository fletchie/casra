/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <tuple>
#include <vector>

#include "CASRA/base/RKIntegrator.h"

#define T0 25.0
#define A 2.0

using namespace std;

/*!
 * A simple test system is defined giving the
 * first-order ODE dy/dx = tS
 * @param t The parameter
 * @param y The dependent variable
 * @return The function value (equal to dy/dx)
 */
double tS(double t, double y){
  return - A * (y - T0);
}

vector<double> newtonLaw(double t, vector<double> y){

  vector<double> yNew = vector<double>(6, 0.0);

  yNew[0] = - A * (y[0] - T0);
  yNew[1] = - A * (y[1] - T0);
  yNew[2] = - A * (y[2] - T0);
  yNew[3] = - A * (y[3] - T0);
  yNew[4] = - A * (y[4] - T0);
  yNew[5] = - A * (y[5] - T0);

  return yNew;

}

vector<double> testSystem3(double t, vector<double> y){

  vector<double> yNew = vector<double>(2, 0.0);

  yNew[0] = y[0] + y[1];
  yNew[1] = 4.0 * y[0] + y[1];

  return yNew;

}

int scalarIterateTest(double tFinal){

  double relTol = 0.1;
  double tInit = 0.0;
  double yInit = 0.0;

  //make_tuple(tNew, ynp4, hNew, k7);
  tuple<double, double, double, double> res;
  double t, y, h, kPrev;

  kPrev = numeric_limits<double>::min();

  h = 0.1;

  t = tInit;
  y = yInit;

  while (t < tFinal){
    res = rungeKuttaScalarIterate(&tS, t, y, h, relTol, kPrev);
    t = get<0>(res);
    y = get<1>(res);
    h = get<2>(res);
    kPrev = get<3>(res);
    cout << t << " " << y << " " << h << endl;
  }

  if (fabs(y - T0)/T0 < 1e-2){
    cerr << "SUCCESS: Rk integrator successfully converged to analytical solution." << endl;
    return 0;
  }
  else{
    cerr << "FAILURE: Rk integrator failed to converged to analytical solution." << endl;
    return 1;
  }
}

int vectorIterateTest(double tFinal){

  double relTol = 0.1;
  double tInit = 0.0;

  //make_tuple(tNew, ynp4, hNew, k7);
  tuple<double, vector<double>, double, vector<double>> res;
  double t, h;

  vector<double> kPrev;
  vector<double> y = {-5, 0, 5, 10, 15, 20};

  h = 0.1;

  t = tInit;

  int it = 0;
  while (t < tFinal){
    res = rungeKuttaVectorIterate(&newtonLaw, t, y, h, relTol, kPrev);
    t = get<0>(res);
    y = get<1>(res);
    h = get<2>(res);
    kPrev = get<3>(res);
    cout << t << " " << y[0] << " " << y[1] << " " << y[2] << " " << y[3] << " " << y[4] << " " << h << endl;
    it ++;
  }

  if (fabs(y[0] - T0)/T0 < 1e-2){
    cout << "SUCCESS: vector Rk integrator successfully converged." << endl;
    return 0;
  }
  else{
    cout << "FAILURE: vector Rk integrator failed to converged." << endl;
    return 1;
  }
}


/*!
 * Unit test for Dormand-Prince RK45 integrator
 * for scalar equations
 */
int main(int argc, char* argv[])
{

  int scIt = scalarIterateTest(200.0);

  return scIt;

}
