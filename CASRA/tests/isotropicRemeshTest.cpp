/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <map>
#include <chrono>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/remeshSupport.h"
#include "CASRA/mesh/testMeshes.h"
#include "CASRA/mesh/marchingCubes.h"
#include "CASRA/geometries/generateGeometries.h"
#include "CASRA/evaporationModels/levelSetModel/lscore.h"

#include "isotropicremesher/isotropicremesher.h"
#include "isotropicremesher/isotropichalfedgemesh.h"

#include "CASRA/field/testFields.h"
#include "CASRA/io/modelDataio.h"

using namespace std;

using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::seconds;

#define TOL 1e-2

int testRemeshSphere(int xDim, int yDim, int zDim, double Rc);
int testRemeshTip(int xDim, int yDim, int zDim, double Rc);

int main(int argc, char *argv[]){

  cerr << "Isotropic remesh test" << endl;
  //int testremesh = testRemeshSphere(60, 60, 60, 20);
  int testremesh = testRemeshTip(100, 100, 100, 15);
  return 0;
}

int testRemeshSphere(int xDim, int yDim, int zDim, double Rc){

  int pass = 0;

  cerr << "\tCreating test spherical field...";
  scalarField3D voxelField = buildSphericalField(xDim, yDim, zDim, Rc);
  cerr << "\tcomplete." << endl;

  TriangularMesh surfaceMesh;

  cout << "\tmarching cubes" << endl;
  marchingCubes(voxelField, 0.0, surfaceMesh);
  cout << "\tmarching cubes complete" << endl;

  double ovol = surfaceMesh.calculateVolume();

  vtkObject out;
  out.setWithTriangularMesh(surfaceMesh);
  writeVtk(out, "test_results/isotropic_remesh_test/raw_surface.vtk");

  cout << "\tRemesh one" << endl;
  auto t1 = high_resolution_clock::now();
  surfaceMesh = performRemesh(surfaceMesh, 2, 1.0);
  auto t2 = high_resolution_clock::now();
  duration<double> ms_double = t2 - t1;
  cout << "\ttime to remesh: " << ms_double.count() << endl;

  for (int i=0;i<surfaceMesh.vertices.size();i++){
    if (surfaceMesh.vertices[i].px < 30.0){
      surfaceMesh.vertices[i].locked = true;
    }
  }

  cout << "\tRemesh two" << endl;
  t1 = high_resolution_clock::now();
  surfaceMesh = performRemesh(surfaceMesh, 2, 1.5);
  t2 = high_resolution_clock::now();
  ms_double = t2 - t1;
  cout << "\ttime to remesh: " << ms_double.count() << endl;

  if (surfaceMesh.checkManifold() != 0){
    pass = 1;
  }
  if (surfaceMesh.checkWatertight() != 0){
    pass = 1;
  }

  double thvol = 4.0/3.0 * PI * pow(Rc, 3);
  double mvol = surfaceMesh.calculateVolume();

  if (fabs(ovol - mvol)/ovol > TOL) {
    pass = 1;
  }

  out.setWithTriangularMesh(surfaceMesh);
  writeVtk(out, "test_results/isotropic_test/remeshed_surface.vtk");

  if (pass == 0)
    cerr << "SUCCESS: Resulting mesh as expected." << endl;
  else
    cerr << "FAILURE: Resulting mesh not as expected." << endl;

  return pass;

}


int testRemeshTip(int xDim, int yDim, int zDim, double Rc) {

    int pass = 0;

    tuple<TriangularMesh, int> outData = buildTipMesh(10.0, 20.0, 70, 8.0);
    TriangularMesh surfaceMesh = get<0>(outData);
    TriangularMesh contourMesh;

    double minX = pointDimMinimum(surfaceMesh.vertices, 0);
    double maxX = pointDimMaximum(surfaceMesh.vertices, 0);
    double minY = pointDimMinimum(surfaceMesh.vertices, 1);
    double maxY = pointDimMaximum(surfaceMesh.vertices, 1);
    double minZ = pointDimMinimum(surfaceMesh.vertices, 2);
    double maxZ = pointDimMaximum(surfaceMesh.vertices, 2);

    cerr << "\tCreating test spherical field...";
    LevelSetDomain lSDom(100, 100, 100, Point(minX - 1, minY - 1, minZ - 1), 1.0);
    lSDom.initialiseSDF(surfaceMesh);
    scalarField3D& voxelField = lSDom.lSField;
    marchingCubes(voxelField, 0.0, contourMesh);

    vtkObject out;


    vector<double> maxHeights;
    vector<double> targetLengthFractions;
    double maxPanelLength = 0.5 * 30.0 / (1.5 * PI * 1.0); //maximum panel length based of fraction of the maximum specimen base diameter

    int m = 0;
    while (false == false) {
        maxHeights.push_back(maxZ / 1.0 - m * 15.0 + 1e-6);
        targetLengthFractions.push_back(0.9 * pow(1.6, m));

        if (maxHeights[m] < 0.0) {
            break;
        }

        if (targetLengthFractions[m] > maxPanelLength) {
            targetLengthFractions[m] = maxPanelLength + 1e-50;
            break;
        }

        m += 1;
    }

    out.setWithTriangularMesh(contourMesh);
    writeVtk(out, "remesh_out/raw_surface.vtk");
    //perform adaptive remeshing of the specimen surface along the shank
    cout << "\tRemesh one" << endl;
    auto t1 = high_resolution_clock::now();
    surfaceMesh = performAdaptiveRemesh(contourMesh, maxHeights,
        targetLengthFractions, 2,
        1.0, 1.0);
    auto t2 = high_resolution_clock::now();
    duration<double> ms_double = t2 - t1;
    cout << "\ttime to remesh: " << ms_double.count() << endl;

    out.setWithTriangularMesh(surfaceMesh);
    writeVtk(out, "remesh_out/remeshed_surface.vtk");


    return 0;

}
