/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/testMeshes.h"
#include "CASRA/base/graphMath.h"

#define SMALL 1.0e-10

using namespace std;

int testMeshAlgorithms();

int main(int argc, char* argv[])
{
  int testRes = testMeshAlgorithms();
  return testRes;
}

int testMeshAlgorithms(){

  int pass = 0;

  //create mesh
  cerr << "Creating test mesh ";
  TriangularMesh testMesh = buildRectangularMesh(0.0, 0.5,
                                                 0.0, 1.0,
                                                 0.0, 1.0);

  cerr << "\t test mesh created." << endl;

  //check if two manifold
  cerr << "Checking test mesh is manifold: ";
  int mRet = testMesh.checkManifold();
  if (mRet == 1){
    pass = 1;
    cout << "FAILURE" << endl;
  }
  else {
    cout << "SUCCESS" << endl;
  }

  cerr << "Checking test mesh has correct volume: ";
  double vol = testMesh.calculateVolume();
  cout << "mesh vol: " << fabs(vol) << endl;
  if ((fabs(vol) - 0.5 > SMALL)) {
    pass = 1;
    cout << "FAILURE" << endl;
  }
  else {
    cout << "SUCCESS" << endl;
  }

  cerr << "Checking test mesh is watertight: ";
  int wRet = testMesh.checkWatertight();
  if (wRet == 1){
    pass = false;
    cout << "FAILURE" << endl;
  }
  else {
    cout << "SUCCESS" << endl;
  }

  cerr << "Checking test mesh has correct number of edges: ";
  int edges = testMesh.edgeNum();
  if (edges != 18) {
    pass = 1;
    cout << "FAILURE" << endl;
  }
  else {
    cout << "SUCCESS" << endl;
  }

  cerr << "Test positive intersection with ray: ";
  Point Pt1(0.25, 0.5, 0.0);
  Vec Vt1(0.0, 1.0, 0.0);
  int intersects = testMesh.intersections(Pt1, Vt1);
  cout << intersects << endl;
  if (intersects != 1) {
    pass = 1;
    cout << "FAILURE" << endl;
  }
  else {
    cout << "SUCCESS" << endl;
  }

  cerr << "Test negative intersection with ray: ";
  Pt1.px -= 0.5;
  intersects = testMesh.intersections(Pt1, Vt1);
  cout << intersects << endl;
  if (intersects != 0) {
    pass = 1;
    cout << "FAILURE" << endl;
  }
  else {
    cout << "SUCCESS" << endl;
  }

  cerr << "Test face to point distance: ";
  Point Pt2(0.2, 0.2, 0.5);
  double dist = testMesh.shortestDistanceToPoint(Pt2);
  cout << dist << endl;
  if (fabs(dist - 0.2) > SMALL){
    pass = 1;
    cout << "FAILURE" << endl;
  }
  else {
    cout << "SUCCESS" << endl;
  }

  cerr << "Test point inside test mesh: ";
  Point Pt3(0.3, 0.1, 0.4);
  int inside = testMesh.insideMesh(Pt3);
  cout << inside << endl;
  if (inside != 0){
    pass = 1;
    cout << "FAILURE" << endl;
  }
  else {
    cout << "SUCCESS" << endl;
  }

  cerr << "Test point outside test mesh: ";
  Point Pt4(-0.6, -0.3, 1.1);
  int outside = testMesh.insideMesh(Pt4);
  cout << outside << endl;
  if (outside != 1){
    pass = 1;
    cout << "FAILURE" << endl;
  }
  else {
    cout << "SUCCESS" << endl;
  }

  cerr << "Test that mesh 1 is a single connected mesh" << endl;
  vector<vector<long>> ffAdjacencyList = testMesh.buildFaceFaceAdjacencyList();
  vector<int> connectivity = DFSIterative(ffAdjacencyList, 0);

  int fail1 = 0;

  for (int i=0;i<connectivity.size();i++){
    if (connectivity[i] == 0){
      pass = 1;
      fail1 = 1;
    }
  }

  if (fail1 == 0)
    cerr << "SUCCESS" << endl;
  else
    cerr << "FAILURE" << endl;

  int vCount = testMesh.vertices.size();

  testMesh.vertices.push_back(Point(-5, -5, -5));
  testMesh.vertices.push_back(Point(-4, -5, -5));
  testMesh.vertices.push_back(Point(-4, -4, -5));
  testMesh.faces.push_back(Face(vCount, vCount+1, vCount+2));

  cerr << "Test that mesh 2 has multiple components where expected" << endl;
  vector<vector<long>> ffAdjacencyList2 = testMesh.buildFaceFaceAdjacencyList();
  vector<int> connectivity2 = DFSIterative(ffAdjacencyList2, 0);

  int fail = 0;
  for (int i=0;i<connectivity2.size()-1;i++){
    if (connectivity2[i] == 0){
      pass = 1;
      fail = 1;
    }
  }

  if (connectivity2[connectivity2.size()-1] == 1){
    pass = 1;
    fail = 1;
  }

  if (fail == 0)
    cerr << "SUCCESS" << endl;
  else
    cerr << "FAILURE" << endl;

  return pass;

}
