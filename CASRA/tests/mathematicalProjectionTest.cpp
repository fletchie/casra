/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>

#include "CASRA/base/point.h"
#include "CASRA/specimenModels/analyticModel.h"
#include "CASRA/experimentalData/experimentalData.h"

#define SMALL 1.0e-10
#define ANGLEAPPROXTOL 0.1

using namespace std;

int testBasProjection();

int main(int argc, char* argv[])
{
  int testRes = testBasProjection();
  return testRes;
}

int testBasProjection(){

  vector<ICFTempPoint> imageCompressionFactor = {ICFTempPoint(0, 1.0)};
  vector<ShankTempPoint> sangles = {ShankTempPoint(0, 0.15)};

  double initRad = 1.0;
  Point initialPosition(0.0, 0.0, 0.0);
  int pass = 0;

  ShankBasEmitterModel emitter(imageCompressionFactor, initRad,
                               initialPosition, sangles);

  DetectionEvent dIon;
  dIon.dx = 0.1;
  dIon.dy = -0.05;

  AnalyticDetector detector(2.0, 10.0, 1.0);

  emitter.calculateFOV(detector);

  if (fabs(emitter.thetaMax - atan2(detector.radius, detector.flightpath)) < SMALL){
    cout << "SUCCESS: FOV matches that expected under a linear back-projection." << endl;
  }
  else {
    cout << "FAILURE: FOV does not match that expected under a linear back-projection." << endl;
    pass = 1;
  }

  emitter.detectorToCapTransfer(dIon, detector, 0);
  emitter.reconPlacement(dIon);

  //small angle approximation
  if ((fabs((dIon.sx - dIon.dx * initRad/(detector.flightpath + initRad))/dIon.sx) < ANGLEAPPROXTOL) &&
      (fabs((dIon.sy - dIon.dy * initRad/(detector.flightpath + initRad))/dIon.sy) < ANGLEAPPROXTOL) &&
      (fabs(dIon.sz - initRad) < ANGLEAPPROXTOL)){
    cout << "SUCCESS: back-projected ion position matches the expected result under the small angle approximation." << endl;
  }
  else{
    cout << "FAILURE: back-projected ion position fails to match expected result under the small angle approximation." << endl;
    pass = 1;
  }


  return pass;

}
