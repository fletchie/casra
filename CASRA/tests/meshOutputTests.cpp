/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/io/modelDataio.h"

#define SMALL 1.0e-30

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

using namespace std;

typedef vector<vector<double>> pointsType;
typedef vector<PointData> pointDataType;
typedef vector<vector<int>> cellsType;
typedef vector<CellData> cellDataType;

int testMeshOutputAlgorithms();
TriangularMesh createClosedTestMesh();

int main(int argc, char* argv[])
{
  int testRes = testMeshOutputAlgorithms();
  return testRes;
}

int testMeshOutputAlgorithms(){

  int retcode;
  int pass = 0;
  string filename = "";

  //create mesh
  cerr << "Triangular mesh write test" << endl;
  cerr << "\tCreating test mesh \n";
  TriangularMesh testMesh = createClosedTestMesh();
  vtkObject out;
  PointData pd1;
  pd1.key = "name1";
  pd1.values = vector<double>(testMesh.vertices.size(), 1.0);
  string filenameTriangleTest = "test_results/read_write_tests/triangle_mesh_test.vtk";
  out.setWithTriangularMesh(testMesh);
  out.pointData.push_back(pd1);
  CellData cd1;
  cd1.key = "cd1";
  cd1.values = vector<vector<double>>(testMesh.faces.size(), vector<double>(3, 2.0));
  out.cellData.push_back(cd1);
  retcode = writeVtk(out, filenameTriangleTest);

  if (retcode == 1)
    cerr << "\tFAILURE" << endl;
  else
    cerr << "\tSUCCESS" << endl;

  if (retcode == 1){
    pass = 1;
  }


  //VTK 3D scalar field write test
  cerr << "Field mesh write test" << endl;
  cerr << "\ttest mesh created." << endl;
  filename = "test_results/read_write_tests/field_test.vtk";
  scalarField3D field = scalarField3D(10, scalarField2D(10, scalarField1D(10, 2.0)));
  out.setWithField(field);
  retcode = writeVtk(out, filename);

  if (retcode == 1)
    cerr << "\tFAILURE" << endl;
  else
    cerr << "\tSUCCESS" << endl;

  if (retcode == 1){
    pass = 1;
  }

  //VTK reader test
  cerr << "Triangular mesh read test" << endl;
  vtkObject in;
  retcode = readVTKUnstructuredGrid(in, filenameTriangleTest);

  if (retcode == 1)
    cerr << "\tFAILURE" << endl;
  else
    cerr << "\tSUCCESS" << endl;

  if (retcode == 1){
    pass = 1;
  }

  return pass;

}

/*!
* Function for creating ean example test mesh for the level set library tests
*/
TriangularMesh createClosedTestMesh(){

  //create empty test rectangle mesh
  TriangularMesh closedTestMesh;

  //create points
  Point P0(0.0, 0.0, 0.0);
  Point P1(1.0, 0.0, 0.0);
  Point P2(0.0, 1.0, 0.0);
  Point P3(1.0, 1.0, 0.0);
  Point P4(0.0, 0.0, 1.0);
  Point P5(1.0, 0.0, 1.0);
  Point P6(0.0, 1.0, 1.0);
  Point P7(1.0, 1.0, 1.0);

  //create faces - must be defined consistently anticlockwise
  Face F0(0, 1, 3);
  Face F1(0, 3, 2);

  Face F2(5, 4, 7);
  Face F3(7, 4, 6);

  Face F4(0, 4, 5);
  Face F5(5, 1, 0);

  Face F6(2, 7, 6);
  Face F7(2, 3, 7);

  Face F8(0, 6, 4);
  Face F9(0, 2, 6);

  Face F10(1, 5, 7);
  Face F11(1, 7, 3);

  //add mesh vertices
  closedTestMesh.vertices.push_back(P0);
  closedTestMesh.vertices.push_back(P1);
  closedTestMesh.vertices.push_back(P2);
  closedTestMesh.vertices.push_back(P3);
  closedTestMesh.vertices.push_back(P4);
  closedTestMesh.vertices.push_back(P5);
  closedTestMesh.vertices.push_back(P6);
  closedTestMesh.vertices.push_back(P7);

  //add mesh faces
  closedTestMesh.faces.push_back(F0);
  closedTestMesh.faces.push_back(F1);
  closedTestMesh.faces.push_back(F2);
  closedTestMesh.faces.push_back(F3);
  closedTestMesh.faces.push_back(F4);
  closedTestMesh.faces.push_back(F5);
  closedTestMesh.faces.push_back(F6);
  closedTestMesh.faces.push_back(F7);
  closedTestMesh.faces.push_back(F8);
  closedTestMesh.faces.push_back(F9);
  closedTestMesh.faces.push_back(F10);
  closedTestMesh.faces.push_back(F11);

  return closedTestMesh;

}
