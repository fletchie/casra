/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>
#include <tuple>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/remeshSupport.h"
#include "CASRA/mesh/testMeshes.h"
#include "CASRA/mesh/marchingCubes.h"

#include "CASRA/field/testFields.h"

#include "CASRA/geometries/generateGeometries.h"
#include "CASRA/electrostatics/constantBEM.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/electrostatics/solverParser.h"

#include "CASRA/base/RKIntegrator.h"

#define RELSOLTOL 0.01

using namespace std;

int projectionTest();

const double admissTol = 0.2;

int main(int argc, char* argv[])
{
  int testRes = projectionTest();
  return testRes;
}

void newtonLawIon(double t, vector<double>& yNew, vector<double>& y, std::tuple<OctreeNode*>& params){

  double q = 1.0;
  double m = 1.0;
  double v1, v2, v3 = 0.0;

  OctreeNode *top = get<0>(params);

  Point currentPos(y[0], y[1], y[2]);
  top->calculateField(v1, v2, v3, currentPos, admissTol);
  double Ex = v1;
  double Ey = v2;
  double Ez = v3;

  yNew[0] = y[3];
  yNew[1] = y[4];
  yNew[2] = y[5];
  yNew[3] = -q/m * Ex;
  yNew[4] = -q/m * Ey;
  yNew[5] = -q/m * Ez;

  return;

}

int projectionTest(){

  int pass = 0;

  //build test tip mesh for octree test
  tuple<TriangularMesh, int> outData = buildTipMesh(10.0, 20.0, 100.0, 8.0);
  TriangularMesh& surfaceMesh = get<0>(outData);

  surfaceMesh = performRemesh(surfaceMesh, 6, 0.15);
  surfaceMesh.setMeshProperties();

  electrostaticBemSolver bemSolver(0.2);
  bemSolver.u = vector<double>(surfaceMesh.faces.size(), 1.0);
  bemSolver.calculateElectrostaticMatrix(surfaceMesh);
  bemSolver.matrixVectorProduct(bemSolver.rhs, bemSolver.Fmat, bemSolver.u);
  solveField(bemSolver.Pmat, bemSolver.rhs, bemSolver.q, bemSolver.Amat, bemSolver.AmatDimSize);
  surfaceMesh.assignCellData(bemSolver.q, "field");

  int retCode = get<1>(outData);

  double minCoords[3] = {pointDimMinimum(surfaceMesh.vertices, 0), pointDimMinimum(surfaceMesh.vertices, 1), pointDimMinimum(surfaceMesh.vertices, 2)};
  double maxCoords[3] = {pointDimMaximum(surfaceMesh.vertices, 0), pointDimMaximum(surfaceMesh.vertices, 1), pointDimMaximum(surfaceMesh.vertices, 2)};

  double relTol = 1e-5;

  OctreeNode top(minCoords, maxCoords,
                 &surfaceMesh,
                 &bemSolver);

  vtkObject out;

  cout << "calculating ion trajectories" << endl;
  for (int f=0;f<surfaceMesh.faces.size();f++){
    if (surfaceMesh.faces[f].centre.pz > (minCoords[2] + 0.8 * maxCoords[2])){
      tuple<double, vector<double>, double, vector<double>> res;
      double t, h;
      vector<double> kPrev;
      vector<Point> trajectory;
      Point launchPosition(surfaceMesh.faces[f].centre.px + 0.5 * surfaceMesh.faces[f].normal.vx,
                           surfaceMesh.faces[f].centre.py + 0.5 * surfaceMesh.faces[f].normal.vy,
                           surfaceMesh.faces[f].centre.pz + 0.5 * surfaceMesh.faces[f].normal.vz);
      trajectory.push_back(launchPosition);
      vector<double> y = {launchPosition.px, launchPosition.py, launchPosition.pz, 0.0, 0.0, 0.0};

      h = 1e-4;
      tuple<OctreeNode*> params;

      std::vector<double> k1(6, 0.0);
      std::vector<double> k2(6, 0.0);
      std::vector<double> k3(6, 0.0);
      std::vector<double> k4(6, 0.0);
      std::vector<double> k5(6, 0.0);
      std::vector<double> k6(6, 0.0);
      std::vector<double> k7(6, 0.0);
      std::vector<double> ynp4(6, 0.0);
      std::vector<double> ynp5(6, 0.0);

      for (int i=0;i<30;i++){
        res = rungeKuttaVectorIterateParams(&newtonLawIon, t, y, h, relTol, kPrev, params, k1, k2, k3, k4, k5, k6, k7, ynp4, ynp5);
        t = get<0>(res);
        y = get<1>(res);
        h = get<2>(res);
        kPrev = get<3>(res);
        Point ny(y[0], y[1], y[2]);
        trajectory.push_back(ny);
      }
      out.addTrajectory(trajectory);
    }
  }

  cout << "trajectory calculation complete." << endl;

  writeVtk(out, "test_results/projection-test/trajectories.vtk");
  out.setWithTriangularMesh(surfaceMesh);
  writeVtk(out, "test_results/projection-test/surface.vtk");
  cout << "ion projection calculation complete" << endl;

  return pass;

}
