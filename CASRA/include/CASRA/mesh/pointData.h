#ifndef POINTDATA_H

#define POINTDATA_H

#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <cmath>

#define PI 3.141592

#include "CASRA/mesh/triangularMesh.h"

class PointDataset {

public:
  std::vector<Point> points;
  void assignPointData(std::vector<double> pointData, std::string key);
};

#endif
