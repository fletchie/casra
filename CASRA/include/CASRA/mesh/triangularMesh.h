#ifndef TRIANGULARMESH_H

#define TRIANGULARMESH_H

#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <cmath>

#include "CASRA/base/point.h"
#include "CASRA/base/graphMath.h"

#define PI 3.141592

class Face {
  public:
    long p1;
    long p2;
    long p3;
    Point centre;
    Vec normal;
    std::map<std::string, std::vector<double>> cellData;
    double area;
    double ls2 = std::numeric_limits<double>::max();
    double ls = std::numeric_limits<double>::max();
    bool isSurface = false;

    double evapRate;
    double magnification;
    double M11;
    double M12;
    double M21;
    double M22;

    Face(long, long, long);
    Face();
    int rayIntersection(Point& Pl, Vec& Vl, Point& P1, Point& P2, Point& P3);
    double distanceToPoint(Point& Pl, Point& P1, Point& P2, Point& P3);
    void setCentre(std::vector<Point>&);
    void setNormal(std::vector<Point>&);
    void setArea(std::vector<Point>&);
    void setLongestSideSqrd(std::vector<Point>&);
    void setLongestSide(std::vector<Point>&);

};


std::vector<Face> operator+(std::vector<Face>& a, long b);


class TriangularMesh {

public:
  std::vector<Point> vertices;
  std::vector<Point> normals;
  std::vector<Face> faces;
  std::vector<double> isSurface;
  std::vector<std::vector<long>> vertexFaceAdjacencyList;
  std::vector<double> evapRates;

  double vol = -10.0;

  std::map<std::string, std::vector<double>> surfacePointFields;
  std::map<std::string, std::vector<std::vector<double>>> surfaceCellFields;

  double minCoords[3];
  double maxCoords[3];

  void setBoundingBox();
  int checkManifold();
  double calculateVolume();
  int edgeNum();
  int checkWatertight();
  int intersections(Point& Pl, Vec& Vl);
  double shortestDistanceToPoint(Point& Pl, double distanceTol = 2.25);
  int insideMesh(Point& Pl);
  void clear();
  void rescaleMesh(Point bottomLeftDomainCorner, double cellWidth);
  void rescaleMesh(Point bottomLeftDomainCorner, double cellWidthX, double cellWidthY, double cellWidthZ);
  void setCentres();
  void setNormals();
  void setLongestSideSqrd();
  void setLongestSides();
  void setAreas();
  void averageVertexNormals();
  void setMeshProperties();
  void assignCellData(std::vector<double> cellData, std::string key);
  std::vector<std::vector<long>> buildFaceFaceAdjacencyList();
  std::vector<std::vector<long>> buildVertexVertexAdjacencyList();
  std::vector<std::vector<long>> buildVertexFaceAdjacencyList();
  void removeDuplicateVertices();
  std::vector<double> distanceFromSurface(TriangularMesh& prevSurface, std::vector<long>& surfacePatch);
  std::vector<double> distanceFromSurfaceFace(TriangularMesh& prevSurface, std::vector<long>& surfacePatch);
  std::tuple<std::vector<TriangularMesh>, std::vector<double>, int> detachSeparateMeshComponents();
};

template <size_t n>
double calculateDeterminant(double mat[n][n]);
std::vector<double> averageField(std::vector<Face>& faces, std::vector<double>& field);
double pointDimMinimum(std::vector<Point> vec, int dim);
double pointDimMaximum(std::vector<Point> vec, int dim);
double pointDimMean(std::vector<Point>& vec, int dim);
Point pointMean(std::vector<Point*>& vec);
Point pointMean(std::vector<Point>& vec);

void findBarycentricCoordinates3D(Point& pointP, Point& pointA, Point& pointB, Point& pointC, double& u, double& v, double& w);

// Compute barycentric coordinates (u, v, w) for
// point p with respect to triangle (a, b, c)
inline void findBarycentricCoordinates2D(Point& pointP, Point& pointA, Point& pointB, Point& pointC, double& u, double& v, double& w)
{

    double v1px = pointB.px - pointA.px;
    double v1py = pointB.py - pointA.py;
    double v1pz = pointB.pz - pointA.pz;

    double v2px = pointC.px - pointA.px;
    double v2py = pointC.py - pointA.py;
    double v2pz = pointC.pz - pointA.pz;

    double v3px = pointP.px - pointA.px;
    double v3py = pointP.py - pointA.py;
    double v3pz = pointP.pz - pointA.pz;

    double den = v1px * v2py - v2px * v1py;
    v = (v3px * v2py - v2px * v3py) / den;
    w = (v1px * v3py - v3px * v1py) / den;
    u = 1.0 - v - w;
}

#endif
