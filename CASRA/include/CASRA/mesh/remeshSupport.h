#ifndef REMESHSUPPORT_H

#define REMESHSUPPORT_H

#include <vector>

#include "CASRA/mesh/triangularMesh.h"

TriangularMesh& performRemesh(TriangularMesh& surfaceMesh, int it = 4,
                              double targetLengthFraction = 1.0);

TriangularMesh& performAdaptiveRemesh(TriangularMesh& surfaceMesh,
                                      std::vector<double> remeshHeightLandmarks,
                                      std::vector<double> targetLengths,
                                      int it = 2, double cellWidthZ = 1.0,
                                      double startShankExtZ = 1e-10);

TriangularMesh& performAdaptiveRemeshDeprecated(TriangularMesh& surfaceMesh,
                                                std::vector<double> remeshHeightLandmarks,
                                                std::vector<double> targetLengths,
                                                int it = 2, double cellWidthZ = 1.0,
                                                double startShankExtZ = 1e-10);

#endif
