#ifndef TESTMESHES_H

#define TESTMESHES_H

#include "CASRA/mesh/triangularMesh.h"

TriangularMesh buildRectangularMesh(double xmin, double xmax,
                                    double ymin, double ymax,
                                    double zmin, double zmax);

#endif
