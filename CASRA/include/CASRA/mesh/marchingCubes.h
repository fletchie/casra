#ifndef MARCHINGCUBES_H

#define MARCHINGCUBES_H

#include <vector>
#include <functional>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/field/scalarField.h"

//hash function for unordered_mapping featuring vector keys 
//defined from https://stackoverflow.com/questions/16792751/hashmap-for-2d3d-coordinates-i-e-vector-of-doubles/47928817
struct hashFunc {
    unsigned int operator()(const std::vector<double>& V) const {
        unsigned int h1 = std::hash<double>()(V[0]);
        unsigned int h2 = std::hash<double>()(V[1]);
        unsigned int h3 = std::hash<double>()(V[2]);
        return (h1 ^ (h2 << 1))^h3;
    }
};

struct equalsFunc {
    bool operator()(const std::vector<double>& lhs, const std::vector<double>& rhs) const {
        return (lhs[0] == rhs[0]) && (lhs[1] == rhs[1]) && (lhs[2] == rhs[2]);
    }
};

/*!
 * Class for triangular panels making up extracted isocontour
 */
class isosurfacePanel {
  public:
    Point p[3];
};

/*!
 * Class for considering extraction from a single grid cell of the voxel field
 */
class gridCell {
  public:
    Point p[8];
    double val[8];
};

int contourCell(gridCell& grid, double& isolevel, isosurfacePanel *triangles);
Point vertexEdgeInterpolation(double& isolevel, Point& p1, Point& p2, double& valp1, double& valp2);
void marchingCubes(scalarField3D& field, double isolevel, TriangularMesh& outMesh);
void unpackUniqueVertices(std::vector<std::vector<std::vector<double>>>& trilist, TriangularMesh *mesh);

#endif
