#ifndef INITIALISER_H

#define INITIALISER_H

#include <vector>
#include <string>
#include <map>

#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_utils.hpp"

#include "CASRA/evaporationLaws/evaporationLaws.h"
#include "CASRA/specimenModels/numericalModel.h"
#include "CASRA/mesh/triangularMesh.h"

class ControlDict {
  public:
    double stopIts;
    double stopTime;
    double stopApexHeight;
};

class ReadEvaporationModel {
  public:
    std::string evaporationModel;
    int gridCells[3];
    double timeStepFraction;
    long reinitItGap;
};

class ReadSpecimenModel {
  public:
    TMSpecimen specimenModel;
    double minSpecimenLength;
    double shankAngle;
    bool extendShank;
};

class ReadThermalModel {
  public:
    std::string thermalModel;
    double T0;
    double TMax;
    double dir[3];
};

class ReadExternalSystemModel {
public:
    std::vector<std::string> systemModelNames;
    std::vector<TriangularMesh> systemModelMeshes;
    std::vector<double> systemModelVoltages;
};

class ModelParams {
  public:
    int simId = -1;
    ControlDict cDict;
    ReadEvaporationModel eModel;
    ReadSpecimenModel sModel;
    ReadThermalModel tModel;
    ReadExternalSystemModel esModel;
};



AhhreniusLawIsotropic loadAhhreniusIsotropicLaw(rapidxml::xml_node<>* phasePhysicsModelNode);
DeformedAhhreniusLawIsotropic loadDeformedAhhreniusIsotropicLaw(rapidxml::xml_node<>* phasePhysicsModelNode);
LinearLawIsotropic loadLinearIsotropicLaw(rapidxml::xml_node<>* phasePhysicsModelNode);

ModelParams initialiseModelFromXML(std::string cfname, std::string cdirectory = ".");

#endif
