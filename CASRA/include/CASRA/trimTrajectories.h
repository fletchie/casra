#ifndef TRIMTRAJECTORIES_h

#define TRIMTRAJECTORIES_h

#include <string>

//only currently required for reading in specimen mesh, chargedOptics library
//does not explicitly depend on LS model
#include "CASRA/evaporationModels/initialiser.h"
#include "CASRA/chargedOptics/opticsInitialiser.h"

using namespace std;

int trimTrajectories(ModelParams modelParams, OpticsParams opticsParams, std::string modelDir, std::string writeDir);

#endif
