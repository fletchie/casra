#ifndef THERMALMODELS_H

#define THERMALMODELS_H

#include <vector>
#include <stdexcept>

#include "CASRA/mesh/triangularMesh.h"

class BaseThermalModel {

  public:
    double T0;

    BaseThermalModel() {

    }
    BaseThermalModel(double T0);

};


class UniformTemperatureModel : public BaseThermalModel {

  public:
    UniformTemperatureModel() {

    }
    UniformTemperatureModel(double T0);
    std::vector<double> calculateTemperature(TriangularMesh& surfaceMesh);

};


class InstantaneousHeatingModel : public BaseThermalModel {

  public:
    double TMax;
    double direction[3];

    InstantaneousHeatingModel() {

    }
    InstantaneousHeatingModel(double dir[3], double TMax, double T0);
    std::vector<double> calculateTemperature(TriangularMesh& surfaceMesh);

};


#endif
