#ifndef HITMAPCALCULATION_H

#define HITMAPCALCULATION_H

#include <tuple>
#include <vector>
#include <string>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/chargedOptics/chargedOptics.h"
#include "CASRA/field/scalarField.h"
#include "CASRA/specimenModels/numericalModel.h"

int computeCode(double& x, double& y, double& sxmin, double& symin, double& sxmax, double& symax);
std::tuple<bool, double, double, double, double> cohenSutherland(double sxmin, double symin,
                                                                 double sxmax, double symax,
                                                                 double x1, double y1,
                                                                 double x2, double y2);

bool pointIntriangle(double& px, double& py, Point& detPos1, Point& detPos2, Point& detPos3);
double triangleRectangleIntersection(double& sxmin, double& symin, double& sxmax, double& symax,
                                     Point& detPos1, Point& detPos2, Point& detPos3, double& triangleArea);

std::tuple<scalarField3D, scalarField3D> calculateHitmap(std::vector<Point>& ion1SamplePos, std::vector<Point>& ion1DetectorPos,
                             std::vector<Point>& ion2SamplePos, std::vector<Point>& ion2DetectorPos,
                             std::vector<Point>& ion3SamplePos, std::vector<Point>& ion3DetectorPos,
                             std::vector<double>& evapRate, std::vector<double>& magnification,
                             const double detectorRadius, const int xres, const int yres);

scalarField3D calculatePhaseMap(std::vector<Point>& ion1SamplePos, std::vector<Point>& ion1DetectorPos,
                                std::vector<Point>& ion2SamplePos, std::vector<Point>& ion2DetectorPos,
                                std::vector<Point>& ion3SamplePos, std::vector<Point>& ion3DetectorPos,
                                std::vector<int>& ion1phaseIds, std::vector<int>& ion2phaseIds,
                                std::vector<int>& ion3phaseIds, std::vector<double>& evapRates,
                                std::vector<double>& magnification, TMSpecimen& specimenModel,
                                const double detectorRadius,
                                const int xres, const int yres);

#endif
