#ifndef CHARGEDOPTICS_H

#define CHARGEDOPTICS_H

#include <tuple>
#include <vector>
#include <string>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/electrostatics/constantBEM.h"

#include "CASRA/specimenModels/numericalModel.h"

class Ion{

  public:
    double charge;
    double mass;

    Point launchPos;
    Point finalPos;

    std::vector<Point> trajectory;
    std::vector<Point> velocity;
    double ionicVolume;

    std::string phase;
    int phaseIndex;

    bool writeOutput = true;

    float pulseVoltage = 0.0;
    double ionIdentifier = -1.0;

    Ion(){ };
    Ion(double charge, double mass);

};

class SyntheticPeak{

  public:
    double lower;
    double upper;
    double ionicVolume;
    std::vector<std::string> ionLabels;
    std::vector<int> ionNumbers;

    void labelToPeak(std::string ionLabel, double ionicVolume, int massToChargeLabel);

};

class Detector{

  public:
    Point centre;
    Vec normal;
    double radius;

    double A;
    double B;
    double C;
    double D;

    //local coordinate vectors
    double Xx;
    double Xy;
    double Xz;
    double Yx;
    double Yy;
    double Yz;

    Detector(Point pos, Vec normal, double radius);
    bool linearProjection(Point& ionPosition, Vec& ionLaunchNormal, double kappa);
    bool calculatedTrajectoryIntersectionPoint(Ion& testIon);
    int planeSide(Point& testPos);
    Point convertLocalcoordinate(Point& intersectionPoint);

};

class OpticsDomain{

  public:
    double xmin;
    double xmax;
    double ymin;
    double ymax;
    double zmin;
    double zmax;

    double distanceThreshold;

    Detector* detector;

    OpticsDomain(double xmin, double xmax, double ymin, double ymax, double zmin,
                 double zmax, Detector& detector,
                 double distanceThreshold = std::numeric_limits<double>::max());
    OpticsDomain(double xmin, double xmax, double ymin, double ymax, double zmin,
                 double zmax, Detector* detector,
                 double distanceThreshold = std::numeric_limits<double>::max());
    OpticsDomain(double xmin, double xmax, double ymin, double ymax, double zmin,
                 double zmax, double distanceThreshold = std::numeric_limits<double>::max());

    bool checkOutsideBounds(Point& testPoint);
    bool terminateTrajectoryIterator(Ion& testIon, TriangularMesh& surfaceMesh);
    std::tuple<std::vector<Ion>> calculateUniformChargedOptics(std::vector<long> launchSites,
                                                               TriangularMesh& surfaceMesh,
                                                               electrostaticBemSolver& bemSolver,
                                                               TMSpecimen& specimenModel,
                                                               double launchDistance, double relTol = 1e-5);
  std::tuple<std::vector<Ion>> calculateWeightedChargedOptics(std::vector<Point>& launchPositions, std::vector<Vec>& launchNormals,
                                                              std::vector<int>& launchPhase,
                                                              TriangularMesh& surfaceMesh,
                                                              electrostaticBemSolver& bemSolver, TMSpecimen& specimenModel,
                                                              double launchDistance, double relTol = 1e-5);

};
std::vector<long> determineUniformLaunchSites(TriangularMesh& surfaceMesh);
std::tuple<std::vector<Point>, std::vector<Vec>, std::vector<int>, std::vector<Phase*>> determineWeightedLaunchSites(TriangularMesh& surfaceMesh,
                                                                                                                     TriangularMesh& previousSurfaceState,
                                                                                                                     TriangularMesh& nextSurfaceState,
                                                                                                                     TMSpecimen& specimenModel,
                                                                                                                     std::vector<double>& evapVols);

std::tuple<double, double, double, double, double> calculateLocalMagnification(Point& ion1LaunchPos, Point& ion2LaunchPos, Point& ion3LaunchPos, Point& ion1DetectorPos, Point& ion2DetectorPos, Point& ion3DetectorPos);
void lawOfMotion(double t, std::vector<double>& yNew, std::vector<double>& y, std::tuple<OctreeNode*, Ion*>& params);
double minimiseCutoffIntersection(Point& pointA, Point& pointB, TriangularMesh& surfaceMesh, double cutoffDistance);

//template class for determining the sign of a passed variable (> < operators must be defined for the template variable)
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

template<typename T>
void bigEndianBufferPush(T *dataLoc, char *buffer, int size) {

  //copy binary to memory
  memcpy(buffer, dataLoc, size);
  std::reverse(buffer, buffer + size); //reverse endianness (file should be big endian)

}

template<typename T>
void smallEndianBufferPush(T *dataLoc, char *buffer, int size) {

  //copy binary to memory
  memcpy(buffer, dataLoc, size);

}

void rangeFileSyntheticDataWriter(std::string filename, std::vector<SyntheticPeak> peaks);
void eposSyntheticDataWriter(std::string filename, std::vector<Ion> ions, bool header = true);
void posSyntheticDataWriter(std::string filename, std::vector<Ion> ions, bool header = true);
void atoSyntheticDataWriter(std::string filename, std::vector<Ion> ions, bool header = true);
std::vector<SyntheticPeak> createPeakSet(std::map<std::string, double> ionLabelVolumeMap);

#endif
