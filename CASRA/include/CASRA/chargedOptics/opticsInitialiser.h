#ifndef OPTICS_INITIALISER_H

#define OPTICS_INITIALISER_H

#include <vector>
#include <string>
#include <map>
#include <numeric>

#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_utils.hpp"

#include "CASRA/mesh/triangularMesh.h"

class transferParams{
  public:
    bool enabled = false;
    std::string type;
    std::vector<double> functionParameters;
};

class detectorParams{
  public:
    bool enabled = true;
    Point position;
    Vec normal;
    double radius;
    int xCells;
    int yCells;
};

class OpticsParams {
  public:
    int simId = -1;
    std::string projectionType = "uniform";
    int itStep = 1;
    int itStart = 0;
    int itStop = std::numeric_limits<int>::max();
    double cutoffDistance = -100.0; //negative cutoff distance values disable the trajectory integration termination condition
    double launchDistance = 0.0;
    bool outputMapping = true;
    bool outputTrajectories = true;
    bool outputHitmap = true;
    bool outputMagnification = true;
    detectorParams dparams;
    transferParams tparams;
    std::vector<std::string> exportFileTypes;
};

OpticsParams initialiseModelOpticsFromXML(std::string cfname);

#endif
