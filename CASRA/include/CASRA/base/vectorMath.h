#ifndef VECTORMATH_H
#define VECTORMATH_H

#include <vector>
#include <iostream>

typedef std::vector<std::vector<double>> matrix;

std::vector<double> operator+(std::vector<double> a, std::vector<double> b);
std::vector<double> operator+(std::vector<double> a, double b);
std::vector<double> operator-(std::vector<double> a);
std::vector<double> operator+(double b, std::vector<double> a);
std::vector<double> operator*(double b, std::vector<double> a);
std::vector<double> operator*(std::vector<double> a, double b);
std::vector<double> operator/(std::vector<double>& a, std::vector<double>& b);
double minimum(std::vector<double> vec);
double maximum(std::vector<double> vec);
double minimum(std::vector<std::vector<double>> vec, int dim);
double maximum(std::vector<std::vector<double>> vec, int dim);
double vecMagnitude(std::vector<double>& a, std::vector<double>& b);
std::vector<double> abs(std::vector<double> vec);

inline matrix initialiseMatrix(int xDim, int yDim, double value = 0.0){

  matrix nMatrix = matrix(xDim, std::vector<double>(yDim, value));
  return nMatrix;

}

inline void diagMatmulTr(matrix& out, std::vector<double>& A, matrix& B) {
    const int xDim = B.size();
    const int yDim = B[0].size();

#if defined(_OPENMP)
#pragma omp parallel for
#endif
    for (int i = 0; i<xDim; i++) {
        for (int j = 0; j<yDim; j++) {
            out[i][j] = B[i][j];
            out[i][j] *= A[j];
        }
    }
}

inline void diagMatmul(matrix& out, std::vector<double>& A, matrix& B){
  const int xDim = B.size();
  const int yDim = B[0].size();

  #if defined(_OPENMP)
  #pragma omp parallel for
  #endif
  for (int i=0;i<xDim;i++){
    for (int j=0;j<yDim;j++){
        out[i][j] = B[i][j];
        out[i][j] *= A[i];
    }
  }
}

inline void diagMatmul(std::vector<double>& out, std::vector<double>& A, std::vector<double>& B){

  const int xDim = A.size();

  for (int i=0;i<xDim;i++){
      out[i] = B[i];
      out[i] *= A[i];
  }
}

inline matrix matmul(matrix& A, matrix& B){
  int xDim = A.size();
  int yDim = A[0].size();
  matrix outMatrix = initialiseMatrix(xDim, yDim, 0.0);

  for (int i=0;i<A.size();i++){
    for (int j=0;j<A[0].size();j++){
      double rcval = 0.0;
      for (int k=0;k<A[0].size();k++){
        rcval += A[i][k] * B[k][j];
      }
      outMatrix[i][j] = rcval;
    }
  }
  return outMatrix;
}

inline std::vector<double> matmul(matrix& A, std::vector<double>& B){
  std::vector<double> outVec = std::vector<double>(B.size(), 0.0);
  for (int i=0;i<A.size();i++){
    for (int j=0;j<B.size();j++){
      outVec[i] += A[i][j] * B[j];
    }
  }
  return outVec;
}

#endif
