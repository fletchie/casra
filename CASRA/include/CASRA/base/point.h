#ifndef POINT_H
#define POINT_H

#include <vector>
#include <string>
#include <cstring>
#include <iostream>
#include <math.h>
#include <map>

class Vec {

  public:
    double vx = 0.0;
    double vy = 0.0;
    double vz = 0.0;

    Vec(){

    }
    Vec(double, double, double);

};

Vec operator+(Vec& a, Vec& b);
Vec operator+(Vec&& a, Vec&& b);
Vec operator+(Vec&& a, Vec& b);
Vec operator+(Vec& a, Vec&& b);
Vec operator*(Vec&& a, double mult);
Vec operator*(Vec& a, double mult);


class Point {

  public:
    double px = 0.0;
    double py = 0.0;
    double pz = 0.0;
    bool locked = false;

    Vec averagedNormal;

    std::map<std::string, std::vector<double>> pointData;

    /*!
     * Point Initialiser declaration 1 - initialisation of point coordinates
     * @param x The x coordinate to be initialised
     * @param y The y coordinate to be initialised
     * @param z The z coordinate to be initialised
     *
     */
    Point(double px, double py, double pz) : px{ px }, py{ py }, pz{ pz }{

    }

    /*!
     * Point Initialiser declaration 2 - initially undefined point coordinates
     */
    Point() {

    }

    Point operator+(Point& b) {

        Point nPoint(px + b.px, py + b.py, pz + b.pz);
        return nPoint;

    }

    Point operator+(Point&& b) {

        Point nPoint(px + b.px, py + b.py, pz + b.pz);
        return nPoint;

    }


    void operator+=(Point& b) {

        px += b.px;
        py += b.py;
        pz += b.pz;

    }

    Point operator-(Point& b) {

        Point nPoint(px - b.px, py - b.py, pz - b.pz);
        return nPoint;

    }

    Point operator-(Point&& b) {

        Point nPoint(px - b.px, py - b.py, pz - b.pz);
        return nPoint;

    }

    void operator-=(Point& b) {

        px -= b.px;
        py -= b.py;
        pz -= b.pz;

    }

    Point operator/(double denom) {

        Point nPoint(px / denom, py / denom, pz / denom);
        return nPoint;
    }

    void operator=(const Point& b) {

        px = b.px;
        py = b.py;
        pz = b.pz;
        locked = b.locked;

        averagedNormal.vx = b.averagedNormal.vx;
        averagedNormal.vy = b.averagedNormal.vy;
        averagedNormal.vz = b.averagedNormal.vz;
    }

    Point operator*(double mult) {

        Point nPoint(px * mult, py * mult, pz * mult);
        return nPoint;
    }

    void operator*=(double mult) {

        px *= mult;
        py *= mult;
        pz *= mult;
    }

    void printPoint();
    int checkEquals(Point point2, double tol);

};

inline double norm(Point& a, Point& b){
  return sqrt((a.px - b.px)*(a.px - b.px) + (a.py - b.py)*(a.py - b.py) + (a.pz - b.pz)*(a.pz - b.pz));
}

inline double dot(Point& a, Point& b){
  return ((a.px * b.px) + (a.py * b.py) + (a.pz * b.pz));
}

/*!
 * Function for performing rounding to a fixed number of decimal places
 * @param value The variable to round (float or double)
 * @param decimal_places The number of decimal places to round to
 */
inline float round_up(float value, const int decimal_places) {
    float multiplier = 1.0;
    for (int i = 0; i < decimal_places; i++) {
        multiplier *= 10.0;
    }
    return std::ceil(value * multiplier+1) / multiplier;
}

/*!
 * Function for performing rounding to a fixed number of decimal places
 * @param value The variable to round (float or double)
 * @param decimal_places The number of decimal places to round to
 */
inline double round_up(double value, const int decimal_places) {
    double multiplier = 1.0;
    for (int i = 0; i < decimal_places; i++) {
        multiplier *= 10.0;
    }
    return std::ceil(value * multiplier+1) / multiplier;
}


Point extractPoint(std::string strPoint);
std::vector<std::string> extractPointChar(std::string strPoint);

#endif
