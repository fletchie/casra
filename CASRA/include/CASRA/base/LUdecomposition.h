#ifndef LUDECOMPOSITION_H
#define LUDECOMPOSITION_H

#include <vector>

#include "CASRA/base/vectorMath.h"

int LUSolveLP(matrix& mat, std::vector<double>& b, std::vector<double>& x);
int LUSolve(matrix& mat, std::vector<double>& b, std::vector<double>& x);
void LUdecomposition(matrix& mat, matrix& lower, matrix& upper);
std::vector<double> backSub(matrix& U, std::vector<double>& b);
std::vector<double> forSub(matrix& U, std::vector<double>& b);

#endif
