/*! \file RKIntegrator.h
 * RK integration
 */

#ifndef RKINTEGRATOR_H

#define RKINTEGRATOR_H

#include <vector>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <tuple>

#include "CASRA/base/vectorMath.h"

#define FAC 0.9
#define EXP 0.2

//Butcher Tablaeu values for Dormand-prince RK45 method
const double C1 = 0.0;

const double C2 = 1.0/5.0;
const double A21 = 1.0/5.0;

const double C3 = 3.0/10.0;
const double A31 = 3.0/40.0;
const double A32 = 9.0/40.0;

const double C4 = 4.0/5.0;
const double A41 = 44.0/45.0;
const double A42 = -56.0/15.0;
const double A43 = 32.0/9.0;

const double C5 = 8.0/9.0;
const double A51 = 19372.0/6561.0;
const double A52 = -25360.0/2187.0;
const double A53 = 64448.0/6561.0;
const double A54 = -212.0/729.0;

const double C6 = 1.0;
const double A61 = 9017.0/3168.0;
const double A62 = -355.0/33.0;
const double A63 = 46732.0/5247.0;
const double A64 = 49.0/176.0;
const double A65 = -5103.0/18656.0;

const double C7 = 1.0;
const double A71 = 35.0/384.0;
const double A72 = 0.0;
const double A73 = 500.0/1113.0;
const double A74 = 125.0/192.0;
const double A75 = -2187.0/6784.0;
const double A76 = 11.0/84.0;

const double B11 = A71;
const double B21 = A72;
const double B31 = A73;
const double B41 = A74;
const double B51 = A75;
const double B61 = A76;
const double B71 = 0.0;

const double B12 = 5179.0/57600.0;
const double B22 = 0.0;
const double B32 = 7571.0/16695.0;
const double B42 = 393.0/640.0;
const double B52 = -92097.0/339200.0;
const double B62 = 187.0/2100.0;
const double B72 = 1.0/40.0;

std::tuple<double, double, double, double> rungeKuttaScalarIterate(double (*f)(double, double),
                                                                   double t,
                                                                   double yn,
                                                                   double h,
                                                                   double relTol,
                                                                   double kPrev);


std::tuple<double, std::vector<double>, double, std::vector<double>> rungeKuttaVectorIterate(std::vector<double> (*newton)(double, std::vector<double>),
                                                                                             double t,
                                                                                             std::vector<double> yn,
                                                                                             double h,
                                                                                             double relTol,
                                                                                             std::vector<double> kPrev);


/*!
 * The Runge Kutta Integrator for using Dormand-Prince RK45.
 * Takes an additional template object instance that is passed to the function giving
 * the gradient term
 * @param f Function pointer providing gradient of first-order ODE
 * @param t current time
 * @param yn Current y value
 * @param h previous stepsize estimate
 * @param relTol Tolerance parameter
 * @param kPrev The previous k7 calculated (to be k1 under DPRk45)
 * @param params The parameter object instance that is passed to the pointed function (i.e. param1)
 * @return tuple of integrator outputs (tNew, ynp1, hNew, k7)
 */
template<typename T>
std::tuple<double, std::vector<double>, double, std::vector<double>> rungeKuttaVectorIterateParams(void (*f)(double, std::vector<double>&, 														 std::vector<double>&, T& params),
                                                                                                   double& t,
                                                                                                   std::vector<double>& yn,
                                                                                                   double& h,
                                                                                                   double& relTol,
                                                                                                   std::vector<double>& kPrev, T& params, 
                                                                                                   std::vector<double>& k1, 
                                                                                                   std::vector<double>& k2,
                                                                                                   std::vector<double>& k3,
                                                                                                   std::vector<double>& k4,
                                                                                                   std::vector<double>& k5,
                                                                                                   std::vector<double>& k6,
                                                                                                   std::vector<double>& k7,
                                                                                                   std::vector<double>& ynp4,
                                                                                                   std::vector<double>& ynp5) {


 double hNew;

 //first same as last property holds as Bi1 match A7i
 //for Dormand Prince
 if (kPrev.size() == 0){
   f(t, k1, yn, params);
 }
 else {
   k1 = kPrev;
 }

 double err = std::numeric_limits<double>::max();
 double sc = 0.0;
 double tNew = t + h * C7;
 double tau;
 int it = 0;
 double time = 0.0;

 while (err > relTol) {
   ynp5 = yn + h * (A21 * k1);
   f(t + h * C2, k2, ynp5, params);
   ynp5 = yn + h * (A31 * k1 + A32 * k2);
   f(t + h * C3, k3, ynp5, params);
   ynp5 = yn + h * (A41 * k1 + A42 * k2 + A43 * k3);
   f(t + h * C4, k4, ynp5, params);
   ynp5 = yn + h * (A51 * k1 + A52 * k2 + A53 * k3 + A54 * k4);
   f(t + h * C5, k5, ynp5, params);
   ynp5 = yn + h * (A61 * k1 + A62 * k2 + A63 * k3 + A64 * k4 + A65 * k5);
   f(t + h * C6, k6, ynp5, params);
   ynp5 = yn + h * (A71 * k1 + A72 * k2 + A73 * k3 + A74 * k4 + A75 * k5 + A76 * k6);
   f(tNew, k7, ynp5, params);
   ynp4 = yn + h * (B12 * k1 + B22 * k2 + B32 * k3 + B42 * k4 + B52 * k5 + B62 * k6 + B72 * k7);
   //err = vecMagnitude(ynp5, ynp4) + 1e-100;
   err = sqrt((ynp5[0] - ynp4[0]) * (ynp5[0] - ynp4[0]) + (ynp5[1] - ynp4[1]) * (ynp5[1] - ynp4[1]) + (ynp5[2] - ynp4[2]) * (ynp5[2] - ynp4[2])
            + (ynp5[3] - ynp4[3]) * (ynp5[3] - ynp4[3]) + (ynp5[4] - ynp4[4]) * (ynp5[4] - ynp4[4]) + (ynp5[5] - ynp4[5]) * (ynp5[5] - ynp4[5])) + 1e-100;

   h = h * 0.5;
   it++;
 }

 tau = FAC * h * 2.0 * std::pow(relTol/err, 0.2);
 h = std::min(std::max(tau, 1e-50), 1e50);

 std::tuple<double, std::vector<double>, double, std::vector<double>> res = std::make_tuple(tNew, ynp5, h, k7);

 return res;

}

#endif
