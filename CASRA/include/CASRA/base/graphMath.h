#ifndef GRAPHMATH_H
#define GRAPHMATH_H

#include <vector>

/*!
 * Implementation of the Depth First Search (DFS) algorithm for traversing
 * graph trees. Used to determine disconnected mesh components.
 *
 * @param connectivityMatrix the graph adjacency list
 * @param initNode The initial node index to start the search
 * @return a vector of whether each node is connected (1) or not (0) to the starting node
 */
std::vector<int> DFSIterative(std::vector<std::vector<long>> connectivityMatrix, long initNode);

#endif
