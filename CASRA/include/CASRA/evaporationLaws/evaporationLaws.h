#ifndef EVAPORATIONLAWS_H

#define EVAPORATIONLAWS_H

#include <vector>
#include <any>
#include <math.h>

#define BOLTZMANN 1.38064852e-23
//#define BOLTZMANN 1

class EvaporationLaw {

};

class CurvatureLawIsotropic: public EvaporationLaw {

  public:
    double materialModifier;

    CurvatureLawIsotropic(){

    }

    CurvatureLawIsotropic(double materialModifier);

};

class LinearLawIsotropic: public EvaporationLaw {

  public:
    double evapField;

    LinearLawIsotropic(){

    }

    LinearLawIsotropic(double evapField);
    std::vector<double> evaporationRates(std::vector<double> field, std::vector<double> T);
    std::vector<double> evaporationRates(std::vector<double> field, double T);
    double evaporationRate(double field, double T);

};

class AhhreniusLawIsotropic: public EvaporationLaw {

  public:
    double preFactor;
    std::vector<double> C;
    std::vector<double> Cexp;
    double evapField;

    AhhreniusLawIsotropic(){

    }
    AhhreniusLawIsotropic(double preFactor, std::vector<double> C, std::vector<double> Cexp, double evapField);

    std::vector<double> evaporationRates(std::vector<double> field, std::vector<double> T);
    std::vector<double> evaporationRates(std::vector<double> field, double T);
    double evaporationRate(double field, double T);
    double logEvaporationRate(double field, double T);
    double instantaneousEvapFluxVoltageRamp(double field,
                                            double T);
};

class DeformedAhhreniusLawIsotropic: public EvaporationLaw {

  public:
    double preFactor;
    std::vector<double> C;
    std::vector<double> Cexp;
    double evapField;
    double d = 0.0;

    DeformedAhhreniusLawIsotropic(){

    }

    DeformedAhhreniusLawIsotropic(double preFactor, std::vector<double> C, std::vector<double> Cexp, double evapField, double d = 0.0);

    std::vector<double> evaporationRates(std::vector<double> field, std::vector<double> T);
    std::vector<double> evaporationRates(std::vector<double> field, double T);
    double evaporationRate(double field, double T);
    double logEvaporationRate(double field, double T);
    double instantaneousEvapFluxVoltageRamp(double field,
                                            double T);

};

double instantaneousSFVoltageRamp(std::vector<double> fields, std::vector<double> evapFields, double fieldReductionFactor);

inline double linearLaw(double field, double A){
  return field/A;
}

inline double deformedAhhreniusLaw(double field, double evapField, double beta, double A, double d){
  double lval = log(A) + 1.0/d * log(1.0 - d * beta * (1.0 - field/evapField));
  return exp(lval);
}

template<typename Law>
double secantMethod(double x0, double x1, double field, double T, double tol, int maxIts, Law* evapLaw){
  double ctol = std::numeric_limits<double>::max();
  int it = 0;
  double xnm1 = x0;
  double xn = x1;
  double fVnm1, fVn, xnp1;
  fVnm1 = evapLaw->logEvaporationRate(field * xnm1, T) - 1.0;
  while ((ctol > tol) && (it < maxIts)){
    fVn = evapLaw->logEvaporationRate(field * xn, T) - 1.0;
    xnp1 = xn - fVn * (xn - xnm1)/(fVn - fVnm1);
    it++;
    ctol = fabs((xnp1 - xn)/xnp1);
    xn = xnp1;
    xnm1 = xn;
    fVnm1 = fVn;
  }

  return xn;
}

inline double ahhreniusLaw(double field, double evapField, double beta, double A);
std::vector<double> ahhreniusLaw(std::vector<double> field, std::vector<double> evapField, std::vector<double> beta, double A);
std::vector<double> ahhreniusLaw(std::vector<double> field, std::vector<double> evapField, double beta, double A);

#endif
