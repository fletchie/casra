#ifndef EXPERIMENTAL_DATA_H

#define EXPERIMENTAL_DATA_H

#include <vector>
#include <string>

class ICFTempPoint {
  public:
    long eventIndex;
    double ICF;

  ICFTempPoint(long eventIndex, double icf);

};

class ShankTempPoint {
  public:
    long eventIndex;
    double alpha;

  ShankTempPoint(long eventIndex, double angle);

};

class VoltageTempPoint {
  public:
    long eventIndex;
    double kF;

  VoltageTempPoint(long eventIndex, double kF);

};


class SpeciesType{

  public:
    std::string speciesName;
};

class RangedPeak{

  public:
    double lower;
    double upper;
    std::string colorcode;
    int charge;
    std::vector<int> comp;
    double ionicVol;

};

class RangeData{

  public:
    std::string sourceFileName;
    std::vector<SpeciesType> species;
    std::vector<RangedPeak> peaks;
    int SpeciesNum;
    int PeakNum;

    RangeData();
    int readFile(std::string filename);
    unsigned int findElement(std::string element);
    std::string constructFormula(RangedPeak *rPeak);

};


class DetectionEvent{
public:

  double dx; /**< ion detected x-coordinate (mm) */
  double dy; /**< ion detected y-coordinate (mm) */

  /*!local spherical polar coordinates on specimen apex (with radius given by R)*/
  double theta; /**< ion uncompressed polar angle (radians) */
  double psi; /**< ion compressed polar angle (radians) */
  double phi; /**< ion azimuthal (radians) */

  double sx; /**< ion reconstructed x-coordinate (nm) */
  double sy; /**< ion reconstructed y-coordinate (nm) */
  double sz; /**< ion reconstructed z-coordinate (nm) */

  double dv; /**< ionic volume (nm<SUP>3</SUP>) */

  char buffer[4]; /**< buffer for copying from and to binary files */

  double m2c; /**< calculated mass to charge ratio (Da) */
  double tof; /**< recorded time of flight (ns) */
  double dcVoltage; /**< DC evaporation voltage (kV) */
  double pulseVoltage; /**< Pulsed evaporation voltage (kV) */

  bool isRanged; /**<whether ion is ranged or not */
  std::vector<RangedPeak*> rangedPeaks; /**<vector of pointers to ranged peak profiles. */
  std::vector<double> rangedPeaksWeights; /**<vector of doubles representing the particular weights of the ranged peak profiles. */

  unsigned int pulsesSinceLastEventPulse; //a.u.
  unsigned int ionsPerPulse; //a.u.

  int clusterId;
  int pulseNumber;

  DetectionEvent();

};

/*!
* Class for the atom probe data sets.
*/
class APTDataSet{
public:
  std::vector<DetectionEvent> events; /**< Vector of Ion instances making up the dataset. */
  bool ranged; /**< Whether this ion has been ranged or not. */
  RangeData rData; /**< Ion range data object instance. */

  APTDataSet();

  void addDetectionEvent(DetectionEvent& event);

  void simpleRangeDataSet(RangeData& rData);

  void eposDataReader(std::string filename, unsigned long start = 0, unsigned long stop = -1);

  void atoDataReader(std::string filename, unsigned long start = 0, unsigned long stop = -1);

  void eposDataWriter(std::string filename);

  void atoDataWriter(std::string filename);

  void posDataWriter(std::string filename);

  void filterExperimentDataOutsideFOV(double detectorRadius,
                                      std::vector<ICFTempPoint>& icfVals,
                                      std::vector<ShankTempPoint>& shankVals,
                                      std::vector<VoltageTempPoint>& voltageVals);

  int writeData(std::string filename, std::string appendFileName);
  int readData(std::string filename, std::string appendFileName);
};

#endif
