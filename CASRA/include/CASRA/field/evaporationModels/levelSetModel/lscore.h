#ifndef LSCORE_H

#define LSCORE_H

#include <vector>
#include <stdexcept>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/field/scalarField.h"
#include "CASRA/specimenModels/numericalModel.h"
#include "CASRA/electrostatics/constantBEM.h"
#include "CASRA/thermalModels/laserModels.h"

class LevelSetDomain {

public:
  long xDim;
  long yDim;
  long zDim;

  double cellWidthX;
  double cellWidthY;
  double cellWidthZ;

  Point bottomLeftDomainCorner;
  bool lockBase;

  scalarField3D lSField;
  scalarField3D tField;
  scalarField3D vField;
  scalarField3D mField;

  scalarField3D dFieldX;
  scalarField3D dFieldY;
  scalarField3D dFieldZ;

  scalarField3D dFieldXm;
  scalarField3D dFieldYm;
  scalarField3D dFieldZm;

  scalarField3D dFieldXp;
  scalarField3D dFieldYp;
  scalarField3D dFieldZp;

  scalarField3D ddFieldX;
  scalarField3D ddFieldY;
  scalarField3D ddFieldZ;

  scalarField3D upwindBG;
  scalarField3D upwindFG;

  scalarField3D dPhi;

  //laserModel *lModel;

  double timestep;
  TriangularMesh currentSurfaceState;
  TriangularMesh previousSurfaceState;

  scalarField3D evapFieldGrid;
  scalarField3DInt phaseGrid;

  double initialGap = 0.0;

  int contours = -1;

  LevelSetDomain(int nDim, int yDim, int zDim,
                 Point bottomLeftDomainCorner,
                 double cellWidth = 1.0);

  void initialiseScalarField();
  TriangularMesh* extractSurface(std::vector<float>);
  void initialiseSDF(TriangularMesh&);
  void initialiseSDF(bool lockBase, int xDim, int yDim, int zDim);
  int sussmanReinitialisation(double atol = 1e-2, long its = 10000);
  double iterateField(TriangularMesh*, bool);
  void constructExtensionVelocity(TriangularMesh& surfaceMesh, std::vector<double>& sVel, int shank = 0);
  void constructExtensionVelocityAdaptive(TriangularMesh& surfaceMesh,  std::vector<double>& sVel, std::vector<Point>& decimatedPoints,
                                          std::vector<double>& decimatedVel,
                                          scalarField3D& field, double narrowband, int shank = 0);
  void assignToPhaseGrid(TriangularMesh& phase, int phaseIndex);
  void assignSpecimenModel(TMSpecimen spModel);
  void assignSpecimenModel(TMSpecimen spModel, bool lockBase, int xDim, int yDim, int zDim);
  std::vector<int> determineLocalPhase(TriangularMesh& surfaceMesh);
  std::vector<double> assignSurfaceEvaporationFields(TriangularMesh& surfaceMesh, std::vector<int>& pInd);
  std::vector<double> assignSurfaceEvaporationRates(TriangularMesh& surfaceMesh, std::vector<double>& surfaceField, std::vector<double>& T, std::vector<int>& pInd);
  double voltageRamping(TriangularMesh& surfaceMesh, std::vector<double>& surfaceField, std::vector<double>& T, std::vector<int>& pInd);

  void resizeLSField();

  TMSpecimen& getSpecimenModel(){
    return this->specimenModel;
  }

private:
  TMSpecimen specimenModel;
  void redefineFields();

};

class LSIterator{
  public:
    double maxT;
    double T;

    long it;
    long maxIt;
    LevelSetDomain* lSDomain;
    double cfl;

    LSIterator(LevelSetDomain* lSDomain, double cfl = 0.1);

};

class CurvatureIterator: public LSIterator{

  public:
    double A;

    CurvatureIterator(LevelSetDomain* lSDomain, double cfl = 0.1);
    CurvatureIterator(LevelSetDomain& lSDomain, double cfl = 0.1);
    std::tuple<int, TriangularMesh> iterate();
    void setTerminationConditions(double maxT = std::numeric_limits<double>::max(),
                                  long maxIt  = std::numeric_limits<long>::max());
    void initialiseParameterGrid();

};

class ElectrostaticConstantBEMIterator: public LSIterator{

  private:
    int heatingModelMode = -1;
    UniformTemperatureModel uTM;
    InstantaneousHeatingModel iHM;

  public:
    double A;
    double beta;
    electrostaticBemSolver *bemSolver;
    double maxApexHeight;

    double minSpecimenLength;
    double shankAngle = 0.0;
    bool adaptive;

    ElectrostaticConstantBEMIterator(LevelSetDomain* lSDomain, double cfl = 0.1, double etol = 1e-3, double minSpecimenLength = std::numeric_limits<double>::min(), bool extendShank = false, double shankAngle = 0.0);
    ElectrostaticConstantBEMIterator(LevelSetDomain& lSDomain, double cfl = 0.1, double etol = 1e-3, double minSpecimenLength = std::numeric_limits<double>::min(), bool extendShank = false, double shankAngle = 0.0);
    ~ElectrostaticConstantBEMIterator();
    std::tuple<int, TriangularMesh, int> iterate();
    void setTerminationConditions(double maxT = std::numeric_limits<double>::max(),
                                  long maxIt  = std::numeric_limits<long>::max(),
                                  double maxApexHeight = std::numeric_limits<long>::min());
    std::vector<double> initialSolutionGuess(TriangularMesh& newMesh, TriangularMesh& oldMesh, std::vector<double>& x);

    void shankExtension(TriangularMesh& surfaceMesh);
    double adaptiveRemeshingStep(TriangularMesh& previousSurfaceState,
                                 double maxDepthStep,
                                 double minDepthStep,
                                 double currentMaxHeight);

    void setThermalModel(UniformTemperatureModel uTM);
    void setThermalModel(InstantaneousHeatingModel iHM);

};

#endif
