#ifndef TESTFIELDS_H

#define TESTFIELDS_H

#include "CASRA/field/scalarField.h"

scalarField3D buildSphericalField(int x, int y, int z, double Rc);
scalarField3D buildRectangleSignField(int x, int y, int z);

#endif
