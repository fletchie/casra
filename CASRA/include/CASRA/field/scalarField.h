#ifndef SCALARFIELD_H

#define SCALARFIELD_H

#include <vector>

#include "CASRA/mesh/triangularMesh.h"

using scalarField4DInt = std::vector<std::vector<std::vector<std::vector<int>>>>;
using scalarField3DInt = std::vector<std::vector<std::vector<int>>>;
using scalarField2DInt = std::vector<std::vector<int>>;
using scalarField1DInt = std::vector<int>;

using scalarField4D = std::vector<std::vector<std::vector<std::vector<double>>>>;
using scalarField3D = std::vector<std::vector<std::vector<double>>>;
using scalarField2D = std::vector<std::vector<double>>;
using scalarField1D = std::vector<double>;

scalarField3D operator+(scalarField3D&& a, double b);
scalarField3D operator-(scalarField3D&& a, double b);
scalarField3D operator+(scalarField3D&& a, scalarField3D&& b);
scalarField3D operator-(scalarField3D&& a, scalarField3D&& b);
scalarField3D operator+(scalarField3D& a, scalarField3D& b);
scalarField3D operator-(scalarField3D& a, scalarField3D& b);
void operator+=(scalarField3D& a, scalarField3D&& b);
void operator-=(scalarField3D& a, scalarField3D&& b);
void operator+=(scalarField3D& a, scalarField3D& b);
void operator-=(scalarField3D& a, scalarField3D& b);
void operator+=(scalarField3D& a, double b);
void operator-=(scalarField3D& a, double b);

inline scalarField3D operator*(scalarField3D& a, scalarField3D& b) {
  scalarField3D field = a;
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }
  const int xDim = a.size();
  const int yDim = a[0].size();
  const int zDim = a[0][0].size();
  for (int i=0;i<xDim;i++){
    for (int j=0;j<yDim;j++){
      for (int k=0;k<zDim;k++){
        field[i][j][k] = a[i][j][k] * b[i][j][k];
      }
    }
  }
  return field;
}

scalarField3D operator*(scalarField3D&& a, scalarField3D&& b);
scalarField3D operator*(scalarField3D& a, scalarField3D&& b);
scalarField3D operator*(scalarField3D&& a, double b);
scalarField3D operator*(scalarField3D& a, double b);
void operator*=(scalarField3D& a, scalarField3D& b);
void operator*=(scalarField3D& a, scalarField3D&& b);
void operator*=(scalarField3D& a, double b);
scalarField3D operator>(scalarField3D a, double value);
scalarField3D operator<(scalarField3D a, double value);
scalarField3D operator/(scalarField3D a, scalarField3D b);
scalarField3D operator/(scalarField3D a, double b);
void operator/=(scalarField3D& a, scalarField3D b);
void operator/=(scalarField3D& a, double b);

inline void setField(scalarField3D& field, double value){
  const int fsizex = field.size();
  const int fsizey = field[0].size();
  const int fsizez = field[0][0].size();
  for (int i=0;i<fsizex;i++){
    for (int j=0;j<fsizey;j++){
      for (int k=0;k<fsizez;k++){
        field[i][j][k] = value;
      }
    }
  }

  return;
}

//2D field functions defined here
scalarField2D initialiseField(int xDim, int yDim, double value);

//3D field functions defined here
scalarField3D initialiseField(int xDim, int yDim, int zDim, double value);
scalarField4D initialiseField(int xDim, int yDim, int zDim, int kDim, double value);

scalarField3DInt initialiseFieldInt(int xDim, int yDim, int zDim, int value);
scalarField4DInt initialiseFieldInt(int xDim, int yDim, int zDim, int kDim, int value);

scalarField3D cornerInterpolation(scalarField3D& field1);
void initialiseSDF(scalarField3D& field, TriangularMesh* boundaryMesh, Point bottomLeftDomainCorner, double cellWidth);

void maximum(scalarField3D& out, scalarField3D& field1, scalarField3D& field2);
void minimum(scalarField3D& out, scalarField3D& field1, scalarField3D& field2);

void maximum(scalarField3D& out, scalarField3D& field1, double val);
void minimum(scalarField3D& out, scalarField3D& field1, double val);

scalarField3D maximum(scalarField3D field1, scalarField3D field2);
scalarField3D minimum(scalarField3D field1, scalarField3D field2);
scalarField3D maximum(scalarField3D field1, double value2);
scalarField3D minimum(scalarField3D field1, double value2);
double sum(scalarField3D field);

void multiplyField(scalarField3D& field1, scalarField3D& field2, scalarField3D& nField);
void multiplyField(scalarField3D& field1, double factor, scalarField3D& nField);
void posField(scalarField3D& field, scalarField3D& nField);
void negField(scalarField3D& field, scalarField3D& nField);
void absField(scalarField3D& field, scalarField3D& nField);
scalarField3D absField(scalarField3D& field);

void addField(scalarField3D& field, scalarField3D& vField, scalarField3D& nField, double nFactor = 1.0);
void addField(scalarField3D& field, double constant, scalarField3D& nField);
double max(scalarField3D& field);
double min(scalarField3D& field);
void dotField(scalarField3D& dFieldX, scalarField3D& dFieldY, scalarField3D& dFieldZ, scalarField3D& tField);
void sqrtField(scalarField3D& field, scalarField3D& nField);
void divideField(scalarField3D& field, scalarField3D& dField, scalarField3D& nField);
void centralDifferenceField(scalarField3D& field, scalarField3D& dFieldX, scalarField3D& dFieldY, scalarField3D& dFieldZ, double hx, double hy, double hz);
void smoothField(scalarField3D& field, scalarField3D& nField, double weighting = 1.0/7.0, bool disableZeros = false);
void copyField(scalarField3D& field, scalarField3D& nField);

void forwardDifferenceField(scalarField3D& field, scalarField3D& dFieldX, scalarField3D& dFieldY, scalarField3D& dFieldZ, double hx, double hy, double hz);
void backwardDifferenceField(scalarField3D& field, scalarField3D& dFieldX, scalarField3D& dFieldY, scalarField3D& dFieldZ, double hx, double hy, double hz);
void upwindGradient(scalarField3D& dFieldXm, scalarField3D& dFieldYm, scalarField3D& dFieldZm, scalarField3D& dFieldXp, scalarField3D& dFieldYp, scalarField3D& dFieldZp, scalarField3D& upwindBG, scalarField3D& upwindFG);
void upwindDPhi(scalarField3D& vField, scalarField3D& upwindBG, scalarField3D& upwindFG, scalarField3D& dPhi);

scalarField3D pow(scalarField3D field, double power);

inline void maximum(scalarField3D& out, scalarField3D& field1, scalarField3D& field2){
  const int fsizex = field1.size();
  const int fsizey = field1[0].size();
  const int fsizez = field1[0][0].size();
  for (int i=0;i<fsizex;i++){
    for (int j=0;j<fsizey;j++){
      for (int k=0;k<fsizez;k++){
        out[i][j][k] = field1[i][j][k];
        if (field2[i][j][k] > field1[i][j][k]){
          out[i][j][k] = field2[i][j][k];
        }
      }
    }
  }
  return;
}

inline void minimum(scalarField3D& out, scalarField3D& field1, scalarField3D& field2){
  const int fsizex = field1.size();
  const int fsizey = field1[0].size();
  const int fsizez = field1[0][0].size();
  for (int i=0;i<fsizex;i++){
    for (int j=0;j<fsizey;j++){
      for (int k=0;k<fsizez;k++){
        out[i][j][k] = field1[i][j][k];
        if (field2[i][j][k] < field1[i][j][k]){
          out[i][j][k] = field2[i][j][k];
        }
      }
    }
  }
  return;
}

inline scalarField3D rmaximum(scalarField3D&& field1, scalarField3D&& field2){
  scalarField3D out = field1;
  const int fsizex = field1.size();
  const int fsizey = field1[0].size();
  const int fsizez = field1[0][0].size();
  for (int i=0;i<fsizex;i++){
    for (int j=0;j<fsizey;j++){
      for (int k=0;k<fsizez;k++){
        if (field2[i][j][k] > field1[i][j][k]){
          out[i][j][k] = field2[i][j][k];
        }
      }
    }
  }
  return out;
}

inline scalarField3D rminimum(scalarField3D&& field1, scalarField3D&& field2){
  scalarField3D out = field1;
  const int fsizex = field1.size();
  const int fsizey = field1[0].size();
  const int fsizez = field1[0][0].size();
  for (int i=0;i<fsizex;i++){
    for (int j=0;j<fsizey;j++){
      for (int k=0;k<fsizez;k++){
        if (field2[i][j][k] < field1[i][j][k]){
          out[i][j][k] = field2[i][j][k];
        }
      }
    }
  }
  return out;
}

inline void maximum(scalarField3D& out, scalarField3D& field1, double val){
  for (int i=0;i<field1.size();i++){
    for (int j=0;j<field1[0].size();j++){
      for (int k=0;k<field1[0][0].size();k++){
        out[i][j][k] = field1[i][j][k];
        if (val > field1[i][j][k]){
          out[i][j][k] = val;
        }
      }
    }
  }
  return;
}

inline void minimum(scalarField3D& out, scalarField3D& field1, double val){
  for (int i=0;i<field1.size();i++){
    for (int j=0;j<field1[0].size();j++){
      for (int k=0;k<field1[0][0].size();k++){
        out[i][j][k] = field1[i][j][k];
        if (val < field1[i][j][k]){
          out[i][j][k] = val;
        }
      }
    }
  }
  return;
}


#endif
