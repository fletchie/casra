#ifndef SIMULATEEVAPORATION_H

#define SIMULATEEVAPORATION_H

#include <vector>
#include <string>

#include "CASRA/evaporationModels/initialiser.h"

int simulateEvaporation(ModelParams modelParams, std::string writeDir);

#endif
