#ifndef ANALYTICAL_MODEL_H

#define ANALYTICAL_MODEL_H

#include <vector>
#include <string>

#include "CASRA/experimentalData/experimentalData.h"
#include "CASRA/base/point.h"

class AnalyticDetector{
  public:
    double efficiency = 1.0;
    double flightpath = 0.102;
    double radius = 0.038;

    AnalyticDetector(double, double, double);

};

class EmitterModel{

  public:
    double evapVol;

  EmitterModel();

};

class AnalyticEmitterModel : public EmitterModel{

  public:
    double R0;
    double R;
    Point apexCentre = Point(0.0, 0.0, 0.0);

  AnalyticEmitterModel(double initRad, Point initialPosition);
  void reconPlacement(DetectionEvent& rIon);

};


class BasEmitterModel : public AnalyticEmitterModel{

public:
  std::vector<ICFTempPoint> imagecompression; /**< vector of image compression timepoint object instances */
  long unsigned ICFIt; /**< The currently active image compression value in imagecompression */
  double ICF; /**< The currently active image compression value (a.u.) */
  double thetaMax; /**< The instrument half-angular FOV (radians)*/
  double dz; /**< The apex z increment (nm)*/

  BasEmitterModel(std::vector<ICFTempPoint> imageCompressionFactor, double initRad, Point initialPosition);
  void detectorToCapTransfer(DetectionEvent& recIon, AnalyticDetector& detector, long unsigned rNum);
  void capShift(DetectionEvent& recIon, AnalyticDetector& detector);
  void calculateFOV(AnalyticDetector& detector);

};

/*!
* Class for the Bas emitter model constrained by a piecewise constant shank angle.
*/
class ShankBasEmitterModel : public BasEmitterModel {

  public:
    std::vector<ShankTempPoint> shank; /**< vector of shank time point object instances for defining the piecewise shank angle. */
    unsigned long shankIt; /**> unsigned integer-type for selecting the active shank time point object instance. */
    double alpha; /**< The active emitter shank angle (radians) */

  ShankBasEmitterModel(std::vector<ICFTempPoint> imageCompressionFactor,
                       double initRad, Point initialPosition,
                       std::vector<ShankTempPoint> sangles);

  void evolveRadius(DetectionEvent&, long unsigned);

};

/*!
* Class for the Bas emitter model constrained by the voltage curve (via a piecewise constant k-factor).
*/
class VoltageBasEmitterModel : public BasEmitterModel {

  public:
    std::vector<VoltageTempPoint> kFactors; /**< vector of k-factor time point object instances for defining the piecewise k-factor. */
    unsigned long kFactorIt; /**> unsigned integer-type for selecting the active k-factor time point object instance. */
    double kFactor; /**> The active emitter k-factor (a.u.). */
    double bulkEvapField; /**> The bulk evaporation field for determining the radius (Vnm<SUP>-1</SUP>) */

  VoltageBasEmitterModel(std::vector<ICFTempPoint> imageCompressionFactor,
                         double initRad, Point initialPosition,
                         std::vector<VoltageTempPoint> kFactors,
                         double bulkEvapField);

  void setInitialRadius(DetectionEvent& rIon);
  void evolveRadius(DetectionEvent&, long unsigned);

};


#endif
