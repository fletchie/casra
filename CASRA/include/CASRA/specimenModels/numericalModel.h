#ifndef NUMERICAL_MODEL_H

#define NUMERICAL_MODEL_H

#include <vector>
#include <string>
#include <memory>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/evaporationLaws/evaporationLaws.h"

class Phase{

  public:
    std::string name; /*!< phase  name */
    TriangularMesh phaseBoundary; /*!< phase boundary */
    int setEvapLawMode = 0;
    bool ghost = false; /*!< whether ghost phase or not (ghost phases are invisible to evaporation physics)*/

    AhhreniusLawIsotropic ALI; /*!< Ahhrenius law attributes */
    DeformedAhhreniusLawIsotropic DALI; /*!< Deformed Ahhrenius law attributes */
    LinearLawIsotropic LLI; /*!< Linear law attributes */
    CurvatureLawIsotropic CLI; /*!< Linear law attributes */

    std::vector<std::string> elements;
    std::vector<double> elementRatios;
    std::vector<double> cumulativeElementRatios;
    std::vector<double> elementIonicVolumes;

    std::map<std::string, std::string> substituteElements; /*!< ghost phase only - for substituting underlying non-ghost phase elements*/
    std::map<std::string, double> substituteRatios; /*!< ghost phase only - for substituting underlying non-ghost phase element ratios*/
    std::map<std::string, double> substituteIonicVolumes; /*!< ghost phase only - for substituting underlying non-ghost phase ionic volumes*/

    double cumulativeMax = 0.0;
    std::string fileLocation = "";

    Phase(){

    };

    Phase(AhhreniusLawIsotropic law, std::string name = "phase");
    Phase(LinearLawIsotropic law, std::string name = "phase");
    Phase(DeformedAhhreniusLawIsotropic law, std::string name = "phase");
    Phase(CurvatureLawIsotropic law, std::string name = "phase");
    void addPhaseBoundary(TriangularMesh phaseBoundary);
};

class TMSpecimen{

  public:
    std::vector<Phase> phases; /*!< list of phases */
    double minSpecimenLength = 0.0; /*!< minimum specimen length to extend the shank to*/

    void appendPhase(Phase);
    void appendPhase(TriangularMesh surfaceMesh);
    double localEvapField(Point p);

};


#endif
