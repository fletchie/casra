#ifndef VACUUMFIELDSOLVER_H

#define VACUUMFIELDSOLVER_H

#include <string>
#include <tuple>

std::tuple<double, double, int> extractSpatialGridRange(std::string range);

#endif
