#ifndef GEOMETRIES_H
#define GEOMETRIES_H

#include <tuple>

#include "CASRA/mesh/triangularMesh.h"

std::tuple<TriangularMesh, int> buildTipMesh(double R0, double R1, double length, double offset = 0.0);
std::tuple<TriangularMesh, int> buildSphere(double R, Point cp, int n = 20);
std::tuple<TriangularMesh, int> buildRectangularMesh(Point P1, Point P2, Point P3, Point P4,
                                                     Point P5, Point P6, Point P7, Point P8);
std::tuple<TriangularMesh, int> buildUpperShell(double outerRadius, double innerRadius, Point cp, int n);

#endif
