#ifndef SOLVERPARSER_H

#define SOLVERPARSER_H

#include <vector>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/field/scalarField.h"
#include "CASRA/electrostatics/constantBEM.h"

//void solveFieldl(matrix& LHS, std::vector<double>& RHS, std::vector<double>& sol);
int solveField(matrix& LHS, std::vector<double>& RHS, std::vector<double>& sol, double*& Amat, int& AmatDimSize);
//void solveFieldNL(matrix& mat, std::vector<double>& rvec, std::vector<double>& sol);
//void solveFieldEigen(matrix& mat, std::vector<double>& rvec, std::vector<double>& sol);
//void solveFieldEigen2(matrix& mat, std::vector<double>& rvec, std::vector<double>& sol);

#endif
