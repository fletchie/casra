#ifndef CONSTANTBEM_H

#define CONSTANTBEM_H
#include <math.h>
#include <memory>
#include <tuple>
#include <iostream>

#include "CASRA/base/vectorMath.h"
#include "CASRA/mesh/triangularMesh.h"

const double t = 1.0/3.0;
const double s = (1.0 - pow(15.0, 0.5))/7.0;
const double r = (1.0 + pow(15.0, 0.5))/7.0;
const double w1 = 9.0/40.0;
const double w2 = (155.0 + pow(15, 0.5))/1200.0;
const double w3 = (155.0 - pow(15, 0.5))/1200.0;
const double int1 = t + 2.0 * t * s;
const double int2 = t - t * s;
const double int3 = t + 2.0 * t * r;
const double int4 = t - t * r;

class electrostaticBemSolver{

  public:
    double tol;

    matrix Fmat; //2D matrices
    matrix Pmat; //2D matrices
    matrix preConditioner; //2D matrices
    std::vector<double> u;
    std::vector<double> q;
    std::vector<double> avq;
    std::vector<double> previousq;
    std::vector<double> lhs;
    std::vector<double> rhs;

    int AmatDimSize = 0;
    double* Amat = nullptr;

    electrostaticBemSolver(double tol = 0.2);
    void calculateElectrostaticMatrix(TriangularMesh& surfaceMesh);
    double analyticPDiag(Point& cx, Point& ya, Point& yb, Point& yc, double& area);
    double greensFunction(Point& cx, Point& cy);
    double normGreensFunction(Point& cx, Point& cy, Point& normal);
    double vecGradGreensFunction(Point& cx, Point& cy, double& top);

    double numPij(Point& cx, Point& ya, Point& yb, Point& yc, double& area);
    double numFij(Point& cx, Point& ya, Point& yb, Point& yc, double& area, Point& normal);
    void matij(double& Pij, double& Fij, Point& cx, Point& ya, Point& yb,
               Point& yc, double& area, Point& normal, Point& b,
               double& Pij1, double& Pij2, double& Fij1, double& Fij2);

    double numKijx(Point& cx, Point& ya, Point& yb, Point& yc, double& area);
    double numKijy(Point& cx, Point& ya, Point& yb, Point& yc, double& area);
    double numKijz(Point& cx, Point& ya, Point& yb, Point& yc, double& area);
    inline void numKij(double& Kijx, double& Kijy, double& Kijz, Point& cx,
                       Point& ya, Point& yb, Point& yc, double& area);

    ~electrostaticBemSolver() {
        std::cout << "cleaning up electrostatic solver" << std::endl;
        delete[] Amat;
    }

    inline void matrixVectorProduct(std::vector<double>& rhs,
    			             matrix& mat, std::vector<double>& vec) {

        const int matSize = mat.size();
        rhs.resize(matSize);
        #if defined(_OPENMP)
        #pragma omp parallel for
        #endif
        for (int i = 0; i < matSize; i++) {
            rhs[i] = 0.0;
            for (int j = 0; j < matSize; j++) {
                rhs[i] += mat[i][j] * vec[j];
            }
        }
    }

};

/*!
 * Defines Green function and Green function gradients for the electrostatic problem
 */
inline double electrostaticBemSolver::greensFunction(Point& cx, Point& cy){
  double vcx = cx.px - cy.px;
  double vcy = cx.py - cy.py;
  double vcz = cx.pz - cy.pz;
  return 0.15915494309189535 * 1.0/std::sqrt(vcx*vcx + vcy*vcy + vcz*vcz);
}

inline double electrostaticBemSolver::normGreensFunction(Point& cx, Point& cy, Point& normal){
    double vcx = cx.px - cy.px;
    double vcy = cx.py - cy.py;
    double vcz = cx.pz - cy.pz;
    double mag = vcx*vcx + vcy*vcy + vcz*vcz;
    mag = mag * mag * mag;
    return -0.07957747154594767 * (vcx * normal.px + vcy * normal.py + vcz * normal.pz) * 1.0/ std::sqrt(mag);
}

inline double electrostaticBemSolver::vecGradGreensFunction(Point& cx, Point& cy, double& top){
    double dx = cx.px - cy.px;
    double dy = cx.py - cy.py;
    double dz = cx.pz - cy.pz;
    double dist2 = dx*dx + dy*dy + dz*dz;
    return top/std::sqrt(dist2 * dist2 * dist2);
}

class OctreeNode{

  public:
    double minCoords[3];
    double maxCoords[3];
    double centre[3] = {0.0, 0.0, 0.0};

    double radius;
    OctreeNode *parent; //top node indicated by nullPtr

    bool leaf;
    int level;

    std::vector<std::unique_ptr<OctreeNode>> children;

    std::vector<long> memberFaces;

    double zerothMomentK;
    double firstMomentK[3];

    TriangularMesh* surfaceMesh;
    electrostaticBemSolver* bemSolver;

    OctreeNode(double minCoords[3], double maxCoords[3],
               TriangularMesh* surfaceMesh,
               electrostaticBemSolver* bemSolver,
               OctreeNode *parent = nullptr);

    OctreeNode(double minCoords[3], double maxCoords[3],
              TriangularMesh& surfaceMesh,
              electrostaticBemSolver& bemSolver,
              OctreeNode *parent = nullptr);

    void inherit();
    void clusterParameters();
    void calculateMoments();
    double calculateZerothMomentK();
    void splitOctreeNode();
    void buildBoundingMesh(TriangularMesh& octreeMesh);
    void calculateField(double& v1, double& v2, double& v3, Point& evalPoint, const double& admissTol);
    void directClusterPanelFieldEvaluation(double& v1, double& v2, double& v3, Point& evalPoint);
};

#endif
