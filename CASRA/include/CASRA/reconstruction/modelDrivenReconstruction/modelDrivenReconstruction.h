#ifndef MODELDRIVENRECONSTRUCTION_H

#define MODELDRIVENRECONSTRUCTION_H

#include <vector>
#include <map>
#include <iostream>
#include <cstring>

#include "CASRA/base/point.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/experimentalData/experimentalData.h"
#include "CASRA/field/scalarField.h"
#include "CASRA/mesh/triangularMesh.h"

#define SCALEFACTOR 1.2

/*!
 * Class definition for the trajectory mapping
 */
class TrajectoryMapping{

  public:
    std::vector<Point> detectorSpacePositionsC1; /*!< vector of panel corner 1 launch positions on the specimen surface */
    std::vector<Point> detectorSpacePositionsC2; /*!< vector of panel corner 2 launch positions on the specimen surface */
    std::vector<Point> detectorSpacePositionsC3; /*!< vector of panel corner 3 launch positions on the specimen surface */

    std::vector<Point> sampleSpacePositionsC1; /*!< vector of panel corner 1 impact positions on the detector */
    std::vector<Point> sampleSpacePositionsC2; /*!< vector of panel corner 2 impact positions on the detector */
    std::vector<Point> sampleSpacePositionsC3; /*!< vector of panel corner 3 impact positions on the detector */

    std::vector<std::string> phaseC1; /*!< vector of panel corner 1 identified model phase (determined from launch position) */
    std::vector<std::string> phaseC2; /*!< vector of panel corner 2 identified model phase (determined from launch position) */
    std::vector<std::string> phaseC3; /*!< vector of panel corner 3 identified model phase (determined from launch position) */

    std::vector<double> evapRates; /*!< vector of panel local surface evaporation rates */
    std::vector<int> iterations; /*!< vector of iterations at which panels' projection is performed */

    TrajectoryMapping(TsvObject&);
    void setMapping(TsvObject&);

};

class landmarkParams{
  public:
    long experimentLandmark;
    std::string modelLandmark;
    int modelIt = -1;
    double landmarkIonicVolume = 0.0;
    double landmarkEvapVolume = 0.0;

  landmarkParams(long experimentLandmark, std::string modelLandmark);
  landmarkParams(long experimentLandmark, std::string modelLandmark, int modelIt);
};


std::map<int, double> determineEvapVolumeDepths(TrajectoryMapping& trajectoryMap,
                                                double detectorRadius,
                                                double detectorEfficiency);

std::vector<landmarkParams> determineModelLandmarkPositions(TrajectoryMapping& trajectoryMap,
                                                            std::vector<landmarkParams>& reconLandmarks,
                                                            double detectorRadius);

void filterExperimentDataOutsideFOV(std::vector<landmarkParams>& reconLandmarks, APTDataSet& dataset, double detectorRadius);
void determineLandmarkIonicVolumes(std::vector<landmarkParams>& reconLandmarks, std::map<int, double>& modelEvapVolumes);
std::vector<double> assignIonDepthPositions(APTDataSet& dataset, std::vector<landmarkParams>& reconLandmarks);

std::tuple<scalarField3D, scalarField3D, scalarField3D> constructDetectorMappingStack(TrajectoryMapping& trajectoryMap,
                                                                                      const int startIt,
                                                                                      const int finalIt,
                                                                                      const double detectorRadius,
                                                                                      const int xDim, const int yDim);

void modelDrivenReconstructionProtocol(APTDataSet& detectorEventData, std::vector<landmarkParams>& reconLandmarks,
                                       scalarField3D& mappingX, scalarField3D& mappingY, scalarField3D& mappingZ,
                                       std::map<int, double>& evapVolumes, const int xDim, const int yDim, const double detectorRadius);

#endif
