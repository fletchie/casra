#ifndef POINTPROJECTIONMODEL_H

#define POINTPROJECTIONMODEL_H

#include <vector>
#include <iostream>
#include <cstring>

#include "CASRA/specimenModels/analyticModel.h"

void dataReconstruction(APTDataSet& dataset, ShankBasEmitterModel& emitter, AnalyticDetector& detector);
void dataReconstruction(APTDataSet& dataset, VoltageBasEmitterModel& emitter, AnalyticDetector& detector);

#endif
