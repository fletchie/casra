#ifndef RECONSTRUCTION_INITIALISER_H

#define RECONSTRUCTION_INITIALISER_H

#include <vector>
#include <string>
#include <map>

#if __has_include(<filesystem>)
  #include <filesystem>
#elif __has_include(<experimental/filesystem>)
  #include <experimental/filesystem>
  namespace std {
      namespace filesystem = experimental::filesystem;
  }
#endif

#include "CASRA/base/point.h"
#include "CASRA/experimentalData/experimentalData.h"
#include "CASRA/reconstruction/modelDrivenReconstruction/modelDrivenReconstruction.h"

extern const char* supportedXMLConfigFileExt[];

class analyticSpecimenModel {
  public:
    //define specimen model parameters
    std::string specimenType;
    double R0 = 25e-9;
    double initialHeight = 0.0;
    std::vector<ShankTempPoint> shank;
    std::vector<VoltageTempPoint> kFactors;
    Point initialPosition = Point(0.0, 0.0, 0.0);
    double evapField = 20.0e9;

};

class InstrumentModel {
  //define instrument model parameters
  public:
    std::string instrumentType;
    double detectorRadius;
    double flightpath;
    double efficiency;
};

class ProjectionModel {
public:
  //define projection parameters
  std::string projectionType;
  std::vector<ICFTempPoint> imagecompression;
};

class pointProjectionReconstructionParams {
  public:
    analyticSpecimenModel sModel;
    InstrumentModel iModel;
    ProjectionModel pModel;

    bool specimenOpen = false;
    bool instrumentOpen = false;
    bool projectionOpen = false;

  pointProjectionReconstructionParams();
};

class trajectoryMappingParams{

  public:
      std::string location;

};

class modelDrivenReconstructionParams {
  public:
    trajectoryMappingParams tMapping;
    std::vector<landmarkParams> landmarks;
    InstrumentModel iModel;

  modelDrivenReconstructionParams();
};

pointProjectionReconstructionParams pointProjectionReconstructionInitialiser(std::string cfname);
modelDrivenReconstructionParams modelDrivenReconstructionInitialiser(std::string cfname);

std::string reconType(std::string cfname);

#endif
