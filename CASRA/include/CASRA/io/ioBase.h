#ifndef IOBASE_H

#define IOBASE_H

#include <vector>
#include <cstring>
#include <algorithm>
#include <iostream>

#if __has_include(<filesystem>)
  #include <filesystem>
#elif __has_include(<experimental/filesystem>)
  #include <experimental/filesystem>
  namespace std {
      namespace filesystem = experimental::filesystem;
  }
#endif

template<typename T>
void bigEndianBufferPull(T *nloc, char *dataLoc, int size) {

  //copy binary to memory
  char *buffer = new char[size];
  memcpy(buffer, dataLoc, size);
  std::reverse(buffer, buffer + size); //reverse endianness (file should be big endian)
  memcpy(nloc, buffer, size);

  delete[] buffer;

}

template<typename T>
void smallEndianBufferPull(T *nloc, char *dataLoc, int size) {

  //copy binary to memory
  memcpy(nloc, dataLoc, size);

}

/*!
 * Add short delay to ensure directory creation
 */
void delay();
int createDirectoryTree(std::string filename);

#endif
