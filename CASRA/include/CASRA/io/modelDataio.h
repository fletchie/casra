#ifndef MODELDATAIO_H

#define MODELDATAIO_H

#include <vector>
#include <map>
#include <string>

#if __has_include(<filesystem>)
  #include <filesystem>
#elif __has_include(<experimental/filesystem>)
  #include <experimental/filesystem>
  namespace std {
      namespace filesystem = experimental::filesystem;
  }
#endif

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/pointData.h"
#include "CASRA/field/scalarField.h"
#include "CASRA/base/point.h"

class PointData {
  public:
    std::string key;
    std::vector<double> values;
};

class CellData {
  public:
    std::string key;
    std::vector<std::vector<double>> values;
    void setValues(std::vector<double> values, std::string keyName);
};

class vtkObject{
  public:
    std::vector<std::vector<double>> vertices;
    std::vector<std::vector<long>> cells;
    std::vector<PointData> pointData;
    std::vector<CellData> cellData;

    int setWithField(scalarField3D& field, Point bottomLeftDomainCorner, double cellWidthX, double cellWidthY, double cellWidthZ);
    int setWithField(scalarField3D& field, Point bottomLeftDomainCorner = Point(0.0, 0.0, 0.0), double cellWidth = 1.0);
    int setWithField(scalarField3DInt& field, Point bottomLeftDomainCorner = Point(0.0, 0.0, 0.0), double cellWidth = 1.0);
    int setWithField(scalarField4D& field, Point bottomLeftDomainCorner, double cellWidthX, double cellWidthY, double cellWidthZ);

    int setWithTriangularMesh(TriangularMesh& mesh);
    int setWithPointData(PointDataset& mesh);

    TriangularMesh triangularMeshFromVtk();
    std::tuple<std::vector<std::vector<Point>>, std::vector<std::vector<Point>>, std::vector<long>, std::vector<int>> lineSegmentMeshFromVTK();
    int addTrajectory(std::vector<Point>& trajectory);
    int addTrajectory(std::vector<Point>& trajectory, std::vector<Point>& velocities, long launchSite, int launchPhase);
    int addTrajectory(std::vector<Point>& trajectory, std::vector<Point>& velocities, long launchSite, double evapRate);
};

class TsvObject{
  public:

    std::vector<std::string> columnOrder;
    std::map<int, std::vector<double>> outDoubles;
    std::map<int, std::vector<std::string>> outStrings;
    std::map<int, std::vector<int>> outInts;

    int writeTsv(std::string outFile, bool header = true);
    int readHeaderedTSV(std::string inputFile);

};

void delay();
//void writeHDFTest(const char *filename, scalarField3D& field);
//scalarField1D flattenField(scalarField3D& field);

int writeVtk(vtkObject out, std::string filename);
int readVTKUnstructuredGrid(vtkObject& in, std::string filename);
int writeVtkSeries(std::string filename, std::vector<std::string> names, std::vector<std::string> times);

#endif
