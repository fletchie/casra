#ifndef APTWRITERS_H

#define APTWRITERS_H

#include <vector>
#include <cstring>
#include "CASRA/experimentalData/experimentalData.h"

extern const char* supportedAPTOutputFileExt[];

template<typename T>
void bigEndianBufferPush(T *dataLoc, char *buffer, int size) {

  //copy binary to memory
  memcpy(buffer, dataLoc, size);
  std::reverse(buffer, buffer + size); //reverse endianness (file should be big endian)

}

template<typename T>
void smallEndianBufferPush(T *dataLoc, char *buffer, int size) {

  //copy binary to memory
  memcpy(buffer, dataLoc, size);

}

#endif
