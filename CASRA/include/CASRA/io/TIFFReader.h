#ifndef TIFFREADER_H

#define TIFFREADER_H

#include <vector>
#include <cstring>

std::vector<std::vector<std::vector<std::vector<double>>>> readTiff(std::string voxelFieldFile);

#endif
