#ifndef EXTRACTTOMOPHASES_H

#define EXTRACTTOMOPHASES_H

#include <vector>
#include <string>
#include <cstring>

#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_utils.hpp"
#include "rapidxml/rapidxml_print.hpp"
#include "CASRA/mesh/triangularMesh.h"

int runExtractorXML(std::string xmlFile);
int runPhaseExtractor(std::string iConfigFile, std::string oConfigFile);
TriangularMesh extractPhase(rapidxml::xml_node<> *buildNode, rapidxml::xml_document<>& xmlDoc, bool modifyXML = false, std::string phasename = "");
#endif
