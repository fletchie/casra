#ifndef TIPBUILD_H

#define TIPBUILD_H

#include <vector>
#include <string>

#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_utils.hpp"
#include "rapidxml/rapidxml_print.hpp"

#include "CASRA/mesh/triangularMesh.h"

int tipBuild(std::string iConfigFile, std::string oConfigFile);
int writeToConfig(rapidxml::xml_document<>* doc, rapidxml::xml_node<>* specimenPhaseMeshNode, TriangularMesh tipMesh);
TriangularMesh meshbuild(rapidxml::xml_node<> *specimenPhaseMeshNode, rapidxml::xml_document<>& xmlDoc, bool modifyXML = false);

#endif
