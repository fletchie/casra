Library authors: See original repository link below

[Original library source](https://github.com/akuukka/quickhull)

original source: 

This implementation is 100% Public Domain.

Feel free to use.
 
C++11 is needed to compile it.
