//  mgmres.hpp
// https://people.sc.fsu.edu/~jburkardt/cpp_src/mgmres/mgmres.html
//  Thanks to Philip Sakievich for correcting mismatches between this HPP
//  file and the corresponding CPP file, 23 March 2016!
//
inline void mult_givens ( double c, double s, int k, double g[] );
inline double r8vec_dot ( int n, double a1[], double a2[] );
void ax_mult ( int n, double a[], double x[],
  double w[] );
int gmres ( int n, double a[], double x[],
  double rhs[], int itMax, int itRestar, double absTol, double relTol );
