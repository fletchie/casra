//****************************************************************************
// The original code by Lili Ju and John Burkardt has been modified to support
// OpenMP parallelisation, dense matrix format, and BLAS math library linkage.

// This modified version remains under the GNU LGPL license.
// Author: 2021 Charles Fletcher
//****************************************************************************


# include <cmath>
# include <cstdlib>
# include <ctime>
# include <fstream>
# include <iomanip>
# include <iostream>

#if defined(_OPENMP)
  #include <omp.h>
#endif


#if BLAS_FOUND == 1
extern "C"
{
    #include MATH_HEADER
}
#endif

using namespace std;

# include "mgmres/mgmres.h"

void ax_mult ( int n, double a[], double x[],
  double w[] )

//****************************************************************************80
//
//  Purpose:
//
//    AX_ST computes A*x for a matrix stored in sparse triplet form.
//
//  Discussion:
//
//    The matrix A is assumed to be sparse.  To save on storage, only
//    the nonzero entries of A are stored.  For instance, the K-th nonzero
//    entry in the matrix is stored by:
//
//      A(K) = value of entry,
//      IA(K) = row of entry,
//      JA(K) = column of entry.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    18 July 2007
//
//  Author:
//
//    Original C version by Lili Ju.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Richard Barrett, Michael Berry, Tony Chan, James Demmel,
//    June Donato, Jack Dongarra, Victor Eijkhout, Roidan Pozo,
//    Charles Romine, Henk van der Vorst,
//    Templates for the Solution of Linear Systems:
//    Building Blocks for Iterative Methods,
//    SIAM, 1994,
//    ISBN: 0898714710,
//    LC: QA297.8.T45.
//
//    Tim Kelley,
//    Iterative Methods for Linear and Nonlinear Equations,
//    SIAM, 2004,
//    ISBN: 0898713528,
//    LC: QA297.8.K45.
//
//    Yousef Saad,
//    Iterative Methods for Sparse Linear Systems,
//    Second Edition,
//    SIAM, 2003,
//    ISBN: 0898715342,
//    LC: QA188.S17.
//
//  Parameters:
//
//    Input, int N, the order of the system.
//
//    Input, int NZ_NUM, the number of nonzeros.
//
//    Input, int IA[NZ_NUM], JA[NZ_NUM], the row and column indices
//    of the matrix values.
//
//    Input, double A[NZ_NUM], the matrix values.
//
//    Input, double X[N], the vector to be multiplied by A.
//
//    Output, double W[N], the value of A*X.

// The original code by Lili Ju and John Burkardt has been modified to support
// openMP parallelisation, dense matrix format, and BLAS math library linkage.
// This new version remains under the GNU LGPL license.
// Author: Charles Fletcher, University of Oxford, 2021

//
{

  for (int i = 0; i < n; i++ )
  {
    w[i] = 0.0;
  }

  #if defined(_OPENMP)
  #pragma omp parallel
  #endif
  {
  double w0;
  #if defined(_OPENMP)
  #pragma omp for
  #endif
  for (int i = 0; i < n; i++ ){
    w0 = 0.0;
    for (int j = 0; j < n; j++ ){
      w0 += a[i + j*n] * x[j];
    }
    w[i] = w0;
  }
  }

}

//****************************************************************************80

int gmres ( int n, double a[], double x[],
  double rhs[], int itMax, int itRestart, double absTol, double relTol )

//****************************************************************************80
//
//  Purpose:
//
//    MGMRES_ST applies restarted GMRES to a matrix in sparse triplet form.
//
//  Discussion:
//
//    The linear system A*X=B is solved iteratively.
//
//    The matrix A is assumed to be stored in dense column-major form as
//    a one dimensional vector.
//
//    The "matrices" H and V are treated as one-dimensional vectors
//    which store the matrix data in row major form.
//
//    This requires that references to H[I][J] be replaced by references
//    to H[I+J*(MR+1)] and references to V[I][J] by V[I+J*N].
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    18 July 2007
//
//  Author:
//
//    Original C version by Lili Ju.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Richard Barrett, Michael Berry, Tony Chan, James Demmel,
//    June Donato, Jack Dongarra, Victor Eijkhout, Roidan Pozo,
//    Charles Romine, Henk van der Vorst,
//    Templates for the Solution of Linear Systems:
//    Building Blocks for Iterative Methods,
//    SIAM, 1994,
//    ISBN: 0898714710,
//    LC: QA297.8.T45.
//
//    Tim Kelley,
//    Iterative Methods for Linear and Nonlinear Equations,
//    SIAM, 2004,
//    ISBN: 0898713528,
//    LC: QA297.8.K45.
//
//    Yousef Saad,
//    Iterative Methods for Sparse Linear Systems,
//    Second Edition,
//    SIAM, 2003,
//    ISBN: 0898715342,
//    LC: QA188.S17.
//
//  Parameters:
//
//    Input, int N, the order of the linear system.
//
//    Input, double A[N*N], the matrix values (column major ordered).
//
//    Input/output, double X[N]; on input, an approximation to
//    the solution.  On output, an improved approximation.
//
//    Input, double RHS[N], the right hand side of the linear system.
//
//    Input, int ITR_MAX, the maximum number of (outer) iterations to take.
//
//    Input, int MR, the maximum number of (inner) iterations to take.
//    MR must be less than N.
//
//    Input, double TOL_ABS, an absolute tolerance applied to the
//    current residual.
//
//    Input, double TOL_REL, a relative tolerance comparing the
//    current residual to the initial residual.

// The original code by Lili Ju and John Burkardt has been modified to support
// openMP parallelisation, dense matrix format, and BLAS math library linkage.
// This new version remains under the GNU LGPL license.
// Author: Charles Fletcher, University of Oxford, 2021

//
{

  double av;
  double *c;
  double delta = 1.0e-03;
  double *g;
  double *h;
  double htmp;
  int i;
  int itr;
  int itr_used;
  int j;
  int k;
  int k_copy;
  double mu;
  double *r;
  double rho;
  double rho_tol;
  double *s;
  double *v;
  bool verbose = true;
  double *y;

  double bmag;
  double norm_err;

  c = new double[itRestart];
  g = new double[itRestart+1];
  h = new double[(itRestart+1)*itRestart];
  r = new double[n];
  s = new double[itRestart];
  v = new double[n*(itRestart+1)];
  y = new double[itRestart+1];

  itr_used = 0;

  if ( n < itRestart )
  {
    cerr << "\n";
    cerr << "GMRES - Fatal error\n";
    cerr << "  N < itRestart.\n";
    cerr << "  N = " << n << "\n";
    cerr << "  itRestart = " << itRestart << "\n";
    exit ( 1 );
  }

  for ( itr = 1; itr <= itMax; itr++ )
  {

    #if BLAS_FOUND == 1
        cblas_dgemv (CblasColMajor,CblasNoTrans,n,n,1.0,a,n,x,1,0.0,r,1);
    #else
      ax_mult ( n, a, x, r );
    #endif

    for ( i = 0; i < n; i++ )
    {
      r[i] = rhs[i] - r[i];
    }

    rho = sqrt ( r8vec_dot ( n, r, r ) );
    bmag = sqrt ( r8vec_dot ( n, rhs, rhs ) );

    if ( itr == 1 )
    {
      rho_tol = rho * relTol;
    }

    for ( i = 0; i < n; i++)
    {
      v[i+0*n] = r[i] / rho;
    }

    g[0] = rho;

    for ( i = 1; i <= itRestart; i++ )
    {
      g[i] = 0.0;
    }

    for ( i = 0; i < itRestart+1; i++ )
    {
      for ( j = 0; j < itRestart; j++ )
      {
        h[i+j*(itRestart+1)] = 0.0;
      }
    }

    for ( k = 1; k <= itRestart; k++ ) {
      k_copy = k;

      #if BLAS_FOUND == 1
        cblas_dgemv (CblasColMajor,CblasNoTrans,n,n,1.0,a,n,v+(k-1)*n,1,0.0,v+k*n,1);
      #else
        ax_mult (n, a, v+(k-1)*n, v+k*n);
      #endif

      av = sqrt ( r8vec_dot ( n, v+k*n, v+k*n ) );

      for ( j = 1; j <= k; j++ ) {
        h[(j-1)+(k-1)*(itRestart+1)] = r8vec_dot ( n, v+k*n, v+(j-1)*n );

        for ( i = 0; i < n; i++ )
        {
          v[i+k*n] -= h[(j-1)+(k-1)*(itRestart+1)] * v[i+(j-1)*n];
        }
      }

      h[k+(k-1)*(itRestart+1)] = sqrt ( r8vec_dot ( n, v+k*n, v+k*n ) );

      if ( ( av + delta * h[k+(k-1)*(itRestart+1)] ) == av )
      {
         for ( j = 1; j <= k; j++ )
         {
           htmp = r8vec_dot ( n, v+k*n, v+(j-1)*n );
           h[(j-1)+(k-1)*(itRestart+1)] = h[(j-1)+(k-1)*(itRestart+1)] + htmp;

           for ( i = 0; i < n; i++ )
           {
             v[i+k*n] = v[i+k*n] - htmp * v[i+(j-1)*n];
           }
         }
         h[k+(k-1)*(itRestart+1)] = sqrt ( r8vec_dot ( n, v+k*n, v+k*n ) );
      }

      if ( h[k+(k-1)*(itRestart+1)] != 0.0 )
      {
        for ( i = 0; i < n; i++ )
        {
          v[i+k*n] = v[i+k*n] / h[k+(k-1)*(itRestart+1)];
        }
      }

      if ( 1 < k )
      {
        for ( i = 1; i <= k+1; i++ )
        {
          y[i-1] = h[(i-1)+(k-1)*(itRestart+1)];
        }
        for ( j = 1; j <= k - 1; j++ )
        {
          mult_givens ( c[j-1], s[j-1], j-1, y );
        }
        for ( i = 1; i <= k+1; i++ )
        {
          h[i-1+(k-1)*(itRestart+1)] = y[i-1];
        }
      }

      mu = sqrt ( pow ( h[(k-1)+(k-1)*(itRestart+1)], 2 )
                + pow ( h[ k   +(k-1)*(itRestart+1)], 2 ) );
      c[k-1] =  h[(k-1)+(k-1)*(itRestart+1)] / mu;
      s[k-1] = -h[ k   +(k-1)*(itRestart+1)] / mu;
      h[(k-1)+(k-1)*(itRestart+1)] = c[k-1] * h[(k-1)+(k-1)*(itRestart+1)]
                            - s[k-1] * h[ k   +(k-1)*(itRestart+1)];
      h[k+(k-1)*(itRestart+1)] = 0;
      mult_givens ( c[k-1], s[k-1], k-1, g );

      rho = fabs ( g[k] );

      itr_used = itr_used + 1;

      norm_err = rho/bmag;

      if (norm_err  <= rho_tol && norm_err <= absTol )
      {
        break;
      }
    }

    k = k_copy - 1;
    y[k] = g[k] / h[k+k*(itRestart+1)];

    for ( i = k; 1 <= i; i-- )
    {
      y[i-1] = g[i-1];
      for ( j = i+1; j <= k+1; j++ )
      {
        y[i-1] = y[i-1] - h[(i-1)+(j-1)*(itRestart+1)] * y[j-1];
      }
      y[i-1] = y[i-1] / h[(i-1)+(i-1)*(itRestart+1)];
    }

    for ( i = 1; i <= n; i++ )
    {
      for ( j = 1; j <= k + 1; j++ )
      {
        x[i-1] = x[i-1] + v[(i-1)+(j-1)*n] * y[j-1];
      }
    }

    if ( norm_err <= rho_tol && norm_err <= absTol )
    {
      break;
    }
  }

  //output solver metadata
  cout << "\n";
  cout << "GMRES\n";
  cout << "  Number of iterations = " << itr_used << "\n";
  cout << "  Final residual = " << rho << "\n";
  cout << "  Final normalised error = " << norm_err << "\n";

  delete[] c;
  delete[] g;
  delete[] h;
  delete[] r;
  delete[] s;
  delete[] v;
  delete[] y;

  if ( norm_err <= rho_tol && norm_err <= absTol )
  {
    return 0;
  }
  else {
    return 1;
  }
}
//****************************************************************************80

inline void mult_givens ( double c, double s, int k, double g[] )

//****************************************************************************80
//
//  Purpose:
//
//    MULT_GIVENS applies a Givens rotation to two successive entries of a vector.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    08 August 2006
//
//  Author:
//
//    Original C version by Lili Ju.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Richard Barrett, Michael Berry, Tony Chan, James Demmel,
//    June Donato, Jack Dongarra, Victor Eijkhout, Roidan Pozo,
//    Charles Romine, Henk van der Vorst,
//    Templates for the Solution of Linear Systems:
//    Building Blocks for Iterative Methods,
//    SIAM, 1994,
//    ISBN: 0898714710,
//    LC: QA297.8.T45.
//
//    Tim Kelley,
//    Iterative Methods for Linear and Nonlinear Equations,
//    SIAM, 2004,
//    ISBN: 0898713528,
//    LC: QA297.8.K45.
//
//    Yousef Saad,
//    Iterative Methods for Sparse Linear Systems,
//    Second Edition,
//    SIAM, 2003,
//    ISBN: 0898715342,
//    LC: QA188.S17.
//
//  Parameters:
//
//    Input, double C, S, the cosine and sine of a Givens
//    rotation.
//
//    Input, int K, indicates the location of the first vector entry.
//
//    Input/output, double G[K+2], the vector to be modified.  On output,
//    the Givens rotation has been applied to entries G(K) and G(K+1).
//
{
  double g1;
  double g2;

  g1 = c * g[k] - s * g[k+1];
  g2 = s * g[k] + c * g[k+1];

  g[k]   = g1;
  g[k+1] = g2;

}
//****************************************************************************80

inline double r8vec_dot ( int n, double a1[], double a2[] )

//****************************************************************************80
//
//  Purpose:
//
//    R8VEC_DOT computes the dot product of a pair of R8VEC's.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    03 July 2005
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int N, the number of entries in the vectors.
//
//    Input, double A1[N], A2[N], the two vectors to be considered.
//
//    Output, double R8VEC_DOT, the dot product of the vectors.
//
{
  double value = 0.0;

  for (int i = 0; i < n; i++ )
  {
    value += a1[i] * a2[i];
  }

  return value;
}
//****************************************************************************80
