Modified version of an existing GMRES solver implementation, enabling dense matrix format, Math library linking, and OpenMP parallelisation 

Original authors: Lili Ju

[Original library source](https://people.sc.fsu.edu/~jburkardt/cpp_src/mgmres/mgmres.html)

Library under LGPLv3 (see attached license)
