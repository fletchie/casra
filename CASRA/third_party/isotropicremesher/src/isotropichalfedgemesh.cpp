/*
 *  Copyright (c) 2020-2021 Jeremy HU <jeremy-at-dust3d dot org>. All rights reserved.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:

 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.

 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *
 *
 * The original code by Jeremy HU has been modified to support adaptive remeshing
 * This modified version remains under an MIT License
 * Author: Charles Fletcher, University of Oxford, 2021
 */
#include <unordered_map>

#include <algorithm>
#include "isotropicremesher/isotropichalfedgemesh.h"

//threshold for dot product between pre-collapse and post-collapse normal. Any
//edge collapse that results in a value below this fails. Mitigates chance of
//panel flipping from occuring.
#define NORMTHRESHOLD 0.25

IsotropicHalfedgeMesh::~IsotropicHalfedgeMesh()
{
    while (nullptr != m_vertexAllocLink) {
        Vertex *vertex = m_vertexAllocLink;
        m_vertexAllocLink = vertex->_allocLink;
        delete vertex;
    }

    while (nullptr != m_faceAllocLink) {
        Face *face = m_faceAllocLink;
        m_faceAllocLink = face->_allocLink;
        delete face;
    }

    while (nullptr != m_halfedgeAllocLink) {
        Halfedge *halfedge = m_halfedgeAllocLink;
        m_halfedgeAllocLink = halfedge->_allocLink;
        delete halfedge;
    }
}

IsotropicHalfedgeMesh::IsotropicHalfedgeMesh(const std::vector<Vector3> &vertices,
    const std::vector<std::vector<size_t>> &faces,
    const std::vector<bool> &active)
{
    std::vector<Vertex *> halfedgeVertices;
    halfedgeVertices.reserve(vertices.size());
    long ind = 0;
    for (const auto &it: vertices) {
        Vertex *vertex = newVertex();
        vertex->position = it;
        vertex->locked = active[ind];
        ind++;
        halfedgeVertices.push_back(vertex);
    }

    std::vector<Face *> halfedgeFaces;
    halfedgeFaces.reserve(faces.size());
    for (const auto &it: faces) {
        Face *face = newFace();
        halfedgeFaces.push_back(face);
    }

    std::unordered_map<uint64_t, Halfedge *> halfedgeMap;
    for (size_t faceIndex = 0; faceIndex < faces.size(); ++faceIndex) {
        const auto &indices = faces[faceIndex];
        if (3 != indices.size()) {
            std::cerr << "Found non-triangle, face count:" << indices.size() << std::endl;
            continue;
        }
        std::vector<Halfedge *> halfedges(indices.size());
        for (size_t i = 0; i < 3; ++i) {
            size_t j = (i + 1) % 3;
            const auto &first = indices[i];
            const auto &second = indices[j];

            Vertex *vertex = halfedgeVertices[first];
            ++vertex->initialFaces;

            Halfedge *halfedge = newHalfedge();
            halfedge->startVertex = vertex;
            halfedge->leftFace = halfedgeFaces[faceIndex];

            if (nullptr == halfedge->leftFace->halfedge) {
                halfedge->leftFace->halfedge = halfedge;
            }
            if (nullptr == vertex->firstHalfedge) {
                vertex->firstHalfedge = halfedge;
            }

            halfedges[i] = halfedge;

            auto insertResult = halfedgeMap.insert({makeHalfedgeKey(first, second), halfedge});
            if (!insertResult.second) {
                std::cerr << "Found repeated halfedge:" << first << "," << second << std::endl;
            }
        }
        linkFaceHalfedges(halfedges);
    }
    for (auto &it: halfedgeMap) {
        auto halfedgeIt = halfedgeMap.find(swapHalfedgeKey(it.first));
        if (halfedgeIt == halfedgeMap.end())
            continue;
        it.second->oppositeHalfedge = halfedgeIt->second;
        halfedgeIt->second->oppositeHalfedge = it.second;
    }

    for (Vertex *vertex = m_firstVertex; nullptr != vertex; vertex = vertex->nextVertex) {
        vertex->_isBoundary = false;
        vertex->_valence = vertexValence(vertex, &vertex->_isBoundary);
        if (vertex->_isBoundary) {
            if (vertex->_valence == vertex->initialFaces + 1)
                continue;
        }
        else {
            if (vertex->_valence == vertex->initialFaces)
                continue;
        }
        vertex->featured = true;
        vertex->locked = true;
    }
}

inline void IsotropicHalfedgeMesh::linkFaceHalfedges(std::vector<IsotropicHalfedgeMesh::Halfedge *> &halfedges)
{
    const size_t halfedgesSize = halfedges.size();
    for (size_t i = 0; i < halfedgesSize; ++i) {
        size_t j = (i + 1) % halfedgesSize;
        halfedges[i]->nextHalfedge = halfedges[j];
        halfedges[j]->previousHalfedge = halfedges[i];
    }
}

inline void IsotropicHalfedgeMesh::updateFaceHalfedgesLeftFace(std::vector<IsotropicHalfedgeMesh::Halfedge *> &halfedges,
    IsotropicHalfedgeMesh::Face *leftFace)
{
    for (auto &it: halfedges)
        it->leftFace = leftFace;
}

inline void IsotropicHalfedgeMesh::linkHalfedgePair(IsotropicHalfedgeMesh::Halfedge *first, IsotropicHalfedgeMesh::Halfedge *second)
{
    if (nullptr != first)
        first->oldOppositeHalfedge = first->oppositeHalfedge;
        first->oppositeHalfedge = second;
    if (nullptr != second)
        second->oldOppositeHalfedge = second->oppositeHalfedge;
        second->oppositeHalfedge = first;
}

inline void IsotropicHalfedgeMesh::unlinkHalfedgePair(IsotropicHalfedgeMesh::Halfedge *first, IsotropicHalfedgeMesh::Halfedge *second)
{
      first->oppositeHalfedge = first->oldOppositeHalfedge;
      second->oppositeHalfedge = second->oldOppositeHalfedge;
}

double IsotropicHalfedgeMesh::averageEdgeLength()
{
    double totalLength = 0.0;
    size_t halfedgeCount = 0;
    for (Face *face = m_firstFace; nullptr != face; face = face->nextFace) {
        const auto &startHalfedge = face->halfedge;
        Halfedge *halfedge = startHalfedge;
        do {
            const auto &nextHalfedge = halfedge->nextHalfedge;
            totalLength += (halfedge->startVertex->position - nextHalfedge->startVertex->position).length();
            ++halfedgeCount;
        } while (halfedge != startHalfedge);
    }
    if (0 == halfedgeCount)
        return 0.0;
    return totalLength / halfedgeCount;
}

IsotropicHalfedgeMesh::Face *IsotropicHalfedgeMesh::newFace()
{
    Face *face = new Face;

    face->_allocLink = m_faceAllocLink;
    m_faceAllocLink = face;

    face->debugIndex = ++m_debugFaceIndex;
    if (nullptr != m_lastFace) {
        m_lastFace->nextFace = face;
        face->previousFace = m_lastFace;
    } else {
        m_firstFace = face;
    }
    m_lastFace = face;
    return face;
}

IsotropicHalfedgeMesh::Vertex *IsotropicHalfedgeMesh::newVertex()
{
    Vertex *vertex = new Vertex;

    vertex->_allocLink = m_vertexAllocLink;
    m_vertexAllocLink = vertex;

    vertex->debugIndex = ++m_debugVertexIndex;
    if (nullptr != m_lastVertex) {
        m_lastVertex->nextVertex = vertex;
        vertex->previousVertex = m_lastVertex;
    } else {
        m_firstVertex = vertex;
    }
    m_lastVertex = vertex;
    return vertex;
}

IsotropicHalfedgeMesh::Halfedge *IsotropicHalfedgeMesh::newHalfedge()
{
    Halfedge *halfedge = new Halfedge;

    halfedge->_allocLink = m_halfedgeAllocLink;
    m_halfedgeAllocLink = halfedge;

    halfedge->debugIndex = ++m_debugHalfedgeIndex;
    return halfedge;
}

IsotropicHalfedgeMesh::Face *IsotropicHalfedgeMesh::moveToNextFace(IsotropicHalfedgeMesh::Face *face)
{
    if (nullptr == face) {
        face = m_firstFace;
        if (nullptr == face)
            return nullptr;
        if (!face->removed)
            return face;
    }
    do {
        face = face->nextFace;
    } while (nullptr != face && face->removed);
    return face;
}

IsotropicHalfedgeMesh::Vertex *IsotropicHalfedgeMesh::moveToNextVertex(IsotropicHalfedgeMesh::Vertex *vertex)
{
    if (nullptr == vertex) {
        vertex = m_firstVertex;
        if (nullptr == vertex)
            return nullptr;
        if (!vertex->removed)
            return vertex;
    }
    do {
        vertex = vertex->nextVertex;
    } while (nullptr != vertex && vertex->removed);
    return vertex;
}

void IsotropicHalfedgeMesh::breakFace(IsotropicHalfedgeMesh::Face *leftOldFace,
    IsotropicHalfedgeMesh::Halfedge *halfedge,
    IsotropicHalfedgeMesh::Vertex *breakPointVertex,
    std::vector<IsotropicHalfedgeMesh::Halfedge *> &leftNewFaceHalfedges,
    std::vector<IsotropicHalfedgeMesh::Halfedge *> &leftOldFaceHalfedges)
{
    std::vector<Halfedge *> leftFaceHalfedges = {
        halfedge->previousHalfedge,
        halfedge,
        halfedge->nextHalfedge
    };

    Face *leftNewFace = newFace();
    leftNewFace->halfedge = leftFaceHalfedges[2];
    leftFaceHalfedges[2]->leftFace = leftNewFace;

    leftNewFaceHalfedges.push_back(newHalfedge());
    leftNewFaceHalfedges.push_back(newHalfedge());
    leftNewFaceHalfedges.push_back(leftFaceHalfedges[2]);
    linkFaceHalfedges(leftNewFaceHalfedges);
    updateFaceHalfedgesLeftFace(leftNewFaceHalfedges, leftNewFace);
    leftNewFaceHalfedges[0]->startVertex = leftFaceHalfedges[0]->startVertex;
    leftNewFaceHalfedges[1]->startVertex = breakPointVertex;

    leftOldFaceHalfedges.push_back(leftFaceHalfedges[0]);
    leftOldFaceHalfedges.push_back(halfedge);
    leftOldFaceHalfedges.push_back(newHalfedge());
    linkFaceHalfedges(leftOldFaceHalfedges);
    updateFaceHalfedgesLeftFace(leftOldFaceHalfedges, leftOldFace);
    leftOldFaceHalfedges[2]->startVertex = breakPointVertex;

    breakPointVertex->firstHalfedge = leftNewFaceHalfedges[1];

    linkHalfedgePair(leftNewFaceHalfedges[0], leftOldFaceHalfedges[2]);

    leftOldFace->halfedge = leftOldFaceHalfedges[0];

}

void IsotropicHalfedgeMesh::breakEdge(IsotropicHalfedgeMesh::Halfedge *halfedge)
{
    Face *leftOldFace = halfedge->leftFace;
    Halfedge *oppositeHalfedge = halfedge->oppositeHalfedge;
    Face *rightOldFace = nullptr;

    if (nullptr != oppositeHalfedge)
        rightOldFace = oppositeHalfedge->leftFace;

    Vertex *breakPointVertex = newVertex();
    breakPointVertex->position = (halfedge->startVertex->position +
        halfedge->nextHalfedge->startVertex->position) * 0.5;

    breakPointVertex->featured = halfedge->startVertex->featured &&
        halfedge->nextHalfedge->startVertex->featured;

    breakPointVertex->locked = halfedge->startVertex->locked &&
        halfedge->nextHalfedge->startVertex->locked;

    std::vector<Halfedge *> leftNewFaceHalfedges;
    std::vector<Halfedge *> leftOldFaceHalfedges;
    breakFace(leftOldFace, halfedge, breakPointVertex,
        leftNewFaceHalfedges, leftOldFaceHalfedges);

    if (nullptr != rightOldFace) {
        std::vector<Halfedge *> rightNewFaceHalfedges;
        std::vector<Halfedge *> rightOldFaceHalfedges;
        breakFace(rightOldFace, oppositeHalfedge, breakPointVertex,
            rightNewFaceHalfedges, rightOldFaceHalfedges);
        linkHalfedgePair(leftOldFaceHalfedges[1], rightNewFaceHalfedges[1]);
        linkHalfedgePair(leftNewFaceHalfedges[1], rightOldFaceHalfedges[1]);
    }
}

bool IsotropicHalfedgeMesh::testVertexAround(Vertex *vertex, Vertex *testVertex)
{
    const auto &startHalfedge = vertex->firstHalfedge;
    if (nullptr == startHalfedge)
        return false;

    Halfedge *loopHalfedge = startHalfedge;
    do {
        if (loopHalfedge->nextHalfedge->startVertex == testVertex)
            return true;
        if (nullptr == loopHalfedge->oppositeHalfedge) {
            loopHalfedge = startHalfedge;
            do {
                if (loopHalfedge->previousHalfedge->startVertex == testVertex)
                    return true;
                loopHalfedge = loopHalfedge->previousHalfedge->oppositeHalfedge;
                if (nullptr == loopHalfedge)
                    break;
            } while (loopHalfedge != startHalfedge);
            break;
        }
        loopHalfedge = loopHalfedge->oppositeHalfedge->nextHalfedge;
    } while (loopHalfedge != startHalfedge);

    return false;
}

void IsotropicHalfedgeMesh::collectVerticesAroundVertex(Vertex *vertex, std::vector<Vertex *> *vertices)
{
    Halfedge *startHalfedge = vertex->firstHalfedge;
    if (startHalfedge == nullptr)
        return;

    Halfedge *loopHalfedge = startHalfedge;
    do {
        vertices->push_back(loopHalfedge->nextHalfedge->startVertex);
        if (loopHalfedge->oppositeHalfedge == nullptr) {
            loopHalfedge = startHalfedge;
            do {
                vertices->push_back(loopHalfedge->previousHalfedge->startVertex);
                loopHalfedge = loopHalfedge->previousHalfedge->oppositeHalfedge;
                if (loopHalfedge == nullptr)
                    break;
            } while (loopHalfedge != startHalfedge);
            break;
        }
        loopHalfedge = loopHalfedge->oppositeHalfedge->nextHalfedge;
    } while (loopHalfedge != startHalfedge);
}

size_t IsotropicHalfedgeMesh::vertexValence(Vertex *vertex, bool *isBoundary)
{
    const auto &startHalfedge = vertex->firstHalfedge;
    if (nullptr == startHalfedge)
        return 0;

    size_t valence = 0;

    Halfedge *loopHalfedge = startHalfedge;
    do {
        ++valence;
        if (nullptr == loopHalfedge->oppositeHalfedge) {
            if (nullptr != isBoundary)
                *isBoundary = true;
            loopHalfedge = startHalfedge;
            do {
                ++valence;
                loopHalfedge = loopHalfedge->previousHalfedge->oppositeHalfedge;
                if (nullptr == loopHalfedge)
                    break;
            } while (loopHalfedge != startHalfedge);
            break;
        }
        loopHalfedge = loopHalfedge->oppositeHalfedge->nextHalfedge;
    } while (loopHalfedge != startHalfedge);

    return valence;
}

bool IsotropicHalfedgeMesh::testLengthSquaredAroundVertex(Vertex *vertex,
    const Vector3 &target,
    double maxEdgeLengthSquared)
{
    const auto &startHalfedge = vertex->firstHalfedge;
    if (nullptr == startHalfedge)
        return false;

    Halfedge *loopHalfedge = startHalfedge;
    do {
        if ((loopHalfedge->nextHalfedge->startVertex->position -
                target).lengthSquared() > maxEdgeLengthSquared) {
            return true;
        }
        if (nullptr == loopHalfedge->oppositeHalfedge) {
            loopHalfedge = startHalfedge;
            do {
                if ((loopHalfedge->previousHalfedge->startVertex->position -
                        target).lengthSquared() > maxEdgeLengthSquared) {
                    return true;
                }
                loopHalfedge = loopHalfedge->previousHalfedge->oppositeHalfedge;
                if (nullptr == loopHalfedge)
                    break;
            } while (loopHalfedge != startHalfedge);
            break;
        }
        loopHalfedge = loopHalfedge->oppositeHalfedge->nextHalfedge;
    } while (loopHalfedge != startHalfedge);

    return false;
}

void IsotropicHalfedgeMesh::pointerVertexToNewVertex(Vertex *vertex, Vertex *replacement)
{
    const auto &startHalfedge = vertex->firstHalfedge;
    if (nullptr == startHalfedge)
        return;

    Halfedge *loopHalfedge = startHalfedge;
    do {
        loopHalfedge->oldVertex = loopHalfedge->startVertex;
        loopHalfedge->startVertex = replacement;
        if (nullptr == loopHalfedge->oppositeHalfedge) {
            loopHalfedge = startHalfedge;
            do {
                loopHalfedge->oldVertex = loopHalfedge->startVertex;
                loopHalfedge->startVertex = replacement;
                loopHalfedge = loopHalfedge->previousHalfedge->oppositeHalfedge;
                if (nullptr == loopHalfedge)
                    break;
            } while (loopHalfedge != startHalfedge);
            break;
        }
        loopHalfedge = loopHalfedge->oppositeHalfedge->nextHalfedge;
    } while (loopHalfedge != startHalfedge);
}

inline void IsotropicHalfedgeMesh::recoverPointerVertexOldVertex(Vertex *vertex)
{
    const auto &startHalfedge = vertex->firstHalfedge;
    if (nullptr == startHalfedge)
        return;

    Halfedge *loopHalfedge = startHalfedge;
    do {
        loopHalfedge->startVertex = loopHalfedge->oldVertex;
        if (nullptr == loopHalfedge->oppositeHalfedge) {
            loopHalfedge = startHalfedge;
            do {
                loopHalfedge->startVertex = loopHalfedge->oldVertex;
                loopHalfedge = loopHalfedge->previousHalfedge->oppositeHalfedge;
                if (nullptr == loopHalfedge)
                    break;
            } while (loopHalfedge != startHalfedge);
            break;
        }
        loopHalfedge = loopHalfedge->oppositeHalfedge->nextHalfedge;
    } while (loopHalfedge != startHalfedge);
}


void IsotropicHalfedgeMesh::relaxVertex(Vertex *vertex)
{
    if (vertex->_isBoundary || vertex->_valence <= 0)
        return;

    const auto &startHalfedge = vertex->firstHalfedge;
    if (nullptr == startHalfedge)
        return;

    Vector3 position;
    Vector3 projectedPosition;

    Halfedge *loopHalfedge = startHalfedge;
    do {
        position += loopHalfedge->oppositeHalfedge->startVertex->position;
        loopHalfedge = loopHalfedge->oppositeHalfedge->nextHalfedge;
    } while (loopHalfedge != startHalfedge);

    position /= vertex->_valence;

    projectedPosition = Vector3::projectPointOnLine(vertex->position, position, position + vertex->_normal);
    if (projectedPosition.containsNan() || projectedPosition.containsInf()) {
        vertex->relaxed = true;
        vertex->nposition = position;
        return;
    }

    vertex->relaxed = true;
    vertex->nposition = projectedPosition;
    //vertex->position = projectedPosition;
}

bool IsotropicHalfedgeMesh::flipEdge(Halfedge *halfedge)
{
    Vertex *topVertex = halfedge->previousHalfedge->startVertex;
    Vertex *bottomVertex = halfedge->oppositeHalfedge->previousHalfedge->startVertex;

    Vertex *leftVertex = halfedge->startVertex;
    Vertex *rightVertex = halfedge->oppositeHalfedge->startVertex;

    if (testVertexAround(topVertex, bottomVertex) == true)
        return false;

    bool isLeftBoundary = false;
    int leftValence = (int)vertexValence(leftVertex, &isLeftBoundary);
    if (leftValence <= 3) //only perform collapse if vertex neighbours will not be reduced below 3
        return false;

    bool isRightBoundary = false;
    int rightValence = (int)vertexValence(rightVertex, &isRightBoundary);
    if (rightValence <= 3)
        return false;

    bool isTopBoundary = false;
    int topValence = (int)vertexValence(topVertex, &isTopBoundary);

    bool isBottomBoundary = false;
    int bottomValence = (int)vertexValence(bottomVertex, &isBottomBoundary);

    auto deviation = [](int valence, bool isBoundary) {
        return std::pow(valence - (isBoundary ? 4 : 6), 2);
    };

    int oldDeviation = deviation(topValence, isTopBoundary) +
        deviation(bottomValence, isBottomBoundary) +
        deviation(leftValence, isLeftBoundary) +
        deviation(rightValence, isRightBoundary);
    int newDeviation = deviation(topValence + 1, isTopBoundary) +
        deviation(bottomValence + 1, isBottomBoundary) +
        deviation(leftValence - 1, isLeftBoundary) +
        deviation(rightValence - 1, isRightBoundary);

    if (newDeviation >= oldDeviation)
        return false;

    Halfedge *opposite = halfedge->oppositeHalfedge;

    Face *topFace = halfedge->leftFace;
    Face *bottomFace = opposite->leftFace;

    /*
    if ((halfedge->startVertex->locked || halfedge->startVertex->featured ||
         halfedge->oppositeHalfedge->startVertex->locked || halfedge->oppositeHalfedge->startVertex->featured ||
         halfedge->oppositeHalfedge->previousHalfedge->startVertex->locked || halfedge->oppositeHalfedge->previousHalfedge->startVertex->featured ||
         halfedge->previousHalfedge->startVertex->locked || halfedge->previousHalfedge->startVertex->featured))
         return false;
    */

    Vector3& v0 = halfedge->startVertex->position;
    Vector3& v1 = halfedge->oppositeHalfedge->startVertex->position;
    Vector3& v2 = halfedge->oppositeHalfedge->previousHalfedge->startVertex->position;
    Vector3& v3 = halfedge->previousHalfedge->startVertex->position;

    Vector3 otopFaceNormal = Vector3::normal(v3, v0, v1);
    Vector3 obottomFaceNormal = Vector3::normal(v1, v0, v2);
    Vector3 ntopFaceNormal = Vector3::normal(v3, v0, v2);
    Vector3 nbottomFaceNormal = Vector3::normal(v1, v3, v2);

    double norm1 = obottomFaceNormal.x() * nbottomFaceNormal.x() + obottomFaceNormal.y() * nbottomFaceNormal.y() + obottomFaceNormal.z() * nbottomFaceNormal.z();
    double norm2 = ntopFaceNormal.x() * otopFaceNormal.x() + ntopFaceNormal.y() * otopFaceNormal.y() + ntopFaceNormal.z() * otopFaceNormal.z();
    if (norm2 < NORMTHRESHOLD || norm1 < NORMTHRESHOLD)
      return false;

    if (leftVertex->firstHalfedge == halfedge)
        leftVertex->firstHalfedge = opposite->nextHalfedge;

    if (rightVertex->firstHalfedge == opposite)
        rightVertex->firstHalfedge = halfedge->nextHalfedge;

    halfedge->nextHalfedge->leftFace = bottomFace;
    opposite->nextHalfedge->leftFace = topFace;

    halfedge->startVertex = bottomVertex;
    opposite->startVertex = topVertex;

    topFace->halfedge = halfedge;
    bottomFace->halfedge = opposite;

    std::vector<Halfedge *> newLeftHalfedges = {
        halfedge->previousHalfedge,
        opposite->nextHalfedge,
        halfedge
    };
    std::vector<Halfedge *> newRightHalfedges = {
        opposite->previousHalfedge,
        halfedge->nextHalfedge,
        opposite,
    };

    linkFaceHalfedges(newLeftHalfedges);
    linkFaceHalfedges(newRightHalfedges);

    return true;
}

bool IsotropicHalfedgeMesh::collapseEdge(Halfedge *halfedge, double maxEdgeLengthSquared)
{
    Halfedge *opposite = halfedge->oppositeHalfedge;

    Vertex *topVertex = halfedge->previousHalfedge->startVertex;
    Vertex *bottomVertex = opposite->previousHalfedge->startVertex;

    if (topVertex == bottomVertex)
        return false;

    if (topVertex->featured || topVertex->locked)
        return false;

    if (bottomVertex->featured || bottomVertex->locked)
        return false;

    Vector3 collapseTo = (halfedge->startVertex->position +
       halfedge->nextHalfedge->startVertex->position) * 0.5;

    if (testLengthSquaredAroundVertex(halfedge->startVertex, collapseTo, maxEdgeLengthSquared))
        return false;
    if (testLengthSquaredAroundVertex(halfedge->nextHalfedge->startVertex, collapseTo, maxEdgeLengthSquared))
        return false;


    std::vector<Vertex*> neighborVertices;
    std::vector<Vertex*> otherNeighborVertices;
    std::vector<Vertex*> sharedNeighborVertices;
    neighborVertices.reserve(20);
    otherNeighborVertices.reserve(20);
    sharedNeighborVertices.reserve(20);

    collectVerticesAroundVertex(halfedge->startVertex, &neighborVertices);
    collectVerticesAroundVertex(halfedge->nextHalfedge->startVertex, &otherNeighborVertices);
    std::sort(neighborVertices.begin(), neighborVertices.end());
    std::sort(otherNeighborVertices.begin(), otherNeighborVertices.end());

    std::vector<Vertex*>::iterator nverticesend = std::unique(neighborVertices.begin(), neighborVertices.end());
    std::vector<Vertex*>::iterator overticesend = std::unique(otherNeighborVertices.begin(), otherNeighborVertices.end());

    std::set_intersection(neighborVertices.begin(), nverticesend,
        otherNeighborVertices.begin(), overticesend,
        std::back_inserter(sharedNeighborVertices));

    if (sharedNeighborVertices.size() != 2) { //there must be exactly two shared vertices to avoid non-manifold meshes arising
        return false;
    }


    Vertex *leftVertex = halfedge->startVertex;
    Vertex *rightVertex = opposite->startVertex;
    Face *topFace = halfedge->leftFace;
    Face *bottomFace = opposite->leftFace;

    //added condition to avoid collapse if either neighbouring panels are fixed
    if (topFace->locked || bottomFace->locked){
      return false;
    }

    //Pre-calculate surrounding face normals of collapse edge candidate
    Halfedge *opposite1 = leftVertex->firstHalfedge;
    Halfedge *opposite2 = rightVertex->firstHalfedge;

    //define average normal in vicinity
    Vector3 averageNorm(0.0, 0.0, 0.0);
    double nf = 0.0;

    do {
      Face* face = opposite1->leftFace;
      //check face2 hasn't flipped
      if (face->removed == false){
        face->_normal = Vector3::normal(face->halfedge->previousHalfedge->startVertex->position,
                                        face->halfedge->startVertex->position,
                                        face->halfedge->nextHalfedge->startVertex->position);
        averageNorm += face->_normal;
        nf += 1.0;
      }

      opposite1 = opposite1->oppositeHalfedge->nextHalfedge;
    } while (opposite1 != leftVertex->firstHalfedge);

    do {
      Face* face = opposite2->leftFace;
      //check face2 hasn't flipped
      if (face->removed == false){
        face->_normal = Vector3::normal(face->halfedge->previousHalfedge->startVertex->position,
                                        face->halfedge->startVertex->position,
                                        face->halfedge->nextHalfedge->startVertex->position);
      averageNorm += face->_normal;
      nf += 1.0;
      }

      opposite2 = opposite2->oppositeHalfedge->nextHalfedge;
    } while (opposite2 != rightVertex->firstHalfedge);

    averageNorm /= nf;

    //perform candidate collapse - replace all half edge left vertex pointers with right vertex vertex
    pointerVertexToNewVertex(leftVertex, rightVertex);

    Halfedge *old1 = rightVertex->firstHalfedge;
    Halfedge *old2 = topVertex->firstHalfedge;
    Halfedge *old3 = bottomVertex->firstHalfedge;

    if (topFace == rightVertex->firstHalfedge->leftFace ||
            bottomFace == rightVertex->firstHalfedge->leftFace) {
        rightVertex->firstHalfedge = opposite->previousHalfedge->oppositeHalfedge;
    }
    if (topFace == topVertex->firstHalfedge->leftFace ||
            bottomFace == topVertex->firstHalfedge->leftFace) {
        topVertex->firstHalfedge = halfedge->nextHalfedge->oppositeHalfedge;
    }
    if (bottomFace == bottomVertex->firstHalfedge->leftFace ||
            bottomFace == bottomVertex->firstHalfedge->leftFace) {
        bottomVertex->firstHalfedge = opposite->nextHalfedge->oppositeHalfedge;
    }

    Vector3 preColl = rightVertex->position;
    rightVertex->position = collapseTo;

    bool leftVertexOldRemState = leftVertex->removed;
    bool topFaceOldRemState = topFace->removed;
    bool bottomFaceOldRemState = bottomFace->removed;

    leftVertex->removed = true;
    topFace->removed = true;
    bottomFace->removed = true;

    linkHalfedgePair(halfedge->previousHalfedge->oppositeHalfedge,
        halfedge->nextHalfedge->oppositeHalfedge);
    linkHalfedgePair(opposite->previousHalfedge->oppositeHalfedge,
        opposite->nextHalfedge->oppositeHalfedge);


    //check that collapsed mesh does not result in a significant change to remaining
    //panel normals
    double norm1 = 1.0;
    double norm2 = 1.0;
    double norm3 = 1.0;

    opposite2 = rightVertex->firstHalfedge;
    do {
      Face* face = opposite2->leftFace;
      if (face->removed == false){
        face->_tnormal = Vector3::normal(face->halfedge->previousHalfedge->startVertex->position,
                                         face->halfedge->startVertex->position,
                                         face->halfedge->nextHalfedge->startVertex->position);

        //calculate dot product between pre-collapse and post-collapse panel normal
        norm1 = face->_normal.x() * face->_tnormal.x() + face->_normal.y() * face->_tnormal.y() + face->_normal.z() * face->_tnormal.z();
        norm2 = averageNorm.x() * face->_normal.x() + averageNorm.y() * face->_normal.y() + averageNorm.z() * face->_normal.z();
        norm3 = averageNorm.x() * face->_tnormal.x() + averageNorm.y() * face->_tnormal.y() + averageNorm.z() * face->_tnormal.z();

        if (norm1 < NORMTHRESHOLD){

          //significant change in panel normal detected. Undo collapse and restore mesh to pre-collapse state.
          unlinkHalfedgePair(halfedge->previousHalfedge->oppositeHalfedge, halfedge->nextHalfedge->oppositeHalfedge);
          unlinkHalfedgePair(opposite->previousHalfedge->oppositeHalfedge, opposite->nextHalfedge->oppositeHalfedge);

          leftVertex->removed = leftVertexOldRemState;
          topFace->removed = topFaceOldRemState;
          bottomFace->removed = bottomFaceOldRemState;

          rightVertex->position = preColl;

          rightVertex->firstHalfedge = old1;
          topVertex->firstHalfedge = old2;
          bottomVertex->firstHalfedge = old3;

          recoverPointerVertexOldVertex(leftVertex);

          //report collapse as failed
          return false;
      }
    }
      opposite2 = opposite2->oppositeHalfedge->nextHalfedge;
    } while (opposite2 != rightVertex->firstHalfedge);

    //report collapse as successful
    return true;
}

void IsotropicHalfedgeMesh::updateVertexValences()
{
    for (Vertex *vertex = m_firstVertex; nullptr != vertex; vertex = vertex->nextVertex) {
        if (vertex->removed)
            continue;
        vertex->_isBoundary = false;
        vertex->_valence = vertexValence(vertex, &vertex->_isBoundary);
    }
}

void IsotropicHalfedgeMesh::updateTriangleNormals()
{
    for (Face *face = m_firstFace; nullptr != face; face = face->nextFace) {
        if (face->removed)
            continue;
        auto &startHalfedge = face->halfedge;
        face->_normal = Vector3::normal(startHalfedge->previousHalfedge->startVertex->position,
            startHalfedge->startVertex->position,
            startHalfedge->nextHalfedge->startVertex->position);
    }
}

void IsotropicHalfedgeMesh::updateVertexNormals()
{
    for (Vertex *vertex = m_firstVertex; nullptr != vertex; vertex = vertex->nextVertex) {
        if (vertex->removed)
            continue;
        vertex->_normal = Vector3();
    }

    for (Face *face = m_firstFace; nullptr != face; face = face->nextFace) {
        if (face->removed)
            continue;
        auto &startHalfedge = face->halfedge;
        Vector3 faceNormal = face->_normal *
            Vector3::area(startHalfedge->previousHalfedge->startVertex->position,
                startHalfedge->startVertex->position,
                startHalfedge->nextHalfedge->startVertex->position);
        startHalfedge->previousHalfedge->startVertex->_normal += faceNormal;
        startHalfedge->startVertex->_normal += faceNormal;
        startHalfedge->nextHalfedge->startVertex->_normal += faceNormal;
    }

    for (Vertex *vertex = m_firstVertex; nullptr != vertex; vertex = vertex->nextVertex) {
        if (vertex->removed)
            continue;
        vertex->_normal.normalize();
    }
}

void IsotropicHalfedgeMesh::featureHalfedge(Halfedge *halfedge, double radians)
{
    if (-1 != halfedge->featureState)
        return;

    auto &opposite = halfedge->oppositeHalfedge;
    if (nullptr == opposite) {
        halfedge->startVertex->featured = true;
        halfedge->startVertex->locked = true;
        halfedge->featureState = 1;
        return;
    }

    if (Vector3::angle(halfedge->leftFace->_normal,
            opposite->leftFace->_normal) >= radians) {
        halfedge->featureState = opposite->featureState = 1;
        halfedge->startVertex->featured = opposite->startVertex->featured = true;
        halfedge->startVertex->locked = opposite->startVertex->locked = true;
        return;
    }

    halfedge->featureState = opposite->featureState = 0;
}

void IsotropicHalfedgeMesh::featureBoundaries()
{
    for (Face *face = m_firstFace; nullptr != face; face = face->nextFace) {
        if (face->removed)
            continue;
        auto &startHalfedge = face->halfedge;
        if (nullptr == startHalfedge->previousHalfedge->oppositeHalfedge) {
            startHalfedge->previousHalfedge->startVertex->featured = true;
            startHalfedge->previousHalfedge->startVertex->locked = true;
        }
        if (nullptr == startHalfedge->oppositeHalfedge) {
            startHalfedge->startVertex->featured = true;
            startHalfedge->startVertex->locked = true;
        }
        if (nullptr == startHalfedge->nextHalfedge->oppositeHalfedge) {
            startHalfedge->nextHalfedge->startVertex->featured = true;
            startHalfedge->nextHalfedge->startVertex->locked = true;
        }
    }
}

void IsotropicHalfedgeMesh::featureEdges(double radians)
{
    for (Face *face = m_firstFace; nullptr != face; face = face->nextFace) {
        if (face->removed)
            continue;
        auto &startHalfedge = face->halfedge;
        featureHalfedge(startHalfedge->previousHalfedge, radians);
        featureHalfedge(startHalfedge, radians);
        featureHalfedge(startHalfedge->nextHalfedge, radians);
    }
}
