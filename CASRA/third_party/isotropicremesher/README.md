Modified version of an isotropic remeshing library to allow for adaptive remeshing and efficient compilation on both MSVC and GCC.

Original authors: Jeremy HU

[Original library source](https://github.com/huxingyi/isotropicremesher)
