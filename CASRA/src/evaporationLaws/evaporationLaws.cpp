/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <vector>
#include <stdexcept>
#include <iostream>

#include "CASRA/evaporationLaws/evaporationLaws.h"
#include "CASRA/base/vectorMath.h"

using namespace std;

/*!
 * Class initialiser for the Linear isotropic law
 * @param evapField The phase-specific effective evaporation field
 *
 */
CurvatureLawIsotropic::CurvatureLawIsotropic(double materialModifier): materialModifier{materialModifier} {

}

/*!
 * Class initialiser for the Linear isotropic law
 * @param evapField The phase-specific effective evaporation field
 *
 */
LinearLawIsotropic::LinearLawIsotropic(double evapField): evapField{evapField} {

}

/*!
 * Class initialiser for the Ahhrenius isotropic law
 * @param preFactor The Ahhrenius expential prefactor
 * @param C The barrier exponent prefactor
 * @param evapField The phase-specific effective evaporation field
 *
 */
AhhreniusLawIsotropic::AhhreniusLawIsotropic(double preFactor,
                                             vector<double> C,
                                             vector<double> Cexp,
                                             double evapField): preFactor{preFactor},
                                                                C{C},
                                                                Cexp{Cexp},
                                                                evapField{evapField} {

}

/*!
 * Class initialiser for the deformed-Ahhrenius isotropic law
 * @param preFactor The Ahhrenius expential prefactor
 * @param C The barrier exponent prefactor
 * @param evapField The phase-specific effective evaporation field
 * @param d The deformation coefficient
 */
DeformedAhhreniusLawIsotropic::DeformedAhhreniusLawIsotropic(double preFactor,
                                                             vector<double> C,
                                                             vector<double> Cexp,
                                                             double evapField,
                                                             double d): preFactor{preFactor},
                                                                        C{C},
                                                                        Cexp{Cexp},
                                                                        evapField{evapField},
                                                                        d{d} {

}

/*!
 * Method for calculating evaporation rate at a single surface position for a
 * defined LinearLawIsotropic instance.
 * @param field specimen surface field
 * @param T specimen surface temperature
 * @return evapRate calculated surface panel evaporation rate
 */
double LinearLawIsotropic::evaporationRate(double field, double T){

  return  field * (this->evapField * 1.0);

}

/*!
 * Method for calculating evaporation rates for a defined LinearLawIsotropic instance
 * @param field specimen surface panel fields
 * @param T specimen surface panel temperature
 * @return evapRates calculated surface panel evaporation rates
 */
vector<double> LinearLawIsotropic::evaporationRates(vector<double> field, double T){

  return  field * (this->evapField * 1.0);

}

/*!
 * Method for calculating evaporation rates for a defined LinearLawIsotropic instance
 * @param field specimen surface panel fields
 * @param T specimen surface panel temperatures
 * @return evapRates calculated surface panel evaporation rates
 */
vector<double> LinearLawIsotropic::evaporationRates(vector<double> field, vector<double> T){

  vector<double> evapRates;
  for (int i=0;i<field.size();i++){
    double lval = field[i] * this->evapField * 1.0;
    evapRates.push_back(lval);
  }

  return evapRates;
}

/*!
 * Method for calculating evaporation rates for a defined AhhreniusLawIsotropic instance
 * @param field specimen surface panel fields
 * @param T specimen surface panel temperature
 * @return evapRates calculated surface panel evaporation rates
 */
vector<double> AhhreniusLawIsotropic::evaporationRates(vector<double> field, double T){

  vector<double> evapRates;
  for (int i=0;i<field.size();i++){
    double lval = log(this->preFactor);

    for (int j=0;j<this->C.size();j++){
      lval -= this->C[j]/(BOLTZMANN * T) * pow(1.0 - field[i]/this->evapField, this->Cexp[j]);
    }

    evapRates.push_back(exp(lval));
  }

  return evapRates;
}

/*!
 * Method for calculating evaporation rate at a particular surface position
 * for a defined AhhreniusLawIsotropic instance.
 * @param field specimen surface field
 * @param T specimen surface temperature
 * @return evapRate calculated surface evaporation rate
 */
double AhhreniusLawIsotropic::evaporationRate(double field, double T){

  double lval = log(this->preFactor);
  for (int j=0;j<this->C.size();j++){
    lval -= this->C[j]/(BOLTZMANN * T) * pow(1.0 - field/this->evapField, this->Cexp[j]);
  }

  return exp(lval);

}

/*!
 * Method for calculating evaporation rates for a defined AhhreniusLawIsotropic instance
 * @param field specimen surface panel fields
 * @param T specimen surface panel temperatures
 * @return evapRates calculated surface panel evaporation rates
 */
vector<double> AhhreniusLawIsotropic::evaporationRates(vector<double> field, vector<double> T){

  try {
    if (field.size() != T.size()){
      throw 1;
    }
  }
  catch (int n) {
      cerr << "passed electric field/temperature surface patches of differing lengths. Throwing exception." << endl;
      exit(1);
  }

  vector<double> evapRates;
  for (int i=0;i<field.size();i++){
    double lval = log(this->preFactor);
    for (int j=0;j<this->C.size();j++){
      lval -= this->C[j]/(BOLTZMANN * T[i]) * pow(1.0 - field[i]/this->evapField, this->Cexp[j]);
    }
    evapRates.push_back(exp(lval));
  }

  return evapRates;
}

/*!
 * Method for calculating evaporation rate at a particular position
 * for a defined DeformedAhhreniusLawIsotropic instance.
 * @param field specimen surface field
 * @param T specimen surface temperature
 * @return evapRate calculated surface evaporation rate
 */
double DeformedAhhreniusLawIsotropic::evaporationRate(double field, double T){

  double lval = log(this->preFactor);
  if (this->d != 0.0) {
    for (int j=0;j<this->C.size();j++){
      lval += 1.0/this->d * log(1.0 - this->d * this->C[j]/(BOLTZMANN * T) * pow((1.0 - field/this->evapField), this->Cexp[j]));
    }
  }
  else{
    for (int j=0;j<this->C.size();j++){
      lval -= this->C[j]/(BOLTZMANN * T) * pow(1.0 - field/this->evapField, this->Cexp[j]);
    }
  }

  return exp(lval);

}

/*!
 * Method for calculating surface evaporation rates or a defined DeformedAhhreniusLawIsotropic instance.
 * @param field specimen surface fields
 * @param T specimen surface temperatures
 * @return evapRates calculated surface evaporation rates
 */
vector<double> DeformedAhhreniusLawIsotropic::evaporationRates(vector<double> field, vector<double> T){

  try {
    if (field.size() != T.size()){
      throw 1;
    }
  }
  catch (int n) {
      cerr << "passed electric field/temperature surface patches of differing lengths. Throwing exception." << endl;
      exit(1);
  }

  vector<double> evapRates;
  if (this->d != 0.0) {
      for (int i=0;i<field.size();i++){
        double lval = log(this->preFactor);
        for (int j=0;j<this->C.size();j++){
          lval += 1.0/this->d * log(1.0 - this->d * this->C[j]/(BOLTZMANN * T[i]) * pow((1.0 - field[i]/this->evapField), this->Cexp[j]));
        }
        evapRates.push_back(exp(lval));
      }
  }
  else{
    for (int i=0;i<field.size();i++){
      double lval = log(this->preFactor);
      for (int j=0;j<this->C.size();j++){
        lval -= this->C[j]/(BOLTZMANN * T[i]) * pow(1.0 - field[i]/this->evapField, this->Cexp[j]);
      }
      evapRates.push_back(exp(lval));
    }
  }

  return evapRates;
}

/*!
 * Method for calculating surface evaporation rates or a defined DeformedAhhreniusLawIsotropic instance.
 * @param field specimen surface fields
 * @param T specimen surface temperature
 * @return evapRates calculated surface evaporation rates
 */
vector<double> DeformedAhhreniusLawIsotropic::evaporationRates(vector<double> field, double T){

  vector<double> evapRates;
  if (this->d != 0.0) {
      for (int i=0;i<field.size();i++){
        double lval = log(this->preFactor);
        for (int j=0;j<this->C.size();j++){
          lval += 1.0/this->d * log(1.0 - this->d * this->C[j]/(BOLTZMANN * T) * pow((1.0 - field[i]/this->evapField), this->Cexp[j]));
        }
        evapRates.push_back(exp(lval));
      }
  }
  else{
    for (int i=0;i<field.size();i++){
      double lval = log(this->preFactor);
      for (int j=0;j<this->C.size();j++){
        lval -= this->C[j]/(BOLTZMANN * T) * pow(1.0 - field[i]/this->evapField, this->Cexp[j]);
      }
      evapRates.push_back(exp(lval));
    }
  }

  return evapRates;
}

/*!
 * Description:
 * Function simulating a simple algorithm for voltage ramping.
 * This function rescales the maximum surface ratio value of
 * the electric field and evaporation field to a fixed field
 * reduction factor. This is a very crude imitation of the true
 * voltage algorithm depending on a constant flux.
 *
 * Future developments:
 * Should aim to fix the total ion flux within the FOV based off numerical
 * optimisation of the evaporation rate equations (e.g. via Secants method).
 * However, this will likely require directly coupling the uniform ion
 * projection to the evaporation model to determine the changing FOV.
 *
 * @param fields vector of the surface panel electric fields
 * @param evapFields vector of the surface material evaporation fields
 * @param fieldReductionFactor The field reduction factor to be achieved by the algorithm
 * @return the rescaling to apply to the simulation voltage
 *
 */
double instantaneousSFVoltageRamp(vector<double> fields,
                                  vector<double> evapFields,
                                  double fieldReductionFactor){

  double fr = maximum(fields/evapFields);
  return fieldReductionFactor/fr;
}


/*!
 * Function simulating a simple algorithm for voltage ramping, fixing a maximum evaporation flux on the surface.
 * Finds the minimum root via Secant's method.
 * @param field The maximum surface field
 * @param T the maximum local temperature
 * @return the voltage rescaling term
 *
 */
 double AhhreniusLawIsotropic::instantaneousEvapFluxVoltageRamp(double field,
                                                                double T){

   return secantMethod(1000.0, 1100.0, field, T, 5e-4, 20, this);
 }

 /*!
  * Function simulating a simple algorithm for voltage ramping, fixing a maximum evaporation flux on the surface.
  * Finds the minimum root via Secant's method.
  * @param field The maximum surface field
  * @param T the maximum local temperature
  * @return the voltage rescaling term
  *
  */
double DeformedAhhreniusLawIsotropic::instantaneousEvapFluxVoltageRamp(double field,
                                                                       double T){

  return secantMethod(1000.0, 1100.0, field, T, 5e-4, 20, this);

}

double AhhreniusLawIsotropic::logEvaporationRate(double field, double T){

  double lval = log(this->preFactor);
  for (int j=0;j<this->C.size();j++){
    lval -= this->C[j]/(BOLTZMANN * T) * pow(1.0 - field/this->evapField, this->Cexp[j]);
  }

  return lval;

}

double DeformedAhhreniusLawIsotropic::logEvaporationRate(double field, double T){

  double lval = log(this->preFactor);
  if (this->d != 0.0) {
    for (int j=0;j<this->C.size();j++){
      lval += 1.0/this->d * log(1.0 - this->d * this->C[j]/(BOLTZMANN * T) * pow((1.0 - field/this->evapField), this->Cexp[j]));
    }
  }
  else{
    for (int j=0;j<this->C.size();j++){
      lval -= this->C[j]/(BOLTZMANN * T) * pow(1.0 - field/this->evapField, this->Cexp[j]);
    }
  }

  return lval;

}

double ahhreniusLaw(double field, double evapField, double beta, double A){
  double lval = log(A) - beta * (1.0 - field/evapField);
  return exp(lval);
}

vector<double> ahhreniusLaw(vector<double> field, vector<double> evapField, vector<double> beta, double A){

  try {
    if ((field.size() != evapField.size()) || (field.size() != beta.size())){
      throw 1;
    }
  }
  catch (int n) {
      cerr << "passed electric field/evap field/temperature values of differing lengths. Throwing exception." << endl;
      exit(1);
  }

  vector<double> evapRates;
  for (int i=0;i<field.size();i++){
    double lval = log(A) - beta[i] * (1.0 - field[i]/evapField[i]);
    evapRates.push_back(exp(lval));
  }
  return evapRates;
}

vector<double> ahhreniusLaw(vector<double> field, vector<double> evapField, double beta, double A){

  try {
    if (field.size() != evapField.size()){
      throw 1;
    }
  }
  catch (int n) {
      cerr << "passed electric field/evap field values of differing lengths. Throwing exception." << endl;
      exit(1);
  }

  vector<double> evapRates;
  for (int i=0;i<field.size();i++){
    double lval = log(A) - beta * (1.0 - field[i]/evapField[i]);
    evapRates.push_back(exp(lval));
  }
  return evapRates;
}
