# Evaporation laws library

Contains the source files required to build the library defining various model field evaporation laws. These laws are phase-specific, allowing a mix of the following laws to be defined within the same simulation. 

Currently support for the following evaporation laws are included: 

* A linear law - this has the form $A \cdot Q(\bold{E}(\bold{s}))$, where $A$ is a phase-specific parameter and $Q(\bold{E}(\bold{s}))$ is the activation barrier. 
* An Ahhrenius law - this has the form $A \exp{-\frac{1}{k_b T(\bold{s})} Q(\bold{E}(\bold{s}))}$, where $A$ is a phase-specific parameter, $k_B$ is the Boltzmann constant, $T(\bold{s})$ is the local surface temperature (determined by the simulation's [thermal model](../thermalModel)), and $Q(\bold{E}(\bold{s}))$ is the activation barrier.
* A deformed Ahhrenius law - A generalisation of the above Ahhrenius law that includes an additional deforming parameter $d$. In the literature, such a parameter is incorporated to include the effect of quantum tunnelling. This has the form $A (1 - \frac{d}{k_b T} Q(\bold{E}(\bold{s})))^{1/d}$, where $A$ is a phase-specific parameter, $k_B$ is the Boltzmann constant, $T$ is the local surface temperature), and $Q(\bold{E}(\bold{s}))$ is the activation barrier. In the limit of $d \rightarrow 0$, an Ahhrenius law is obtained. 
