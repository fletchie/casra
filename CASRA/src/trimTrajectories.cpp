/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <cstdio>
#include <tuple>

#if __has_include(<filesystem>)
  #include <filesystem>
#elif __has_include(<experimental/filesystem>)
  #include <experimental/filesystem>
  namespace std {
      namespace filesystem = experimental::filesystem;
  }
#endif

#include "CASRA/trimTrajectories.h"
#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/pointData.h"
#include "CASRA/specimenModels/numericalModel.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/chargedOptics/chargedOptics.h"
#include "CASRA/chargedOptics/hitmapCalculation.h"

using namespace std;

string defaultWriteDir = "output";

const char *helpSynonyms[] = {"-h", "-help", ""};
const char* supportedXMLConfigFileExt[] = {"xml", ""};

//initialise help strings for -h mode
string configFileArgHelp = "File location of model configuration file. \n\tSupported extensions: ";
string modeldirArgHelp = "Directory location for input specimen surface stack.";
string writeDirArgHelp = "Directory location to write calculated trajectory results.";

//define constants
#define NANOMETRECONV 1e-9
#define NANOMETREVOLCONV 1e-27

#define DETECTORRADIUSINFLATOR 1.5 //>sqrt(2)

/*!
 *
 * Function to calculate trajectories from an inputted model msim directory
 * @param configFile The simulation configuration file location
 * @param modelDir The model directory location
 * @param writeDir The output location to write trajectory results to (placed in 'writeDir/projection')
 * @return 0 if successful, 1 otherwise
 */
int trimTrajectories(ModelParams modelParams, OpticsParams opticsParams, string modelDir, string writeDir) {

  if (opticsParams.projectionType != "uniform"){
    cerr << "trajectoryTrimmer is only designed to work with <ionProjection type='uniform' .../>" << endl;
    cerr << "For realistic ion projection, only the trajectorySimulator program must be run (assuming a detector has been defined)." << endl;
    exit(1);
  }


  if (opticsParams.dparams.enabled == false){
    cerr << "ERROR: trajectoryTrimmer program requires the simulated detector to be specified within the model configuration file. This can be done through the following xml structure: " << endl;
    cerr << "'<ionProjection type='uniform' stop='30' cutoffDistance='20'>\n\t<detector type='planar' radius='80' position='(0.0, 0.0, 200.0)' normal='(-1.0, 1.0, 1.0)'/>\n\t<transferFunction type=\"linear\" kappa=\"0.8\"/>\n</ionProjection>" << endl;
    exit(1);
  }

  //create reference to loaded specimen model
  TMSpecimen& smodel = modelParams.sModel.specimenModel;

  //load specimen surface data
  vector<string> directories;
  vector<string> fdirectories;
  vector<string> projectionSortedDirectories;
  vector<int> fileorder;

  if (opticsParams.itStart > 1) {
    opticsParams.itStart -= 1;
    opticsParams.itStop -= 1;
  }

  //load in surface file locations from directory
  filesystem::directory_iterator end_itr;
  for (filesystem::directory_iterator itr(modelDir + "/projection/trajectories"); itr != end_itr;++itr){
        directories.push_back(itr->path().u8string());
  }

  //if trajectories directory exists, clear subdirectories
  if (filesystem::is_directory(writeDir + "/projection/trimmed_trajectories")){
    for (const auto& entry : filesystem::directory_iterator(writeDir + "/projection/trimmed_trajectories"))
         filesystem::remove_all(entry.path());
  }

  if (opticsParams.outputHitmap == true && (opticsParams.projectionType == "uniform")){
    //if trajectories directory exists, clear subdirectories
    if (filesystem::is_directory(writeDir + "/projection/hitmap")){
      for (const auto& entry : filesystem::directory_iterator(writeDir + "/projection/hitmap"))
           filesystem::remove_all(entry.path());
    }
  }

  if (opticsParams.outputMagnification == true && (opticsParams.projectionType == "uniform")){
    //if trajectories directory exists, clear subdirectories
    if (filesystem::is_directory(writeDir + "/projection/magnifications")){
      for (const auto& entry : filesystem::directory_iterator(writeDir + "/projection/magnifications"))
           filesystem::remove_all(entry.path());
    }
  }

  //reorder loaded surface file locations
  for (int p=0;p<directories.size();p++){
    string surfaceFileName = directories[p];
    reverse(surfaceFileName.begin(), surfaceFileName.end());
    int loc = surfaceFileName.find("_");
    if (loc == string::npos){
      cerr << "WARNING: file '" << directories[p] <<
              "' numbering incorrect. Ignoring file." << endl;
      continue;
    }
    string end = surfaceFileName.substr(0, loc);
    reverse(end.begin(), end.end());
    loc = end.find(".");
    if (loc == string::npos){
      cerr << "WARNING: file '" << directories[p] <<
              "' does not have suffix (cannot be identified as vtk). Ignoring file." << endl;
      continue;
    }
    string fileSuffix = end.substr(loc+1);
    if (fileSuffix != "vtk"){
      cerr << "WARNING: file '" << directories[p] <<
      "' identified as not having .vtk suffix. Ignoring file." << endl;
      continue;
    }

    string surfaceNum = end.substr(0, loc);

    fdirectories.push_back(directories[p]);
    fileorder.push_back(stoi(surfaceNum));
  }

  vector<int> directoryFileInds(fdirectories.size(), 0);
  iota(directoryFileInds.begin(),directoryFileInds.end(),0);
  sort(directoryFileInds.begin(), directoryFileInds.end(), [&](int i,int j){return fileorder[i] < fileorder[j];});

  for (int i=0;i<directoryFileInds.size();i++){
    projectionSortedDirectories.push_back(fdirectories[directoryFileInds[i]]);
  }

  vector<string> trajectoriesNames;
  vector<string> trajectoriesTimes;

  cout << "read and sorted model surface files" << endl;

  double relTol = 1e-4;

  //set to true to initially clear file and write mapping tsv header first time mapping tsv write is called
  bool mappingHeader = true;

  TriangularMesh prevSurface;

  int xnum = 10;
  int ynum = 10;

  vector<vector<Point>> mappingVisualisation(xnum * ynum, vector<Point>(0));

  int projectionIt = 0;

  for (int p=0;p<projectionSortedDirectories.size();p++){

    //construct corresponding surface mesh file location
    string surfaceLocation = projectionSortedDirectories[p];
    reverse(surfaceLocation.begin(), surfaceLocation.end());
    string surfaceSuffix = surfaceLocation.substr(0, surfaceLocation.find("_"));
    reverse(surfaceSuffix.begin(), surfaceSuffix.end());
    string surfaceStateLocation = "./msim/surfaces/surface_" + surfaceSuffix;

    string surfaceIndex = surfaceSuffix.substr(0, surfaceSuffix.find("."));
    if (stoi(surfaceIndex) > opticsParams.itStop){
      break;
    }

    cout << "Performing trajectory postprocessing for trajectory iteration: " << p+1 << endl;
    vtkObject in1;
    vtkObject in2;

    cout << projectionSortedDirectories[p] << endl;
    //read in the ion trajectory data to be trimmed
    readVTKUnstructuredGrid(in1, projectionSortedDirectories[p]);
    tuple<vector<vector<Point>>,
                 vector<vector<Point>>,
                 vector<long>,
                 vector<int>> trajectoryRes = in1.lineSegmentMeshFromVTK();

    //read in corresponding surface mesh at which the projection is performed
    cout << surfaceStateLocation << endl;
    readVTKUnstructuredGrid(in2, surfaceStateLocation);
    TriangularMesh surfaceMesh = in2.triangularMeshFromVtk();

    if (surfaceMesh.vertices.size() == 0){
      cerr << "Specimen surface: '" << surfaceStateLocation << "' cannot be loaded. Skipping." << endl;
      continue;
    }

    //construct averaged normal at vertex based off neighbouring panels
    surfaceMesh.averageVertexNormals();
    surfaceMesh.vertexFaceAdjacencyList = surfaceMesh.buildVertexFaceAdjacencyList();

    vector<vector<Point>> trajectories = get<0>(trajectoryRes);
    vector<vector<Point>> velocities = get<1>(trajectoryRes);
    vector<long> launchSites = get<2>(trajectoryRes);
    vector<int> launchPhases = get<3>(trajectoryRes);

    Detector detector(opticsParams.dparams.position,
                      opticsParams.dparams.normal,
                      opticsParams.dparams.radius * DETECTORRADIUSINFLATOR);

    vector<Ion> launchedIons;
    vector<Ion> filteredIons;
    vector<long> filteredLaunchSites;

    for (int i=0;i<trajectories.size();i++){
      Ion launchedIon;
      launchedIon.trajectory = trajectories[i];
      launchedIon.velocity = velocities[i];
      launchedIon.launchPos = trajectories[i][0];
      launchedIon.finalPos = trajectories[i].back();
      launchedIon.phase = smodel.phases[launchPhases[i]].name;
      launchedIon.phaseIndex = launchPhases[i];
      launchedIons.push_back(launchedIon);
    }

    bool transfer = opticsParams.tparams.enabled;
    double kappa;

    if (opticsParams.tparams.functionParameters.size() > 0){
      kappa = opticsParams.tparams.functionParameters[0];
    }

    int intersections = 0;
    for (int i=0;i<launchedIons.size();i++){
      bool intersects = detector.calculatedTrajectoryIntersectionPoint(launchedIons[i]);
      if (intersects == true){
        filteredIons.push_back(launchedIons[i]);
        filteredLaunchSites.push_back(launchSites[i]);
        intersections++;
      }
      else if (transfer == true) {
        Vec ionVelocity(launchedIons[i].velocity.back().px, launchedIons[i].velocity.back().py, launchedIons[i].velocity.back().pz);
        bool intersects = detector.linearProjection(launchedIons[i].trajectory.back(), ionVelocity, kappa);
        if (intersects == true){
          filteredIons.push_back(launchedIons[i]);
          filteredLaunchSites.push_back(launchSites[i]);
          intersections++;
        }
      }
    }

    vector<double> evapVols(filteredLaunchSites.size());
    if (prevSurface.vertices.size() > 0){
      evapVols = surfaceMesh.distanceFromSurface(prevSurface, filteredLaunchSites);
    }

    if (intersections == 0){
      cerr << "\tNo intersections found with detector. If cutoff distance set " <<
              "lower than flightpath (detector position z-coord), then set higher or " <<
              "define a transfer function." << endl;
    }

    //final step is to load surface meshes, and calculate resulting panel magnification
    if (opticsParams.outputTrajectories == true){

      vtkObject out;

      for (int t=0;t<filteredIons.size();t++){
        Point dPoint = detector.convertLocalcoordinate(filteredIons[t].trajectory.back());
        if (sqrt(dPoint.px * dPoint.px + dPoint.py * dPoint.py) <= opticsParams.dparams.radius){
          out.addTrajectory(filteredIons[t].trajectory, filteredIons[t].velocity, filteredLaunchSites[t], evapVols[t]);
        }
      }

      string projLoc = projectionSortedDirectories[p];
      reverse(projLoc.begin(), projLoc.end());
      string trajectorySuffix = projLoc.substr(0, projLoc.find("_"));
      reverse(trajectorySuffix.begin(), trajectorySuffix.end());

      writeVtk(out, writeDir + "/projection/trimmed_trajectories/trimmed_trajectories_" + trajectorySuffix);
      trajectoriesNames.push_back("trimmed_trajectories/trimmed_trajectories_" + trajectorySuffix);
      trajectoriesTimes.push_back(to_string(p+1));

      cout << "Writing trajectories series file to: " << "projection/trimmed_trajectories.vtk.series" << endl;
      writeVtkSeries(writeDir + "/projection/trimmed_trajectories.vtk.series", trajectoriesNames, trajectoriesTimes);
    }

    if ((opticsParams.outputMapping == true || opticsParams.outputHitmap) && (opticsParams.projectionType == "uniform")){

      int num = stoi(surfaceSuffix.substr(0, surfaceSuffix.find(".")));

      string mappingLoc = writeDir + "/reconstruction/mapping.tsv";

      TsvObject trajectoryMapping;

      trajectoryMapping.columnOrder.push_back("iteration");
      trajectoryMapping.columnOrder.push_back("face_index");
      trajectoryMapping.columnOrder.push_back("c1px");
      trajectoryMapping.columnOrder.push_back("c1py");
      trajectoryMapping.columnOrder.push_back("c1pz");
      trajectoryMapping.columnOrder.push_back("c1dx");
      trajectoryMapping.columnOrder.push_back("c1dy");
      trajectoryMapping.columnOrder.push_back("c1dz");
      trajectoryMapping.columnOrder.push_back("c1ph");
      trajectoryMapping.columnOrder.push_back("c2px");
      trajectoryMapping.columnOrder.push_back("c2py");
      trajectoryMapping.columnOrder.push_back("c2pz");
      trajectoryMapping.columnOrder.push_back("c2dx");
      trajectoryMapping.columnOrder.push_back("c2dy");
      trajectoryMapping.columnOrder.push_back("c2dz");
      trajectoryMapping.columnOrder.push_back("c2ph");
      trajectoryMapping.columnOrder.push_back("c3px");
      trajectoryMapping.columnOrder.push_back("c3py");
      trajectoryMapping.columnOrder.push_back("c3pz");
      trajectoryMapping.columnOrder.push_back("c3dx");
      trajectoryMapping.columnOrder.push_back("c3dy");
      trajectoryMapping.columnOrder.push_back("c3dz");
      trajectoryMapping.columnOrder.push_back("c3ph");
      trajectoryMapping.columnOrder.push_back("magnification");
      trajectoryMapping.columnOrder.push_back("evap_rate");

      map<vector<long>, bool> surfaceFaceFlipMap;
      for (int i=0;i<surfaceMesh.faces.size();i++){
        vector<long> entry = {surfaceMesh.faces[i].p1, surfaceMesh.faces[i].p2, surfaceMesh.faces[i].p3};
        vector<long> order = {surfaceMesh.faces[i].p1, surfaceMesh.faces[i].p2, surfaceMesh.faces[i].p3};
        sort(entry.begin(), entry.end());
        bool flip = true;
        if ((order[0] == entry[0]) && (order[1] == entry[1]) && (order[2] == entry[2]))
          flip = false;
        if ((order[0] == entry[1]) && (order[1] == entry[2]) && (order[2] == entry[0]))
          flip = false;
        if ((order[0] == entry[2]) && (order[1] == entry[0]) && (order[2] == entry[1]))
          flip = false;
        surfaceFaceFlipMap[entry] = flip;
      }

      map<int, vector<int>> candidateFaceMap;
      //local magnification
      for (int i=0;i<filteredLaunchSites.size();i++){
        for (int j=0;j<surfaceMesh.vertexFaceAdjacencyList[filteredLaunchSites[i]].size();j++){
          int faceCandidate = surfaceMesh.vertexFaceAdjacencyList[filteredLaunchSites[i]][j];
          candidateFaceMap[faceCandidate].push_back(i);
        }
      }

      vector<double> magnifications;
      vector<double> evapRates;
      vector<Point> ion1SamplePos;
      vector<Point> ion1DetectorPos;
      vector<Point> ion2SamplePos;
      vector<Point> ion2DetectorPos;
      vector<Point> ion3SamplePos;
      vector<Point> ion3DetectorPos;
      vector<int> ion1Phase;
      vector<int> ion2Phase;
      vector<int> ion3Phase;

      vtkObject outMag;

      PointDataset pointMeshDetector;
      TriangularMesh triangularMeshDetector;
      PointData pd1;
      pd1.key = "magnification";
      PointData pd2;
      pd2.key = "J00(dx/sx)";
      PointData pd3;
      pd3.key = "J01(dx/sy)";
      PointData pd4;
      pd4.key = "J10(dy/sx)";
      PointData pd5;
      pd5.key = "J11(dy/sy)";

      TriangularMesh triangularMeshSample;
      CellData cd1;
      cd1.key = "magnification";
      CellData cd2;
      cd2.key = "J00(dx/sx)";
      CellData cd3;
      cd3.key = "J01(dx/sy)";
      CellData cd4;
      cd4.key = "J10(dy/sx)";
      CellData cd5;
      cd5.key = "J11(dy/sy)";

      long ic = 0;

      for (map<int, vector<int>>::iterator it = candidateFaceMap.begin(); it != candidateFaceMap.end(); it++){

        //only include panels that intersect detector
        if (it->second.size() == 3){

          vector<long> entry = {filteredLaunchSites[it->second[0]],
                                filteredLaunchSites[it->second[1]],
                                filteredLaunchSites[it->second[2]]};
          sort(entry.begin(), entry.end());
          bool flip = surfaceFaceFlipMap[entry];

          Ion ion1 = filteredIons[it->second[0]];
          Ion ion2 = filteredIons[it->second[1]];
          Ion ion3 = filteredIons[it->second[2]];
          if (flip == true){
            ion2 = filteredIons[it->second[0]];
            ion1 = filteredIons[it->second[1]];
          }

          //convert to local x-y coordinates on detector
          Point ion1DetPos = detector.convertLocalcoordinate(ion1.trajectory.back());
          Point ion2DetPos = detector.convertLocalcoordinate(ion2.trajectory.back());
          Point ion3DetPos = detector.convertLocalcoordinate(ion3.trajectory.back());

          for (int mx=0;mx<10;mx++){
            for (int my=0;my<10;my++){

            double intParamX = ((double)mx)/(10-1);
            double intParamY = ((double)my)/(10-1);

              Point mPos(-opticsParams.dparams.radius * (1.0 - intParamX) + opticsParams.dparams.radius * intParamX,
                         - opticsParams.dparams.radius * (1.0 - intParamY) + opticsParams.dparams.radius * intParamY,
                         0.0);
              if ((mPos.px*mPos.px + mPos.py*mPos.py) < (opticsParams.dparams.radius * opticsParams.dparams.radius)){
                double u, v, w;
                findBarycentricCoordinates2D(mPos, ion1DetPos, ion2DetPos, ion3DetPos, u, v, w);

                if ((u <= 1.0) && (u >= 0.0) && (w <= 1.0) && (w >= 0.0) && (v <= 1.0) && (v >= 0.0)){
                  Point interpolatedLaunchPos = ion1.launchPos * u + ion2.launchPos * v + ion3.launchPos * w;
                  if (mappingVisualisation[mx*10+my].size() <= projectionIt){
                    mappingVisualisation[mx*10+my].push_back(interpolatedLaunchPos);
                  }
                }
              }
            }
          }

          ion1SamplePos.push_back(ion1.launchPos);
          ion2SamplePos.push_back(ion2.launchPos);
          ion3SamplePos.push_back(ion3.launchPos);
          ion1DetectorPos.push_back(ion1DetPos);
          ion2DetectorPos.push_back(ion2DetPos);
          ion3DetectorPos.push_back(ion3DetPos);
          ion1Phase.push_back(ion1.phaseIndex);
          ion2Phase.push_back(ion2.phaseIndex);
          ion3Phase.push_back(ion3.phaseIndex);

          tuple<double,
                double,
                double,
                double,
                double> magnificationResults = calculateLocalMagnification(ion1.launchPos,
                                                                           ion2.launchPos,
                                                                           ion3.launchPos,
                                                                           ion1DetPos,
                                                                           ion2DetPos,
                                                                           ion3DetPos);

          double magnification = -get<0>(magnificationResults);
          magnifications.push_back(magnification);

          double evapRate = (evapVols[it->second[0]] + evapVols[it->second[1]] + evapVols[it->second[2]])/3.0;
          evapRates.push_back(evapRate);

          Point panelCentre((ion1DetPos.px + ion2DetPos.px + ion3DetPos.px)/3.0,
                            (ion1DetPos.py + ion2DetPos.py + ion3DetPos.py)/3.0,
                            (ion1DetPos.pz + ion2DetPos.pz + ion3DetPos.pz)/3.0);

          pointMeshDetector.points.push_back(panelCentre);
          triangularMeshSample.vertices.push_back(ion1.launchPos);
          triangularMeshSample.vertices.push_back(ion2.launchPos);
          triangularMeshSample.vertices.push_back(ion3.launchPos);
          triangularMeshDetector.vertices.push_back(ion1DetPos);
          triangularMeshDetector.vertices.push_back(ion2DetPos);
          triangularMeshDetector.vertices.push_back(ion3DetPos);
          Face sface(ic, ic+1, ic+2);
          triangularMeshSample.faces.push_back(sface);
          triangularMeshDetector.faces.push_back(sface);
          ic += 3;

          pd1.values.push_back(get<0>(magnificationResults));
          pd2.values.push_back(-get<1>(magnificationResults));
          pd3.values.push_back(-get<2>(magnificationResults));
          pd4.values.push_back(-get<3>(magnificationResults));
          pd5.values.push_back(-get<4>(magnificationResults));

          cd1.values.push_back({get<0>(magnificationResults)});
          cd2.values.push_back({-get<1>(magnificationResults)});
          cd3.values.push_back({-get<2>(magnificationResults)});
          cd4.values.push_back({-get<3>(magnificationResults)});
          cd5.values.push_back({-get<4>(magnificationResults)});

          trajectoryMapping.outInts[0].push_back(num);
          trajectoryMapping.outInts[1].push_back(it->first);
          trajectoryMapping.outDoubles[2].push_back(ion1.launchPos.px);
          trajectoryMapping.outDoubles[3].push_back(ion1.launchPos.py);
          trajectoryMapping.outDoubles[4].push_back(ion1.launchPos.pz);
          trajectoryMapping.outDoubles[5].push_back(ion1DetPos.px);
          trajectoryMapping.outDoubles[6].push_back(ion1DetPos.py);
          trajectoryMapping.outDoubles[7].push_back(ion1DetPos.pz);
          trajectoryMapping.outStrings[8].push_back(ion1.phase);
          trajectoryMapping.outDoubles[9].push_back(ion2.launchPos.px);
          trajectoryMapping.outDoubles[10].push_back(ion2.launchPos.py);
          trajectoryMapping.outDoubles[11].push_back(ion2.launchPos.pz);
          trajectoryMapping.outDoubles[12].push_back(ion2DetPos.px);
          trajectoryMapping.outDoubles[13].push_back(ion2DetPos.py);
          trajectoryMapping.outDoubles[14].push_back(ion2DetPos.pz);
          trajectoryMapping.outStrings[15].push_back(ion2.phase);
          trajectoryMapping.outDoubles[16].push_back(ion3.launchPos.px);
          trajectoryMapping.outDoubles[17].push_back(ion3.launchPos.py);
          trajectoryMapping.outDoubles[18].push_back(ion3.launchPos.pz);
          trajectoryMapping.outDoubles[19].push_back(ion3DetPos.px);
          trajectoryMapping.outDoubles[20].push_back(ion3DetPos.py);
          trajectoryMapping.outDoubles[21].push_back(ion3DetPos.pz);
          trajectoryMapping.outStrings[22].push_back(ion3.phase);
          trajectoryMapping.outDoubles[23].push_back(magnification);
          trajectoryMapping.outDoubles[24].push_back(evapRate);
        }
      }

      if (opticsParams.outputMagnification == true){

        triangularMeshDetector.removeDuplicateVertices();
        outMag.setWithTriangularMesh(triangularMeshDetector);
        outMag.cellData.push_back(cd1);
        outMag.cellData.push_back(cd2);
        outMag.cellData.push_back(cd3);
        outMag.cellData.push_back(cd4);
        outMag.cellData.push_back(cd5);

        writeVtk(outMag, writeDir + "/projection/magnifications/detector/detector_magnification" + surfaceSuffix);

        triangularMeshSample.removeDuplicateVertices();
        outMag.setWithTriangularMesh(triangularMeshSample);
        outMag.cellData.push_back(cd1);
        outMag.cellData.push_back(cd2);
        outMag.cellData.push_back(cd3);
        outMag.cellData.push_back(cd4);
        outMag.cellData.push_back(cd5);
        writeVtk(outMag, writeDir + "/projection/magnifications/surface/surface_magnification" + surfaceSuffix);
      }

      if (opticsParams.outputMapping == true){
        trajectoryMapping.writeTsv(mappingLoc, mappingHeader);
        cout << "writing to trajectory mapping" << endl;
      }

      mappingHeader = false;
      cout << "calculating hitmap" << endl;

      int xres = opticsParams.dparams.xCells;
      int yres = opticsParams.dparams.yCells;

      if (opticsParams.outputHitmap == true){

        tuple<vector<vector<vector<double>>>,
        vector<vector<vector<double>>>> hitmapRes = calculateHitmap(ion1SamplePos, ion1DetectorPos,
                                                                    ion2SamplePos, ion2DetectorPos,
                                                                    ion3SamplePos, ion3DetectorPos,
                                                                    evapRates, magnifications,
                                                                    opticsParams.dparams.radius,
                                                                    xres, yres);

        vector<vector<vector<double>>> densityHitmap = get<0>(hitmapRes);
        vector<vector<vector<double>>> crossoverHitmap = get<1>(hitmapRes);

        vtkObject hitmapOut;
        double hitmapCellWidthX = opticsParams.dparams.radius * 2.0/xres;
        double hitmapCellWidthY = opticsParams.dparams.radius * 2.0/yres;

        Point bottomLeftHitmapcorner(-opticsParams.dparams.radius + hitmapCellWidthX/2.0,
                                     -opticsParams.dparams.radius + hitmapCellWidthY/2.0,
                                     0.0);

        hitmapOut.setWithField(densityHitmap, bottomLeftHitmapcorner, hitmapCellWidthX, hitmapCellWidthY, 1.0);
        writeVtk(hitmapOut, writeDir + "/projection/hitmap/density/density_hitmap" + surfaceSuffix);

        hitmapOut.setWithField(crossoverHitmap, bottomLeftHitmapcorner, hitmapCellWidthX, hitmapCellWidthY, 1.0);
        writeVtk(hitmapOut, writeDir + "/projection/hitmap/crossover/crossover_hitmap" + surfaceSuffix);

        vector<vector<vector<double>>> phaseMap = calculatePhaseMap(ion1SamplePos,
                                                                    ion1DetectorPos,
                                                                    ion2SamplePos,
                                                                    ion2DetectorPos,
                                                                    ion3SamplePos,
                                                                    ion3DetectorPos,
                                                                    ion1Phase,
                                                                    ion2Phase,
                                                                    ion3Phase,
                                                                    evapRates,
                                                                    magnifications,
                                                                    smodel,
                                                                    opticsParams.dparams.radius,
                                                                    xres, yres);

        hitmapOut.setWithField(phaseMap, bottomLeftHitmapcorner, hitmapCellWidthX, hitmapCellWidthY, 1.0);
        writeVtk(hitmapOut, writeDir + "/projection/hitmap/phase/phasemap" + surfaceSuffix);

        cout << "calculation complete" << endl;

      }

      prevSurface = surfaceMesh;
      projectionIt++;
    }
  }

  if ((opticsParams.outputMapping == true) && (opticsParams.projectionType == "uniform")){

    cout << "outputting detector mapping visualisation (bijective assumption)..." << endl;

    vtkObject mappingVisOut;
    for (int m=0;m<mappingVisualisation.size();m++){
      mappingVisOut.addTrajectory(mappingVisualisation[m]);
    }
    writeVtk(mappingVisOut, writeDir + "/projection/mappingVisualisation.vtk");

    cout << "detector mapping visualisation written to : '" << writeDir << "/projection/mappingVisualisation.vtk'" << endl;

  }

  return 0;

}

/*!
 *
 * Function called on running CASRA simulated evaporation tool from the command line
 * takes command line arguments
 */
int main(int argc, char* argv[])
{

  cout << "Welcome to CASRA's postprocessing trajectory trimmer program. " <<
          "\nRun `trajectoryTrimmer -h` for help." << endl;

  int ret = 0;

  char modelDirArg[] = "--model";

  string modelConfigFile = "";
  string modelDir = "";
  string writeDir = "";

  string argSymbol = "--";
  string argInputVal;

  bool helpEnabled = false;

  //if no input detected
  if (argc == 1) {
    modelDir = ".";
  }
  else if (argc == 2) { //check if help passed
    int a = 0;
    while (strlen(helpSynonyms[a]) != 0) {
      if (strcmp(argv[1], helpSynonyms[a]) == 0) {
        helpEnabled = true;
        break;
      }
      a ++;
    }
  }
  else {
    if (!(argc % 2)) {
      cout << "ERROR: Incomplete number of inputs detected. Please check all arguments are defined." << endl;
      return 1;
    }
    else {
      for (int i=1; i < argc; i++){
        if (i % 2 == 1) {
          argInputVal = (string)argv[i];
          if (argSymbol == argInputVal.substr(0, 2)) {
            //command line input argument detected
            if (strcmp(modelDirArg, argv[i]) == 0) {
              cout << "Supplied argument: " << modelDirArg << " " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                modelDir = (string)argv[i+1];
              }
              else {
                modelDir = ".";
              }
            }
            else {
              cerr << "ERROR: Unidentified input argument name detected. Please check input." << endl;
              return 1;
            }
          }
          else {
            cerr << "ERROR: Input argument name " << argv[i] <<
                    " not correctly marked as a command line argument. Please check input." << endl;
            return 1;
          }
        }
      }
    }
  }

  if (modelDir == "") {
    modelDir = ".";
  }

  if (helpEnabled == false){
    //read in model parameters and ion projection parameters
    ModelParams modelParams = initialiseModelFromXML(modelDir + "/configuration.xml");
    OpticsParams opticsParams = initialiseModelOpticsFromXML(modelDir + "/configuration.xml");

    //run charged optics simulator
    ret = trimTrajectories(modelParams, opticsParams, modelDir, modelDir);

    if (ret > 0) {
      cout << "ERROR: Execution failed." << endl;
    }
    else {
      cout << "Execution succeeded." << endl;
    }
  }

  if (helpEnabled == true) {

    //Assemble model configuration file help string to append supported range file extension types
    int h = 0;
    while (strlen(supportedXMLConfigFileExt[h]) != 0) {
      configFileArgHelp = configFileArgHelp + (string)supportedXMLConfigFileExt[h] + ",";
      h++;
    }
    configFileArgHelp.erase(configFileArgHelp.end()-1,configFileArgHelp.end());

    //Assemble write directory configuration file help string
    writeDirArgHelp = writeDirArgHelp;

    //output help message
    cout << "Valid input arguments: \n"
         << modelDirArg << " : " << modeldirArgHelp << endl;
  }

  return ret;

}
