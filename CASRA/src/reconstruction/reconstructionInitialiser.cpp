/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <vector>
#include <regex>
#include <sstream>
#include <any>

#include "rapidxml/rapidxml_utils.hpp"
#include "CASRA/reconstruction/reconstructionInitialiser.h"

using namespace std;

const char* supportedXMLConfigFileExt[] = {"xml", ""};

string defaultSpecimenType = "ShankBasEmitterModel";
string defaultApexPosition = "0.0, 0.0, 0.0";
double defaultInitialRadius = 25.0e-9;
double defaultBulkEvapField = 20.0e9;
string defaultProjectionType = "bas";
double defaultShankAngle = 0.0;
double defaultKFactor = 2.0;
double defaultImageCompression = 1.65;

double defaultInstrumentRadius = 0.038;
double defaultInstrumentFlightpath = 0.102;
double defaultInstrumentEfficiency = 0.5;

pointProjectionReconstructionParams::pointProjectionReconstructionParams(){

}

modelDrivenReconstructionParams::modelDrivenReconstructionParams(){

}

/*!
 * Function for reading header of reconConfiguration.xml to determine the reconstruction
 * mode (e.g. whether pointProjectionReconstruction or modelDrivenReconstruction).
 * @param cfname The input reconstruction configuration filename
 * @return the reconstruction type/mode name
 */
string reconType(string cfname){

  char * charcfname = new char[cfname.length() + 1];
  strcpy(charcfname, cfname.c_str());

  //load the xml file using the rapidxml library
  rapidxml::file<> xmlFile(charcfname);
  rapidxml::xml_document<> doc;
  doc.parse<0>(xmlFile.data());

  string reconstructionType = string(doc.first_node()->name());
  return reconstructionType;

}

//to modify instance attributes directly must pass instance by reference (as done here)

/*!
 * Function for initialising the point projection reconstruction protocol.
 * This function reads xml parameter configuration files and sets the passed
 * `pointProjectionReconstructionParams` object instance.
 * This function makes use of the rapidxml
 * library for parsing the xml file.
 *
 * @param cfname The configuration file filename.
 * @return pointProjectionReconstructionParams the point projection reconstruction parameters instance
 *
 */
pointProjectionReconstructionParams pointProjectionReconstructionInitialiser(string cfname) {

    pointProjectionReconstructionParams reconparams;

    char * charcfname = new char[cfname.length() + 1];
    strcpy(charcfname, cfname.c_str());

    //load the xml file using the rapidxml library
    rapidxml::file<> xmlFile(charcfname);
    rapidxml::xml_document<> doc;
    doc.parse<0>(xmlFile.data());

    rapidxml::xml_node<> *root = nullptr;
    rapidxml::xml_node<> *modelNode = nullptr;
    rapidxml::xml_node<> *instrumentNode = nullptr;
    rapidxml::xml_node<> *projectionModelNode = nullptr;
    rapidxml::xml_node<> *imageCompressionNode = nullptr;
    rapidxml::xml_node<> *shankModeNode = nullptr;
    rapidxml::xml_node<> *VoltageModeNode = nullptr;
    rapidxml::xml_node<> *tempPointNode = nullptr;

    root = doc.first_node("pointProjectionReconstruction");
    if (root == nullptr){
      cerr << "ERROR: xml file not defined as a pointProjectionReconstruction protocol initialiser."<< endl;
      exit(1);
    }
    //load specimen model configuration parameters
    if (root->first_node("specimenModel") != nullptr){
      rapidxml::xml_node<> *modelNode = root->first_node("specimenModel");
      if (modelNode->first_attribute("type") != nullptr)
        reconparams.sModel.specimenType = modelNode->first_attribute("type")->value();
      else {
        cerr << "Cannot find model type. Assuming a shank reconstruction with attribute type='" << defaultSpecimenType << "'." << endl;
        reconparams.sModel.specimenType = defaultSpecimenType;
      }
      if (modelNode->first_node("initialState") != nullptr){
        rapidxml::xml_node<> *initialStateNode = modelNode->first_node("initialState");
        if (reconparams.sModel.specimenType == "shankBasEmitterModel"){
          reconparams.sModel.R0 = stod(initialStateNode->first_attribute("R0")->value());
        }
        reconparams.sModel.initialPosition = extractPoint(initialStateNode->first_attribute("apexPosition")->value());
      }
      else {
        cerr << "Cannot find initial state node. Assuming an initial state of R0='" <<
                 defaultInitialRadius << "' and apexPosition='" << defaultApexPosition << "'" << endl;
        reconparams.sModel.R0 = defaultInitialRadius;
        reconparams.sModel.initialPosition = extractPoint(defaultApexPosition);
      }
      if (reconparams.sModel.specimenType == "voltageBasEmitterModel"){
        if (modelNode->first_node("physics") != nullptr){
          rapidxml::xml_node<> *physicsNode = modelNode->first_node("physics");
          reconparams.sModel.evapField = stod(physicsNode->first_attribute("bulkEvapField")->value());
          cout << "field: " << reconparams.sModel.evapField  << endl;
        }
        else {
          cerr << "Cannot find physics node. Assuming an initial bulk evaporation field of bulkEvapField='" << defaultBulkEvapField << "'Vm^{-1}"  << endl;
          reconparams.sModel.evapField = defaultBulkEvapField;
        }
      }

      //load shank angle info from specimen model configuration parameters
      if (reconparams.sModel.specimenType == "shankBasEmitterModel") {
        cout << "\tShank reconstruction mode selected." << endl;
        shankModeNode = modelNode->first_node("shankModeParameters");
        if (shankModeNode == nullptr){
          cerr << "No 'shankModeParameters' node specifying the shank geometry parameters found. Using the default shank angle for whole reconstruction of angle='" << defaultShankAngle << "' degrees" << endl;
          ShankTempPoint tp(0, defaultShankAngle);
          reconparams.sModel.shank.push_back(tp);
        }
        else{
          for (tempPointNode = shankModeNode->first_node(); tempPointNode != NULL; tempPointNode = tempPointNode->next_sibling())
          {
            long eventIndex = stol(tempPointNode->first_attribute("eventIndex")->value());
            double shankAngle = stod(tempPointNode->first_attribute("angle")->value()) * PI/180.0;
            ShankTempPoint tp(eventIndex, shankAngle);
            reconparams.sModel.shank.push_back(tp);
          }
          if (reconparams.sModel.shank.size() == 0){
            cerr << "No shank mode subnodes found. Using default shank angle for whole reconstruction of angle='" << defaultShankAngle << "' degrees" << endl;
            ShankTempPoint tp(0, defaultShankAngle);
            reconparams.sModel.shank.push_back(tp);
          }
        }
        if (reconparams.sModel.shank.size() == 1){
          reconparams.sModel.shank[0].eventIndex = 0;
        }
      }
      else if (reconparams.sModel.specimenType == "voltageBasEmitterModel") {
        VoltageModeNode = modelNode->first_node("voltageModeParameters");
        for (tempPointNode = VoltageModeNode->first_node(); tempPointNode != NULL; tempPointNode = tempPointNode->next_sibling())
        {
          long eventIndex = stol(tempPointNode->first_attribute("eventIndex")->value());
          double kFactor = stod(tempPointNode->first_attribute("k")->value());
          VoltageTempPoint tp(eventIndex, kFactor);
          reconparams.sModel.kFactors.push_back(tp);
        }
        if (reconparams.sModel.kFactors.size() == 0){
          cerr << "No voltage mode subnodes found. Using default k-factor for whole reconstruction of k='" << defaultKFactor << "'" << endl;
          VoltageTempPoint tp(0, defaultKFactor);
          reconparams.sModel.kFactors.push_back(tp);
        }
        if (reconparams.sModel.kFactors.size() == 1){
          reconparams.sModel.kFactors[0].eventIndex = 0;
        }
      }
    }
    else {
      cerr << "ERROR: No specimen model found. Please specify this node to continue with the reconstruction." << endl;
      exit(1);
    }

    //load instrument configuration parameters
    if (root->first_node("instrument") != nullptr){
      instrumentNode = root->first_node("instrument");
      reconparams.iModel.detectorRadius = stod(instrumentNode->first_attribute("radius")->value());
      reconparams.iModel.efficiency = stod(instrumentNode->first_attribute("efficiency")->value());
      reconparams.iModel.flightpath = stod(instrumentNode->first_attribute("flightpath")->value());
      cout << "Atom probe instrument geometry specified: detector radius = " << reconparams.iModel.detectorRadius <<
              ", flightpath = " << reconparams.iModel.flightpath << ", detection efficiency = " << reconparams.iModel.efficiency << endl;
    }
    else{
      cerr << "Cannot find an instrument node in the xml pointProjectionReconstruction configuration file." <<
              " Using the default values matching the LAWATAP of " <<
              "<instrument radius='" << defaultInstrumentRadius << "' flightpath='" << defaultInstrumentFlightpath <<
                "' efficiency='" << defaultInstrumentEfficiency << "'/>";
      reconparams.iModel.detectorRadius = defaultInstrumentRadius;
      reconparams.iModel.flightpath = defaultInstrumentFlightpath;
      reconparams.iModel.efficiency = defaultInstrumentEfficiency;
    }

    //load projection configuration parameters
    if (root->first_node("projection") != nullptr){
      projectionModelNode = root->first_node("projection");
      if (projectionModelNode->first_attribute("type") != nullptr){
        reconparams.pModel.projectionType = projectionModelNode->first_attribute("type")->value();
        cout << "\t'" << reconparams.pModel.projectionType << "' mathematical projection model selected." << endl;

      }
      else {
        cerr << "No projection model identified. Using default option of type='" << defaultProjectionType << "'" << endl;
        reconparams.pModel.projectionType = defaultProjectionType;
      }

      imageCompressionNode = projectionModelNode->first_node("imageCompressionParameters");
      for (tempPointNode = imageCompressionNode->first_node(); tempPointNode != NULL; tempPointNode = tempPointNode->next_sibling())
      {
        long tempIon = stol(tempPointNode->first_attribute("eventIndex")->value());
        double tempICF = stod(tempPointNode->first_attribute("ICF")->value());
        ICFTempPoint tp(tempIon, tempICF);
        reconparams.pModel.imagecompression.push_back(tp);
      }
      if (reconparams.pModel.imagecompression.size() == 0){
        cerr << "No image compression subnodes found. Using default image compression for whole reconstruction of ICF='" << defaultImageCompression << "'" << endl;
        ICFTempPoint tp(0, defaultImageCompression);
        reconparams.pModel.imagecompression.push_back(tp);
      }
      if (reconparams.pModel.imagecompression.size() == 1){
        reconparams.pModel.imagecompression[0].eventIndex = 0;
      }

    }
    else {
      cerr << "ERROR: No projection node specified. Please specify a mathematical projection model to continue." << endl;
      exit(1);
    }

    delete[] charcfname;

    return reconparams;
}

/*!
 * Function for initialising the model driven reconstruction protocol.
 * This function reads xml parameter configuration files and sets the passed
 * `modelDrivenReconstructionParams` object instance.
 * This function makes use of the rapidxml
 * library for parsing the xml file.
 *
 * @param cfname The configuration file filename.
 * @return modelDrivenReconstructionParams the model driven reconstruction parameters instance
 *
 */
modelDrivenReconstructionParams modelDrivenReconstructionInitialiser(string cfname) {

    modelDrivenReconstructionParams reconparams;

    char * charcfname = new char[cfname.length() + 1];
    strcpy(charcfname, cfname.c_str());

    //load the xml file using the rapidxml library
    rapidxml::file<> xmlFile(charcfname);
    rapidxml::xml_document<> doc;
    doc.parse<0>(xmlFile.data());

    rapidxml::xml_node<> *root = nullptr;
    rapidxml::xml_node<> *mappingNode = nullptr;
    rapidxml::xml_node<> *landmarksNode = nullptr;
    rapidxml::xml_node<> *instrumentNode = nullptr;

    root = doc.first_node("modelDrivenReconstruction");
    if (root == nullptr){
      cerr << "ERROR: xml file not defined as a modelDrivenReconstruction protocol initialiser." << endl;
      exit(1);
    }

    //load specimen model configuration parameters
    if (root->first_node("trajectoryMapping") != nullptr){
      rapidxml::xml_node<> *mappingNode = root->first_node("trajectoryMapping");
      if (mappingNode->first_attribute("location") != nullptr)
        reconparams.tMapping.location = mappingNode->first_attribute("location")->value();
      else {
        cerr << "\tNo trajectory mapping location specified." << endl;
        if (filesystem::is_regular_file("./projection/mapping.tsv")){
          cerr << "\tAssuming mapping location: " << "projection/mapping.tsv" << endl;
          reconparams.tMapping.location = "./projection/mapping.tsv";
        }
        else{
          cerr << "\tERROR: Cannot find mapping within default search location directory." << endl;
          cerr << "\tPlease specify the location of the trajectory mapping to continue i.e. `location='mapping_loc'" << endl;
          exit(1);
        }
      }
    }
    else {
      cerr << "\tERROR: Please specify the trajectory mapping node within the reconstruction configuration xml file." << endl;
      cerr << "\t<trajectoryMapping location='mapping_loc'/>" << endl;
      exit(1);
    }

    landmarksNode = root->first_node("landmarks");
    if (landmarksNode == nullptr){
      cerr << "\tNo 'landmarks' node for matching the alignment of the trajectory mapping to the experimental detection event sequence." << endl;
      cerr << "\tAssuming default alignment of first ranged ion to start of passed mapping, and final ranged ion to end of passed mapping." << endl;
      landmarkParams lm1(0, "", 0);
      landmarkParams lm2(numeric_limits<long>::max(), "", numeric_limits<int>::max());
      reconparams.landmarks.push_back(lm1);
      reconparams.landmarks.push_back(lm2);
    }
    else{
      for (rapidxml::xml_node<> *tempLandmarkNode = landmarksNode->first_node(); tempLandmarkNode != NULL; tempLandmarkNode = tempLandmarkNode->next_sibling())
      {
        long rangedEventIndex = stol(string(tempLandmarkNode->first_attribute("rangedEvaporationEventIndex")->value()));
        string modelCondition = string(tempLandmarkNode->first_attribute("modelPosition")->value());
        landmarkParams lm(rangedEventIndex, modelCondition);
        reconparams.landmarks.push_back(lm);
      }
      if (reconparams.landmarks.size() == 0){
        cerr << "\tNo 'landmarks' node for matching the alignment of the trajectory mapping to the experimental detection event sequence." << endl;
        cerr << "\tAssuming default alignment of first ranged ion to start of passed mapping, and final ranged ion to end of passed mapping." << endl;
        landmarkParams lm1(0, "", 0);
        landmarkParams lm2(numeric_limits<long>::max(), "", numeric_limits<int>::max());
        reconparams.landmarks.push_back(lm1);
        reconparams.landmarks.push_back(lm2);
      }
    }

    //load instrument configuration parameters
    if (root->first_node("instrument") != nullptr){
      instrumentNode = root->first_node("instrument");
      reconparams.iModel.detectorRadius = stod(instrumentNode->first_attribute("radius")->value());
      reconparams.iModel.efficiency = stod(instrumentNode->first_attribute("efficiency")->value());
      cout << "Atom probe instrument geometry specified: detector radius = " << reconparams.iModel.detectorRadius <<
              ", detection efficiency = " << reconparams.iModel.efficiency << endl;
    }
    else{
      cerr << "Cannot find an instrument node in the xml pointProjectionReconstruction configuration file." <<
              " Using the default values matching the LAWATAP of " <<
              "<instrument radius='" << defaultInstrumentRadius << "' efficiency='" << defaultInstrumentEfficiency << "'/>";
      reconparams.iModel.detectorRadius = defaultInstrumentRadius;
      reconparams.iModel.efficiency = defaultInstrumentEfficiency;
    }

    delete[] charcfname;

    return reconparams;
}
