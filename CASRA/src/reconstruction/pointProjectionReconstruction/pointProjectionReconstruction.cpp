/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>

#include <fstream>
#include <algorithm>
#include <vector>
#include <math.h>

#include "CASRA/reconstruction/pointProjectionReconstruction/pointProjectionReconstruction.h"

using namespace std;

/*!
 * This function performs data reconstructions using a particular emitter model
 * and detector definition.
 *
 * @param emitter An emitter object instance. This should be one of the subclasses
 * of EmitterModel.
 * @param lawatap An atom probe instrument object instance.
 * @return void
 */
void dataReconstruction(APTDataSet& dataset, ShankBasEmitterModel& emitter, AnalyticDetector& detector){

  emitter.apexCentre.pz -= emitter.R;
  const long eventNum = dataset.events.size();

  for  (long a=0;a<eventNum;a++){
    DetectionEvent& detectorEvent = dataset.events[a];
    emitter.calculateFOV(detector);
    emitter.detectorToCapTransfer(detectorEvent, detector, a);
    emitter.reconPlacement(detectorEvent);

    if ((detectorEvent.isRanged==true) || (dataset.ranged == false)) {
      emitter.capShift(detectorEvent, detector);
      emitter.evolveRadius(detectorEvent, a);
    }
  }

  return;
}

/*!
* This function performs data reconstructions using a particular emitter model
* and detector definition.
*
* @param emitter An emitter object instance. This should be one of the subclasses
* of EmitterModel.
* @param lawatap An atom probe instrument object instance.
* @return void
*/
void dataReconstruction(APTDataSet& dataset, VoltageBasEmitterModel& emitter, AnalyticDetector& detector){

  if (dataset.events.size() > 0)
    emitter.setInitialRadius(dataset.events[0]);

  const long eventNum = dataset.events.size();
  for  (long a=0;a<eventNum;a++){
    DetectionEvent& detectorEvent = dataset.events[a];
    emitter.calculateFOV(detector);
    emitter.detectorToCapTransfer(detectorEvent, detector, a);
    emitter.reconPlacement(detectorEvent);

    if ((detectorEvent.isRanged==true) || (dataset.ranged == false)) {
      emitter.capShift(detectorEvent, detector);
      emitter.evolveRadius(detectorEvent, a);
    }
  }

  return;
}
