/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>

#include <fstream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <set>
#include <numeric>

#include "CASRA/reconstruction/modelDrivenReconstruction/modelDrivenReconstruction.h"

using namespace std;

/*!
 * Initialiser for the trajectory mapping instance - sets instance parameters from inputted TsvObject containing the trajectory mapping data.
 *
 * @param mappingData The TSV data object for initialising the trajectoryMapping
 */
TrajectoryMapping::TrajectoryMapping(TsvObject& mappingData){

  int c1dxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1dx") - mappingData.columnOrder.begin();
  int c2dxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2dx") - mappingData.columnOrder.begin();
  int c3dxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3dx") - mappingData.columnOrder.begin();
  int c1dyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1dy") - mappingData.columnOrder.begin();
  int c2dyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2dy") - mappingData.columnOrder.begin();
  int c3dyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3dy") - mappingData.columnOrder.begin();

  int c1pxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1px") - mappingData.columnOrder.begin();
  int c1pyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1py") - mappingData.columnOrder.begin();
  int c1pzInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1pz") - mappingData.columnOrder.begin();
  int c2pxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2px") - mappingData.columnOrder.begin();
  int c2pyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2py") - mappingData.columnOrder.begin();
  int c2pzInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2pz") - mappingData.columnOrder.begin();
  int c3pxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3px") - mappingData.columnOrder.begin();
  int c3pyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3py") - mappingData.columnOrder.begin();
  int c3pzInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3pz") - mappingData.columnOrder.begin();

  int c1phInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1ph") - mappingData.columnOrder.begin();
  int c2phInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2ph") - mappingData.columnOrder.begin();
  int c3phInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3ph") - mappingData.columnOrder.begin();

  int itInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "iteration") - mappingData.columnOrder.begin();
  int evapInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "evap_rate") - mappingData.columnOrder.begin();

  const int mappingDataSize = mappingData.outStrings[c1dxInd].size();
  for (int i=0;i<mappingDataSize;i++){
    double c1dx = stod(mappingData.outStrings[c1dxInd][i]);
    double c1dy = stod(mappingData.outStrings[c1dyInd][i]);
    Point detSpaceC1(c1dx, c1dy, 0.0);
    detectorSpacePositionsC1.push_back(detSpaceC1);

    double c2dx = stod(mappingData.outStrings[c2dxInd][i]);
    double c2dy = stod(mappingData.outStrings[c2dyInd][i]);
    Point detSpaceC2(c2dx, c2dy, 0.0);
    detectorSpacePositionsC2.push_back(detSpaceC2);

    double c3dx = stod(mappingData.outStrings[c3dxInd][i]);
    double c3dy = stod(mappingData.outStrings[c3dyInd][i]);
    Point detSpaceC3(c3dx, c3dy, 0.0);
    detectorSpacePositionsC3.push_back(detSpaceC3);

    double c1px = stod(mappingData.outStrings[c1pxInd][i]);
    double c1py = stod(mappingData.outStrings[c1pyInd][i]);
    double c1pz = stod(mappingData.outStrings[c1pzInd][i]);
    Point sampleSpaceC1(c1px, c1py, c1pz);
    sampleSpacePositionsC1.push_back(sampleSpaceC1);

    double c2px = stod(mappingData.outStrings[c2pxInd][i]);
    double c2py = stod(mappingData.outStrings[c2pyInd][i]);
    double c2pz = stod(mappingData.outStrings[c2pzInd][i]);
    Point sampleSpaceC2(c2px, c2py, c2pz);
    sampleSpacePositionsC2.push_back(sampleSpaceC2);

    double c3px = stod(mappingData.outStrings[c3pxInd][i]);
    double c3py = stod(mappingData.outStrings[c3pyInd][i]);
    double c3pz = stod(mappingData.outStrings[c3pzInd][i]);
    Point sampleSpaceC3(c3px, c3py, c3pz);
    sampleSpacePositionsC3.push_back(sampleSpaceC3);

    string c1phase = mappingData.outStrings[c1phInd][i];
    string c2phase = mappingData.outStrings[c2phInd][i];
    string c3phase = mappingData.outStrings[c3phInd][i];

    phaseC1.push_back(c1phase);
    phaseC2.push_back(c2phase);
    phaseC3.push_back(c3phase);

    double it = stoi(mappingData.outStrings[itInd][i]);
    iterations.push_back(it);

    double eR = stod(mappingData.outStrings[evapInd][i]);
    evapRates.push_back(eR);

  }
}


/*!
 * Function for resetting a trajectory mapping instance from a TsvObject containing the trajectory mapping data
 *
 * @param mappingData The TSV data object for setting the trajectoryMapping
 * @return void
 */
void TrajectoryMapping::setMapping(TsvObject& mappingData){

  //clear old mapping data from TrajectoryMapping instance
  detectorSpacePositionsC1.clear();
  detectorSpacePositionsC2.clear();
  detectorSpacePositionsC3.clear();
  sampleSpacePositionsC1.clear();
  sampleSpacePositionsC2.clear();
  sampleSpacePositionsC3.clear();
  iterations.clear();
  evapRates.clear();
  phaseC1.clear();
  phaseC2.clear();
  phaseC3.clear();

  int c1dxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1dx") - mappingData.columnOrder.begin();
  int c2dxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2dx") - mappingData.columnOrder.begin();
  int c3dxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3dx") - mappingData.columnOrder.begin();
  int c1dyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1dy") - mappingData.columnOrder.begin();
  int c2dyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2dy") - mappingData.columnOrder.begin();
  int c3dyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3dy") - mappingData.columnOrder.begin();

  int c1pxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1px") - mappingData.columnOrder.begin();
  int c1pyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1py") - mappingData.columnOrder.begin();
  int c1pzInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1pz") - mappingData.columnOrder.begin();
  int c2pxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2px") - mappingData.columnOrder.begin();
  int c2pyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2py") - mappingData.columnOrder.begin();
  int c2pzInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2pz") - mappingData.columnOrder.begin();
  int c3pxInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3px") - mappingData.columnOrder.begin();
  int c3pyInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3py") - mappingData.columnOrder.begin();
  int c3pzInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3pz") - mappingData.columnOrder.begin();

  int c1phInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c1ph") - mappingData.columnOrder.begin();
  int c2phInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c2ph") - mappingData.columnOrder.begin();
  int c3phInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "c3ph") - mappingData.columnOrder.begin();

  int itInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "iteration") - mappingData.columnOrder.begin();
  int evapInd = find(mappingData.columnOrder.begin(), mappingData.columnOrder.end(), "evap_rate") - mappingData.columnOrder.begin();

  const int mappingDataSize = mappingData.outStrings[c1dxInd].size();
  for (int i=0;i<mappingDataSize;i++){
    double c1dx = stod(mappingData.outStrings[c1dxInd][i]);
    double c1dy = stod(mappingData.outStrings[c1dyInd][i]);
    Point detSpaceC1(c1dx, c1dy, 0.0);
    detectorSpacePositionsC1.push_back(detSpaceC1);

    double c2dx = stod(mappingData.outStrings[c2dxInd][i]);
    double c2dy = stod(mappingData.outStrings[c2dyInd][i]);
    Point detSpaceC2(c2dx, c2dy, 0.0);
    detectorSpacePositionsC2.push_back(detSpaceC2);

    double c3dx = stod(mappingData.outStrings[c3dxInd][i]);
    double c3dy = stod(mappingData.outStrings[c3dyInd][i]);
    Point detSpaceC3(c3dx, c3dy, 0.0);
    detectorSpacePositionsC3.push_back(detSpaceC3);

    double c1px = stod(mappingData.outStrings[c1pxInd][i]);
    double c1py = stod(mappingData.outStrings[c1pyInd][i]);
    double c1pz = stod(mappingData.outStrings[c1pzInd][i]);
    Point sampleSpaceC1(c1px, c1py, c1pz);
    sampleSpacePositionsC1.push_back(sampleSpaceC1);

    double c2px = stod(mappingData.outStrings[c2pxInd][i]);
    double c2py = stod(mappingData.outStrings[c2pyInd][i]);
    double c2pz = stod(mappingData.outStrings[c2pzInd][i]);
    Point sampleSpaceC2(c2px, c2py, c2pz);
    sampleSpacePositionsC2.push_back(sampleSpaceC2);

    double c3px = stod(mappingData.outStrings[c3pxInd][i]);
    double c3py = stod(mappingData.outStrings[c3pyInd][i]);
    double c3pz = stod(mappingData.outStrings[c3pzInd][i]);
    Point sampleSpaceC3(c3px, c3py, c3pz);
    sampleSpacePositionsC3.push_back(sampleSpaceC3);

    string c1phase = mappingData.outStrings[c1phInd][i];
    string c2phase = mappingData.outStrings[c2phInd][i];
    string c3phase = mappingData.outStrings[c3phInd][i];

    phaseC1.push_back(c1phase);
    phaseC2.push_back(c2phase);
    phaseC3.push_back(c3phase);

    double it = stoi(mappingData.outStrings[itInd][i]);
    iterations.push_back(it);

    double eR = stod(mappingData.outStrings[evapInd][i]);
    evapRates.push_back(eR);
  }

  return;
}

landmarkParams::landmarkParams(long experimentLandmark, string modelLandmark): experimentLandmark{experimentLandmark},modelLandmark{modelLandmark} {

}

landmarkParams::landmarkParams(long experimentLandmark, string modelLandmark, int modelIt): experimentLandmark{experimentLandmark},modelLandmark{modelLandmark}, modelIt{modelIt} {

}

/*!
 * Function for determining the total evaporation volume at each model iteration
 * from which charged optics calculation is performed
 *
 * @param trajectoryMap The trajectory mapping instance (see class definition)
 * @param detectorRadius The detector radius
 * @param detectorEfficiency The detector efficiency (instrument efficiency)
 * @return The map of the total evaporated volume (double) at each model iteration (int)
 */
map<int, double> determineEvapVolumeDepths(TrajectoryMapping& trajectoryMap,
                                           double detectorRadius,
                                           double detectorEfficiency){

  double detectorRadiusSqr = detectorRadius * detectorRadius;

  int it = -1;
  map<int, double> evapVols;

  int v = -1;
  vector<int> uniqueits;

  const int mappingSize = trajectoryMap.detectorSpacePositionsC1.size();

  for (int i=0;i<mappingSize;i++){
    int cIt = trajectoryMap.iterations[i];
    if (it != cIt){
      uniqueits.push_back(cIt);
      evapVols[cIt] = 0.0;
      //cout << cIt << " " << it << endl;
      if (v > -1)
        evapVols[cIt] = evapVols[it];
      it = cIt;
      v++;
    }

    double c1dx = trajectoryMap.detectorSpacePositionsC1[i].px;
    double c1dy = trajectoryMap.detectorSpacePositionsC1[i].py;
    double c2dx = trajectoryMap.detectorSpacePositionsC2[i].px;
    double c2dy = trajectoryMap.detectorSpacePositionsC2[i].py;
    double c3dx = trajectoryMap.detectorSpacePositionsC3[i].px;
    double c3dy = trajectoryMap.detectorSpacePositionsC3[i].py;

    double cdx = (c1dx + c2dx + c3dx)/3.0;
    double cdy = (c1dy + c2dy + c3dy)/3.0;

    if ((cdx * cdx + cdy * cdy) <= detectorRadiusSqr){

      Point& cp1 = trajectoryMap.detectorSpacePositionsC1[i];
      Point& cp2 = trajectoryMap.detectorSpacePositionsC2[i];
      Point& cp3 = trajectoryMap.detectorSpacePositionsC3[i];

      //calculate panel area via Heron's formula
      double ABl = norm(cp2, cp1);
      double ACl = norm(cp3, cp1);
      double BCl = norm(cp3, cp2);
      double s = (ABl + ACl + BCl) * 0.5;
      double panelArea = sqrt(s * (s - ABl) * (s - BCl) * (s - ACl));

      evapVols[it] += trajectoryMap.evapRates[i] * panelArea;

    }
  }

  return evapVols;

}

/*!
 * Function for performing model-driven reconstruction given position landmarks
 * and the trajectory mapping stack
 *
 * @param detectorEventData The detector event data
 * @param reconLandmarks The reconstruction landmarks
 * @param mappingX The 3D grid defining the inverse trajectory mapping in the x-coordinate
 * @param mappingY The 3D grid defining the inverse trajectory mapping in the y-coordinate
 * @param mappingZ The 3D grid defining the inverse trajectory mapping in the z-coordinate
 * @param evapVolumes The total evaporation volume at each model iteration from which charged optics calculation was performed
 * @param xDim The number of grid cells in the x-dimension within the mapping grids
 * @param yDim The number of grid cells in the y-dimension within the mapping grids
 * @param detectorRadius The detector radius
 * @return void
 */
void modelDrivenReconstructionProtocol(APTDataSet& detectorEventData, vector<landmarkParams>& reconLandmarks,
                                       scalarField3D& mappingX, scalarField3D& mappingY, scalarField3D& mappingZ,
                                       map<int, double>& evapVolumes, const int xDim, const int yDim, const double detectorRadius){

  int currentLandmark = 0;
  int currentProjectionIt = 0;
  int ic = 0;

  double nextLandmarkVol = reconLandmarks[1].landmarkEvapVolume;
  double currentVolume = evapVolumes[reconLandmarks[0].modelIt];
  double currentIonicVolume = reconLandmarks[0].landmarkIonicVolume;
  map<int, double>::iterator it = evapVolumes.begin();
  map<int, double>::iterator nx = std::next(it, 1);
  vector<bool> outputIon(detectorEventData.events.size(), false);

  cout << reconLandmarks[reconLandmarks.size()-1].experimentLandmark << endl;

  for (int i=reconLandmarks[0].experimentLandmark;i<reconLandmarks[reconLandmarks.size()-1].experimentLandmark;i++){
    if (currentVolume >= nextLandmarkVol){
      currentLandmark++;
      if (currentLandmark == reconLandmarks.size()-1)
        break;
      nextLandmarkVol = reconLandmarks[currentLandmark+1].landmarkEvapVolume;
      currentIonicVolume = reconLandmarks[currentLandmark].landmarkIonicVolume;
    }
    if (currentVolume >= nx->second){
      ++it;
      currentProjectionIt += 1;
      if (currentProjectionIt == mappingZ.size()-1)
        break;
      nx = std::next(it, 1);
    }
    double t = (currentVolume - it->second)/(nx->second - it->second);
    int idx = (int)((detectorEventData.events[i].dx + detectorRadius*SCALEFACTOR) * xDim/(2.0 * detectorRadius*SCALEFACTOR));
    int idy = (int)((detectorEventData.events[i].dy + detectorRadius*SCALEFACTOR) * yDim/(2.0 * detectorRadius*SCALEFACTOR));
    detectorEventData.events[i].sx = mappingX[currentProjectionIt][idy][idx] * (1.0 - t) + mappingX[currentProjectionIt+1][idy][idx] * (t);
    detectorEventData.events[i].sy = mappingY[currentProjectionIt][idy][idx] * (1.0 - t) + mappingY[currentProjectionIt+1][idy][idx] * (t);
    detectorEventData.events[i].sz = mappingZ[currentProjectionIt][idy][idx] * (1.0 - t) + mappingZ[currentProjectionIt+1][idy][idx] * (t);
    outputIon[i] = true;
    ic++;
    currentVolume += currentIonicVolume;
  }

  for (int i=detectorEventData.events.size()-1;i>=0;--i){
    if (outputIon[i] == false){
      detectorEventData.events.erase(detectorEventData.events.begin()+i);
    }
  }

  return;
}

/*!
 * Function for returning landmark positions within the model-driven reconstruction
 * based off the inputted user landmarks
 * For floating model landmarks (e.g. position defined by when entering or exiting a phase),
 * this function identifies the matching model iteration.
 * @param trajectoryMap The loaded trajectory map instance
 * @param reconLandmarks The user inputted landmarks
 * @param detectorRadius The detector radius
 * @return The modified reconstruction landmarks, with floating model landmark positions defined within the model iteration sequence.
 */
vector<landmarkParams> determineModelLandmarkPositions(TrajectoryMapping& trajectoryMap,
                                                       vector<landmarkParams>& reconLandmarks,
                                                       double detectorRadius){

  cout << "Identifying model landmark positions." << endl;
  double detectorRadiusSqrd = detectorRadius * detectorRadius;

  vector<landmarkParams> nlandmarks;

  vector<double> dx;
  vector<double> dy;
  vector<string> ionPhases;
  vector<int> iteration;

  const int trajectoryMapSize = trajectoryMap.sampleSpacePositionsC1.size();
  for (int i=0;i<trajectoryMapSize;i++){
    double c1dx = trajectoryMap.detectorSpacePositionsC1[i].px;
    double c1dy = trajectoryMap.detectorSpacePositionsC1[i].py;
    string c1phase = trajectoryMap.phaseC1[i];
    if (c1dx*c1dx+c1dy*c1dy <= detectorRadiusSqrd){
      dx.push_back(c1dx);
      dy.push_back(c1dy);
      ionPhases.push_back(c1phase);
      iteration.push_back(trajectoryMap.iterations[i]);
    }

    double c2dx = trajectoryMap.detectorSpacePositionsC2[i].px;
    double c2dy = trajectoryMap.detectorSpacePositionsC2[i].py;
    string c2phase = trajectoryMap.phaseC2[i];
    if (c2dx*c2dx+c2dy*c2dy <= detectorRadiusSqrd){
      dx.push_back(c2dx);
      dy.push_back(c2dy);
      ionPhases.push_back(c2phase);
      iteration.push_back(trajectoryMap.iterations[i]);
    }

    double c3dx = trajectoryMap.detectorSpacePositionsC3[i].px;
    double c3dy = trajectoryMap.detectorSpacePositionsC3[i].py;
    string c3phase = trajectoryMap.phaseC3[i];
    if (c3dx*c3dx+c3dy*c3dy <= detectorRadiusSqrd){
      dx.push_back(c3dx);
      dy.push_back(c3dy);
      ionPhases.push_back(c3phase);
      iteration.push_back(trajectoryMap.iterations[i]);
    }
  }

  //obtain unique model ion projection iterations
  set<int> uniqueModelIterations;
  for (int i=0;i<trajectoryMapSize;i++){
    uniqueModelIterations.insert(trajectoryMap.iterations[i]);
  }

  const int reconLandmarksSize = reconLandmarks.size();
  for (int l=0;l<reconLandmarksSize;l++){
    int delimPos = reconLandmarks[l].modelLandmark.find(":");
    if (delimPos == string::npos){
      try {
      landmarkParams lm(reconLandmarks[l].experimentLandmark, reconLandmarks[l].modelLandmark, stoi(reconLandmarks[l].modelLandmark));
      nlandmarks.push_back(lm);
      reconLandmarks[l].modelIt = stoi(reconLandmarks[l].modelLandmark);
      if (stoi(reconLandmarks[l].modelLandmark) > (*uniqueModelIterations.rbegin())){
        cerr << "The manually selected model iteration landmark is greater than evaluated ion trajectories" << endl;
        exit(1);
      }

      }
      catch(std::invalid_argument& e){
        cerr << "Model landmark input " << reconLandmarks[l].modelLandmark << "invalid." << endl;
        cerr << "If the model landmark is fixed, then its value must be an integer corresponding to a model iteration." << endl;
        cerr << "if the model landmark is floating, then it should have the format '`phase_name`:enter' or '`phase_name`:exit'" << endl;
        exit(1);
      }
    }
    else {
      string landmarkMode = reconLandmarks[l].modelLandmark.substr(delimPos+1);
      string landmarkPhaseName = reconLandmarks[l].modelLandmark.substr(0, delimPos);
      if (landmarkMode == "enter"){ //determine the model iteration where the defined landmark phase enters the FOV
        bool set = false;
        for (int i=0;i<iteration.size();i++){
          if (ionPhases[i] == landmarkPhaseName){
            landmarkParams lm(reconLandmarks[l].experimentLandmark, reconLandmarks[l].modelLandmark, iteration[i]);
            lm.modelIt = iteration[i];
            nlandmarks.push_back(lm);
            set = true;
            break;
          }
        }
        if (set == false){
          cerr << "Could not set landmark " << reconLandmarks[l].modelLandmark << " as the named phase is never found to enter the model FOV." << endl;
          cerr << "ignoring landmark from reconstruction." << endl;
        }
      }
      else if (landmarkMode == "exit"){ //determine the model iteration where the defined landmark phase exits the FOV
        bool entered = false;
        int cIt = iteration[0];
        bool set = false;
        bool found = false;

        for (int i=0;i<iteration.size();i++){
          if (ionPhases[i] == landmarkPhaseName){
            entered = true;
            found = true;
          }
          if (iteration[i] > cIt){
            if (entered == true && found == false){
              landmarkParams lm(reconLandmarks[l].experimentLandmark, reconLandmarks[l].modelLandmark, cIt);
              cout << reconLandmarks[l].experimentLandmark << " " << reconLandmarks[l].modelLandmark << " " << cIt << endl;
              lm.modelIt = cIt;
              nlandmarks.push_back(lm);
              set = true;
              break;
            }
            found = false;
            cIt = iteration[i];
          }
        }
        if (set == false){
          cerr << "Could not set landmark " << reconLandmarks[l].modelLandmark << " as the named phase is never found to exit the model FOV." << endl;
          cerr << "ignoring landmark from reconstruction." << endl;
        }
      }
      else {
        cerr << "Floating landmark `" << reconLandmarks[l].modelLandmark << "` unrecognised." << endl;
        cerr << "Format should be one of '`phase_name`:enter' or '`phase_name`:exit'" << endl;
        exit(1);
      }
    }
  }

  cout << "Model landmark positions identified:" << endl;

  //reorder by model iteration
  vector<int> orderInd(nlandmarks.size(), 0);
  iota(orderInd.begin(), orderInd.end(), 0);
  sort(orderInd.begin(), orderInd.end(), [&](int i,int j){return nlandmarks[i].modelIt < nlandmarks[j].modelIt;});

  vector<landmarkParams> nnlandmarks;
  for (int l=0;l<orderInd.size();l++){
    nnlandmarks.push_back(nlandmarks[orderInd[l]]);
    if (l > 0){
      if (nnlandmarks[l].experimentLandmark < nnlandmarks[l-1].experimentLandmark){
        cerr << "ERROR: experimental landmark detector event indexes and model iteration landmark positions not monotonic." << endl;
        cerr << "Please check inputted landmarks in the configuration file." << endl;
        exit(1);
      }
    }
  }

  if (nnlandmarks.size() < 2){
    cerr << "ERROR: A minimum of two landmarks is required to constrain the reconstruction. Please check the inputted landmarks within the configuration file e.g. phasenames" << endl;
    exit(1);
  }

  return nnlandmarks;

}

/*!
 * Function for filtering out detection events outside of the detector FOV and
 * adjusting reconstruction landmark positions
 * to account for this.
 *
 * @param reconLandmarks The reconstruction landmark positions (matching model iterations to experiment evaporation sequence positions)
 * @param dataset The dataset of detection events
 * @param detectorRadius The detector radius;
 * return void
 */
void filterExperimentDataOutsideFOV(vector<landmarkParams>& reconLandmarks, APTDataSet& dataset, double detectorRadius){

  double detectorRadiusSqrd = detectorRadius * detectorRadius;

  //remove ions from dataset outside of landmarks, and update landmark positions
  int beginning = reconLandmarks[0].experimentLandmark;
  int end = reconLandmarks[reconLandmarks.size()-1].experimentLandmark;
  dataset.events.erase(dataset.events.begin(), dataset.events.begin() + beginning);
  for (int l=0;l<reconLandmarks.size();l++){
    reconLandmarks[l].experimentLandmark - beginning;
  }
  dataset.events.erase(dataset.events.begin() + end, dataset.events.end());

  //remove ions from dataset outside of set detector radius, and update landmark positions
  int it = 0;
  int ic = 0;
  int lp = 0;
  while (dataset.events.size() > it){
    ic++;
    if (dataset.events[it].dx*dataset.events[it].dx + dataset.events[it].dy*dataset.events[it].dy > detectorRadiusSqrd){
       dataset.events.erase(dataset.events.begin()+it);
    }
    else {
      it++;
    }

    if (reconLandmarks[lp].experimentLandmark == ic){
      reconLandmarks[lp].experimentLandmark = it;
      lp++;
    }
    if (lp == reconLandmarks.size())
      return;
  }

  return;
}

/*!
 * Function for determining the ionic volumes to use for different landmarks
 * to assure experiment-model landmark position alignment
 *
 * @param reconLandmarks The reconstruction landmarks
 * @param modelEvapVolumes The total evaporated volume at different projection iterations of the model
 * return void
 */
void determineLandmarkIonicVolumes(std::vector<landmarkParams>& reconLandmarks, map<int, double>& modelEvapVolumes){

  map<int, double> interpEvapVols;

  //interpolate volume at non ion-projection iterations
  for (map<int, double>::iterator eVolsIt = modelEvapVolumes.begin();eVolsIt != next(modelEvapVolumes.end(),-1);eVolsIt++){
    map<int, double>::iterator eVolsItNext = next(eVolsIt, 1);
    int firstIt = eVolsIt->first;
    int finalIt = eVolsItNext->first;
    for (int j=firstIt;j<=finalIt;j++){
      double t = (double)(j- firstIt)/(double)(finalIt - firstIt);
      interpEvapVols[j] = eVolsIt->second * (1.0 - t) + eVolsItNext->second * t;
    }
  }

  const int reconLandmarksSize = reconLandmarks.size();
  for (int l=0;l<reconLandmarksSize-1;l++){
    double landmarkIonicVolume = (interpEvapVols[reconLandmarks[l+1].modelIt] - interpEvapVols[reconLandmarks[l].modelIt])/(reconLandmarks[l+1].experimentLandmark - reconLandmarks[l].experimentLandmark);
    if (landmarkIonicVolume < 0.0){
      cerr << "ERROR: landmark ionic volume cannot be negative. Something has gone wrong with the landmark placement." << endl;
      exit(1);
    }
    reconLandmarks[l].landmarkIonicVolume = landmarkIonicVolume;
  }

  for (int l=0;l<reconLandmarksSize;l++){
    reconLandmarks[l].landmarkEvapVolume = interpEvapVols[reconLandmarks[l].modelIt];
  }

  return;
}

/*!
 * Function for determining the depth positions (total evaporated specimen
 * volumes) for detection events
 *
 * @param dataset The dataset of detection events
 * @param reconLandmarks The reconstruction landmarks
 * return The vector of total evaporated specimen volumes at each detection event
 */
vector<double> assignIonDepthPositions(APTDataSet& dataset, vector<landmarkParams>& reconLandmarks){

  vector<double> detectorSpaceDepthPosition;
  int landmarkInd = 0;
  double currentIonicVolume = reconLandmarks[0].landmarkIonicVolume;
  double currentVolume = reconLandmarks[0].landmarkIonicVolume;

  for (int i=0;i<dataset.events.size();i++){
    if (i >= reconLandmarks[landmarkInd+1].experimentLandmark){
      currentIonicVolume = reconLandmarks[landmarkInd+1].landmarkIonicVolume;
      landmarkInd++;
    }
    detectorSpaceDepthPosition.push_back(currentVolume);
    currentVolume += currentIonicVolume;
    if (reconLandmarks.size() == landmarkInd){
      break;
    }
  }

  return detectorSpaceDepthPosition;

}

/*!
 * Function for constructing the inverse trajectory mapping stack, describing how
 * points on the detector at particular depth positions in the evaporation sequence correspond
 * to positions on the specimen surface patch within the FOV.
 *
 * @param trajectoryMap The trajectory mapping
 * @param startIt The starting model iteration for the stack
 * @param finalIt The final model iteration for the stack
 * @param detectorRadius The detector radius
 * @param xDim The number of detector stack x-cells
 * @param yDim The number of detector stack y-cells
 * @return A tuple of three x-y-z inverse trajectory mapping grid stacks (defining the trajectory mapping)
 */
tuple<scalarField3D, scalarField3D, scalarField3D> constructDetectorMappingStack(TrajectoryMapping& trajectoryMap,
                                                                                 const int startIt,
                                                                                 const int finalIt,
                                                                                 const double detectorRadius,
                                                                                 const int xDim, const int yDim) {

  set<int> spanningModelIts;
  const int trajectoryMapSize = trajectoryMap.sampleSpacePositionsC1.size();

  vector<double> minx;
  vector<double> miny;
  vector<double> maxx;
  vector<double> maxy;

  for (int i=0;i<trajectoryMapSize;i++){
    if ((trajectoryMap.iterations[i] >= startIt) && (trajectoryMap.iterations[i] <= finalIt))
      spanningModelIts.insert(trajectoryMap.iterations[i]);
  }

  vector<vector<vector<double>>> detectorSpaceGridX(spanningModelIts.size(),
                                                    vector<vector<double>>(yDim,
                                                                           vector<double>(xDim, 0.0)));
  vector<vector<vector<double>>> detectorSpaceGridY(spanningModelIts.size(),
                                                    vector<vector<double>>(yDim,
                                                                           vector<double>(xDim, 0.0)));
  vector<vector<vector<double>>> detectorSpaceGridZ(spanningModelIts.size(),
                                                    vector<vector<double>>(yDim,
                                                                           vector<double>(xDim, 0.0)));

  for (int p=0;p<trajectoryMapSize;p++){
    Point& detPosC1 = trajectoryMap.detectorSpacePositionsC1[p];
    Point& detPosC2 = trajectoryMap.detectorSpacePositionsC2[p];
    Point& detPosC3 = trajectoryMap.detectorSpacePositionsC3[p];

    Point& SamplePosC1 = trajectoryMap.sampleSpacePositionsC1[p];
    Point& SamplePosC2 = trajectoryMap.sampleSpacePositionsC2[p];
    Point& SamplePosC3 = trajectoryMap.sampleSpacePositionsC3[p];

    //construct bounding box for panel
    double bbminx = min({detPosC1.px, detPosC2.px, detPosC3.px});
    double bbminy = min({detPosC1.py, detPosC2.py, detPosC3.py});
    double bbmaxx = max({detPosC1.px, detPosC2.px, detPosC3.px});
    double bbmaxy = max({detPosC1.py, detPosC2.py, detPosC3.py});

    minx.push_back(bbminx);
    maxx.push_back(bbmaxx);
    miny.push_back(bbminy);
    maxy.push_back(bbmaxy);
  }

  #if defined(_OPENMP)
  #pragma omp parallel for schedule(dynamic)
  #endif
  for (int x=0;x<xDim;x++){
    for (int y=0;y<yDim;y++){

      double tx = (double)x/(xDim-1);
      double ty = (double)y/(yDim-1);

      double detXPos = -detectorRadius * (1.0 - tx) + detectorRadius * tx;
      double detYPos = -detectorRadius * (1.0 - ty) + detectorRadius * ty;

      Point binCentre(detXPos, detYPos, 0.0);

      double detCellMaxEvapRate = -numeric_limits<double>::max();

      Point interpSamplePos(0.0, 0.0, 0.0);

      int currentIt = startIt;
      int ic = 0;
      int pc = 0;

      for (int p=0;p<trajectoryMapSize;p++){
        pc++;
        if (trajectoryMap.iterations[p] < startIt || trajectoryMap.iterations[p] > finalIt)
          continue;
        if (trajectoryMap.iterations[p] > currentIt){
          currentIt = trajectoryMap.iterations[p];
          detCellMaxEvapRate = -numeric_limits<double>::max();
          detectorSpaceGridX[ic][y][x] = interpSamplePos.px;
          detectorSpaceGridY[ic][y][x] = interpSamplePos.py;
          detectorSpaceGridZ[ic][y][x] = interpSamplePos.pz;
          ic++;
        }

        Point& detPosC1 = trajectoryMap.detectorSpacePositionsC1[p];
        Point& detPosC2 = trajectoryMap.detectorSpacePositionsC2[p];
        Point& detPosC3 = trajectoryMap.detectorSpacePositionsC3[p];

        Point& SamplePosC1 = trajectoryMap.sampleSpacePositionsC1[p];
        Point& SamplePosC2 = trajectoryMap.sampleSpacePositionsC2[p];
        Point& SamplePosC3 = trajectoryMap.sampleSpacePositionsC3[p];

        double evapRate = trajectoryMap.evapRates[p];

        if ((binCentre.px < minx[p]) || (binCentre.px > maxx[p]) || (binCentre.py < miny[p]) || (binCentre.py > maxy[p]))
          continue;

        //check if bin point inside triangle
        double u, v, w;
        findBarycentricCoordinates2D(binCentre, detPosC1, detPosC2, detPosC3, u, v, w);

        if (u < 0.0 || u > 1.0 || v < 0.0 || v > 1.0 || w < 0.0 || w > 1.0)
          continue;

        if (evapRate <= detCellMaxEvapRate)
          continue;

        detCellMaxEvapRate = evapRate;
        interpSamplePos = SamplePosC1 * u + SamplePosC2 * v + SamplePosC3 * w;
      }

      detectorSpaceGridX[ic][y][x] = interpSamplePos.px;
      detectorSpaceGridY[ic][y][x] = interpSamplePos.py;
      detectorSpaceGridZ[ic][y][x] = interpSamplePos.pz;
    }
  }

  return tuple<scalarField3D, scalarField3D, scalarField3D>({detectorSpaceGridX,
                                                             detectorSpaceGridY,
                                                             detectorSpaceGridZ});
}
