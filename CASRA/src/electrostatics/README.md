# electrostatics library

This directory contains the source files for building CASRA's electrostatics module. So far only a 3D BEM solver is currently provided. Future CASRA versions may include meshfree solvers (e.g. distributed point source models). 

