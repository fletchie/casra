/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <set>
#include <numeric>
#include <memory>

#include "CASRA/electrostatics/constantBEM.h"
#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/base/vectorMath.h"

using namespace std;

const double kernelConst = 1.0 / (4.0 * PI);

electrostaticBemSolver::electrostaticBemSolver(double tol) : tol{tol*tol} {

}

/*!
 * Function for calculating the constant BEM electrostatic problem matrices
 *
 * @param surfaceMesh The specimen surface mesh
 * @return void
 */
void electrostaticBemSolver::calculateElectrostaticMatrix(TriangularMesh& surfaceMesh) {

  const long size = surfaceMesh.faces.size();

  Fmat.resize(size);
  Pmat.resize(size);
  for (long i = 0; i < size; ++i) {
      Fmat[i].resize(size);
      Pmat[i].resize(size);
  }

  vector<double> areas(size);
  vector<double> ls2(size);
  surfaceMesh.normals.resize(size);

  double tx, ty, tz;

  double tol = this->tol;

  for (long j=0;j<size;j++){

    Point& ya = surfaceMesh.vertices[surfaceMesh.faces[j].p1];
    Point& yb = surfaceMesh.vertices[surfaceMesh.faces[j].p2];
    Point& yc = surfaceMesh.vertices[surfaceMesh.faces[j].p3];

    tx = (yb.py - ya.py) * (yc.pz - ya.pz) - (yb.pz - ya.pz) * (yc.py - ya.py);
    ty = (yb.pz - ya.pz) * (yc.px - ya.px) - (yb.px - ya.px) * (yc.pz - ya.pz);
    tz = (yb.px - ya.px) * (yc.py - ya.py) - (yb.py - ya.py) * (yc.px - ya.px);

    double area = 0.5 * std::sqrt(tx*tx + ty*ty + tz*tz);

    areas[j] = area;

    Point normal;
    double normVal;

    normal.px = ((yb.py - ya.py) * (yc.pz - ya.pz) - (yb.pz - ya.pz) * (yc.py - ya.py));
    normal.py = ((yb.pz - ya.pz) * (yc.px - ya.px) - (yb.px - ya.px) * (yc.pz - ya.pz));
    normal.pz = ((yb.px - ya.px) * (yc.py - ya.py) - (yb.py - ya.py) * (yc.px - ya.px));
    normVal =std::sqrt(normal.px*normal.px + normal.py*normal.py + normal.pz*normal.pz);

    normal.px /= normVal;
    normal.py /= normVal;
    normal.pz /= normVal;

    surfaceMesh.normals[j] = normal;

    //calculate longest side of mesh panels
    double svx = yb.px - ya.px;
    double svy = yb.py - ya.py;
    double svz = yb.pz - ya.pz;
    double s1 = svx*svx + svy*svy + svz*svz;

    svx = yc.px - yb.px;
    svy = yc.py - yb.py;
    svz = yc.pz - yb.pz;
    double s2 = svx*svx + svy*svy + svz*svz;

    if (s2 > s1){
      s1 = s2;
    }

    svx = yc.px - ya.px;
    svy = yc.py - ya.py;
    svz = yc.pz - ya.pz;
    double s3 = svx*svx + svy*svy + svz*svz;

    if (s3 > s1){
      s1 = s3;
    }

    ls2[j] = s1;

  }

  //parallelisation of double integral loop via openMP
  #if defined(_OPENMP)
  #pragma omp parallel
  #endif
  {

  double tdist2;
  Point cx;
  Point cy;
  double cvx, cvy, cvz;
  Point workPoint;
  double val1 = 0.0;
  double val2 = 0.0;
  double Pij1 = 0.0;
  double Pij2 = 0.0;
  double Fij1 = 0.0;
  double Fij2 = 0.0;

  double cyx, cyy, cyz;

  #if defined(_OPENMP)
  #pragma omp for schedule(dynamic)
  #endif
  for (long i=0;i<size;i++){

    cx = (surfaceMesh.vertices[surfaceMesh.faces[i].p1] + surfaceMesh.vertices[surfaceMesh.faces[i].p2] + surfaceMesh.vertices[surfaceMesh.faces[i].p3]) * 0.333333333;

    for (long j=0;j<size;j++){

        Pmat[i][j] = 0.0;
        Fmat[i][j] = 0.0;

        if (i != j) {

            Point& ya = surfaceMesh.vertices[surfaceMesh.faces[j].p1];
            Point& yb = surfaceMesh.vertices[surfaceMesh.faces[j].p2];
            Point& yc = surfaceMesh.vertices[surfaceMesh.faces[j].p3];

            cy.px = (ya.px + yb.px + yc.px) * 0.333333333;
            cy.py = (ya.py + yb.py + yc.py) * 0.333333333;
            cy.pz = (ya.pz + yb.pz + yc.pz) * 0.333333333;

            cvx = cx.px - cy.px;
            cvy = cx.py - cy.py;
            cvz = cx.pz - cy.pz;

            tdist2 = cvx * cvx + cvy * cvy + cvz * cvz;

            Pmat[i][j] = greensFunction(cx, cy) * areas[j];
            Fmat[i][j] = normGreensFunction(cx, cy, surfaceMesh.normals[j]) * areas[j];

            if (ls2[j] > tol * tdist2) {
                matij(val1, val2, cx, ya, yb, yc, areas[j], surfaceMesh.normals[j], workPoint, Pij1, Pij2, Fij1, Fij2);
                Pmat[i][j] = Pmat[i][j] * w1 + val1;
                Fmat[i][j] = Fmat[i][j] * w1 + val2;
                //Pmat[i][j] = Pmat[i][j] * w1 + numPij(cx, ya, yb, yc, areas[j]);
                //Fmat[i][j] = Fmat[i][j] * w1 + numFij(cx, ya, yb, yc, areas[j], surfaceMesh.normals[j]);
            }
        }
    }
  }
  }

  #if defined(_OPENMP)
  #pragma omp parallel
  #endif
  {
    Point cx;
    #if defined(_OPENMP)
    #pragma omp for
    #endif
  for (long j=0;j<size;j++){

    Point& ya = surfaceMesh.vertices[surfaceMesh.faces[j].p1];
    Point& yb = surfaceMesh.vertices[surfaceMesh.faces[j].p2];
    Point& yc = surfaceMesh.vertices[surfaceMesh.faces[j].p3];

    cx = (ya + yb + yc) * 0.333333333;

    this->Fmat[j][j] = -1.5;
    this->Pmat[j][j] = analyticPDiag(cx, ya, yb, yc, areas[j]);

  }
}

return;
}

/*!
 * degree 5 gaussian Cubature
 * source: https://arxiv.org/pdf/1606.03743.pdf

 * Function for calculating the numerical solution to the F matrix of the singular BIE
 * Calculates field in domain
*/
inline void electrostaticBemSolver::matij(double& Pij, double& Fij,
                                          Point& cx, Point& ya, Point& yb,
                                          Point& yc, double& area, Point& normal,
                                          Point& b, double& Pij1, double& Pij2,
                                          double& Fij1, double& Fij2) {

    b.px = ya.px * int1 + (yb.px + yc.px) * int2;
    b.py = ya.py * int1 + (yb.py + yc.py) * int2;
    b.pz = ya.pz * int1 + (yb.pz + yc.pz) * int2;

    Fij1 = normGreensFunction(cx, b, normal);
    Pij1 = greensFunction(cx, b);

    b.px = yb.px * int1 + (ya.px + yc.px) * int2;
    b.py = yb.py * int1 + (ya.py + yc.py) * int2;
    b.pz = yb.pz * int1 + (ya.pz + yc.pz) * int2;

    Fij1 += normGreensFunction(cx, b, normal);
    Pij1 += greensFunction(cx, b);

    b.px = yc.px * int1 + (yb.px + ya.px) * int2;
    b.py = yc.py * int1 + (yb.py + ya.py) * int2;
    b.pz = yc.pz * int1 + (yb.pz + ya.pz) * int2;

    Fij1 += normGreensFunction(cx, b, normal);
    Pij1 += greensFunction(cx, b);
    Fij1 *= w2;
    Pij1 *= w2;

    b.px = ya.px * int3 + (yb.px + yc.px) * int4;
    b.py = ya.py * int3 + (yb.py + yc.py) * int4;
    b.pz = ya.pz * int3 + (yb.pz + yc.pz) * int4;

    Fij2 = normGreensFunction(cx, b, normal);
    Pij2 = greensFunction(cx, b);

    b.px = yb.px * int3 + (ya.px + yc.px) * int4;
    b.py = yb.py * int3 + (ya.py + yc.py) * int4;
    b.pz = yb.pz * int3 + (ya.pz + yc.pz) * int4;

    Fij2 += normGreensFunction(cx, b, normal);
    Pij2 += greensFunction(cx, b);

    b.px = yc.px * int3 + (yb.px + ya.px) * int4;
    b.py = yc.py * int3 + (yb.py + ya.py) * int4;
    b.pz = yc.pz * int3 + (yb.pz + ya.pz) * int4;

    Fij2 += normGreensFunction(cx, b, normal);
    Pij2 += greensFunction(cx, b);
    Fij2 *= w3;
    Pij2 *= w3;

    Pij = area * (Pij1 + Pij2);
    Fij = area * (Fij1 + Fij2);

}

/*!
 * degree 5 gaussian Cubature
 * source: https://arxiv.org/pdf/1606.03743.pdf

 * Function for calculating the numerical solution to the F matrix of the singular BIE
 * Calculates field in domain
*/
inline double electrostaticBemSolver::numFij(Point& cx, Point& ya,
                                             Point& yb, Point& yc,
                                             double& area, Point& normal) {

    Point b;
    b.px = ya.px * int1 + (yb.px + yc.px) * int2;
    b.py = ya.py * int1 + (yb.py + yc.py) * int2;
    b.pz = ya.pz * int1 + (yb.pz + yc.pz) * int2;

    double Fij1 = normGreensFunction(cx, b, normal);

    b.px = yb.px * int1 + (ya.px + yc.px) * int2;
    b.py = yb.py * int1 + (ya.py + yc.py) * int2;
    b.pz = yb.pz * int1 + (ya.pz + yc.pz) * int2;

    Fij1 += normGreensFunction(cx, b, normal);

    b.px = yc.px * int1 + (yb.px + ya.px) * int2;
    b.py = yc.py * int1 + (yb.py + ya.py) * int2;
    b.pz = yc.pz * int1 + (yb.pz + ya.pz) * int2;

    Fij1 += normGreensFunction(cx, b, normal);
    Fij1 *= w2;

    b.px = ya.px * int3 + (yb.px + yc.px) * int4;
    b.py = ya.py * int3 + (yb.py + yc.py) * int4;
    b.pz = ya.pz * int3 + (yb.pz + yc.pz) * int4;

    double Fij2 = normGreensFunction(cx, b, normal);

    b.px = yb.px * int3 + (ya.px + yc.px) * int4;
    b.py = yb.py * int3 + (ya.py + yc.py) * int4;
    b.pz = yb.pz * int3 + (ya.pz + yc.pz) * int4;

    Fij2 += normGreensFunction(cx, b, normal);

    b.px = yc.px * int3 + (yb.px + ya.px) * int4;
    b.py = yc.py * int3 + (yb.py + ya.py) * int4;
    b.pz = yc.pz * int3 + (yb.pz + ya.pz) * int4;

    Fij2 += normGreensFunction(cx, b, normal);
    Fij2 *= w3;

    return area * (Fij1 + Fij2);

}

/*!
 * degree 5 gaussian Cubature
 * source: https://arxiv.org/pdf/1606.03743.pdf

 * Function for calculating the numerical solution to the F matrix of the singular BIE
 * Calculates field in domain
*/
inline double electrostaticBemSolver::numPij(Point& cx, Point& ya, Point& yb, Point& yc, double& area) {

    Point b;
    b.px = ya.px * int1 + (yb.px + yc.px) * int2;
    b.py = ya.py * int1 + (yb.py + yc.py) * int2;
    b.pz = ya.pz * int1 + (yb.pz + yc.pz) * int2;

    double Pij1 = greensFunction(cx, b);

    b.px = yb.px * int1 + (ya.px + yc.px) * int2;
    b.py = yb.py * int1 + (ya.py + yc.py) * int2;
    b.pz = yb.pz * int1 + (ya.pz + yc.pz) * int2;

    Pij1 += greensFunction(cx, b);

    b.px = yc.px * int1 + (yb.px + ya.px) * int2;
    b.py = yc.py * int1 + (yb.py + ya.py) * int2;
    b.pz = yc.pz * int1 + (yb.pz + ya.pz) * int2;

    Pij1 += greensFunction(cx, b);
    Pij1 *= w2;

    b.px = ya.px * int3 + (yb.px + yc.px) * int4;
    b.py = ya.py * int3 + (yb.py + yc.py) * int4;
    b.pz = ya.pz * int3 + (yb.pz + yc.pz) * int4;

    double Pij2 = greensFunction(cx, b);

    b.px = yb.px * int3 + (ya.px + yc.px) * int4;
    b.py = yb.py * int3 + (ya.py + yc.py) * int4;
    b.pz = yb.pz * int3 + (ya.pz + yc.pz) * int4;

    Pij2 += greensFunction(cx, b);

    b.px = yc.px * int3 + (yb.px + ya.px) * int4;
    b.py = yc.py * int3 + (yb.py + ya.py) * int4;
    b.pz = yc.pz * int3 + (yb.pz + ya.pz) * int4;

    Pij2 += greensFunction(cx, b);
    Pij2 *= w3;

    return area * (Pij1 + Pij2);

}

double electrostaticBemSolver::analyticPDiag(Point& cx, Point& ya, Point& yb, Point& yc, double& area){

  double a0, a1, a2, nya, nyb, nyc, nda1, ndb1, ndc1, nda2, ndb2, ndc2;
  double l0, l1, l2;

  a0 = norm(yb, ya);
  a1 = norm(yc, yb);
  a2 = norm(ya, yc);

  nya = norm(ya, cx);
  nyb = norm(yb, cx);
  nyc = norm(yc, cx);

  nda1 = (yb.px - cx.px) * (yb.px - ya.px)/a0 +
         (yb.py - cx.py) * (yb.py - ya.py)/a0 +
         (yb.pz - cx.pz) * (yb.pz - ya.pz)/a0;

  ndb1 = (yc.px - cx.px) * (yc.px - yb.px)/a1 +
         (yc.py - cx.py) * (yc.py - yb.py)/a1 +
         (yc.pz - cx.pz) * (yc.pz - yb.pz)/a1;

  ndc1 = (ya.px - cx.px) * (ya.px - yc.px)/a2 +
         (ya.py - cx.py) * (ya.py - yc.py)/a2 +
         (ya.pz - cx.pz) * (ya.pz - yc.pz)/a2;

  nda2 = (ya.px - cx.px) * (yb.px - ya.px)/a0 +
         (ya.py - cx.py) * (yb.py - ya.py)/a0 +
         (ya.pz - cx.pz) * (yb.pz - ya.pz)/a0;

  ndb2 = (yb.px - cx.px) * (yc.px - yb.px)/a1 +
         (yb.py - cx.py) * (yc.py - yb.py)/a1 +
         (yb.pz - cx.pz) * (yc.pz - yb.pz)/a1;

  ndc2 = (yc.px - cx.px) * (ya.px - yc.px)/a2 +
         (yc.py - cx.py) * (ya.py - yc.py)/a2 +
         (yc.pz - cx.pz) * (ya.pz - yc.pz)/a2;

  l0 = std::log((nyb + nda1)/(nya+nda2));
  l1 = std::log((nyc + ndb1)/(nyb+ndb2));
  l2 = std::log((nya + ndc1)/(nyc+ndc2));

  return area * 1.0/(3.0 * PI) * (l0/a0 + l1/a1 + l2/a2);

}


/*!
 * degree 5 gaussian Cubature
 * source: https://arxiv.org/pdf/1606.03743.pdf

 * Function for calculating the numerical solution to the K kernel x component of the hypersingular BIE
 * Calculates field in domain
*/
double electrostaticBemSolver::numKijx(Point& cx, Point& ya, Point& yb, Point& yc, double& area){

    Point b;

    b.px = ya.px * int1 + (yb.px + yc.px) * int2;
    b.py = ya.py * int1 + (yb.py + yc.py) * int2;
    b.pz = ya.pz * int1 + (yb.pz + yc.pz) * int2;
    double top = cx.px - b.px;

    double Kij1 = this->vecGradGreensFunction(cx, b, top);

    b.px = yb.px * int1 + (ya.px + yc.px) * int2;
    b.py = yb.py * int1 + (ya.py + yc.py) * int2;
    b.pz = yb.pz * int1 + (ya.pz + yc.pz) * int2;
    top = cx.px - b.px;

    Kij1 += this->vecGradGreensFunction(cx, b, top);

    b.px = yc.px * int1 + (yb.px + ya.px) * int2;
    b.py = yc.py * int1 + (yb.py + ya.py) * int2;
    b.pz = yc.pz * int1 + (yb.pz + ya.pz) * int2;
    top = cx.px - b.px;

    Kij1 += this->vecGradGreensFunction(cx, b, top);
    Kij1 *= w2;

    b.px = ya.px * int3 + (yb.px + yc.px) * int4;
    b.py = ya.py * int3 + (yb.py + yc.py) * int4;
    b.pz = ya.pz * int3 + (yb.pz + yc.pz) * int4;
    top = cx.px - b.px;

    double Kij2 = this->vecGradGreensFunction(cx, b, top);

    b.px = yb.px * int3 + (ya.px + yc.px) * int4;
    b.py = yb.py * int3 + (ya.py + yc.py) * int4;
    b.pz = yb.pz * int3 + (ya.pz + yc.pz) * int4;
    top = cx.px - b.px;

    Kij2 += this->vecGradGreensFunction(cx, b, top);

    b.px = yc.px * int3 + (yb.px + ya.px) * int4;
    b.py = yc.py * int3 + (yb.py + ya.py) * int4;
    b.pz = yc.pz * int3 + (yb.pz + ya.pz) * int4;
    top = cx.px - b.px;

    Kij2 += this->vecGradGreensFunction(cx, b, top);
    Kij2 *= w3;

  return area * (Kij1 + Kij2);

}


/*!
 * degree 5 gaussian Cubature
 * source: https://arxiv.org/pdf/1606.03743.pdf

 * Function for calculating the numerical solution to the K-kernel components of the hypersingular BIE
 * Note that this function does not calculate the H-kernel components, that are identically zero under the conductor approximation.
 *
 * The hypersingular BIE is used to calculate the electrostatic field in the domain.
*/
inline void electrostaticBemSolver::numKij(double& Kijx, double& Kijy, double& Kijz, Point& cx, Point& ya, Point& yb, Point& yc, double& area) {

    double topx, topy, topz, Kijx2, Kijy2, Kijz2, dist2;
    Point b(0.0, 0.0, 0.0);

    b.px = ya.px * int1 + (yb.px + yc.px) * int2;
    b.py = ya.py * int1 + (yb.py + yc.py) * int2;
    b.pz = ya.pz * int1 + (yb.pz + yc.pz) * int2;

    topx = cx.px - b.px;
    topy = cx.py - b.py;
    topz = cx.pz - b.pz;
    dist2 = (topx * topx + topy * topy + topz * topz);
    dist2 = 1.0 / std::sqrt(dist2 * dist2 * dist2);

    Kijx = topx * dist2;
    Kijy = topy * dist2;
    Kijz = topz * dist2;

    b.px = yb.px * int1 + (ya.px + yc.px) * int2;
    b.py = yb.py * int1 + (ya.py + yc.py) * int2;
    b.pz = yb.pz * int1 + (ya.pz + yc.pz) * int2;

    topx = cx.px - b.px;
    topy = cx.py - b.py;
    topz = cx.pz - b.pz;
    dist2 = (topx * topx + topy * topy + topz * topz);
    dist2 = 1.0 / std::sqrt(dist2 * dist2 * dist2);

    Kijx += topx * dist2;
    Kijy += topy * dist2;
    Kijz += topz * dist2;

    b.px = yc.px * int1 + (yb.px + ya.px) * int2;
    b.py = yc.py * int1 + (yb.py + ya.py) * int2;
    b.pz = yc.pz * int1 + (yb.pz + ya.pz) * int2;

    topx = cx.px - b.px;
    topy = cx.py - b.py;
    topz = cx.pz - b.pz;
    dist2 = (topx * topx + topy * topy + topz * topz);
    dist2 = 1.0 / std::sqrt(dist2 * dist2 * dist2);

    Kijx += topx * dist2;
    Kijy += topy * dist2;
    Kijz += topz * dist2;

    Kijx *= w2;
    Kijy *= w2;
    Kijz *= w2;

    b.px = ya.px * int3 + (yb.px + yc.px) * int4;
    b.py = ya.py * int3 + (yb.py + yc.py) * int4;
    b.pz = ya.pz * int3 + (yb.pz + yc.pz) * int4;

    topx = cx.px - b.px;
    topy = cx.py - b.py;
    topz = cx.pz - b.pz;
    dist2 = (topx * topx + topy * topy + topz * topz);
    dist2 = 1.0 / std::sqrt(dist2 * dist2 * dist2);

    Kijx2 = topx * dist2;
    Kijy2 = topy * dist2;
    Kijz2 = topz * dist2;

    b.px = yb.px * int3 + (ya.px + yc.px) * int4;
    b.py = yb.py * int3 + (ya.py + yc.py) * int4;
    b.pz = yb.pz * int3 + (ya.pz + yc.pz) * int4;

    topx = cx.px - b.px;
    topy = cx.py - b.py;
    topz = cx.pz - b.pz;
    dist2 = (topx * topx + topy * topy + topz * topz);
    dist2 = 1.0 / std::sqrt(dist2 * dist2 * dist2);

    Kijx2 += topx * dist2;
    Kijy2 += topy * dist2;
    Kijz2 += topz * dist2;

    b.px = yc.px * int3 + (yb.px + ya.px) * int4;
    b.py = yc.py * int3 + (yb.py + ya.py) * int4;
    b.pz = yc.pz * int3 + (yb.pz + ya.pz) * int4;

    topx = cx.px - b.px;
    topy = cx.py - b.py;
    topz = cx.pz - b.pz;
    dist2 = (topx * topx + topy * topy + topz * topz);
    dist2 = 1.0 / std::sqrt(dist2 * dist2 * dist2);

    Kijx2 += topx * dist2;
    Kijy2 += topy * dist2;
    Kijz2 += topz * dist2;

    Kijx2 *= w3;
    Kijy2 *= w3;
    Kijz2 *= w3;

    Kijx = area * (Kijx + Kijx2);
    Kijy = area * (Kijy + Kijy2);
    Kijz = area * (Kijz + Kijz2);

    return;
}

/*!
 * degree 5 gaussian Cubature
 * source: https://arxiv.org/pdf/1606.03743.pdf

 * Function for calculating the numerical solution to the K kernel y component of the hypersingular BIE
 * Calculates field in domain
*/
double electrostaticBemSolver::numKijy(Point& cx, Point& ya, Point& yb, Point& yc, double& area){

    Point b;
    b.px = ya.px * int1 + (yb.px + yc.px) * int2;
    b.py = ya.py * int1 + (yb.py + yc.py) * int2;
    b.pz = ya.pz * int1 + (yb.pz + yc.pz) * int2;

    double top = cx.py - b.py;

    double Kij1 = this->vecGradGreensFunction(cx, b, top);

    b.px = yb.px * int1 + (ya.px + yc.px) * int2;
    b.py = yb.py * int1 + (ya.py + yc.py) * int2;
    b.pz = yb.pz * int1 + (ya.pz + yc.pz) * int2;
    top = cx.py - b.py;

    Kij1 += this->vecGradGreensFunction(cx, b, top);

    b.px = yc.px * int1 + (yb.px + ya.px) * int2;
    b.py = yc.py * int1 + (yb.py + ya.py) * int2;
    b.pz = yc.pz * int1 + (yb.pz + ya.pz) * int2;
    top = cx.py - b.py;

    Kij1 += this->vecGradGreensFunction(cx, b, top);
    Kij1 *= w2;

    b.px = ya.px * int3 + (yb.px + yc.px) * int4;
    b.py = ya.py * int3 + (yb.py + yc.py) * int4;
    b.pz = ya.pz * int3 + (yb.pz + yc.pz) * int4;
    top = cx.py - b.py;

    double Kij2 = this->vecGradGreensFunction(cx, b, top);

    b.px = yb.px * int3 + (ya.px + yc.px) * int4;
    b.py = yb.py * int3 + (ya.py + yc.py) * int4;
    b.pz = yb.pz * int3 + (ya.pz + yc.pz) * int4;
    top = cx.py - b.py;

    Kij2 += this->vecGradGreensFunction(cx, b, top);

    b.px = yc.px * int3 + (yb.px + ya.px) * int4;
    b.py = yc.py * int3 + (yb.py + ya.py) * int4;
    b.pz = yc.pz * int3 + (yb.pz + ya.pz) * int4;
    top = cx.py - b.py;

    Kij2 += this->vecGradGreensFunction(cx, b, top);
    Kij2 *= w3;

    return area * (Kij1 + Kij2);

}

/*!
 * degree 5 gaussian Cubature
 * source: https://arxiv.org/pdf/1606.03743.pdf

 * Function for calculating the numerical solution to the K kernel z component of the hypersingular BIE
 * Calculates field in domain
*/
double electrostaticBemSolver::numKijz(Point& cx, Point& ya, Point& yb, Point& yc, double& area){

    Point b;
    b.px = ya.px * int1 + (yb.px + yc.px) * int2;
    b.py = ya.py * int1 + (yb.py + yc.py) * int2;
    b.pz = ya.pz * int1 + (yb.pz + yc.pz) * int2;

    double top = cx.pz - b.pz;
    double Kij1 = this->vecGradGreensFunction(cx, b, top);

    b.px = yb.px * int1 + (ya.px + yc.px) * int2;
    b.py = yb.py * int1 + (ya.py + yc.py) * int2;
    b.pz = yb.pz * int1 + (ya.pz + yc.pz) * int2;
    top = cx.pz - b.pz;

    Kij1 += this->vecGradGreensFunction(cx, b, top);

    b.px = yc.px * int1 + (yb.px + ya.px) * int2;
    b.py = yc.py * int1 + (yb.py + ya.py) * int2;
    b.pz = yc.pz * int1 + (yb.pz + ya.pz) * int2;
    top = cx.pz - b.pz;

    Kij1 += this->vecGradGreensFunction(cx, b, top);
    Kij1 *= w2;

    b.px = ya.px * int3 + (yb.px + yc.px) * int4;
    b.py = ya.py * int3 + (yb.py + yc.py) * int4;
    b.pz = ya.pz * int3 + (yb.pz + yc.pz) * int4;
    top = cx.pz - b.pz;

    double Kij2 = this->vecGradGreensFunction(cx, b, top);

    b.px = yb.px * int3 + (ya.px + yc.px) * int4;
    b.py = yb.py * int3 + (ya.py + yc.py) * int4;
    b.pz = yb.pz * int3 + (ya.pz + yc.pz) * int4;
    top = cx.pz - b.pz;

    Kij2 += this->vecGradGreensFunction(cx, b, top);

    b.px = yc.px * int3 + (yb.px + ya.px) * int4;
    b.py = yc.py * int3 + (yb.py + ya.py) * int4;
    b.pz = yc.pz * int3 + (yb.pz + ya.pz) * int4;
    top = cx.pz - b.pz;

    Kij2 += this->vecGradGreensFunction(cx, b, top);
    Kij2 *= w3;

    return area * (Kij1 + Kij2);

}

OctreeNode::OctreeNode(double minCoords[3], double maxCoords[3],
                       TriangularMesh* surfaceMesh,
                       electrostaticBemSolver* bemSolver,
                       OctreeNode *parent){

  this->minCoords[0] = minCoords[0];
  this->minCoords[1] = minCoords[1];
  this->minCoords[2] = minCoords[2];

  this->maxCoords[0] = maxCoords[0];
  this->maxCoords[1] = maxCoords[1];
  this->maxCoords[2] = maxCoords[2];

  this->surfaceMesh = surfaceMesh;
  this->bemSolver = bemSolver;
  this->parent = parent;
  leaf = false;

  //recursively split octree nodes
  inherit();
  calculateMoments();
  if (memberFaces.size() > 0){
    clusterParameters();
  }
  if (memberFaces.size() > 5){
    splitOctreeNode();
  }
  else {
    leaf = true;
  }
}


OctreeNode::OctreeNode(double minCoords[3], double maxCoords[3],
                       TriangularMesh& surfaceMesh,
                       electrostaticBemSolver& bemSolver,
                       OctreeNode *parent){

  this->minCoords[0] = minCoords[0];
  this->minCoords[1] = minCoords[1];
  this->minCoords[2] = minCoords[2];

  this->maxCoords[0] = maxCoords[0];
  this->maxCoords[1] = maxCoords[1];
  this->maxCoords[2] = maxCoords[2];

  this->surfaceMesh = &surfaceMesh;
  this->bemSolver = &bemSolver;
  this->parent = parent;
  leaf = false;

  //recursively split octree nodes
  inherit();
  calculateMoments();
  if (memberFaces.size() > 0){
    clusterParameters();
  }
  if (memberFaces.size() > 5){
    splitOctreeNode();
  }
  else {
    leaf = true;
  }
}

void OctreeNode::inherit(){
  //if top node
  if (parent == nullptr){
    memberFaces = vector<long>(surfaceMesh->faces.size(), 0);
    iota(memberFaces.begin(), memberFaces.end(), 0);
    level = 0;
  }
  else { //otherwise inherit from children nodes
    for (int i=0;i<parent->memberFaces.size();i++){
      if ((surfaceMesh->faces[parent->memberFaces[i]].centre.px >= minCoords[0]) && (surfaceMesh->faces[parent->memberFaces[i]].centre.px < maxCoords[0]) &&
          (surfaceMesh->faces[parent->memberFaces[i]].centre.py >= minCoords[1]) && (surfaceMesh->faces[parent->memberFaces[i]].centre.py < maxCoords[1]) &&
          (surfaceMesh->faces[parent->memberFaces[i]].centre.pz >= minCoords[2]) && (surfaceMesh->faces[parent->memberFaces[i]].centre.pz < maxCoords[2])){
            memberFaces.push_back(parent->memberFaces[i]);
        }
      }
      level = parent->level + 1;
    }
  }

void OctreeNode::clusterParameters(){

  set<long> pointInds;
  for (int i=0;i<memberFaces.size();i++){
    pointInds.insert(surfaceMesh->faces[memberFaces[i]].p1);
    pointInds.insert(surfaceMesh->faces[memberFaces[i]].p2);
    pointInds.insert(surfaceMesh->faces[memberFaces[i]].p3);
  }
  set<long>::iterator it = pointInds.begin();
  while (it != pointInds.end()) {
    centre[0] += surfaceMesh->vertices[*it].px;
    centre[1] += surfaceMesh->vertices[*it].py;
    centre[2] += surfaceMesh->vertices[*it].pz;
    it++;
  }

  centre[0] /= memberFaces.size();
  centre[1] /= memberFaces.size();
  centre[2] /= memberFaces.size();

  radius = 0.0;
  it = pointInds.begin();
  while (it != pointInds.end()) {
    double dx = surfaceMesh->vertices[*it].px - centre[0];
    double dy = surfaceMesh->vertices[*it].py - centre[1];
    double dz = surfaceMesh->vertices[*it].pz - centre[2];

    double tradius = sqrt(dx*dx + dy*dy + dz*dz);
    if (radius < tradius){
      radius = tradius;
    }
    it++;
  }

}

void OctreeNode::calculateMoments(){
  zerothMomentK = this->calculateZerothMomentK();
}

double OctreeNode::calculateZerothMomentK(){

  double momentValue = 0.0;
  const int memberFacesSize = memberFaces.size();

  for (int i=0;i<memberFacesSize;i++){
    momentValue -= this->surfaceMesh->faces[memberFaces[i]].area * this->bemSolver->q[memberFaces[i]];
  }

  return momentValue * kernelConst;
}

void OctreeNode::splitOctreeNode(){
  double npx = (maxCoords[0] + minCoords[0])/2.0;
  double npy = (maxCoords[1] + minCoords[1])/2.0;
  double npz = (maxCoords[2] + minCoords[2])/2.0;

  double c1MinCoords[3] = {minCoords[0], minCoords[1], minCoords[2]};
  double c1MaxCoords[3] = {npx, npy, npz};

  double c2MinCoords[3] = {npx, minCoords[1], minCoords[2]};
  double c2MaxCoords[3] = {maxCoords[0], npy, npz};

  double c3MinCoords[3] = {minCoords[0], npy, minCoords[2]};
  double c3MaxCoords[3] = {npx, maxCoords[1], npz};

  double c4MinCoords[3] = {npx, npy, minCoords[2]};
  double c4MaxCoords[3] = {maxCoords[0], maxCoords[1], npz};

  double c5MinCoords[3] = {minCoords[0], minCoords[1], npz};
  double c5MaxCoords[3] = {npx, npy, maxCoords[2]};

  double c6MinCoords[3] = {npx, minCoords[1], npz};
  double c6MaxCoords[3] = {maxCoords[0], npy, maxCoords[2]};

  double c7MinCoords[3] = {minCoords[0], npy, npz};
  double c7MaxCoords[3] = {npx, maxCoords[1], maxCoords[2]};

  double c8MinCoords[3] = {npx, npy, npz};
  double c8MaxCoords[3] = {maxCoords[0], maxCoords[1], maxCoords[2]};

  std::unique_ptr<OctreeNode> childNode1(new OctreeNode(c1MinCoords, c1MaxCoords, surfaceMesh, bemSolver, this));
  std::unique_ptr<OctreeNode> childNode2(new OctreeNode(c2MinCoords, c2MaxCoords, surfaceMesh, bemSolver, this));
  std::unique_ptr<OctreeNode> childNode3(new OctreeNode(c3MinCoords, c3MaxCoords, surfaceMesh, bemSolver, this));
  std::unique_ptr<OctreeNode> childNode4(new OctreeNode(c4MinCoords, c4MaxCoords, surfaceMesh, bemSolver, this));
  std::unique_ptr<OctreeNode> childNode5(new OctreeNode(c5MinCoords, c5MaxCoords, surfaceMesh, bemSolver, this));
  std::unique_ptr<OctreeNode> childNode6(new OctreeNode(c6MinCoords, c6MaxCoords, surfaceMesh, bemSolver, this));
  std::unique_ptr<OctreeNode> childNode7(new OctreeNode(c7MinCoords, c7MaxCoords, surfaceMesh, bemSolver, this));
  std::unique_ptr<OctreeNode> childNode8(new OctreeNode(c8MinCoords, c8MaxCoords, surfaceMesh, bemSolver, this));

  if (childNode1->memberFaces.size() > 0){
    children.push_back(std::move(childNode1));
  }
  if (childNode2->memberFaces.size() > 0){
    children.push_back(std::move(childNode2));
  }
  if (childNode3->memberFaces.size() > 0){
    children.push_back(std::move(childNode3));
  }
  if (childNode4->memberFaces.size() > 0){
    children.push_back(std::move(childNode4));
  }
  if (childNode5->memberFaces.size() > 0){
    children.push_back(std::move(childNode5));
  }
  if (childNode6->memberFaces.size() > 0){
    children.push_back(std::move(childNode6));
  }
  if (childNode7->memberFaces.size() > 0){
    children.push_back(std::move(childNode7));
  }
  if (childNode8->memberFaces.size() > 0){
    children.push_back(std::move(childNode8));
  }
}

void OctreeNode::calculateField(double& v1, double& v2, double& v3, Point& evalPoint, const double& admissTol){

  double dx = evalPoint.px - centre[0];
  double dy = evalPoint.py - centre[1];
  double dz = evalPoint.pz - centre[2];

  double pot = 1.0/(dx*dx + dy*dy + dz*dz);

  if (pot * radius * radius < admissTol * admissTol){
    pot = std::sqrt(pot);
    pot = pot * pot * pot;
    v1 -= zerothMomentK * pot * dx;
    v2 -= zerothMomentK * pot * dy;
    v3 -= zerothMomentK * pot * dz;
    return;
  }
  if (memberFaces.size() > 30) {
    const int childrenSize = children.size();
    for (int c=0;c<childrenSize;c++){
      //cout << "cluster field calc: " << memberFaces.size() << endl;
      children[c]->calculateField(v1, v2, v3, evalPoint, admissTol);
    }
    return;
  }



  //cout << "direct field calculation" << endl;
  this->directClusterPanelFieldEvaluation(v1, v2, v3, evalPoint);

  return;
}

inline void OctreeNode::directClusterPanelFieldEvaluation(double& v1, double& v2, double& v3, Point& evalPoint){

  const int memberFacesSize = memberFaces.size();
  double Exa, Eya, Eza;
  double Kijx, Kijy, Kijz;

  for (int f=0;f<memberFacesSize;f++){
    int mf = memberFaces[f];

    Point& centre = surfaceMesh->faces[mf].centre;
    double& area = surfaceMesh->faces[mf].area;
    double& BF = bemSolver->q[mf];

    double dx = evalPoint.px - centre.px;
    double dy = evalPoint.py - centre.py;
    double dz = evalPoint.pz - centre.pz;

    double dist2 = dx * dx + dy * dy + dz * dz;
    double invdist = 1.0/std::sqrt(dist2 * dist2 * dist2) * area * BF;
    Exa = dx * invdist;
    Eya = dy * invdist;
    Eza = dz * invdist;

    //Exa = bemSolver->vecGradGreensFunction(evalPoint, centre, dx) * area * BF;
    //Eya = bemSolver->vecGradGreensFunction(evalPoint, centre, dy) * area * BF;
    //Eza = bemSolver->vecGradGreensFunction(evalPoint, centre, dz) * area * BF;

    if (surfaceMesh->faces[mf].ls2 > bemSolver->tol * bemSolver->tol * dist2){
      Point& ya = surfaceMesh->vertices[surfaceMesh->faces[mf].p1];
      Point& yb = surfaceMesh->vertices[surfaceMesh->faces[mf].p2];
      Point& yc = surfaceMesh->vertices[surfaceMesh->faces[mf].p3];

      Exa *= w1;
      Eya *= w1;
      Eza *= w1;

      bemSolver->numKij(Kijx, Kijy, Kijz, evalPoint, ya, yb, yc, area);
      Exa += Kijx * BF;
      Eya += Kijy * BF;
      Eza += Kijz * BF;

    }

    v1 += Exa * kernelConst;
    v2 += Eya * kernelConst;
    v3 += Eza * kernelConst;

  }

  return;
}

void OctreeNode::buildBoundingMesh(TriangularMesh& octreeMesh){

  long nc = octreeMesh.vertices.size();

  double xmin = minCoords[0];
  double ymin = minCoords[1];
  double zmin = minCoords[2];

  double xmax = maxCoords[0];
  double ymax = maxCoords[1];
  double zmax = maxCoords[2];

  //create points
  Point P0(xmin, ymin, zmin);
  Point P1(xmax, ymin, zmin);
  Point P2(xmin, ymax, zmin);
  Point P3(xmax, ymax, zmin);
  Point P4(xmin, ymin, zmax);
  Point P5(xmax, ymin, zmax);
  Point P6(xmin, ymax, zmax);
  Point P7(xmax, ymax, zmax);

  //create faces - must be defined consistently anticlockwise
  Face F0(0+nc, 1+nc, 3+nc);
  Face F1(0+nc, 3+nc, 2+nc);

  Face F2(5+nc, 4+nc, 7+nc);
  Face F3(7+nc, 4+nc, 6+nc);

  Face F4(0+nc, 4+nc, 5+nc);
  Face F5(5+nc, 1+nc, 0+nc);

  Face F6(2+nc, 7+nc, 6+nc);
  Face F7(2+nc, 3+nc, 7+nc);

  Face F8(0+nc, 6+nc, 4+nc);
  Face F9(0+nc, 2+nc, 6+nc);

  Face F10(1+nc, 5+nc, 7+nc);
  Face F11(1+nc, 7+nc, 3+nc);

  //add mesh vertices
  octreeMesh.vertices.push_back(P0);
  octreeMesh.vertices.push_back(P1);
  octreeMesh.vertices.push_back(P2);
  octreeMesh.vertices.push_back(P3);
  octreeMesh.vertices.push_back(P4);
  octreeMesh.vertices.push_back(P5);
  octreeMesh.vertices.push_back(P6);
  octreeMesh.vertices.push_back(P7);

  //add mesh faces
  octreeMesh.faces.push_back(F0);
  octreeMesh.faces.push_back(F1);
  octreeMesh.faces.push_back(F2);
  octreeMesh.faces.push_back(F3);
  octreeMesh.faces.push_back(F4);
  octreeMesh.faces.push_back(F5);
  octreeMesh.faces.push_back(F6);
  octreeMesh.faces.push_back(F7);
  octreeMesh.faces.push_back(F8);
  octreeMesh.faces.push_back(F9);
  octreeMesh.faces.push_back(F10);
  octreeMesh.faces.push_back(F11);

  for (int i=0;i<children.size();i++){
    children[i]->buildBoundingMesh(octreeMesh);
  }
}
