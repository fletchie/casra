/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <math.h>
#include <vector>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/electrostatics/constantBEM.h"

#include "mgmres/mgmres.h"

#define SMALL 1e-6

//GMRES solver parameters
#define SOLVERTOL 1e-5 //relative tolerance of residual at which convergence is achieved
#define SOLVERRESTARTs 5 //max number of restarts before termination
#define SOLVERITERATIONSPERRESTART 30 //max number of matrix-vector multiplications between restarts
#define SOLVERPRECONDITIONER true //apply the Jacobi preconditioner by default

using namespace std;

/*!
 * Function for passing a particular linear problem to the GMRES solver within
 * mgmres.
 * @param mat A 2D vector defining the problem matrix (LHS)
 * @param rvec The problem load vector (RHS)
 * @param sol The problem solution vector
 * @return (0) if successful, (1) otherwise
 */
int solveField(matrix& mat, vector<double>& rvec, vector<double>& sol, double*& Amat, int& AmatDimSize){

  cout << "Initialising solver..." << endl;

  const int matSize = mat.size();

  //build Jacobi preconditioner
  #if SOLVERPRECONDITIONER == true
    vector<double> jacobiPreconditioner(matSize, 0.0);
    for (int i=0;i<matSize; i++){
      jacobiPreconditioner[i] = 1.0/mat[i][i];
    }

    vector<double> RHS(rvec.size(), 0.0);

    diagMatmul(mat, jacobiPreconditioner, mat);
    diagMatmul(RHS, jacobiPreconditioner, rvec);
  #else
    vector<double>& RHS = rvec;
  #endif

  cout << "copying matrix..." << endl;

  const long dx = mat.size();
  const long dy = mat[0].size();

  const int sp = dx * dy;

  //double *Amat;
  double *x;
  double *w;

  if (Amat == nullptr) {
      AmatDimSize = (int)(sp * 1.25);
      Amat = new double[AmatDimSize];
  }
  else if (AmatDimSize < sp) {
      delete[] Amat;
      AmatDimSize = (int)(sp * 1.25);
      Amat = new double[AmatDimSize];
  }

  x = new double[dx];
  w = new double[dx];

  //unwrap problem matrix into 1D dense array (required input for mgmres) - this copying operation is costly and should be sped up in the future
  #if defined(_OPENMP)
  #pragma omp parallel
  #endif
  {
  int it;

  #if defined(_OPENMP)
  #pragma omp for
  #endif
  for (int i = 0;i<dx;i++){
    for (int j = 0; j<dy;j++){
      it = j * dx + i;
      Amat[it] = mat[i][j];
    }
    w[i] = RHS[i];
    x[i] = -1;
  }
  }

  if (sol.size() == dx){
    for (int i = 0; i<dx; i++){
      x[i] = sol[i]; //output array must be preset to initial guess
    }
  }

  cout << "solving field..." << endl;
  int conv = gmres(dx, Amat, x, w, SOLVERRESTARTs,
                   SOLVERITERATIONSPERRESTART, SOLVERTOL, SOLVERTOL);
  cout << "field solved" << endl;

  //clear any initial guess
  sol.resize(dx);

  //populate solution vector with converged solution
  for (int i=0; i<dx; i++){
    sol[i] = x[i];
  }

  //delete dynamically allocated vectors
  delete[] x;
  delete[] w;

  return conv;

}
