/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <cmath>
#include <stdexcept>
#include <chrono>
#include <string>
#include <numeric>
#include <tuple>
#include <chrono>

#include "CASRA/chargedOptics/hitmapCalculation.h"

using namespace std;

#define SMALL 1e-30

inline int computeCode(double& x, double& y, double& sxmin, double& symin, double& sxmax, double& symax){

  int code = 0;
  if (x < sxmin)
    code |= 1;
  else if (x > sxmax)
    code |= 2;
  if (y < symin)
    code |= 4;
  else if (y > symax)
    code |= 8;

  return code;
}

tuple<bool, double, double, double, double> cohenSutherland(double sxmin, double symin, double sxmax, double symax, double x1, double y1, double x2, double y2){

  double x, y;

  int code1 = computeCode(x1, y1, sxmin, symin, sxmax, symax);
  int code2 = computeCode(x2, y2, sxmin, symin, sxmax, symax);

  int codeOut = 0;
  bool accept = false;

  if ((fabs(y2 - y1) < SMALL) && (fabs(x2 - x1) < SMALL)) {
    tuple<bool, double, double, double, double> intersectionResult = {false, x1, y1, x2, y2};
    return intersectionResult;
  }

  while (true == true) {

    if ((code1 == 0) & (code2 == 0)) {
      accept = true;
      break;
    }
    else if ((code1 & code2) != 0){
      break;
    }
    else {
      x = 1.0;
      y = 1.0;
      if (code1 != 0){
        codeOut = code1;
      }
      else{
        codeOut = code2;
      }

      if (codeOut & 8){
        x = x1 + (x2 - x1) * (symax - y1)/(y2 - y1);
        y = symax;
      }
      else if (codeOut & 4){
        x = x1 + (x2 - x1) * (symin - y1)/(y2 - y1);
        y = symin;
      }
      else if (codeOut & 2){
        x = sxmax;
        y = y1 + (y2 - y1) * (sxmax - x1)/(x2 - x1);
      }
      else if (codeOut & 1){
        x = sxmin;
        y = y1 + (y2 - y1) * (sxmin - x1)/(x2 - x1);
      }

      if (codeOut == code1){
        x1 = x;
        y1 = y;
        code1 = computeCode(x1, y1, sxmin, symin, sxmax, symax);
      }
      else {
        x2 = x;
        y2 = y;
        code2 = computeCode(x2, y2, sxmin, symin, sxmax, symax);
      }
    }
  }

  tuple<bool, double, double, double, double> intersectionResult = {accept, x1, y1, x2, y2};
  return intersectionResult;
}

inline bool pointIntriangle(double& px, double& py, Point& detPos1, Point& detPos2, Point& detPos3){

  double d1 = (px - detPos2.px) * (detPos1.py - detPos2.py) - (detPos1.px - detPos2.px) * (py - detPos2.py);
  double d2 = (px - detPos3.px) * (detPos2.py - detPos3.py) - (detPos2.px - detPos3.px) * (py - detPos3.py);
  double d3 = (px - detPos1.px) * (detPos3.py - detPos1.py) - (detPos3.px - detPos1.px) * (py - detPos1.py);

  bool hasNeg = (d1 < 0.0) | (d2 < 0.0) | (d3 < 0.0);
  bool hasPos = (d1 > 0.0) | (d2 > 0.0) | (d3 > 0.0);

  return !(hasNeg & hasPos);

}

double triangleRectangleIntersection(double& sxmin, double& symin, double& sxmax, double& symax,
                                     Point& detPos1, Point& detPos2, Point& detPos3, double& triangleArea){

  //calculate triangle bounding box
  double txmin = min({detPos1.px, detPos2.px, detPos3.px});
  double txmax = max({detPos1.px, detPos2.px, detPos3.px});
  double tymin = min({detPos1.py, detPos2.py, detPos3.py});
  double tymax = max({detPos1.py, detPos2.py, detPos3.py});

  //check if triangle bounding box is completely outside of rectangle
  if (tymax < symin || txmax < sxmin || tymin > symax || txmin > sxmax)
    return 0.0;

  //check if triangle bounding box is completely inside of rectangle
  if (tymin >= symin && txmin >= sxmin && tymax <= symax && txmax <= sxmax)
    return triangleArea;

  bool p1 = pointIntriangle(sxmin, symin, detPos1, detPos2, detPos3);
  bool p2 = pointIntriangle(sxmax, symin, detPos1, detPos2, detPos3);
  bool p3 = pointIntriangle(sxmin, symax, detPos1, detPos2, detPos3);
  bool p4 = pointIntriangle(sxmax, symax, detPos1, detPos2, detPos3);

  //check if square completely inside triangle
  if (p1 & p2 & p3 & p4)
    return (sxmax - sxmin) * (symax - symin);

  //proceeding to non-trivial intersection cases
  vector<double> polygonx;
  vector<double> polygony;

  polygonx.reserve(8);
  polygony.reserve(8);

  tuple<bool, double, double, double, double> intersectionResultAB = cohenSutherland(sxmin, symin,
                                                                                     sxmax, symax,
                                                                                     detPos1.px, detPos1.py,
                                                                                     detPos2.px, detPos2.py);
  tuple<bool, double, double, double, double> intersectionResultBC = cohenSutherland(sxmin, symin,
                                                                                     sxmax, symax,
                                                                                     detPos2.px, detPos2.py,
                                                                                     detPos3.px, detPos3.py);
  tuple<bool, double, double, double, double> intersectionResultCA = cohenSutherland(sxmin, symin,
                                                                                     sxmax, symax,
                                                                                     detPos3.px, detPos3.py,
                                                                                     detPos1.px, detPos1.py);

  bool ABacc = get<0>(intersectionResultAB);
  double AB1x = get<1>(intersectionResultAB);
  double AB1y = get<2>(intersectionResultAB);
  double AB2x = get<3>(intersectionResultAB);
  double AB2y = get<4>(intersectionResultAB);

  bool BCacc = get<0>(intersectionResultBC);
  double BC1x = get<1>(intersectionResultBC);
  double BC1y = get<2>(intersectionResultBC);
  double BC2x = get<3>(intersectionResultBC);
  double BC2y = get<4>(intersectionResultBC);

  bool CAacc = get<0>(intersectionResultCA);
  double CA1x = get<1>(intersectionResultCA);
  double CA1y = get<2>(intersectionResultCA);
  double CA2x = get<3>(intersectionResultCA);
  double CA2y = get<4>(intersectionResultCA);

  if ((ABacc == false) && (BCacc == false) && (CAacc == false)){
    if (p1 | p2 | p3 | p4){
      return (sxmax - sxmin) * (symax - symin);
    }
    else {
      return 0.0;
    }
  }

  if (ABacc == true){
    polygonx.push_back(AB1x);
    polygony.push_back(AB1y);
    polygonx.push_back(AB2x);
    polygony.push_back(AB2y);
  }

  if (BCacc == true){
    if (ABacc == true){
      if (fabs(AB2x - BC1x) > SMALL | fabs(AB2y - BC1y) > SMALL){
        polygonx.push_back(BC1x);
        polygony.push_back(BC1y);
      }
    }
    else {
      polygonx.push_back(BC1x);
      polygony.push_back(BC1y);
    }
    polygonx.push_back(BC2x);
    polygony.push_back(BC2y);
  }

  if (CAacc == true){
    if (BCacc == true){
      if (fabs(BC2x - CA1x) > SMALL | fabs(BC2y - CA1y) > SMALL){
        polygonx.push_back(CA1x);
        polygony.push_back(CA1y);
      }
    }
    else {
      polygonx.push_back(CA1x);
      polygony.push_back(CA1y);
    }
    polygonx.push_back(CA2x);
    polygony.push_back(CA2y);
  }

  if (p1 == true){
    polygonx.push_back(sxmin);
    polygony.push_back(symin);
  }

  if (p2 == true){
    polygonx.push_back(sxmax);
    polygony.push_back(symin);
  }
  if (p3 == true){
    polygonx.push_back(sxmin);
    polygony.push_back(symax);
  }
  if (p4 == true){
    polygonx.push_back(sxmax);
    polygony.push_back(symax);
  }

  int pc = polygonx.size();

  //calculate polygon segment angles
  double centroid[2] = {0.0, 0.0};
  for (int p=0;p<pc;p++){
    centroid[0] += polygonx[p];
    centroid[1] += polygony[p];
  }

  centroid[0] /= pc;
  centroid[1] /= pc;

  vector<double> ang;
  vector<double> polygonxSorted;
  vector<double> polygonySorted;

  ang.reserve(pc);
  polygonxSorted.reserve(pc);
  polygonySorted.reserve(pc);

  //calculate angles of polygon segments with respect to intersection polygon centroid
  for (int a=0;a<pc;a++){
    ang.push_back(atan2(polygony[a] - centroid[1], polygonx[a] - centroid[0]));
  }

  //perform argsort to arrange polygon anticlockwise
  vector<int> angleSortedIndex(ang.size(), 0);
  iota(angleSortedIndex.begin(),angleSortedIndex.end(),0);
  sort(angleSortedIndex.begin(), angleSortedIndex.end(), [&](int i,int j){return ang[i] < ang[j];});

  for (int a=0;a<pc;a++){
    polygonxSorted.push_back(polygonx[angleSortedIndex[a]]);
    polygonySorted.push_back(polygony[angleSortedIndex[a]]);
  }

  //calculate intersecting polygon area
  //Uses Dirichlet Polygon areas algorithm
  double polygonArea = 0.0;

  for (int a=0;a<(pc-1);a++){
    polygonArea += polygonxSorted[a] * polygonySorted[a+1] - polygonxSorted[a+1] * polygonySorted[a];
  }
  polygonArea += polygonxSorted[pc-1] * polygonySorted[0] - polygonxSorted[0] * polygonySorted[pc-1];
  polygonArea *= 0.5;

  return fabs(polygonArea);

}

/*!
 * Function for calculating the density hitmap and trajectory crossover region via flux projection
 *
 * @param ion1SamplePos panel corner 1 launch ion sample surface positions
 * @param ion1DetectorPos panel corner 1 launch ion detector positions
 * @param ion2SamplePos panel corner 2 launch ion sample surface positions
 * @param ion2DetectorPos panel corner 2 launch ion detector positions
 * @param ion3SamplePos panel corner 3 launch ion sample surface positions
 * @param ion3DetectorPos panel corner 3 launch ion detector positions
 * @param evapRate panel local evaporation rates
 * @param magnification panel local magnifications
 * @param detectorRadius the detector radius
 * @param xres the number of cells along the x-axis
 * @param yres the number of cells along the y-axis
 * @return tuple containing
 *  (1) The density detector hitmap
 *  (2) The crossover detector map
 *
 */
tuple<scalarField3D, scalarField3D> calculateHitmap(vector<Point>& ion1SamplePos, vector<Point>& ion1DetectorPos,
                                    vector<Point>& ion2SamplePos, vector<Point>& ion2DetectorPos,
                                    vector<Point>& ion3SamplePos, vector<Point>& ion3DetectorPos,
                                    vector<double>& evapRate, vector<double>& magnification,
                                    const double detectorRadius, const int xres, const int yres){

  magnification = -magnification;

  //normalise evaporation rates
  double maxEvapRate = maximum(evapRate);
  double maxMagnification = fabs(maximum(magnification));

  for (int i=0;i<evapRate.size();i++){
    evapRate[i] /= maxEvapRate;
  }
  for (int i=0;i<magnification.size();i++){
    magnification[i] /= maxMagnification;
  }

  scalarField3D densityHitmap(xres, vector<vector<double>>(yres, vector<double>(1, 0.0)));
  scalarField3D crossoverHitmap(xres, vector<vector<double>>(yres, vector<double>(1, 0.0)));

  const int sampleSize = ion1SamplePos.size();

  for (int i=0;i<sampleSize;i++){
    double demag = fabs(1.0/magnification[i]);
    Point& detPos1 = ion1DetectorPos[i];
    Point& detPos2 = ion2DetectorPos[i];
    Point& detPos3 = ion3DetectorPos[i];

    //calculate panel area via Heron's formula
    double ABl = norm(detPos2, detPos1);
    double ACl = norm(detPos3, detPos1);
    double BCl = norm(detPos3, detPos2);

    double s = (ABl + ACl + BCl) * 0.5;
    double projectedTriangleArea = std::sqrt(s * (s - ABl) * (s - BCl) * (s - ACl));

    double xstep = detectorRadius/(xres);
    double ystep = detectorRadius/(yres);

    double mag = magnification[i];
    double erate = evapRate[i];

    double ta, tb, sxmin, symin, sxmax, symax, cellArea, projectionIntersectionArea;

    for (int a=0;a<xres;a++){
      for (int b=0;b<yres;b++){
        ta = (double)a/(xres-1);
        tb = (double)b/(yres-1);
        sxmin = - detectorRadius * (1.0 - ta) + detectorRadius * ta - xstep;
        symin = - detectorRadius * (1.0 - tb) + detectorRadius * tb - ystep;
        sxmax = - detectorRadius * (1.0 - ta) + detectorRadius * ta + xstep;
        symax = - detectorRadius * (1.0 - tb) + detectorRadius * tb + ystep;
        cellArea = (sxmax - sxmin) * (symax - symin);
        projectionIntersectionArea = triangleRectangleIntersection(sxmin, symin, sxmax, symax, detPos1, detPos2, detPos3, projectedTriangleArea);
        densityHitmap[a][b][0] += (projectionIntersectionArea * erate) * demag;
        if ((mag < 0) && projectionIntersectionArea > 0.0){
          crossoverHitmap[a][b][0] = 1.0;
        }
      }
    }
  }

  for (int a=0;a<(xres);a++){
    for (int b=0;b<(yres);b++){
      densityHitmap[a][b][0] *= maxEvapRate;
      densityHitmap[a][b][0] /= maxMagnification;
    }
  }

  tuple<scalarField3D, scalarField3D> outputHitmaps = {densityHitmap, crossoverHitmap};

  return outputHitmaps;

}


/*!
 * Function for calculating the local phase map over the detector via nearest neighbour interpolation
 *
 * @param ion1SamplePos panel corner 1 launch ion sample surface positions
 * @param ion1DetectorPos panel corner 1 launch ion detector positions
 * @param ion2SamplePos panel corner 2 launch ion sample surface positions
 * @param ion2DetectorPos panel corner 2 launch ion detector positions
 * @param ion3SamplePos panel corner 3 launch ion sample surface positions
 * @param ion3DetectorPos panel corner 3 launch ion detector positions
 * @param ion1phaseIds panel corner 1 launch ion phase index (index matching phase in configuration file according to phase orders)
 * @param ion2phaseIds panel corner 2 launch ion phase index (index matching phase in configuration file according to phase orders)
 * @param ion3phaseIds panel corner 3 launch ion phase index (index matching phase in configuration file according to phase orders)
 * @param evapRates panel local evaporation rates
 * @param magnification panel local magnifications
 * @param specimenModel the specimen model instance
 * @param detectorRadius the detector radius
 * @param xres the number of cells along the x-axis
 * @param yres the number of cells along the y-axis
 * @return the local detector phasemap
 *
 */
scalarField3D calculatePhaseMap(vector<Point>& ion1SamplePos, vector<Point>& ion1DetectorPos,
                                vector<Point>& ion2SamplePos, vector<Point>& ion2DetectorPos,
                                vector<Point>& ion3SamplePos, vector<Point>& ion3DetectorPos,
                                vector<int>& ion1phaseIds, vector<int>& ion2phaseIds,
                                vector<int>& ion3phaseIds, vector<double>& evapRates,
                                vector<double>& magnification, TMSpecimen& specimenModel,
                                const double detectorRadius,
                                const int xres, const int yres){


  scalarField3D phasemap(xres, vector<vector<double>>(yres, vector<double>(1, 0.0)));
  const int sampleSize = ion1DetectorPos.size();
  const int phaseNum = specimenModel.phases.size();

  #if defined(_OPENMP)
  #pragma omp for schedule(dynamic)
  #endif
  for (int a=0;a<xres;a++){
    for (int b=0;b<yres;b++){
      double tx = (double)a/(xres-1);
      double ty = (double)b/(yres-1);
      double px = - detectorRadius + tx * 2.0 * detectorRadius;
      double py = - detectorRadius + ty * 2.0 * detectorRadius;
      Point binCentre(px, py, 0.0);
      double maxEvapRate = -numeric_limits<double>::max();
      int mp = -1;
      double um, vm, wm;
      double u, v, w;
      Point testPos;

      for (int i=0;i<sampleSize;i++){
        double demag = fabs(1.0/magnification[i]);
        Point& detPos1 = ion1DetectorPos[i];
        Point& detPos2 = ion2DetectorPos[i];
        Point& detPos3 = ion3DetectorPos[i];
        int& ion1phaseId = ion1phaseIds[i];
        int& ion2phaseId = ion2phaseIds[i];
        int& ion3phaseId = ion3phaseIds[i];
        double& evapRate = evapRates[i];

        //check if bin point inside triangle
        findBarycentricCoordinates2D(binCentre, detPos1, detPos2, detPos3, u, v, w);

        if ((u <= 1.0) && (u >= 0.0) && (w <= 1.0) && (w >= 0.0) && (v <= 1.0) && (v >= 0.0)){
          double backProjectedEvapRate = fabs(evapRate * demag);
          if (maxEvapRate < backProjectedEvapRate){
            maxEvapRate = backProjectedEvapRate;
            mp = i;
            um = u;
            vm = v;
            wm = w;
          }
        }
      }

      if (mp > -1){
        int ion1phaseId = ion1phaseIds[mp];
        int ion2phaseId = ion2phaseIds[mp];
        int ion3phaseId = ion3phaseIds[mp];
        if ((ion1phaseId == ion2phaseId) && (ion1phaseId == ion3phaseId)){
          phasemap[a][b][0] = (double)ion1phaseId;
          continue;
        }

        Point& detPos1 = ion1DetectorPos[mp];
        Point& detPos2 = ion2DetectorPos[mp];
        Point& detPos3 = ion3DetectorPos[mp];
        Point& samplePos1 = ion1SamplePos[mp];
        Point& samplePos2 = ion2SamplePos[mp];
        Point& samplePos3 = ion3SamplePos[mp];

        //Point testPos = samplePos1 * um + samplePos2 * vm + samplePos3 * wm;
        testPos.px = samplePos1.px * um + samplePos2.px * vm + samplePos3.px * wm;
        testPos.py = samplePos1.py * um + samplePos2.py * vm + samplePos3.py * wm;
        testPos.pz = samplePos1.pz * um + samplePos2.pz * vm + samplePos3.pz * wm;

        int phaseIndex = 0;
        for (int p = phaseNum; p-- > 1; ){
          int inside = 1 - 2 * specimenModel.phases[p].phaseBoundary.insideMesh(testPos);
          if (inside == 1){
            phaseIndex = p;
            break;
          }
        }
        phasemap[a][b][0] = phaseIndex;
      }
    }
  }

  return phasemap;
}
