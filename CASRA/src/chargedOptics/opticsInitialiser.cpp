/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <vector>
#include <regex>
#include <sstream>
#include <any>
#include <limits>
#include <numeric>
#include <algorithm>

#include "CASRA/chargedOptics/opticsInitialiser.h"
#include "CASRA/io/modelDataio.h"

using namespace std;

const double defaultStopTime = std::numeric_limits<double>::max();
const int defaultStopIts = 100;
const double defaultStopApexHeight = std::numeric_limits<double>::min();
const double defaultTimeStepFraction = 0.5;
const double defaultMinimumSpecimenLength = 0.0;
const double defaultShankAngle = 0.0;
const double defaultDetectorRadius = 100.0;
const double defaultKappa = 1.0;
const int defaultXCells = 100;
const int defaultYCells = 100;

vector<string> defaultExportFileTypes = {"tsv"};

//to modify instance attributes directly must pass instance by reference (as done here)

/*!
* This function reads xml parameter configuration files and sets the passed
* OpticsParams output object instance (e.g. reads io projection parameters).
* This function makes use of the rapidxml
* library for parsing the xml file.
*
* @param simparams The SimulationParams object instance (the output object).
* @param cfname The configuration file filename.
*
*/
OpticsParams initialiseModelOpticsFromXML(std::string cfname) {

    cout << "Reading model configuration file..." << endl;

    char *charcfname = new char[cfname.length() + 1];
    strcpy(charcfname, cfname.c_str());

    //load the xml file using the rapidxml library
    rapidxml::file<> xmlFile(charcfname);
    rapidxml::xml_document<> doc;
    try {
      doc.parse<0>(xmlFile.data());
    }
    catch (const rapidxml::parse_error& e){
          cerr << "\tERROR: XML parsing error prevented reading of passed file due to '" << e.what() << "'" << endl;
          cerr << "\tPlease ensure XML is in a valid format." << endl;
          exit(1);
      }

    cout << "\tsuccessfully found from xml file. Reading now..." << endl;

    rapidxml::xml_node<> *root;

    int simId = -1;

    int itStep = 1;
    int itStart = 0;
    int itStop = numeric_limits<int>::max();
    string projectionType;
    bool outputMapping = true;
    bool outputHitmap = true;
    bool outputTrajectories = true;
    bool outputMagnification = true;
    double cutoffDistance;
    double launchDistance;

    bool detectorEnabled = true;
    Point detectorPosition;
    string detectorPositionStr;
    Point detectorNormal;
    string detectorNormalStr;
    double detectorRadius = defaultDetectorRadius;
    string detectorType;
    int xCells = 30;
    int yCells = 30;

    bool transferEnabled = false;
    double kappa = 1.0;
    string transferType;
    vector<string> exportFileTypes;

    OpticsParams oparams;



    root = doc.first_node();
    if (root->first_attribute("id") != nullptr){
      simId = stoi(root->first_attribute("id")->value());
      cout << "\tsimulation ID of '" << simId << "' assigned" << endl;
      oparams.simId = simId;
    }

    rapidxml::xml_node<> * projectionModelNode = root->first_node("ionProjection");

    if (projectionModelNode == nullptr){
      cerr << "ERROR: No ionProjection xml configuration node detected." << endl;
      exit(1);
    }
    else {
      projectionType = string(projectionModelNode->first_attribute("type")->value());
      if (projectionType == "uniform"){
        cout << "Uniform ion projection selected (one ion per panel)." << endl;

        if (projectionModelNode->first_attribute("step") != nullptr){
          itStep = stoi(projectionModelNode->first_attribute("step")->value());
        }
        else {
          cout << "No 'step' parameter for the spacing between surface states at which ion projection is performed. \nUsing default value of 'step'=" << itStep << endl;
        }
        if (projectionModelNode->first_attribute("start") != nullptr){
          itStart = stoi(projectionModelNode->first_attribute("start")->value());
        }
        else {
          cout << "No 'start' parameter for the first surface state from which to start ion projections. \nUsing default value of 'start'=" << itStart << endl;
        }
        if (projectionModelNode->first_attribute("stop") != nullptr){
          itStop = stoi(projectionModelNode->first_attribute("stop")->value());
        }
        else {
          cout << "No 'stop' parameter for the final surface state at which to stop ion projections. \nUsing default value of all found states." << endl;
        }
        if (projectionModelNode->first_attribute("cutoffDistance") != nullptr){
          cutoffDistance = stod(projectionModelNode->first_attribute("cutoffDistance")->value());
        }
        else {
          cout << "No 'cutoffDistance' parameter at which to terminate ion trajectories. \nDistance cutoff termination condition disabled." << endl;
          cutoffDistance = -1.0;
        }
        if (projectionModelNode->first_attribute("launchDistance") != nullptr){
          launchDistance = stod(projectionModelNode->first_attribute("launchDistance")->value());
        }
        else {
          cout << "No 'launchDistance' parameter at which to terminate ion trajectories. Setting default launch distance to zero (from panel centre)." << endl;
          launchDistance = 0.0;
        }
        if (projectionModelNode->first_attribute("outputTrajectories") != nullptr){
          string outTrajStr = projectionModelNode->first_attribute("outputTrajectories")->value();
          if (outTrajStr == "true" || outTrajStr == "True" || outTrajStr == "y" || outTrajStr == "TRUE" || outTrajStr == "yes"){
            outputTrajectories = true;
          }
          else {
            outputTrajectories = false;
            cout << "false this" << endl;
          }
        }

        if (projectionModelNode->first_attribute("outputMapping") != nullptr){
          string outMapStr = projectionModelNode->first_attribute("outputMapping")->value();
          if (outMapStr == "true" || outMapStr == "True" || outMapStr == "y" || outMapStr == "TRUE" || outMapStr == "yes"){
            outputMapping = true;
          }
          else {
            outputMapping = false;
          }
        }

        if (projectionModelNode->first_attribute("outputMagnification") != nullptr){
          string outputMagStr = projectionModelNode->first_attribute("outputMagnification")->value();
          if (outputMagStr == "true" || outputMagStr == "True" || outputMagStr == "y" || outputMagStr == "TRUE" || outputMagStr == "yes"){
            outputMagnification = true;
          }
          else {
            outputMagnification = false;
          }
        }

        if (projectionModelNode->first_attribute("outputHitmap") != nullptr){
          string outMapStr = projectionModelNode->first_attribute("outputHitmap")->value();
          if (outMapStr == "true" || outMapStr == "True" || outMapStr == "y" || outMapStr == "TRUE" || outMapStr == "yes"){
            outputHitmap = true;
          }
          else {
            outputHitmap = false;
          }
        }
      }
      else if (projectionType == "realistic"){
        cout << "realistic ion projection selected for synthetic data generation (local ion flux proportional to local evaporation rate)." << endl;

        if (projectionModelNode->first_attribute("step") != nullptr){
          itStep = stoi(projectionModelNode->first_attribute("step")->value());
        }
        else {
          cout << "No 'step' parameter for the spacing between surface states at which ion projection is performed. \nUsing default value of 'step'=" << itStep << endl;
        }
        if (projectionModelNode->first_attribute("start") != nullptr){
          itStart = stoi(projectionModelNode->first_attribute("start")->value());
        }
        else {
          cout << "No 'start' parameter for the first surface state from which to start ion projections. \nUsing default value of 'start'=" << itStart << endl;
        }
        if (projectionModelNode->first_attribute("stop") != nullptr){
          itStop = stoi(projectionModelNode->first_attribute("stop")->value());
        }
        else {
          cout << "No 'stop' parameter for the final surface state at which to stop ion projections. \nUsing default value of all found states." << endl;
        }
        if (projectionModelNode->first_attribute("cutoffDistance") != nullptr){
          cutoffDistance = stod(projectionModelNode->first_attribute("cutoffDistance")->value());
        }
        else {
          cout << "No 'cutoffDistance' parameter at which to terminate ion trajectories. \nDistance cutoff termination condition disabled." << endl;
          cutoffDistance = -1.0;
        }
        if (projectionModelNode->first_attribute("launchDistance") != nullptr){
          launchDistance = stod(projectionModelNode->first_attribute("launchDistance")->value());
        }
        else {
          cout << "No 'launchDistance' parameter at which to terminate ion trajectories. Setting default launch distance to zero (from panel centre)." << endl;
          launchDistance = 0.0;
        }
        if (projectionModelNode->first_attribute("exportFileTypes") != nullptr){
          string exportFileTypesStr = projectionModelNode->first_attribute("exportFileTypes")->value();
          string delimiter = ",";
          int pos = exportFileTypesStr.find(delimiter);
          while (pos != string::npos) {
            string fileSuffix = exportFileTypesStr.substr(0, pos);
            exportFileTypesStr.erase(0, pos + delimiter.length());
            exportFileTypes.push_back(fileSuffix);
            pos = exportFileTypesStr.find(delimiter);
          }
          exportFileTypes.push_back(exportFileTypesStr);
        }
        else {
          cout << "No 'exportFileTypes' parameter at which to terminate ion trajectories. Setting default to 'tsv' only." << endl;
          exportFileTypes = defaultExportFileTypes;
        }

        outputTrajectories = false;
        outputHitmap = false;
        outputMapping = false;
        outputMagnification = false;

        if (projectionModelNode->first_attribute("outputTrajectories") != nullptr){
          string outTrajStr = projectionModelNode->first_attribute("outputTrajectories")->value();
          if (outTrajStr == "true" || outTrajStr == "True" || outTrajStr == "y" || outTrajStr == "TRUE" || outTrajStr == "yes"){
            outputTrajectories = true;
          }
          else {
            outputTrajectories = false;
          }
        }
        if (projectionModelNode->first_attribute("outputHitmap") != nullptr){
          string outMapStr = projectionModelNode->first_attribute("outputHitmap")->value();
          if (outMapStr == "true" || outMapStr == "True" || outMapStr == "y" || outMapStr == "TRUE" || outMapStr == "yes"){
            outputHitmap = true;
          }
          else {
            outputHitmap = false;
          }
        }
      }
      else {
        cerr << "\tUnderfined ion simulation mode: '" << projectionType << "'. \n\tSupported modes include: 'uniform', 'realistic'" << endl;
        exit(1);
      }
      rapidxml::xml_node<>* ionTransferNode = projectionModelNode->first_node("transferFunction");
      if (ionTransferNode != nullptr){

      }
      else {

      }
      rapidxml::xml_node<>* detectorNode = projectionModelNode->first_node("detector");
      if (detectorNode != nullptr){
        detectorEnabled = true;
        if (detectorNode->first_attribute("type") != nullptr){
          detectorType = string(detectorNode->first_attribute("type")->value());
          if (detectorType == "planar"){
            cout << "\tPlanar detector type detected." << endl;
          }
          else{
            cerr << "\tERROR: Invalid detector type detected: `" + detectorType + "`. Only `planar` type currently supported. \n\tValid input example in model configuration file: " +
            	"\n\t\t<detector type='planar' radius='80' position='(0.0, 0.0, 200.0)' normal='(0.0, 0.0, 1.0)'/>" << endl;
            exit(1);
          }
          if (detectorNode->first_attribute("radius") != nullptr){
            detectorRadius = stod(detectorNode->first_attribute("radius")->value());
          }
          else {
            cerr << "\tNo detector radius parameter detected. Using default parameter value of '" << defaultDetectorRadius << "'" << endl;
            detectorRadius = defaultDetectorRadius;
          }
          if (detectorNode->first_attribute("position") != nullptr){
            detectorPositionStr = string(detectorNode->first_attribute("position")->value());
            detectorPosition = extractPoint(detectorPositionStr);
          }
          else {
            detectorPosition.px = 0.0;
            detectorPosition.py = 0.0;
            detectorPosition.pz = 100.0;
            cerr << "\tNo detector centre position parameter detected. Using default parameter value of '(0.0, 0.0, 100.0)'. Note that the z-coordinate defines the flightpath." << endl;
          }
          if (detectorNode->first_attribute("normal") != nullptr){
            detectorNormalStr = string(detectorNode->first_attribute("normal")->value());
            detectorNormal = extractPoint(detectorNormalStr);
          }
          else {
            detectorNormal.px = 0.0;
            detectorNormal.py = 0.0;
            detectorNormal.pz = 1.0;
            cerr << "\tNo detector normal parameter detected. Using default parameter value of '(0.0, 0.0, 1.0)' (orientation along z-axis)." << endl;
          }
          if (detectorNode->first_attribute("xcells") != nullptr){
            xCells = stoi(detectorNode->first_attribute("xcells")->value());
          }
          else {
            cerr << "\tNo detector cell number along the x-coordinate detected. Using default parameter value of '" << defaultXCells << "'" << endl;
            xCells = defaultXCells;
          }
          if (detectorNode->first_attribute("ycells") != nullptr){
            yCells = stoi(detectorNode->first_attribute("ycells")->value());
          }
          else {
            cerr << "\tNo detector cell number along the y-coordinate detected. Using default parameter value of '" << defaultYCells << "'" << endl;
            yCells = defaultYCells;
          }
        }
      }
      else {
        cerr << "\tNo detector detected. Detector termination condition disabled." << endl;
        detectorEnabled = false;
      }
      rapidxml::xml_node<>* transferNode = projectionModelNode->first_node("transferFunction");
      if (transferNode != nullptr){
        transferEnabled = true;
        if (transferNode->first_attribute("type") != nullptr){
          transferType = string(transferNode->first_attribute("type")->value());
          if (transferType == "linear"){
            if (transferNode->first_attribute("kappa") != nullptr){
              kappa = stod(transferNode->first_attribute("kappa")->value());
            }
            else {
              kappa = defaultKappa;
              cerr << "\t\t No inputted compression parameter 'kappa' detected. Using default value of " << defaultKappa << endl;
            }
          }
          else {
            cerr << "\t\tERROR:  Detected type: '" << transferType << "'. Currently only linear-type projection-based transfer functions are supported." << endl;
            cerr << "\t\tModel configuration file example: " << endl;
            cerr << "\t\t<transferFunction type='linear' kappa='0.8'/>" << endl;
            exit(1);
          }
        }
      }
      else {
        transferEnabled = false;
      }
    }

    oparams.projectionType = projectionType;
    oparams.itStep = itStep;
    oparams.itStart = itStart;
    oparams.itStop = itStop;
    oparams.cutoffDistance = cutoffDistance;
    oparams.launchDistance = launchDistance;
    oparams.outputTrajectories = outputTrajectories;
    oparams.outputMapping = outputMapping;
    oparams.outputHitmap = outputHitmap;
    oparams.outputMagnification = outputMagnification;

    oparams.dparams.enabled = detectorEnabled;
    oparams.dparams.position = detectorPosition;
    oparams.dparams.normal.vx = detectorNormal.px;
    oparams.dparams.normal.vy = detectorNormal.py;
    oparams.dparams.normal.vz = detectorNormal.pz;
    oparams.dparams.radius = detectorRadius;
    oparams.dparams.xCells = xCells;
    oparams.dparams.yCells = yCells;

    oparams.tparams.enabled = transferEnabled;
    oparams.tparams.type = transferType;
    oparams.tparams.functionParameters.push_back(kappa);
    oparams.exportFileTypes = exportFileTypes;

    cout << "Model configuration file successfully read." << endl;

    delete[] charcfname;

    return oparams;

}
