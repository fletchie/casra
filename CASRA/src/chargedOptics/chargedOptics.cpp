/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <cmath>
#include <stdexcept>
#include <chrono>
#include <string>
#include <cstring>
#include <numeric>
#include <tuple>
#include <chrono>
#include <set>

using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::milliseconds;
using std::chrono::seconds;

#include "CASRA/chargedOptics/chargedOptics.h"
#include "CASRA/base/RKIntegrator.h"
#include "CASRA/io/ioBase.h"
#include "CASRA/io/modelDataio.h"

#define PROJECTIONTHRESHOLD 0.92
#define MAXIT 400
#define MINIMISECUTOFFRTOL 0.01
#define CLUSTERADMISSTOL 0.2

//defines the analysis axis unit normal for which projection laws are performed
//with respect to (by default set to model z-axis)
#define ANALYSISAXISX 0.0
#define ANALYSISAXISY 0.0
#define ANALYSISAXISZ 1.0

#define PHASEASSIGNMENTSTEP 10000

using namespace std;

//supported hexidecimal units (for random colour generation)
char hexChar[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};


/*!
 * class initialiser method for the Ion class
 * @param charge The ion electric charge during integration
 * @param mass The ion mass during integration
 */
Ion::Ion(double charge, double mass): charge{charge}, mass{mass}{

}

/*!
 * OpticsDomain class initialiser method for initialising the OpticsDomain instances.
 * @param xmin The minimum x coordinate bound for the domain
 * @param xmax The maximum x coordinate bound for the domain
 * @param ymin The minimum y coordinate bound for the domain
 * @param ymax The maximum y coordinate bound for the domain
 * @param zmin The minimum z coordinate bound for the domain
 * @param ymax The maximum z coordinate bound for the domain
 * @param detector reference to a detector object instance
 * @param distanceThreshold The distance from the specimen surface at which to terminate ion trajectory integration (with past this distance a transfer function being applied).
 */
OpticsDomain::OpticsDomain(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax, Detector& detector, double distanceThreshold){
  this->xmin = xmin;
  this->ymin = ymin;
  this->zmin = zmin;
  this->xmax = xmax;
  this->ymax = ymax;
  this->zmax = zmax;
  this->detector = &detector;
  this->distanceThreshold = distanceThreshold;
}

/*!
 * OpticsDomain class initialiser method for initialising the OpticsDomain instances.
 * @param xmin The minimum x coordinate bound for the domain
 * @param xmax The maximum x coordinate bound for the domain
 * @param ymin The minimum y coordinate bound for the domain
 * @param ymax The maximum y coordinate bound for the domain
 * @param zmin The minimum z coordinate bound for the domain
 * @param ymax The maximum z coordinate bound for the domain
 * @param detector Pointer to a detector object instance
 * @param distanceThreshold The distance from the specimen surface at which to terminate ion trajectory integration (with past this distance a transfer function being applied).
 */
OpticsDomain::OpticsDomain(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax, Detector* detector, double distanceThreshold){
  this->xmin = xmin;
  this->ymin = ymin;
  this->zmin = zmin;
  this->xmax = xmax;
  this->ymax = ymax;
  this->zmax = zmax;
  this->detector = detector;
  this->distanceThreshold = distanceThreshold;
}

/*!
 * OpticsDomain class initialiser method for initialising the OpticsDomain instances.
 * @param xmin The minimum x coordinate bound for the domain
 * @param xmax The maximum x coordinate bound for the domain
 * @param ymin The minimum y coordinate bound for the domain
 * @param ymax The maximum y coordinate bound for the domain
 * @param zmin The minimum z coordinate bound for the domain
 * @param ymax The maximum z coordinate bound for the domain
 * @param distanceThreshold The distance from the specimen surface at which to terminate ion trajectory integration (with past this distance a transfer function being applied).
 */
OpticsDomain::OpticsDomain(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax, double distanceThreshold){
  this->xmin = xmin;
  this->ymin = ymin;
  this->zmin = zmin;
  this->xmax = xmax;
  this->ymax = ymax;
  this->zmax = zmax;
  this->detector = nullptr;
  this->distanceThreshold = distanceThreshold;

}

/*!
 * OpticsDomain class method for calculating ion trajectories for a particular surfaceMesh.
 * Launches ions from surface panel corners above a particular height threshold.
 *
 * @param launchSites Vector of launch sites, where elements define indices corresponding to mesh vertices
 * @param surfaceMesh The specimen surface mesh to calculate ion trajectories from
 * @param bemSolver The electrostaticBemSolver object instance
 * @param specimenModel The specimen model (defining internal phases)
 * @param launchDistance The normal launch distance of ions from the surface
 * @param relTol The trajectory integrator relative tolerance (Dormand-Prince RK54)
 * @return results including a vector of calculated ion trajectories
 */
tuple<vector<Ion>> OpticsDomain::calculateUniformChargedOptics(vector<long> launchSites, TriangularMesh& surfaceMesh,
                                                               electrostaticBemSolver& bemSolver, TMSpecimen& specimenModel,
                                                               double launchDistance, double relTol){

  surfaceMesh.setBoundingBox();

  auto t1 = high_resolution_clock::now();

  //cout << "\tcalculating octree" << endl;
  OctreeNode top(surfaceMesh.minCoords, surfaceMesh.maxCoords, surfaceMesh, bemSolver);
  cout << "\toctree calculation complete" << endl;

  cout << "\tcalculating ion trajectories" << endl;

  vector<Ion> launchedIons(launchSites.size());
  const int launchSiteSize = launchSites.size();

  #if defined(_OPENMP)
  #pragma omp parallel
  #endif
  {

  #if defined(_OPENMP)
  #pragma omp for schedule(dynamic)
  #endif
  for (int v=0;v<launchSiteSize;v++){
    tuple<double, vector<double>, double, vector<double>> res;
    double t, h;

    Point originalPosition = surfaceMesh.vertices[launchSites[v]];
    Vec averagedNormal = surfaceMesh.vertices[launchSites[v]].averagedNormal;
    Point launchPosition(originalPosition.px + launchDistance * averagedNormal.vx,
                         originalPosition.py + launchDistance * averagedNormal.vy,
                         originalPosition.pz + launchDistance * averagedNormal.vz);
    vector<double> y = {launchPosition.px, launchPosition.py, launchPosition.pz, 0.0, 0.0, 0.0};

    //define initial RK iteration timestep
    h = launchDistance;

    Ion testIon(1.0, 1.0);
    testIon.launchPos = originalPosition;
    testIon.trajectory.push_back(originalPosition);
    testIon.velocity.push_back(Point(0.0, 0.0, 0.0));

    int phaseIndex = 0;
    for (int p = specimenModel.phases.size(); p-- > 1; ){
      int inside = 1.0 - 2.0*(float)specimenModel.phases[p].phaseBoundary.insideMesh(testIon.launchPos);
      if (inside == 1){
        phaseIndex = p;
        break;
      }
    }

    //test to identify original phase of ion/launch position
    testIon.phase = specimenModel.phases[phaseIndex].name;
    testIon.phaseIndex = phaseIndex;

    vector<double> kPrev(6, 0.0);
    std::vector<double> k1(6, 0.0);
    std::vector<double> k2(6, 0.0);
    std::vector<double> k3(6, 0.0);
    std::vector<double> k4(6, 0.0);
    std::vector<double> k5(6, 0.0);
    std::vector<double> k6(6, 0.0);
    std::vector<double> k7(6, 0.0);
    std::vector<double> ynp4(6, 0.0);
    std::vector<double> ynp5(6, 0.0);

    //testIon.trajectory.reserve(50);
    //testIon.velocity.reserve(50);

    tuple<OctreeNode*, Ion*> params = {&top, &testIon};
    for (int it=0;it<MAXIT;it++){
      res = rungeKuttaVectorIterateParams(&lawOfMotion, t, y, h, relTol, kPrev, params, k1, k2, k3, k4, k5, k6, k7, ynp4, ynp5);
      t = get<0>(res);
      y = get<1>(res);
      h = get<2>(res);
      kPrev = get<3>(res);
      Point ny(y[0], y[1], y[2]);
      Point nv(y[3], y[4], y[5]);

      testIon.trajectory.push_back(ny);
      testIon.velocity.push_back(nv);

      bool terminate = this->terminateTrajectoryIterator(testIon, surfaceMesh);
      if (terminate == true){
        break;
      }
    }

    testIon.finalPos = Point(y[0], y[1], y[2]);
    launchedIons[v] = testIon;
  }
  }

  auto t2 = high_resolution_clock::now();
  duration<double> ms_double = t2 - t1;
  cout << "\tion projection time: " << ms_double.count() << endl;

  tuple<vector<Ion>> chargedOpticsResults = {launchedIons};
  return chargedOpticsResults;

}



 /*!
  * OpticsDomain class method for calculating ion trajectories for a particular surfaceMesh.
  * Ions are launch in proportional to the local surface evaporation rate.
  * Launches ions from surface panel corners above a particular height threshold.
  *
  * @param launchSites Vector of launch sites, where elements define launch points
  * @param launchNormals Vector of launch normals, where elements define the initial launch direction
  * @param launchPhase Vector of ion phases, where elements define indices corresponding to phases in specimenModel
  * @param surfaceMesh The specimen surface mesh to calculate ion trajectories from
  * @param bemSolver The electrostaticBemSolver object instance
  * @param specimenModel The specimen model (defining internal phases)
  * @param launchDistance The normal launch distance of ions from the surface
  * @param relTol The trajectory integrator relative tolerance (Dormand-Prince RK54)
  * @return results including a vector of calculated ion trajectories
  */
tuple<vector<Ion>> OpticsDomain::calculateWeightedChargedOptics(vector<Point>& launchPositions, vector<Vec>& launchNormals,
                                                                vector<int>& launchPhase,
                                                                TriangularMesh& surfaceMesh,
                                                                electrostaticBemSolver& bemSolver, TMSpecimen& specimenModel,
                                                                double launchDistance, double relTol){

  surfaceMesh.setBoundingBox();

  auto t1 = high_resolution_clock::now();

  cout << "\tcalculating octree" << endl;
  OctreeNode top(surfaceMesh.minCoords, surfaceMesh.maxCoords, surfaceMesh, bemSolver);
  cout << "\toctree calculation complete" << endl;

  cout << "\tcalculating ion trajectories" << endl;

  vector<Ion> launchedIons(launchPositions.size());
  cout << launchPositions.size() << endl;

  #if defined(_OPENMP)
  #pragma omp parallel
  #endif
  {
  #if defined(_OPENMP)
  #pragma omp for schedule(dynamic)
  #endif
  for (int v=0;v<launchPositions.size();v++){
    tuple<double, vector<double>, double, vector<double>> res;
    double t, h;
    vector<double> kPrev;

    Point originalPosition = launchPositions[v];
    Point launchPosition(originalPosition.px + launchDistance * launchNormals[v].vx,
                         originalPosition.py + launchDistance * launchNormals[v].vy,
                         originalPosition.pz + launchDistance * launchNormals[v].vz);
    vector<double> y = {launchPosition.px, launchPosition.py, launchPosition.pz, 0.0, 0.0, 0.0};

    //define initial RK iteration timestep
    h = 1e-4;

    Ion testIon(1.0, 1.0);
    testIon.launchPos = originalPosition;
    testIon.trajectory.push_back(originalPosition);
    testIon.velocity.push_back(Point(0.0, 0.0, 0.0));

    std::vector<double> k1(6, 0.0);
    std::vector<double> k2(6, 0.0);
    std::vector<double> k3(6, 0.0);
    std::vector<double> k4(6, 0.0);
    std::vector<double> k5(6, 0.0);
    std::vector<double> k6(6, 0.0);
    std::vector<double> k7(6, 0.0);
    std::vector<double> ynp4(6, 0.0);
    std::vector<double> ynp5(6, 0.0);

    testIon.phase = specimenModel.phases[launchPhase[v]].name;
    //testIon.ionicVolume = specimenModel.phases[launchPhase[v]].elementRatios[0];
    tuple<OctreeNode*, Ion*> params = {&top, &testIon};
    for (int it=0;it<MAXIT;it++){
      res = rungeKuttaVectorIterateParams(&lawOfMotion, t, y, h, relTol, kPrev, params, k1, k2, k3, k4, k5, k6, k7, ynp4, ynp5);
      t = get<0>(res);
      y = get<1>(res);
      h = get<2>(res);
      kPrev = get<3>(res);
      Point ny(y[0], y[1], y[2]);
      Point nv(y[3], y[4], y[5]);

      testIon.trajectory.push_back(ny);
      testIon.velocity.push_back(nv);

      bool terminate = this->terminateTrajectoryIterator(testIon, surfaceMesh);
      if (terminate == true){
        break;
      }

      if (it == (MAXIT-1))
        cout << "maximum number of RK iterations reached for trajectory integration." << endl;
    }

    testIon.finalPos = Point(y[0], y[1], y[2]);
    launchedIons[v] = testIon;
  }
  }

  auto t2 = high_resolution_clock::now();
  duration<double> ms_double = t2 - t1;
  cout << "\tion projection time: " << ms_double.count() << endl;
  tuple<vector<Ion>> chargedOpticsResults = {launchedIons};
  return chargedOpticsResults;

}

/*!Check whether current ion position falls within OpticalDomain bounds*/
bool OpticsDomain::checkOutsideBounds(Point& testPoint){
  if ((testPoint.px >= xmin) && (testPoint.px < xmax) && (testPoint.py >= ymin) && (testPoint.py < ymax) && (testPoint.pz >= zmin) && (testPoint.pz < zmax)){
    return false;
  }
  return true;
}

/*!
 * OpticsDomain class method for determining whether an ion still falls within the
 * valid optical domain. If not ion trajectory integration will be terminated.
 * @param testIon The launched ion being calculated
 * @param surfaceMesh The specimen surfaceMesh from which the ion has been launched
 * @return (true) - terminate integration, (false) - continue integration
 */
bool OpticsDomain::terminateTrajectoryIterator(Ion& testIon, TriangularMesh& surfaceMesh){

  Point& testPoint = testIon.trajectory.back();

  //check if outside of specified optical domain
  bool out = this->checkOutsideBounds(testPoint);
  if (out == true){
    return true;
  }

  //check if maximum trajectory distance has been reached. Beyond here, the transfer function will be applied
  double distance = surfaceMesh.shortestDistanceToPoint(testIon.trajectory.back());

  if (distance > this->distanceThreshold){

    //interpolate back to give exact distance
    Point& pointB = testIon.trajectory[testIon.trajectory.size() - 1];
    Point& pointA = testIon.trajectory[testIon.trajectory.size() - 2];
    double t = minimiseCutoffIntersection(pointA, pointB, surfaceMesh, this->distanceThreshold);
    Point intersectionPoint = pointA * (1.0 - t) + pointB * t;
    testIon.trajectory[testIon.trajectory.size() - 1] = intersectionPoint;

    Point intersectionVelocity = testIon.velocity[testIon.velocity.size() - 2] * (1.0 - t) + testIon.velocity[testIon.velocity.size() - 1] * t;

    testIon.velocity[testIon.velocity.size() - 1] = intersectionVelocity;

    return true;
  }

  //if a detector is specified, check if trajectory has passed through the detector
  //if (this->detector != nullptr){
  //  Point& prevTestPoint = testIon.trajectory[testIon.trajectory.size() - 2];
  //  int p1 = this->detector->planeSide(testPoint);
  //  int p2 = this->detector->planeSide(prevTestPoint);
  //  if (p1 != p2){
  //    return true;
  //  }
  //}

  //check if ion inside specimen (Point-in-Polygon test) - if so then terminate trajectory calculator
  //first check if within specimen bounding box

  if ((testPoint.px >= surfaceMesh.minCoords[0]) && (testPoint.px <= surfaceMesh.maxCoords[0]) &&
      (testPoint.py >= surfaceMesh.minCoords[1]) && (testPoint.py <= surfaceMesh.maxCoords[1]) &&
      (testPoint.pz >= surfaceMesh.minCoords[2]) && (testPoint.pz <= surfaceMesh.maxCoords[2])) {
        if (surfaceMesh.insideMesh(testPoint) == 0){
          testIon.writeOutput = false;
          return true;
        }
  }

  return false;
}

/*!
 * Function for calculating the position linearly interpolated between pointA and pointB that is exactly the cutoffDistance
 * away from the passed surfaceMesh. Applies the Secant root finding method.
 * final point given by pointC = pointA * (1 - t) + pointB * t, where t is the calculated interpolation parameter
 * @param pointA The first point
 * @param pointB The second point
 * @param surfaceMesh The surface mesh to calculate distances to
 * @param cutoffDistance The distance from the surface mesh
 * @return t The calculated interpolation parameter defining the interpolated position at the cutoff distance (see the function description above).
 */
double minimiseCutoffIntersection(Point& pointA, Point& pointB, TriangularMesh& surfaceMesh, double cutoffDistance){

  int maxIts = 30;

  double ctol = numeric_limits<double>::max();
  int it = 0;
  double xnm1 = 0;
  double xn = 1;
  double fVnm1, fVn, xnp1;
  fVnm1 = surfaceMesh.shortestDistanceToPoint(pointA) - cutoffDistance;
  Point pointC(0.0, 0.0, 0.0);
  while ((ctol > MINIMISECUTOFFRTOL) && (it < maxIts)){
    pointC = pointA * (1.0 - xn) + pointB * xn;
    fVn = surfaceMesh.shortestDistanceToPoint(pointC) - cutoffDistance;
    xnp1 = xn - fVn * (xn - xnm1)/(fVn - fVnm1);
    it++;
    ctol = fabs((xnp1 - xn)/xnp1);
    xn = xnp1;
    xnm1 = xn;
    fVnm1 = fVn;
  }

  return xn;
}

/*!
 * Function for determining candidate launch sites (located at panel vertices)
 * for a weighted ion projection (proportional to surface evaporation rate).
 *
 * @param surfaceMesh The inputted triangular surface mesh
 * @param previousSurfaceState The previous triangular surface mesh (for determining the local evaporation rate)
 * @param nextSurfaceState The next triangular surface mesh (for determining the local evaporation rate)
 * @param specimenModel The inputted triangular mesh specimen model
 * @param evapVols The surface relative evaporation volumes
 * @return tuple containing launch site positions and corresponding phase indices
 */
tuple<vector<Point>, vector<Vec>, vector<int>, vector<Phase*>> determineWeightedLaunchSites(TriangularMesh& surfaceMesh,
                                                                                            TriangularMesh& previousSurfaceState,
                                                                                            TriangularMesh& nextSurfaceState,
                                                                                            TMSpecimen& specimenModel,
                                                                                            vector<double>& evapVols){

  surfaceMesh.setCentres();
  surfaceMesh.setBoundingBox();

  //calculate evaporation volumes
  double voldiff1 = 0.0;
  double voldiff2 = 0.0;

  if (previousSurfaceState.faces.size() > 0)
    voldiff1 = 0.5 * (previousSurfaceState.vol - surfaceMesh.vol);
  if (nextSurfaceState.faces.size() > 0)
    voldiff2 = 0.5 * (surfaceMesh.vol - nextSurfaceState.vol);

  double totalVol = voldiff2 + voldiff1;

  double projectionCandidateHeight = (surfaceMesh.maxCoords[2] - surfaceMesh.minCoords[2]) * 0.7 + surfaceMesh.minCoords[2];

  //candidates must be above 50% mark
  vector<int> candidateIndices;
  vector<double> cumulativeEvapVols;
  double cumTotal = 0.0;

  const int facesNum = surfaceMesh.faces.size();

  for (int i=0;i<facesNum;i++){
    candidateIndices.push_back(i);
    cumTotal += evapVols[i];
    cumulativeEvapVols.push_back(cumTotal);
  }

  const int cumulativeEvapVolsSize = cumulativeEvapVols.size();
  const int phaseNum = specimenModel.phases.size();

  vector<Point> candidateLaunchSites;
  vector<Vec> candidateLaunchNormals;
  vector<int> candidatePhases;
  vector<Phase*> ghostPhases;
  vector<bool> toLaunch;

  int stepNum = 0;
  double vol = 0.0;
  long it = 0;

  while (vol <= totalVol){
    candidateLaunchSites.resize(candidateLaunchSites.size() + PHASEASSIGNMENTSTEP);
    candidateLaunchNormals.resize(candidateLaunchNormals.size() + PHASEASSIGNMENTSTEP);
    candidatePhases.resize(candidatePhases.size() + PHASEASSIGNMENTSTEP, 0);
    ghostPhases.resize(ghostPhases.size() + PHASEASSIGNMENTSTEP, nullptr);
    toLaunch.resize(toLaunch.size() + PHASEASSIGNMENTSTEP, false);

    #pragma omp parallel
    {
    #pragma omp for schedule(dynamic)
    for (int i=stepNum;i<(stepNum + PHASEASSIGNMENTSTEP);i++){
      double randVol = cumTotal * ((double)rand())/RAND_MAX;
      int launchPanel = 0;
      for (int j=0;j<cumulativeEvapVolsSize;j++){
        if (randVol < cumulativeEvapVols[j]){
          launchPanel = candidateIndices[j];
          break;
        }
      }

      toLaunch[i] = true;

      //generate random coordinate on panel (Barycentric coordinates);
      double u = ((double)rand())/RAND_MAX;
      double v = ((double)rand())/RAND_MAX;
      double w = ((double)rand())/RAND_MAX;
      double mag = u + v + w;
      u /= mag;
      v /= mag;
      w /= mag;

      //determine launch coordinate and launch normal based off randomly determined Barycentric coordinate
      Point launchCoord = (surfaceMesh.vertices[surfaceMesh.faces[launchPanel].p1] * u +
                           surfaceMesh.vertices[surfaceMesh.faces[launchPanel].p2] * v +
                           surfaceMesh.vertices[surfaceMesh.faces[launchPanel].p3] * w);

      Vec launchNormal = (surfaceMesh.vertices[surfaceMesh.faces[launchPanel].p1].averagedNormal * u +
                          surfaceMesh.vertices[surfaceMesh.faces[launchPanel].p2].averagedNormal * v +
                          surfaceMesh.vertices[surfaceMesh.faces[launchPanel].p3].averagedNormal * w);

      if (surfaceMesh.vertices[surfaceMesh.faces[launchPanel].p1].pz < projectionCandidateHeight){
        toLaunch[i] = false;
        continue;
      }

      candidateLaunchSites[i] = launchCoord;
      candidateLaunchNormals[i] = launchNormal;
      ghostPhases[i] = nullptr;

      //perform point-in-polygon check - assign phase
      int phaseIndex = 0;
      for (int p = phaseNum; p-- > 1;){
        int inside = 1 - 2 * specimenModel.phases[p].phaseBoundary.insideMesh(launchCoord);
        if (inside == 1){
          if ((specimenModel.phases[p].ghost == true) && (ghostPhases[i] == nullptr)){
            ghostPhases[i] = &specimenModel.phases[p];
            continue;
          }
          else{
            phaseIndex = p;
            break;
          }
        }
      }
      candidatePhases[i] = phaseIndex;
    }
    }

    long lb = stepNum;
    long ub = stepNum + PHASEASSIGNMENTSTEP;

    //calculate running volume (trim ions at end)
    for (it=lb; it<ub;it++){
      Phase localPhase = specimenModel.phases[candidatePhases[it]];
      const int localPhaseElNum = localPhase.elements.size();

      double effectiveIonicVolume = 0.0;
      if (ghostPhases[it] != nullptr){
        for (int cr = 0; cr<localPhaseElNum; cr++){
          if (ghostPhases[it]->substituteElements.count(localPhase.elements[cr])){
            string oldElement = localPhase.elements[cr];
            localPhase.elements[cr] = ghostPhases[it]->substituteElements[oldElement];
            if (ghostPhases[it]->substituteRatios[oldElement] >= 0.0)
              localPhase.elementRatios[cr] = ghostPhases[it]->substituteRatios[oldElement];
            if (ghostPhases[it]->substituteIonicVolumes[oldElement] >= 0.0)
              localPhase.elementIonicVolumes[cr] = ghostPhases[it]->substituteIonicVolumes[oldElement];
          }
        }
      }

      double maxRatios = 0.0;
      for (int cr = 0; cr<localPhaseElNum; cr++){
        maxRatios += localPhase.elementRatios[cr];
      }
      for (int cr = 0; cr < localPhaseElNum; cr++){
        localPhase.elementRatios[cr] /= maxRatios;
      }
      for (int cr = 0; cr < localPhaseElNum; cr++){
        effectiveIonicVolume += localPhase.elementRatios[cr] * localPhase.elementIonicVolumes[cr];
      }

      if (toLaunch[it] == true)
        vol += effectiveIonicVolume;
      //cout << vol << " " << effectiveIonicVolume << endl;
      if (vol > totalVol){
        break;
      }
    }

    stepNum += PHASEASSIGNMENTSTEP;
  }

  int tr = candidatePhases.size() - 1;
  while (tr > it){
    candidatePhases.pop_back();
    candidateLaunchSites.pop_back();
    candidateLaunchNormals.pop_back();
    toLaunch.pop_back();
    ghostPhases.pop_back();
    tr--;
  }

  const int toLaunchSize = toLaunch.size();

  int toLaunchCount = 0;
  for (int i=0;i<toLaunchSize;i++){
    if (toLaunch[i] == true)
      ++toLaunchCount;
  }


  vector<Point> candidateLaunchSitesLaunch;
  vector<Vec> candidateLaunchNormalsLaunch;
  vector<int> candidatePhasesLaunch;
  vector<Phase*> ghostPhasesLaunch;

  candidateLaunchSitesLaunch.reserve(toLaunchCount);
  candidateLaunchNormalsLaunch.reserve(toLaunchCount);
  candidatePhasesLaunch.reserve(toLaunchCount);
  ghostPhasesLaunch.reserve(toLaunchCount);

  for (int i=0;i<toLaunchSize;i++){
    if (toLaunch[i] == true){
      candidatePhasesLaunch.push_back(candidatePhases[i]);
      candidateLaunchSitesLaunch.push_back(candidateLaunchSites[i]);
      candidateLaunchNormalsLaunch.push_back(candidateLaunchNormals[i]);
      ghostPhasesLaunch.push_back(ghostPhases[i]);
    }
  }

  return {candidateLaunchSitesLaunch, candidateLaunchNormalsLaunch, candidatePhasesLaunch, ghostPhasesLaunch};
}

/*!
 * Function for determining candidate launch sites (located at panel vertices)
 *
 * @param surfaceMesh The inputted triangular surface mesh
 * @return vector of surfaceMesh vertex indices identifying the determined launch sites
 */
vector<long> determineUniformLaunchSites(TriangularMesh& surfaceMesh){

  surfaceMesh.setBoundingBox();

  vector<int> vertexAdded(surfaceMesh.vertices.size(), 0);
  vector<long> launchSites;

  int trajCount = 0;
  vector<int> fProj;
  for (int f=0;f<surfaceMesh.faces.size();f++){
    if ((surfaceMesh.faces[f].centre.pz > (surfaceMesh.minCoords[2] +
                                           PROJECTIONTHRESHOLD * (surfaceMesh.maxCoords[2] -
                                                                  surfaceMesh.minCoords[2])))
        && (fabs(surfaceMesh.isSurface[f] - 1) < 1e-10)){

      trajCount += 1;
      fProj.push_back(f);
      int p1 = surfaceMesh.faces[f].p1;
      int p2 = surfaceMesh.faces[f].p2;
      int p3 = surfaceMesh.faces[f].p3;
      if (vertexAdded[p1] == 0){
        vertexAdded[p1] = 1;
        launchSites.push_back(p1);
      }
      if (vertexAdded[p2] == 0){
        vertexAdded[p2] = 1;
        launchSites.push_back(p2);
      }
      if (vertexAdded[p3] == 0){
        vertexAdded[p3] = 1;
        launchSites.push_back(p3);
      }
    }
  }

  return launchSites;

}

/*!
 * Function implementing the discretised second law of motion for performing charged optics calculations
 *
 * @param t The current time parameter
 * @param y The previous ion phase space solution
 * @param params tuple containing
 *    (1) Pointer to top octreenode of octree for accelerating electrostatics
 *    (2) The ion instance whose trajectory is being integrated
 * @return void
 */
inline void lawOfMotion(double t, vector<double>& yNew, vector<double>& y, tuple<OctreeNode*, Ion*>& params){


  OctreeNode *top = get<0>(params);
  Ion *testIon = get<1>(params);

  double Ex = 0.0;
  double Ey = 0.0;
  double Ez = 0.0;

  double q = testIon->charge;
  double m = testIon->mass;

  Point currentPos(y[0], y[1], y[2]);
  top->calculateField(Ex, Ey, Ez, currentPos, CLUSTERADMISSTOL);

  yNew[0] = y[3];
  yNew[1] = y[4];
  yNew[2] = y[5];
  yNew[3] = -q/m * Ex;
  yNew[4] = -q/m * Ey;
  yNew[5] = -q/m * Ez;

  return;

}

/*!
 * Initialiser method for the detector object instance
 *
 * @param pos The detector centre position
 * @param normal The detector normal vector (normal to detector plane - defines detector orientation)
 * @param radius The detector radius
 */
Detector::Detector(Point pos, Vec normal, double radius): centre{pos}, normal{normal}, radius{radius}{

  double magNorm = sqrt(normal.vx * normal.vx + normal.vy * normal.vy + normal.vz * normal.vz);

  this->normal.vx /= magNorm;
  this->normal.vy /= magNorm;
  this->normal.vz /= magNorm;

  A = this->normal.vx;
  B = this->normal.vy;
  C = this->normal.vz;
  D = this->normal.vx * pos.px + this->normal.vy * pos.py + this->normal.vz * pos.pz;

  //calculate local detector coordinates for later determining local ion detector coordinates
  //performs Gram-Schmidt orthogonalisation
  //required to correct for arbitrary detector orientations

  //defines reference detector coordinate system
  double rXx = 1.0;
  double rXy = 0.0;
  double rXz = 0.0;

  double rYx = 0.0;
  double rYy = 1.0;
  double rYz = 0.0;

  //detector normal - first component of the reoriented orthonormal basis
  double nx = this->normal.vx;
  double ny = this->normal.vy;
  double nz = this->normal.vz;

  //performing Gram-Schmidt orthogonalisation
  double XnDot = rXx * nx + rXy * ny + rXz * nz;
  double YnDot = rYx * nx + rYy * ny + rYz * nz;

  Xx = rXx - XnDot * nx;
  Xy = rXy - XnDot * ny;
  Xz = rXz - XnDot * nz;

  double Xmag = sqrt(Xx * Xx + Xy * Xy + Xz * Xz);

  //X detector coordinate unit vector (correcting for detector orientation)
  Xx /= Xmag;
  Xy /= Xmag;
  Xz /= Xmag;

  double XYDot = rXx * rYx + rXy * rYy + rXz * rYz;

  Yx = rYx - YnDot * nx - XYDot * Xx;
  Yy = rYy - YnDot * ny - XYDot * Xy;
  Yz = rYz - YnDot * nz - XYDot * Xz;

  //Y detector coordinate unit vector (correcting for detector orientation)
  double Ymag = sqrt(Yx * Yx + Yy * Yy + Yz * Yz);

  Yx /= Ymag;
  Yy /= Ymag;
  Yz /= Ymag;

}

/*!
 * Method for calculating the trajectory intersection point on the detector
 * for a particular ion (once determining if it intersects the detector at all).
 *
 * @param testIon The ion being checked (for which its intersection point is being calculated)
 * @return True - intersects detector, False - does not intersect detector
 *
 */
bool Detector::calculatedTrajectoryIntersectionPoint(Ion& testIon){

  vector<Point> nTrajectory;
  vector<Point> nVelocity;

  for (int p=0;p<(testIon.trajectory.size()-1);p++){
    Point p1 = testIon.trajectory[p];
    Point p2 = testIon.trajectory[p+1];

    Point v1 = testIon.velocity[p];
    Point v2 = testIon.velocity[p+1];

    nTrajectory.push_back(p1);
    nVelocity.push_back(v1);

    int sp1 = this->planeSide(p1);
    int sp2 = this->planeSide(p2);

    Point trajectoryNormal = p2 - p1;
    double normVal = sqrt(trajectoryNormal.px * trajectoryNormal.px + trajectoryNormal.py * trajectoryNormal.py + trajectoryNormal.pz * trajectoryNormal.pz);
    trajectoryNormal.px /= normVal;
    trajectoryNormal.py /= normVal;
    trajectoryNormal.pz /= normVal;

    Vec vTrajectoryNormal(trajectoryNormal.px, trajectoryNormal.py, trajectoryNormal.pz);

    if (sp1 != sp2){
      Point intersectionPoint = p1;
      bool intersects = this->linearProjection(intersectionPoint, vTrajectoryNormal, 1.0);

      if (intersects == true){
        nTrajectory.push_back(intersectionPoint);

        //calculate interpolated ion velocity at intersection point
        double d1 = norm(p2, intersectionPoint);
        double dd = norm(p2, p1);
        double t = d1/dd;
        Point intersectionVelocity = v1 * t + v2 * (1.0 - t);
        nVelocity.push_back(intersectionVelocity);

        testIon.trajectory = nTrajectory;
        testIon.velocity = nVelocity;
        testIon.finalPos = intersectionPoint;
      }

      return intersects;
    }
  }
  return false;
}

/*!
 * Detector class method for converting real space (sample space) impact coordinates
 * into detector coordinates using the reoriented orthonormal detector basis (see class initialiser).
 * Result behaviour only defined for a passed position on the detector plane.
 *
 * @param intersectionPoint The intersection point on the detector
 * @return The intersection point in detector coordinates (with the z-coordinate set to zero)
 */
Point Detector::convertLocalcoordinate(Point& intersectionPoint){

  //convert final impact position into local detector coordinates
  Point relativeIntersectionpoint = intersectionPoint - this->centre;

  double xDetPos = Xx * relativeIntersectionpoint.px + Xy * relativeIntersectionpoint.py + Xz * relativeIntersectionpoint.pz;
  double yDetPos = Yx * relativeIntersectionpoint.px + Yy * relativeIntersectionpoint.py + Yz * relativeIntersectionpoint.pz;

  return Point(xDetPos, yDetPos, 0.0);

}

/*!
 * Detector class method for determining which side of the detector plane a position falls
 *
 * @param testPos The test position
 * @return 1 or -1 depending on point-plane side relative to detector orientation
 */
int Detector::planeSide(Point& testPos){
  return sgn(A * testPos.px + B * testPos.py + C * testPos.pz - D);
}

/*!
 * Method for projecting an ion onto the defined detector instance under a linear projection.
 *
 * @param ionPosition The initial ion position.
 * @param launchNormal The ion trajectory normal at ionPosition.
 * @param kappa the linear projection parameter.
 * @return The transfered point onto the detector.
 */
bool Detector::linearProjection(Point& ionPosition, Vec& ionLaunchNormal, double kappa){

  //ensure passed launch vec is normalised
  double lNorm = sqrt(ionLaunchNormal.vx * ionLaunchNormal.vx + ionLaunchNormal.vy * ionLaunchNormal.vy + ionLaunchNormal.vz * ionLaunchNormal.vz);
  ionLaunchNormal.vx /= lNorm;
  ionLaunchNormal.vy /= lNorm;
  ionLaunchNormal.vz /= lNorm;

  double dotDetNormLaunchVec = (ANALYSISAXISX * ionLaunchNormal.vx + ANALYSISAXISY * ionLaunchNormal.vy + ANALYSISAXISZ * ionLaunchNormal.vz);

  double theta = acos(dotDetNormLaunchVec);
  double phi = atan2(ionLaunchNormal.vy, ionLaunchNormal.vx);
  double mtheta = theta * kappa;

  //perform compression of ion launch normal
  Point compressedLaunchNormal(ionLaunchNormal.vx, ionLaunchNormal.vy, ionLaunchNormal.vz);
  compressedLaunchNormal.px = sin(mtheta) * cos(phi);
  compressedLaunchNormal.py = sin(mtheta) * sin(phi);
  compressedLaunchNormal.pz = cos(mtheta);

  //extrapolate onto virtual detector
  double ldn = (this->normal.vx * compressedLaunchNormal.px + this->normal.vy * compressedLaunchNormal.py + this->normal.vz * compressedLaunchNormal.pz);
  Point detPosLaunchPosVec = this->centre - ionPosition;

  double lda = (detPosLaunchPosVec.px * this->normal.vx + detPosLaunchPosVec.py * this->normal.vy + detPosLaunchPosVec.pz * this->normal.vz)/ldn;

  Point projectedPosition = ionPosition + compressedLaunchNormal * lda;
  if ((norm(projectedPosition, this->centre) > this->radius)){
      return false;
  }
  else {
    ionPosition = projectedPosition;
    return true;
  }

  return false;

}

/*!
 * Function for calculating the local magnification (Jacobian matrix (J) and determinant (||J||))
 * for projected panels onto the detector (placed in the x-y plane).
 *
 * @param ion1LaunchPos panel corner 1 original position
 * @param ion2LaunchPos panel corner 2 original position
 * @param ion3LaunchPos panel corner 3 original position
 * @param ion1DetectorPos panel corner 1 projected position
 * @param ion2DetectorPos panel corner 2 projected position
 * @param ion3DetectorPos panel corner 3 projected position
 * @return tuple containing the following results <||J||, J11, J12, J21, J22>
 */
tuple<double, double, double, double, double> calculateLocalMagnification(Point& ion1LaunchPos,
                                                                          Point& ion2LaunchPos,
                                                                          Point& ion3LaunchPos,
                                                                          Point& ion1DetectorPos,
                                                                          Point& ion2DetectorPos,
                                                                          Point& ion3DetectorPos){
  //calculate local surface panel coordinate system basis
  double ABx = ion2LaunchPos.px - ion1LaunchPos.px;
  double ABy = ion2LaunchPos.py - ion1LaunchPos.py;
  double ABz = ion2LaunchPos.pz - ion1LaunchPos.pz;

  double ABl = sqrt(ABx * ABx + ABy * ABy + ABz * ABz);

  double ACx = ion3LaunchPos.px - ion1LaunchPos.px;
  double ACy = ion3LaunchPos.py - ion1LaunchPos.py;
  double ACz = ion3LaunchPos.pz - ion1LaunchPos.pz;

  double ACl = sqrt(ACx * ACx + ACy * ACy + ACz * ACz);

  double BCx = ion3LaunchPos.px - ion2LaunchPos.px;
  double BCy = ion3LaunchPos.py - ion2LaunchPos.py;
  double BCz = ion3LaunchPos.pz - ion2LaunchPos.pz;

  double BCl = sqrt(BCx * BCx + BCy * BCy + BCz * BCz);

  double nABx = ABx/ABl;
  double nABy = ABy/ABl;
  double nABz = ABz/ABl;

  double nx = nABy * ACz - nABz * ACy;
  double ny = -(nABx * ACz - nABz * ACx);
  double nz = nABx * ACy - nABy * ACx;

  double nnorm = sqrt(nx * nx + ny * ny + nz * nz);

  nx /= nnorm;
  ny /= nnorm;
  nz /= nnorm;

  //define global coordinate system (sample space)
  double rXx = 1.0;
  double rXy = 0.0;
  double rXz = 0.0;

  double rYx = 0.0;
  double rYy = 1.0;
  double rYz = 0.0;

  //performing Gram-Schmidt orthogonalisation
  //to calculate local panel coordinate system with respect to the global system above
  double XnDot = rXx * nx + rXy * ny + rXz * nz;
  double YnDot = rYx * nx + rYy * ny + rYz * nz;

  double Xx = rXx - XnDot * nx;
  double Xy = rXy - XnDot * ny;
  double Xz = rXz - XnDot * nz;
  double Xmag = sqrt(Xx * Xx + Xy * Xy + Xz * Xz);

  Xx /= Xmag;
  Xy /= Xmag;
  Xz /= Xmag;

  double XYDot = Xx * rYx + Xy * rYy + Xz * rYz;

  double Yx = rYx - YnDot * nx - XYDot * Xx;
  double Yy = rYy - YnDot * ny - XYDot * Xy;
  double Yz = rYz - YnDot * nz - XYDot * Xz;

  double Ymag = sqrt(Yx * Yx + Yy * Yy + Yz * Yz);

  Yx /= Ymag;
  Yy /= Ymag;
  Yz /= Ymag;

  //construct problem matrix elements
  double Xi = 0.0;
  double Yi = 0.0;
  double Xj = (ABx * Xx + ABy * Xy + ABz * Xz)/(Xx * Xx + Xy * Xy + Xz * Xz);
  double Yj = (ABx * Yx + ABy * Yy + ABz * Yz)/(Yx * Yx + Yy * Yy + Yz * Yz);
  double Xk = (ACx * Xx + ACy * Xy + ACz * Xz)/(Xx * Xx + Xy * Xy + Xz * Xz);
  double Yk = (ACx * Yx + ACy * Yy + ACz * Yz)/(Yx * Yx + Yy * Yy + Yz * Yz);

  //calculate panel area via Heron's formula
  double s = (ABl + ACl + BCl)/2.0;
  double A = sqrt(s * (s - ABl) * (s - BCl) * (s - ACl));

  //calculate Jacobian matrix elements
  double M11 = 1.0/(2.0 * A) * ((Yj - Yk) * ion1DetectorPos.px + (Yk - Yi) * ion2DetectorPos.px + (Yi - Yj) * ion3DetectorPos.px);
  double M21 = 1.0/(2.0 * A) * ((Yj - Yk) * ion1DetectorPos.py + (Yk - Yi) * ion2DetectorPos.py + (Yi - Yj) * ion3DetectorPos.py);
  double M12 = 1.0/(2.0 * A) * ((Xk - Xj) * ion1DetectorPos.px + (Xi - Xk) * ion2DetectorPos.px + (Xj - Xi) * ion3DetectorPos.px);
  double M22 = 1.0/(2.0 * A) * ((Xk - Xj) * ion1DetectorPos.py + (Xi - Xk) * ion2DetectorPos.py + (Xj - Xi) * ion3DetectorPos.py);

  //calculate Jacobian determinant (magnification)
  double J = M11 * M22 - M12 * M21;

  tuple<double, double, double, double, double> mappingRes = {J, M11, M12, M21, M22};

  return mappingRes;

}


/*!
 * Function for writing range files (.rrng) for synthetic data.
 *
 * @param filename The output file location to write the .rrng file to.
 * @param peaks The set of peaks which to write
 *
 */
void rangeFileSyntheticDataWriter(string filename, vector<SyntheticPeak> peaks){

  createDirectoryTree(filename);

  set<string> ionTypes;
  for (int p=0;p<peaks.size();p++){
    for (int j=0;j<peaks[p].ionLabels.size();j++){
      ionTypes.insert(peaks[p].ionLabels[j]);
    }
  }

  ofstream rangeFile(filename);
  rangeFile << "#Generated by CASRA v2 for ranging synthetic datasets." << endl;

  rangeFile << "[Ions]" << endl;
  rangeFile << "Number=" << ionTypes.size() << endl;

  int ic = 0;
  for (set<string>::iterator itIonType = ionTypes.begin(); itIonType != ionTypes.end(); ++itIonType){
    ic++;
    rangeFile << "Ion" << ic << "=" << (*itIonType) << endl;
  }

  rangeFile << "[Ranges]" << endl;
  rangeFile << "Number=" << peaks.size() << endl;

  for (int p=0;p<peaks.size();p++){
    //generate random hexidecimal colour
    string peakColour = "";
    for (int c=0;c<6;c++){
      int randNum = rand() % 16;
      peakColour += hexChar[randNum];
    }
    rangeFile << "Range" << p+1 << "=" << peaks[p].lower << " " << peaks[p].upper << " Vol:" << peaks[p].ionicVolume * 1e27 << " ";

    for (int j=0;j<peaks[p].ionLabels.size();j++){
      rangeFile << peaks[p].ionLabels[j] << ":" << peaks[p].ionNumbers[j] << " ";
    }
    rangeFile << "Color:" << peakColour << endl;
  }
}

/*!
 * Function for writing APT dataset to a POS file.
 *
 * @param filename The output file location to write the .epos file to.
 * @param ions The vector of ions with which to write to the .epos file
 * @param header whether to write a file header or not (e.g. new file or append to file)
 *
 */
void posSyntheticDataWriter(string filename, vector<Ion> ions, bool header){

  createDirectoryTree(filename);

  ofstream APTDataFile;

  if (header)
    APTDataFile.open(filename, ofstream::binary);
  else
    APTDataFile.open(filename, ofstream::binary | ofstream::app);

  int nions = ions.size();

  for (int i=0;i<nions;i++){
    float sx = (float)ions[i].launchPos.px * 1e9;
    float sy = (float)ions[i].launchPos.py * 1e9;
    float sz = (float)ions[i].launchPos.pz * 1e9;
    float m2c = (float)ions[i].ionIdentifier + 1.0;
    float tof = (float)ions[i].ionIdentifier + 1.0;
    float dcVoltage = 0.0;
    float pulseVoltage = (float)ions[i].pulseVoltage * 1000.0;
    float dx = (float)ions[i].finalPos.px * 1e3;
    float dy = (float)ions[i].finalPos.py * 1e3;
    unsigned int pulsesSinceLastEventPulse = 1;
    unsigned int ionsPerPulse = 1;

    char buffer[4];

    bigEndianBufferPush(&sx, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&sy, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&sz, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&m2c, buffer, 4);
    APTDataFile.write(buffer, 4);
  }

  APTDataFile.close();

}

/*!
 * Function for writing APT dataset to an EPOS file.
 *
 * @param filename The output file location to write the .epos file to.
 * @param ions The vector of ions with which to write to the .epos file
 * @param header whether to write a file header or not (e.g. new file or append to file)
 *
 */
void eposSyntheticDataWriter(string filename, vector<Ion> ions, bool header){

  createDirectoryTree(filename);

  ofstream APTDataFile;

  if (header)
    APTDataFile.open(filename, ofstream::binary);
  else
  APTDataFile.open(filename, ofstream::binary | ofstream::app);

  int nions = ions.size();

  for (int i=0;i<nions;i++){
    float sx = (float)ions[i].launchPos.px * 1e9;
    float sy = (float)ions[i].launchPos.py * 1e9;
    float sz = (float)ions[i].launchPos.pz * 1e9;
    float m2c = (float)ions[i].ionIdentifier + 1.0;
    float tof = (float)ions[i].ionIdentifier + 1.0;
    float dcVoltage = 0.0;
    float pulseVoltage = (float)ions[i].pulseVoltage/1000.0;

    float dx = (float)ions[i].finalPos.px * 1e3;
    float dy = (float)ions[i].finalPos.py * 1e3;
    unsigned int pulsesSinceLastEventPulse = 1;
    unsigned int ionsPerPulse = 1;

    char buffer[4];

    bigEndianBufferPush(&sx, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&sy, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&sz, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&m2c, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&tof, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&dcVoltage, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&pulseVoltage, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&dx, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&dy, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&pulsesSinceLastEventPulse, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&ionsPerPulse, buffer, 4);
    APTDataFile.write(buffer, 4);

  }

  APTDataFile.close();

}



/*!
 * Function for writing APT dataset to an ATO file.
 *
 * @param filename The output file location to write the .ato file to.
 * @param ions The vector of ions with which to write to the .ato file
 * @param header whether to write a file header or not (e.g. new file or append to file)
 *
 */
void atoSyntheticDataWriter(string filename, vector<Ion> ions, bool header){

  createDirectoryTree(filename);

  ofstream APTDataFile;

  if (header){
    APTDataFile.open(filename, ofstream::binary);
    const char version[1] = {0x03};
    APTDataFile.write(version, 1);
    APTDataFile.write(version, 1);
    APTDataFile.write(version, 1);
    APTDataFile.write(version, 1);
    APTDataFile.write(version, 1);
    APTDataFile.write(version, 1);
    APTDataFile.write(version, 1);
    APTDataFile.write(version, 1);
  }
  else{
    APTDataFile.open(filename, ofstream::binary | ofstream::app);
  }

  int nions = ions.size();

  char buffer[4];
  for (int i = 0; i < nions; i++){

    float sx = (float)ions[i].launchPos.px * 1e10;
    float sy = (float)ions[i].launchPos.py * 1e10;
    float sz = (float)ions[i].launchPos.pz * 1e10;
    float m2c = (float)ions[i].ionIdentifier;
    float clusterId = (float)1;
    float pulseNumber = (float)(i+1);
    float dcVoltage = 0.0;
    float tof = 1.0;
    float dx = (float)ions[i].finalPos.px * 1e2;
    float dy = (float)ions[i].finalPos.py * 1e2;
    float pulseVoltage = (float)ions[i].pulseVoltage / 1000.0;
    float VVolt = 1.0;
    float FourierR = 1.0;
    float FourierI = 1.0;

    smallEndianBufferPush(&sx, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&sy, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&sz, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&m2c, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&clusterId, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&pulseNumber, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&dcVoltage, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&tof, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&dx, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&dy, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&pulseVoltage, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&VVolt, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&FourierR, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&FourierI, buffer, 4);
    APTDataFile.write(buffer, 4);
  }

  APTDataFile.close();

}

vector<SyntheticPeak> createPeakSet(map<string, double> ionLabelVolumeMap){
  vector<SyntheticPeak> peaks;
  int ic = 0;
  for (map<string, double>::iterator it = ionLabelVolumeMap.begin(); it != ionLabelVolumeMap.end(); ++it){
    SyntheticPeak peak;
    ic++;
    peak.labelToPeak(it->first, it->second, ic);
    peaks.push_back(peak);
  }
  return peaks;
}

void SyntheticPeak::labelToPeak(string ionLabel, double ionicVolume, int massToChargeLabel){
  this->lower = (double)massToChargeLabel - 0.1;
  this->upper = (double)massToChargeLabel + 0.1;
  this->ionicVolume = ionicVolume;
  this->ionLabels.push_back(ionLabel);
  this->ionNumbers.push_back(1);
}
