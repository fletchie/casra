# Charged optics library

This directory contains the source files for building the charged optics library. This includes: 

* The chargedoptics.cpp source file - contains functions for calculating both uniform and realistic ion trajectory fluxes over the emitter surface via the Dormand-Prince RK54 integrator [see the base library](../base). Additional functions are also provided for calculating local magnification, and performing the transfer of ion trajectories onto a detector via a linear projection.
* The hitmapp.cpp source file - contains functions for the calculation of both density, phase, and crossover detector maps from a uniform ion projection. This allows for calculation of hitmaps within seconds, as opposed to Monte-Carlo based approaches. 
* The opticsinitialiser.cpp source file - initialises the charged optics parameters from the model configuration.xml file. 
