/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cstdio>
#include <tuple>

#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_utils.hpp"
#include "rapidxml/rapidxml_print.hpp"

#include "CASRA/tipBuild/tipBuild.h"
#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/io/ioBase.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/specimenModels/numericalModel.h"
#include "CASRA/geometries/generateGeometries.h"

using namespace std;

int tipBuild(string iConfigFile, string oConfigFile){

  //create directory tree for writing the output config file
  int dcRet = createDirectoryTree(oConfigFile);

  cout << "Reading input configuration file..." << endl;

  char *charcfname = new char[iConfigFile.length() + 1];
  strcpy(charcfname, iConfigFile.c_str());

  //load the xml file using the rapidxml library
  rapidxml::file<> xmlFile(charcfname);
  rapidxml::xml_document<> doc;

  try {
    doc.parse<rapidxml::parse_declaration_node | rapidxml::parse_no_data_nodes>(xmlFile.data());
  }
  catch (const rapidxml::parse_error& e){
        cerr << "\tERROR: XML parsing error prevented reading of passed file due to '" << e.what() << "'" << endl;
        cerr << "\tPlease ensure XML is in a valid format." << endl;
        exit(1);
    }

  rapidxml::xml_node<> *root;
  rapidxml::xml_node<> *specimenModelNode;
  rapidxml::xml_node<> *specimenPhasesNode;
  rapidxml::xml_node<> *specimenPhaseNode;
  rapidxml::xml_node<> *specimenPhaseMeshNode;

  root = doc.first_node("model");

  specimenModelNode = root->first_node("specimenModel");
  specimenPhasesNode = specimenModelNode->first_node("phases");

  //iterate through phases in configuration file
  int phaseCount = 0;
  for (specimenPhaseNode = specimenPhasesNode->first_node(); specimenPhaseNode != NULL; specimenPhaseNode = specimenPhaseNode->next_sibling())
  {
    string phaseName = specimenPhaseNode->name();
    if (specimenPhaseNode->first_node("mesh") != nullptr){
      specimenPhaseMeshNode = specimenPhaseNode->first_node("mesh");
      if (specimenPhaseMeshNode->first_node("tipBuilder") != nullptr){
        rapidxml::xml_node<> *tBuildNode;
        cout << "here" << endl;
        for (tBuildNode = specimenPhaseMeshNode->first_node("tipBuilder")->first_node(); tBuildNode != NULL; tBuildNode = tBuildNode->next_sibling()){
          cout << "tip builder instruction found. Performing mesh construction ..." << endl;
          TriangularMesh tMesh = meshbuild(tBuildNode, doc, true);
          cout << "built mesh" << endl;
        }
        specimenPhaseMeshNode->remove_node(specimenPhaseMeshNode->first_node("tipBuilder"));
      }
      else{
        cerr << "No input mesh or tip builder command for '" << phaseName << "' detected." << endl;
        exit(1);
      }
    }
  }

  cout << "\tsuccessfully found from xml file. Reading now..." << endl;

  std::ofstream outFile;
  outFile.open(oConfigFile);

  outFile << doc;
  outFile.close();

  doc.clear();

  delete[] charcfname;

  return 0;

}

TriangularMesh meshbuild(rapidxml::xml_node<> *buildNode, rapidxml::xml_document<>& xmlDoc, bool modifyXML){
  string tipBuildType = string(buildNode->first_attribute("type")->value());
  TriangularMesh outMesh;
  if (tipBuildType == "tip"){
    double shankAngle = stod(buildNode->first_attribute("shankAngle")->value()) * PI/180.0;
    double R0 = stod(buildNode->first_attribute("capRadius")->value());
    double length = stod(buildNode->first_attribute("length")->value());
    double offset = stod(buildNode->first_attribute("offset")->value());
    double R1 = R0 + length * tan(shankAngle);
    cout << "\t\tbuilding tip mesh" << endl;
    tuple<TriangularMesh, int>  outMeshRes = buildTipMesh(R0, R1, length, offset);
    cout << "\t\ttip mesh built" << endl;
    outMesh = get<0>(outMeshRes);
    if (modifyXML == true){
      int wcRet = writeToConfig(&xmlDoc, buildNode->parent()->parent(), outMesh);
    }
  }
  else if (tipBuildType == "sphere"){
    double R = stod(buildNode->first_attribute("radius")->value());
    Point cp = extractPoint(buildNode->first_attribute("centre")->value());
    int n = stoi(buildNode->first_attribute("resolution")->value());

    tuple<TriangularMesh, int> outMeshRes = buildSphere(R, cp, n);
    outMesh = get<0>(outMeshRes);

    cout << "run for sphere" << endl;
    if (modifyXML == true){
      int wcRet = writeToConfig(&xmlDoc, buildNode->parent()->parent(), outMesh);
    }
    cout << "write sphere" << endl;
  }
  else if (tipBuildType == "cuboid"){
    string sP1 = buildNode->first_attribute("P1")->value();
    string sP2 = buildNode->first_attribute("P2")->value();
    string sP3 = buildNode->first_attribute("P3")->value();
    string sP4 = buildNode->first_attribute("P4")->value();
    string sP5 = buildNode->first_attribute("P5")->value();
    string sP6 = buildNode->first_attribute("P6")->value();
    string sP7 = buildNode->first_attribute("P7")->value();
    string sP8 = buildNode->first_attribute("P8")->value();

    Point dP1 = extractPoint(sP1);
    Point dP2 = extractPoint(sP2);
    Point dP3 = extractPoint(sP3);
    Point dP4 = extractPoint(sP4);
    Point dP5 = extractPoint(sP5);
    Point dP6 = extractPoint(sP6);
    Point dP7 = extractPoint(sP7);
    Point dP8 = extractPoint(sP8);

    tuple<TriangularMesh, int> outMeshRes = buildRectangularMesh(dP1, dP2, dP3, dP4,
                                                                 dP5, dP6, dP7, dP8);
    outMesh = get<0>(outMeshRes);
    if (modifyXML == true){
      int wcRet = writeToConfig(&xmlDoc, buildNode->parent()->parent(), outMesh);
    }
  }
  else if (tipBuildType == "upperShell"){
    double outerRadius = stod(buildNode->first_attribute("outerRadius")->value());
    double innerRadius = stod(buildNode->first_attribute("innerRadius")->value());
    string centreStr = buildNode->first_attribute("centre")->value();
    Point centre = extractPoint(centreStr);
    int resolution = stoi(buildNode->first_attribute("resolution")->value());

    tuple<TriangularMesh, int> outMeshRes = buildUpperShell(outerRadius, innerRadius, centre, resolution);
    outMesh = get<0>(outMeshRes);

    //construct the upper shell phase mesh

    if (modifyXML == true){
      int wcRet = writeToConfig(&xmlDoc, buildNode->parent()->parent(), outMesh);
    }
  }

  cout << "complete" << endl;

  return outMesh;
}

int writeToConfig(rapidxml::xml_document<>* doc, rapidxml::xml_node<>* specimenPhaseMeshNode, TriangularMesh tipMesh){

  long currentVertices;
  rapidxml::xml_node<>*vnode;
  if (specimenPhaseMeshNode->first_node("vertices") == nullptr){
    vnode = doc->allocate_node(rapidxml::node_element, "vertices");
    specimenPhaseMeshNode->append_node(vnode);
    specimenPhaseMeshNode->append_attribute(doc->allocate_attribute("type", "TriangularSurfaceMesh"));
    vnode->append_attribute(doc->allocate_attribute("vcount", "0"));
    currentVertices = 0;
  }
  else {
    vnode = specimenPhaseMeshNode->first_node("vertices");
    currentVertices = stol(vnode->first_attribute("vcount")->value());
  }

  for (int i=0;i<tipMesh.vertices.size();i++){
    auto node = doc->allocate_node(rapidxml::node_element, "vertex");
    node->append_attribute(doc->allocate_attribute("px", doc->allocate_string(to_string(tipMesh.vertices[i].px).c_str())));
    node->append_attribute(doc->allocate_attribute("py", doc->allocate_string(to_string(tipMesh.vertices[i].py).c_str())));
    node->append_attribute(doc->allocate_attribute("pz", doc->allocate_string(to_string(tipMesh.vertices[i].pz).c_str())));
    vnode->append_node(node);
  }
  vnode->first_attribute("vcount")->value(doc->allocate_string(to_string(currentVertices + tipMesh.vertices.size()).c_str()));

  long currentFaces;
  rapidxml::xml_node<>*cnode;
  if (specimenPhaseMeshNode->first_node("cells") == nullptr){
    cnode = doc->allocate_node(rapidxml::node_element, "cells");
    specimenPhaseMeshNode->append_node(cnode);
    cnode->append_attribute(doc->allocate_attribute("ccount", "0"));
    currentFaces = 0;
  }
  else {
    cnode = specimenPhaseMeshNode->first_node("cells");
    currentFaces = stol(cnode->first_attribute("ccount")->value());
  }

  for (int i=0;i<tipMesh.faces.size();i++){
    auto node = doc->allocate_node(rapidxml::node_element, "cell");
    node->append_attribute(doc->allocate_attribute("v1", doc->allocate_string(to_string(tipMesh.faces[i].p1 + currentVertices).c_str())));
    node->append_attribute(doc->allocate_attribute("v2", doc->allocate_string(to_string(tipMesh.faces[i].p2 + currentVertices).c_str())));
    node->append_attribute(doc->allocate_attribute("v3", doc->allocate_string(to_string(tipMesh.faces[i].p3 + currentVertices).c_str())));
    cnode->append_node(node);
  }
  cnode->first_attribute("ccount")->value(doc->allocate_string(to_string(currentFaces + tipMesh.faces.size()).c_str()));

  return 0;
}
