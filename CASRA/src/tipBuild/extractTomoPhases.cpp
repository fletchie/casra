/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cstdio>
#include <tuple>
#include <tgmath.h>
#include <numeric>

#include "CASRA/tipBuild/extractTomoPhases.h"
#include "CASRA/io/ioBase.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/marchingCubes.h"
#include "CASRA/tipBuild/tipBuild.h"
#include "CASRA/base/graphMath.h"
#include "CASRA/mesh/remeshSupport.h"
#include "CASRA/io/TIFFReader.h"

using namespace std;

string sfilename = "";
vector<vector<vector<vector<double>>>> extractedImage;
int xDim;
int yDim;
int zDim;

/*!
 * Function for extracting phases from TIFF file (if passed).
 *
 * @param buildNode Pointer to the xml node containing the mesh extractor info.
 * @param xmlDoc The XMl doc instance.
 * @param modifyXMl Whether to modify XML inline or not.
 * @return The extracted triangular phase mesh.
 */
TriangularMesh extractPhase(rapidxml::xml_node<> *buildNode, rapidxml::xml_document<>& xmlDoc, bool modifyXML, string phaseName){

  int smoothsteps = 0;
  double isovalue = 0.0;
  int subSample = 1;
  string filename = "";
  if (buildNode->first_attribute("file") == nullptr){
    if (sfilename == ""){
      cerr << "no input voxel file detected for phase `" <<
              phaseName << "`. Please specify voxel file to read from as xml attribute `file`." << endl;
      exit(1);
    }
  }
  else {
    filename = string(buildNode->first_attribute("file")->value());
  }

  string savemesh = "";
  if (buildNode->first_attribute("savemesh") != nullptr){
    savemesh = string(buildNode->first_attribute("savemesh")->value());
  }

  string x = "x";
  string y = "y";
  string z = "z";

  string invx = "+";
  string invy = "+";
  string invz = "+";

  double xres = 1.0;
  double yres = 1.0;
  double zres = 1.0;
  bool capbase = true;
  bool capapex = false;
  bool largestContourOnly = false;
  double remeshRes = -1.0;

  if (sfilename != filename){
    extractedImage = readTiff(filename);
    sfilename = filename;
    xDim = extractedImage.size();
    yDim = extractedImage[0].size();
    zDim = extractedImage[0][0].size();
  }

  if (buildNode->first_attribute("isovalue") == nullptr){
    cerr << "\tno `isovalue` attribute detected for phase `" <<
            phaseName << "`. Please pass isovalue attribute." << endl;
    exit(1);
  }
  else{
    isovalue = stod(buildNode->first_attribute("isovalue")->value());
  }

  if (buildNode->first_attribute("smoothsteps") != nullptr){
    smoothsteps = stoi(buildNode->first_attribute("smoothsteps")->value());
  }

  if (buildNode->first_attribute("capbase") != nullptr){
    string cpstr = string(buildNode->first_attribute("capbase")->value());
    if (cpstr == "true" || cpstr == "True" || cpstr == "y" || cpstr == "TRUE" || cpstr == "yes"){
      capbase = true;
    }
    else {
      capbase = false;
    }
  }

  if (buildNode->first_attribute("capapex") != nullptr){
    string cpstr = string(buildNode->first_attribute("capapex")->value());
    if (cpstr == "true" || cpstr == "True" || cpstr == "y" || cpstr == "TRUE" || cpstr == "yes"){
      capapex = true;
    }
    else {
      capapex = false;
    }
  }

  if (buildNode->first_attribute("largestContorOnly") != nullptr){
    string cpstr = string(buildNode->first_attribute("largestContorOnly")->value());
    if (cpstr == "true" || cpstr == "True" || cpstr == "y" || cpstr == "TRUE" || cpstr == "yes"){
      largestContourOnly = true;
    }
    else {
      largestContourOnly = false;
    }
  }
  else if (buildNode->first_attribute("largestContourOnly") != nullptr){
    string cpstr = string(buildNode->first_attribute("largestContourOnly")->value());
    if (cpstr == "true" || cpstr == "True" || cpstr == "y" || cpstr == "TRUE" || cpstr == "yes"){
      largestContourOnly = true;
    }
    else {
      largestContourOnly = false;
    }
  }
  else{
    cout << "No attribute `largestContourOnly` detected for phase `" <<
            phaseName << "`. Using default value of '" <<
            largestContourOnly << "' (extract only the largest contour)." << endl;
  }

  if (buildNode->first_attribute("remesh") != nullptr){
    remeshRes = stod(buildNode->first_attribute("remesh")->value());
  }

  if (buildNode->first_attribute("subsample") != nullptr){
    subSample = stoi(buildNode->first_attribute("subsample")->value());
  }
  if (buildNode->first_attribute("imageaxisxyz") != nullptr){
    vector<string> xyz = extractPointChar(buildNode->first_attribute("imageaxisxyz")->value());
    x = xyz[0];
    y = xyz[1];
    z = xyz[2];
  }
  else {
    cout << "Assuming x,y,z axes in order loaded by TIFF reader for phase `" <<
            phaseName << "`. Note that specimen analysis axis should be aligned " <<
            "along z (e.g. here dimension: " << extractedImage[0][0].size() << ")" << endl;
  }
  if (buildNode->first_attribute("axisinv") != nullptr){
    vector<string> xyz = extractPointChar(buildNode->first_attribute("axisinv")->value());
    invx = xyz[0];
    if ((invx != "-") && (invx != "+")){
      cerr << "\tinvalid x-axis inversion for phase `" << phaseName <<
              "`. Should be either `+` (raw) or `-` (invert). \nExtraction failed." << endl;
      exit(1);
    }
    invy = xyz[1];
    if ((invy != "-") && (invy != "+")){
      cerr << "\tinvalid y-axis inversion for phase `" << phaseName <<
              "`. Should be either `+` (raw) or `-` (invert). \nExtraction failed." << endl;
      exit(1);
    }
    invz = xyz[2];
    if ((invz != "-") && (invz != "+")){
      cerr << "\tinvalid z-axis inversion for phase `" << phaseName <<
              "`. Should be either `+` (raw) or `-` (invert). \nExtraction failed." << endl;
      exit(1);
    }
  }
  else {
    cout << "Assuming x,y,z axis inversions are as passed (e.g. uninverted) for phase `" <<
            phaseName << "`. Default `axisinv=(" << invz << ",+" << invy << "," << invz << ")`." << endl;
  }
  if (buildNode->first_attribute("imageresolution") != nullptr){
    Point imageRes = extractPoint(buildNode->first_attribute("imageresolution")->value());
    xres = imageRes.px * subSample;
    yres = imageRes.py * subSample;
    zres = imageRes.pz * subSample;
  }
  else {
    cout << "Assuming uniform voxel resolutions of 1 nm." << endl;
  }

  //calculating subsampled voxel field dimensions
  int sXDim = floor(xDim/subSample);
  int sYDim = floor(yDim/subSample);
  int sZDim = floor(zDim/subSample);

  vector<vector<vector<double>>> processedImage = vector<vector<vector<double>>>(sXDim, vector<vector<double>>(sYDim, vector<double>(sZDim)));
  double maxFieldVal = numeric_limits<double>::min();
  double minFieldVal = numeric_limits<double>::max();
  if (extractedImage[0][0][0].size() == 1){
    cout << "Single colour channel detected. Proceeding phase extraction with channel" << endl;
    for (int i=0;i<sXDim;i++){
      for (int j=0;j<sYDim;j++){
        for (int k=0;k<sZDim;k++){
          int oi = i * subSample;
          int oj = j * subSample;
          int ok = k * subSample;
          processedImage[i][j][k] = extractedImage[oi][oj][ok][0];
          if (processedImage[i][j][k] > maxFieldVal){
            maxFieldVal = processedImage[i][j][k];
          }
          if (processedImage[i][j][k] < minFieldVal){
            minFieldVal = processedImage[i][j][k];
          }
        }
      }
    }
  }
  else if (extractedImage[0][0][0].size() != 1){
    cout << "\tMultiple colour channels in tiff detected. Such tiffs are " <<
            "currently unsupported. Make sure voxel data is a single channel " <<
            "64 bit float or 32 bit int." << endl;
    cout << "Extraction failed." << endl;
    exit(1);
  }

  //cap base - if true
  if (capbase == true){
    cout << "\tcapping base" << endl;
    if (z == "x"){
      for (int j=0;j<sYDim;j++){
        for (int k=0;k<sZDim;k++){
          processedImage[0][j][k] = minFieldVal -1000.0;
          processedImage[1][j][k] = minFieldVal -1000.0;
          processedImage[2][j][k] = minFieldVal -1000.0;
        }
      }
    }
    else if (z == "y"){
      for (int i=0;i<sXDim;i++){
        for (int k=0;k<sZDim;k++){
          processedImage[i][0][k] = minFieldVal -1000.0;
          processedImage[i][1][k] = minFieldVal -1000.0;
          processedImage[i][2][k] = minFieldVal -1000.0;
        }
      }
    }
    else if (z == "z"){
      for (int i=0;i<sXDim;i++){
        for (int j=0;j<sYDim;j++){
          processedImage[i][j][0] = minFieldVal -1000.0;
          processedImage[i][j][1] = minFieldVal -1000.0;
          processedImage[i][j][2] = minFieldVal -1000.0;
        }
      }
    }
  }

  //cap apex - if true
  if (capapex == true){
    cout << "\tcapping apex" << endl;
    if (z == "x"){
      for (int j=0;j<sYDim;j++){
        for (int k=0;k<sZDim;k++){
          processedImage[sXDim-1][j][k] = minFieldVal -1000.0;
          processedImage[sXDim-2][j][k] = minFieldVal -1000.0;
          processedImage[sXDim-3][j][k] = minFieldVal -1000.0;
        }
      }
    }
    else if (z == "y"){
      for (int i=0;i<sXDim;i++){
        for (int k=0;k<sZDim;k++){
          processedImage[i][sYDim-1][k] = minFieldVal -1000.0;
          processedImage[i][sYDim-2][k] = minFieldVal -1000.0;
          processedImage[i][sYDim-3][k] = minFieldVal -1000.0;
        }
      }
    }
    else if (z == "z"){
      for (int i=0;i<sXDim;i++){
        for (int j=0;j<sYDim;j++){
          processedImage[i][j][sZDim-1] = minFieldVal -1000.0;
          processedImage[i][j][sZDim-2] = minFieldVal -1000.0;
          processedImage[i][j][sZDim-3] = minFieldVal -1000.0;
        }
      }
    }
  }

  if (smoothsteps > 0){
    vector<vector<vector<double>>> nField = processedImage;
    cout << "Smoothing field: " << smoothsteps << " times" << endl;
    for (int s=0;s<smoothsteps;s++){
      smoothField(processedImage, nField, 0.1);
      processedImage = nField;
    }
  }

  TriangularMesh outPhaseMesh;
  marchingCubes(processedImage, isovalue, outPhaseMesh);

  if (outPhaseMesh.vertices.size() == 0 || outPhaseMesh.faces.size() == 0){
    cerr << "WARNING: No mesh extracted for phase `" << phaseName <<
            "` at isovalue: " << isovalue << ". Perhaps check that this " <<
            "isovalue gives a valid contour." << endl;
  }

  for (int v=0;v<outPhaseMesh.vertices.size();v++){
    outPhaseMesh.vertices[v].px *= xres;
    outPhaseMesh.vertices[v].py *= yres;
    outPhaseMesh.vertices[v].pz *= zres;
  }

  //reorient axes
  vector<Point> overtices = outPhaseMesh.vertices;
  for (int v=0;v<outPhaseMesh.vertices.size();v++){
    if (x == "x"){
      outPhaseMesh.vertices[v].px = overtices[v].px;
    }
    else if (x == "y"){
      outPhaseMesh.vertices[v].px = overtices[v].py;
    }
    else if (x == "z"){
      outPhaseMesh.vertices[v].px = overtices[v].pz;
    }
    else {
      cout << "unidentified axis label detected for defining extracted voxel " <<
              "field x-axis for phase `" << phaseName <<
              "`. assuming default ordering (x->x)" << endl;
      outPhaseMesh.vertices[v].px = overtices[v].px;
    }
    if (y == "x"){
      outPhaseMesh.vertices[v].py = overtices[v].px;
    }
    else if (y == "y"){
      outPhaseMesh.vertices[v].py = overtices[v].py;
    }
    else if (y == "z"){
      outPhaseMesh.vertices[v].py = overtices[v].pz;
    }
    else {
      cout << "unidentified axis label detected for defining extracted voxel " <<
              "field y-axis for phase `" << phaseName <<
              "`. assuming default ordering (y->y)" << endl;
      outPhaseMesh.vertices[v].py = overtices[v].py;
    }
    if (z == "x"){
      outPhaseMesh.vertices[v].pz = overtices[v].px;
    }
    else if (z == "y"){
      outPhaseMesh.vertices[v].pz = overtices[v].py;
    }
    else if (z == "z"){
      outPhaseMesh.vertices[v].pz = overtices[v].pz;
    }
    else {
      cout << "unidentified axis label detected for defining extracted voxel " <<
              "field z-axis for phase `" <<
              phaseName << "`. assuming default ordering (z->z)" << endl;
      outPhaseMesh.vertices[v].pz = overtices[v].pz;
    }
  }

  double maxVx = numeric_limits<double>::min();
  double maxVy = numeric_limits<double>::min();
  double maxVz = numeric_limits<double>::min();

  for (int v=0;v<outPhaseMesh.vertices.size();v++){
    if (maxVx < outPhaseMesh.vertices[v].px){
      maxVx = outPhaseMesh.vertices[v].px;
    }
    if (maxVy < outPhaseMesh.vertices[v].py){
      maxVy = outPhaseMesh.vertices[v].py;
    }
    if (maxVz < outPhaseMesh.vertices[v].pz){
      maxVz = outPhaseMesh.vertices[v].pz;
    }
  }

  //reorient axes
  for (int v=0;v<outPhaseMesh.vertices.size();v++){
    if (invx == "-"){
      outPhaseMesh.vertices[v].px = maxVx-outPhaseMesh.vertices[v].px;
    }
    if (invy == "-"){
      outPhaseMesh.vertices[v].py  =maxVy-outPhaseMesh.vertices[v].py;
    }
    if (invz == "-"){
      outPhaseMesh.vertices[v].pz = maxVz-outPhaseMesh.vertices[v].pz;
    }
  }

  //extract largest contour if attribute set
  if (largestContourOnly == true){
    cout << "\t Extracting largest contour" << endl;

    vector<TriangularMesh> dComponents;
    vector<int> incFaces(outPhaseMesh.faces.size(), 1);
    vector<vector<long>> faceFaceAdjacencyList = outPhaseMesh.buildFaceFaceAdjacencyList();

    //extract internal isocontours (voids and cavities)
    double mVol  = 0.0;
    int mVolInd = -1;
    int mSec = 0;
    vector<int> surfConnect;
    vector<double> vols;
    while(accumulate(incFaces.begin(), incFaces.end(), 0) != 0){
      long initNode = find(incFaces.begin(), incFaces.end(), 1) - incFaces.begin();
      vector<int> connected = DFSIterative(faceFaceAdjacencyList, initNode); //perform depth first search to extract disconnected mesh components
      TriangularMesh nMesh;
      nMesh.vertices = outPhaseMesh.vertices;
      for (int j=0;j<connected.size();j++){
        if (connected[j] == 1){
          nMesh.faces.push_back(outPhaseMesh.faces[j]);
          incFaces[j] = 0;
        }
      }
      nMesh.removeDuplicateVertices(); //remove duplicate vertices via accelerated hash map method
      double vol = nMesh.calculateVolume();
      vols.push_back(vol);
      if (vol > mVol){
        mVolInd = mSec;
        surfConnect = connected;
        mVol = vol;
      }
      dComponents.push_back(nMesh);
      mSec++;
    }
    outPhaseMesh = dComponents[mVolInd];
    cout << "\t Largest contour extraction complete." << endl;
  }

  //resize phase mesh according to passed mesh resolution
  Point BLP(0.0, 0.0, 0.0);
  cout << xres << " " << yres << " " << zres << endl;
  outPhaseMesh.rescaleMesh(BLP, xres, yres, zres);

  if (remeshRes > 0.0){
    cout << "Perform isotropic remeshing of extracted phase mesh for phase: `" << phaseName << "`." << endl;
    outPhaseMesh = performRemesh(outPhaseMesh, 2, remeshRes);
  }
  else {
    cout << "No remeshing of for phase: `" <<
            phaseName << "` performed (mesh written to config file as extracted from TIFF)." << endl;
  }

  //check whether extracted phase mesh is watertight
  int wt = outPhaseMesh.checkWatertight();
  if (wt == 1){
    cerr << "\tWARNING: Extracted phase mesh is not watertight for phase `" <<
            phaseName << "`. Perhaps check extracted phase mesh for artifacts " <<
            "(via setting phaseExtractor attribute `savemesh='true'`)." << endl;
  }

  if (savemesh != ""){
    cout << "\tsaving extracted phase mesh for phase `" <<
            phaseName << "` to: " << savemesh << endl;
    vtkObject out;
    out.setWithTriangularMesh(outPhaseMesh);
    writeVtk(out, savemesh);
    cout << "\twritten phase mesh successfully" << endl;
  }

  return outPhaseMesh;
}

/*!
 * Function for extracting phases according to instructions within the inputted model configuration file.
 * Writes to an output model configuration file explicitly containing the extracted phase meshes.
 * @param iConfigFile Location of the input configuration file.
 * @param oConfigFile Location of the input configuration file.
 * @return return code
 */
int runPhaseExtractor(string iConfigFile, string oConfigFile){

  //create directory tree for writing the output config file
  int dcRet = createDirectoryTree(oConfigFile);

  cout << "Reading input configuration file..." << endl;

  char *charcfname = new char[iConfigFile.length() + 1];
  strcpy(charcfname, iConfigFile.c_str());

  //load the xml file using the rapidxml library
  rapidxml::file<> xmlFile(charcfname);
  rapidxml::xml_document<> doc;
  try {
    doc.parse<rapidxml::parse_declaration_node | rapidxml::parse_no_data_nodes>(xmlFile.data());
  }
  catch (const rapidxml::parse_error& e){
        cerr << "\tERROR: XML parsing error prevented reading of passed file due to '" << e.what() << "'" << endl;
        cerr << "\tPlease ensure XML is in a valid format." << endl;
        exit(1);
    }

  rapidxml::xml_node<> *root;
  rapidxml::xml_node<> *specimenModelNode;
  rapidxml::xml_node<> *specimenPhasesNode;
  rapidxml::xml_node<> *specimenPhaseNode;
  rapidxml::xml_node<> *specimenPhaseMeshNode;

  root = doc.first_node("model");

  specimenModelNode = root->first_node("specimenModel");
  specimenPhasesNode = specimenModelNode->first_node("phases");

  //iterate through phases in configuration file
  int phaseCount = 0;

  TriangularMesh phaseMesh;

  for (specimenPhaseNode = specimenPhasesNode->first_node(); specimenPhaseNode != NULL; specimenPhaseNode = specimenPhaseNode->next_sibling())
  {
    string phaseName = specimenPhaseNode->name();
    if (specimenPhaseNode->first_node("mesh") != nullptr){
      specimenPhaseMeshNode = specimenPhaseNode->first_node("mesh");
      if (specimenPhaseMeshNode->first_node("phaseExtractor") != nullptr){
        rapidxml::xml_node<> *tBuildNode;
        for (tBuildNode = specimenPhaseMeshNode->first_node("phaseExtractor")->first_node(); tBuildNode != NULL; tBuildNode = tBuildNode->next_sibling()){
          cout << "phase extractor instruction found. Performing mesh construction via field extraction..." << endl;
          TriangularMesh tMesh = extractPhase(tBuildNode, doc, true, phaseName);
          //append extracted mesh to output phase mesh
          tMesh.faces = tMesh.faces + phaseMesh.vertices.size();
          phaseMesh.faces.insert(phaseMesh.faces.end(), tMesh.faces.begin(), tMesh.faces.end());
          phaseMesh.vertices.insert(phaseMesh.vertices.end(), tMesh.vertices.begin(), tMesh.vertices.end());
          cout << "successfully extracted valid mesh" << endl;
        }
        specimenPhaseMeshNode->remove_node(specimenPhaseMeshNode->first_node("phaseExtractor"));
      }
      else{
        cerr << "No input mesh or phase extractor command for '" << phaseName << "' detected." << endl;
        exit(1);
      }
    }
  }

  int wcRet = writeToConfig(&doc, specimenPhaseMeshNode, phaseMesh);

  cout << "writing to output model configuration file..." << endl;

  std::ofstream outFile;
  outFile.open(oConfigFile);

  outFile << doc;
  outFile.close();

  doc.clear();

  delete[] charcfname;

  return 0;
}
