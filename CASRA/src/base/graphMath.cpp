/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>

#include "CASRA/base/graphMath.h"

using namespace std;

/*!
 * Implementation of the Depth First Search (DFS) algorithm for traversing
 * graph trees. Used to determine disconnected mesh components.
 *
 * @param connectivityMatrix the graph adjacency list
 * @param initNode The initial node index to start the search
 * @return a vector of whether each node is connected (1) or not (0) to the starting node
 */
vector<int> DFSIterative(vector<vector<long>> adjacencyList, long initNode){

  long v;

  vector<long> stack;
  stack.push_back(initNode);

  vector<int> connected = vector<int>(adjacencyList.size(), 0);

  while (stack.size() != 0){
    v = stack.back();
    stack.pop_back();
    if (connected[v] == 0){
      connected[v] = 1;
      for (int i=0;i<adjacencyList[v].size();i++){
        stack.push_back(adjacencyList[v][i]);
      }
    }
  }

  return connected;

}
