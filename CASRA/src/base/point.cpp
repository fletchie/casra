/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CASRA/base/point.h"

using namespace std;

Vec operator+(Vec& a, Vec& b) {

  Vec nVec(a.vx + b.vx, a.vy + b.vy, a.vz + b.vz);
  return nVec;

}

Vec operator+(Vec&& a, Vec&& b) {

  Vec nVec(a.vx + b.vx, a.vy + b.vy, a.vz + b.vz);
  return nVec;

}

Vec operator+(Vec&& a, Vec& b) {

  Vec nVec(a.vx + b.vx, a.vy + b.vy, a.vz + b.vz);
  return nVec;

}

Vec operator+(Vec& a, Vec&& b) {

  Vec nVec(a.vx + b.vx, a.vy + b.vy, a.vz + b.vz);
  return nVec;

}

Vec operator*(Vec&& a, double mult) {

  Vec nVec(a.vx*mult, a.vy*mult, a.vz*mult);
  return nVec;
}

Vec operator*(Vec& a, double mult) {

  Vec nVec(a.vx*mult, a.vy*mult, a.vz*mult);
  return nVec;
}

/*!
 * Checks whether point is 'equal' to another point within a certain tolerance
 * @param point2 The second point which is checked
 * @param tol The tolerance of the check
 * return 0 if equal, 1 otherwise
 */
int Point::checkEquals(Point point2, double tol){
  if ((fabs(this->px - point2.px) < tol) && (fabs(this->py - point2.py) < tol) && (fabs(this->pz - point2.pz) < tol)) {
    return 0;
  }
  return 1;
}

Vec::Vec(double vx, double vy, double vz) : vx{vx}, vy{vy}, vz{vz}{

}

/*!
 * Function for extracting a Point instance from a passed string (of comma separated format)
 *
 * @param strPoint string defining the point in the following comma separated format `(x,y,z)`
 * @return The extracted point
 */
Point extractPoint(std::string strPoint){
  std::string processedStrPoint = strPoint.substr(1, strPoint.length() - 2);
  int c1 = processedStrPoint.find(",");
  std::string Px = processedStrPoint.substr(0, c1);
  processedStrPoint = processedStrPoint.substr(c1+1, processedStrPoint.length() - 1);
  int c2 = processedStrPoint.find(",");
  std::string Py = processedStrPoint.substr(0, c2);
  std::string Pz = processedStrPoint.substr(c2+1);
  double dPx, dPy, dPz;

  try {
     dPx = std::stod(Px);
     dPy = std::stod(Py);
     dPz = std::stod(Pz);
   }
   catch (const std::invalid_argument& ia) {
     std::cout << "point '" + strPoint + "' could not be read correctly. Please check format matches (x,y,z).";
     exit(1);
   }

   return Point(dPx, dPy, dPz);

}

/*!
 * Function for extracting a point from a passed string (of comma separated format)
 * and returning it as a char array
 *
 * @param strPoint string defining the point in the following comma separated format `(x,y,z)`
 * @return The extracted char array defining the point
 */
std::vector<std::string> extractPointChar(std::string strPoint){
  std::vector<std::string> point;
  std::string processedStrPoint = strPoint.substr(1, strPoint.length() - 2);
  int c1 = processedStrPoint.find(",");
  std::string Px = processedStrPoint.substr(0, c1);
  processedStrPoint = processedStrPoint.substr(c1+1, processedStrPoint.length() - 1);
  int c2 = processedStrPoint.find(",");
  std::string Py = processedStrPoint.substr(0, c2);
  std::string Pz = processedStrPoint.substr(c2+1);

  point.push_back(Px);
  point.push_back(Py);
  point.push_back(Pz);

  return point;

}
