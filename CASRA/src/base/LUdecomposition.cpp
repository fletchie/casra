/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <iostream>
#include <lapacke.h>

#include "CASRA/base/LUdecomposition.h"
#include "CASRA/base/vectorMath.h"

using namespace std;

/*! Wrapper function for calling LAPACK LU decomposition solver
 *
 * through c headers (LAPACKE)
 * @param mat The 2D problem matrix
 * @param b The RHS problem vector
 * @param x The solution vector
 * @return info The solver error code
 */
 int LUSolveLP(matrix& mat, vector<double>& b, vector<double>& x){

  int n = mat.size();
  int nrhs = 1;

  int lda = n; // lda >= n (col major)
  int ldb = n; // ldb >= n (col major)

  double *A = new double[ lda * n ]; // n x n
  double *B = new double[ ldb * nrhs ]; // n x nrhs
  int *ipiv = new int[ n ]; // pivot indices

  //populate matrices
  #if defined(_OPENMP)
  #pragma omp parallel for
  #endif
  for (int i=0;i<n;i++){
    for (int j=0;j<n;j++){
      A[j*n + i] = mat[i][j]; //column major ordering
    }
    B[i] = b[i];
  }

  //calling LAPACK library through LAPACKE wrapper
  int info = LAPACKE_dgesv( LAPACK_COL_MAJOR, n, nrhs, A, lda, ipiv, B, ldb );

  cout << info << endl;

  if (info != 0){
    return info;
  }

  //obtain solution vector
  for (int i=0;i<n;i++){
    x[i] = B[i];
  }

  delete[] A;
  delete[] B;
  delete[] ipiv;

  return info;

}


/*!
 * Manually implemented LU decomposition solver (in case LAPACK is unavailable)
 *
 * @param mat The 2D problem matrix
 * @param b The RHS problem vector
 * @param x The solution vector
 * @return info The solver error code
 */

int LUSolve(matrix& mat, vector<double>& b, vector<double>& x){

  matrix lower = initialiseMatrix(mat.size(), mat[0].size(), 0.0);
  matrix upper = initialiseMatrix(mat.size(), mat[0].size(), 0.0);

  LUdecomposition(mat, lower, upper);

  vector<double> y = forSub(lower, b);
  x = backSub(upper, y);

  return 0;

}

/*!
 * Function for performing LU factorisation of mat
 * Algorithm is nonparallelisable - therefore LAPACK is preferred.
 * @param mat The 2D input matrix to be factorised
 * @param lower The 2D output lower matrix
 * @param upper The 2D output upper matrix
 */
void LUdecomposition(matrix& mat, matrix& lower, matrix& upper) {

  // Decomposing matrix into Upper and Lower
  // triangular matrix
  int n = mat.size();

  for (int i = 0; i < n; i++) {
      // Upper Triangular
      for (int k = i; k < n; k++) {
          // Summation of L(i, j) * U(j, k)
          double sum = 0;

          for (int j = 0; j < i; j++) {
              sum += (lower[i][j] * upper[j][k]);
          }

          upper[i][k] = mat[i][k] - sum;
      }

      // Lower Triangular
      for (int k = i; k < n; k++)
      {
        lower[i][i] = 1.0; // Diagonal as 1
        if (i != k) {
          // Summation of L(k, j) * U(j, i)
          double sum = 0.0;

          for (int j = 0; j < i; j++) {
              sum += (lower[k][j] * upper[j][i]);
          }

          lower[k][i] = (mat[k][i] - sum) / upper[i][i];
      }
    }
  }
}

/*! Function for performing backward substitution during LU factorisation
 * solves y = U^-1 b if U is lower triangular
 * @param U The 2D input matrix
 * @param b The input RHS vector
 * @return The output vector
 */
vector<double> backSub(matrix& U, vector<double>& b){

    int n = b.size();
    vector<double> x = vector<double>(n, 0.0);

    for (int i=n-1;i>-1;--i) {
        double tmp = b[i];
        for (int j=i;j<n;j++){
          tmp -= U[i][j] * x[j];
        }

        x[i] = tmp/U[i][i];
    }

    return x;
  }

/*! Function for performing forward substitution during LU factorisation
 * solves y = U^-1 b if U is upper triangular
 * @param U The 2D input matrix
 * @param b The input RHS vector
 * @return The output vector
 */
vector<double> forSub(matrix& L, vector<double>& b){

    int n = b.size();
    vector<double> x = vector<double>(n, 0.0);

    for (int i=0;i<n;i++)
    {
        double tmp = b[i];
        for (int j=0;j<i;j++) {
            tmp -= L[i][j] * x[j];
        }

        x[i] = tmp/L[i][i];
    }

    return x;
  }
