/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "CASRA/base/RKIntegrator.h"

using namespace std;


/*!
 * One-dimensional Runge Kutta Integrator for using Dormand-Prince RK45.
 * @param f Function pointer providing gradient of first order ODE
 * @param t current time
 * @param yn Current y value
 * @param h previous stepsize estimate
 * @param relTol Tolerance parameter
 * @param kPrev The previous k7 calculated (to be k1 under DPRk45)
 * @return tuple of integrator outputs (tNew, ynp1, hNew, k7)
 */
tuple<double, double, double, double> rungeKuttaScalarIterate(double (*f)(double, double),
                                                              double t,
                                                              double yn,
                                                              double h,
                                                              double relTol,
                                                              double kPrev){


  double k1, k2, k3, k4, k5, k6, k7, ynp4, ynp5, hNew;

  //first same as last property holds as Bi1 match A7i
  //for Dormand Prince
  if (kPrev == numeric_limits<double>::min()){
    k1 = f(t, yn);
  }
  else {
    k1 = kPrev;
  }

  double err = numeric_limits<double>::max();
  double sc = 0.0;
  double tNew = t + h * C7;
  double tau;

  while (err > relTol) {
    k2 = f(t + h * C2, yn + h * (A21 * k1));
    k3 = f(t + h * C3, yn + h * (A31 * k1 + A32 * k2));
    k4 = f(t + h * C4, yn + h * (A41 * k1 + A42 * k2 + A43 * k3));
    k5 = f(t + h * C5, yn + h * (A51 * k1 + A52 * k2 + A53 * k3 + A54 * k4));
    k6 = f(t + h * C6, yn + h * (A61 * k1 + A62 * k2 + A63 * k3 + A64 * k4 + A65 * k5));

    ynp5 = yn + h * (A71 * k1 + A72 * k2 + A73 * k3 + A74 * k4 + A75 * k5 + A76 * k6);
    k7 = f(tNew, ynp5);
    ynp4 = yn + h * (B12 * k1 + B22 * k2 + B32 * k3 + B42 * k4 + B52 * k5 + B62 * k6 + B72 * k7);
    err = fabs(ynp4 - ynp5) + 1e-100;
    h = h * 0.5;
  }

  tau = FAC * h * 2.0 * pow(relTol/err, 0.2);
  h = min(max(tau, 1e-50), 1e50);

  tuple<double, double, double, double> res = make_tuple(tNew, ynp5, h, k7);

  return res;

}


/*!
 * Multidimensional Runge Kutta Integrator for using Dormand-Prince RK45.
 * @param f Function pointer providing gradient of first order ODE
 * @param t current time
 * @param yn Current y value
 * @param h previous stepsize estimate
 * @param relTol Tolerance parameter
 * @param kPrev The previous k7 calculated (to be k1 under DPRk45)
 * @return tuple of integrator outputs (tNew, ynp1, hNew, k7)
 */
tuple<double, vector<double>, double, vector<double>> rungeKuttaVectorIterate(vector<double> (*f)(double, vector<double>),
                                                                              double t,
                                                                              vector<double> yn,
                                                                              double h,
                                                                              double relTol,
                                                                              vector<double> kPrev){

  vector<double> k1;
  vector<double> k2;
  vector<double> k3;
  vector<double> k4;
  vector<double> k5;
  vector<double> k6;
  vector<double> k7;
  vector<double> ynp4;
  vector<double> ynp5;
  double hNew;

  //first same as last property holds as Bi1 match A7i
  //for Dormand Prince
  if (kPrev.size() == 0){
    k1 = f(t, yn);
  }
  else {
    k1 = kPrev;
  }

  double err = numeric_limits<double>::max();
  double sc = 0.0;
  double tNew = t + h * C7;
  double tau;
  int it = 0;
  while (err > relTol) {
    k2 = f(t + h * C2, yn + h * (A21 * k1));
    k3 = f(t + h * C3, yn + h * (A31 * k1 + A32 * k2));
    k4 = f(t + h * C4, yn + h * (A41 * k1 + A42 * k2 + A43 * k3));
    k5 = f(t + h * C5, yn + h * (A51 * k1 + A52 * k2 + A53 * k3 + A54 * k4));
    k6 = f(t + h * C6, yn + h * (A61 * k1 + A62 * k2 + A63 * k3 + A64 * k4 + A65 * k5));

    ynp5 = yn + h * (A71 * k1 + A72 * k2 + A73 * k3 + A74 * k4 + A75 * k5 + A76 * k6);
    k7 = f(tNew, ynp5);
    ynp4 = yn + h * (B12 * k1 + B22 * k2 + B32 * k3 + B42 * k4 + B52 * k5 + B62 * k6 + B72 * k7);
    err = vecMagnitude(ynp5, ynp4) + 1e-100;

    h = h * 0.5;
    it ++;
  }

  tau = FAC * h * 2.0 * pow(relTol/err, 0.2);
  h = min(max(tau, 1e-50), 1e50);

  tuple<double, vector<double>, double, vector<double>> res = make_tuple(tNew, ynp5, h, k7);

  return res;

}
