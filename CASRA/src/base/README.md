# The base maths library

Contains the source files required to build the base maths library. Defines vector math, graph algorithms, direct solvers, and Runge-Kutta integrators (currently only Dormand-Prince Rk54). 
