/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <math.h>
#include <iostream>

#include "CASRA/base/vectorMath.h"

using namespace std;

/// @private
vector<double> operator-(vector<double> a) {
  vector<double> nvec;
  for (int i=0;i<a.size();i++){
      nvec.push_back(-a[i]);
  }
  return nvec;
}

/// @private
vector<double> operator+(vector<double> a, vector<double> b) {
  vector<double> nvec;
  for (int i=0;i<a.size();i++){
      nvec.push_back(b[i] + a[i]);
  }
  return nvec;
}
/// @private
vector<double> operator+(vector<double> a, double b) {
  vector<double> nvec;
  for (int i=0;i<a.size();i++){
      nvec.push_back(b + a[i]);
  }
  return nvec;
}
/// @private
vector<double> operator+(double b, vector<double> a) {
  vector<double> nvec;
  for (int i=0;i<a.size();i++){
      nvec.push_back(b + a[i]);
  }
  return nvec;
}
/// @private
vector<double> operator*(double b, vector<double> a) {
  vector<double> nvec;
  for (int i=0;i<a.size();i++){
      nvec.push_back(b * a[i]);
  }
  return nvec;
}
/// @private
vector<double> operator*(vector<double> a, double b) {
  vector<double> nvec;
  for (int i=0;i<a.size();i++){
      nvec.push_back(b * a[i]);
  }
  return nvec;
}
/// @private
vector<double> operator/(vector<double>& a, vector<double>& b) {

  try{
    if (a.size() != b.size()){
      throw 1;
    }
  }
  catch (int e){
    cerr << "Differing vector dimensions. Throwing exception." << endl;
    exit(e);
  }

  vector<double> divVec;

  for (int i=0;i<a.size();i++){
    divVec.push_back(a[i]/b[i]);
  }

  return divVec;

}
/// @private
double minimum(vector<double> vec){
  double minimum = numeric_limits<double>::max();
  for (int i=0;i<vec.size();i++){
    if (vec[i] < minimum){
      minimum = vec[i];
    }
  }
  return minimum;
}
/// @private
double vecMagnitude(vector<double>& a, vector<double>& b){

  double mag = 0.0;
  for (int i=0;i<a.size();i++){
    mag += (a[i] - b[i]) * (a[i] - b[i]);
  }

  return sqrt(mag);
}
/// @private
vector<double> abs(vector<double> vec){
  vector<double> absVec;
  for (int i=0;i<vec.size();i++){
    absVec.push_back(fabs(vec[i]));
  }
  return absVec;
}
/// @private
double maximum(vector<double> vec){
  double maximum = numeric_limits<double>::min();
  for (int i=0;i<vec.size();i++){
    if (vec[i] > maximum){
      maximum = vec[i];
    }
  }
  return maximum;
}
/// @private
double minimum(vector<vector<double>> vec, int dim){
  double minimum = numeric_limits<double>::max();
  for (int i=0;i<vec.size();i++){
    if (vec[i][dim] < minimum){
      minimum = vec[i][dim];
    }
  }
  return minimum;
}
/// @private
double maximum(vector<vector<double>> vec, int dim){
  double maximum = numeric_limits<double>::min();
  for (int i=0;i<vec.size();i++){
    if (vec[i][dim] > maximum) {
      maximum = vec[i][dim];
    }
  }
  return maximum;
}
