/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>
#include <iterator>

#include "CASRA/mesh/pointData.h"
#include "CASRA/base/vectorMath.h"

using namespace std;

#define SMALL 1.0e-20

/*!
 * PointDataSet Method for assigning VTK pointData attributes
 *
 * @param pointData The 1D vector point data attributes
 * @param key The key name
 * @return void
 */
void PointDataset::assignPointData(vector<double> pointData, string key){

  try {
    if (this->points.size() != pointData.size()){
      throw 1;
    }
  }
  catch (int n) {
      cerr << "ERROR: number of points differs to number of point data entries." << endl;
      exit(1);
  }

  for (int i=0;i<this->points.size();i++){
    this->points[i].pointData[key] = vector<double>(1, pointData[i]);
  }

  return;
}
