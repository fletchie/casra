/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/testMeshes.h"

/*!
 * Function for creating ean example test mesh for the level set library tests
 * Creates a unit square spanning (xmin->xmax, ymin->ymax, zmin->zmax).
 * @param xmin minimum x coordinate for rectangle
 * @param xmin maximum x coordinate for rectangle
 * @param ymin minimum y coordinate for rectangle
 * @param ymax maximum y coordinate for rectangle
 * @param zmin minimum z coordinate for rectangle
 * @param zmin maximum z coordinate for rectangle
 */
TriangularMesh buildRectangularMesh(double xmin, double xmax,
                                    double ymin, double ymax,
                                    double zmin, double zmax){

  //create empty test rectangle mesh
  TriangularMesh closedTestMesh;

  //create points
  Point P0(xmin, ymin, zmin);
  Point P1(xmax, ymin, zmin);
  Point P2(xmin, ymax, zmin);
  Point P3(xmax, ymax, zmin);
  Point P4(xmin, ymin, zmax);
  Point P5(xmax, ymin, zmax);
  Point P6(xmin, ymax, zmax);
  Point P7(xmax, ymax, zmax);

  //create faces - must be defined consistently anticlockwise
  Face F0(0, 1, 3);
  Face F1(0, 3, 2);

  Face F2(5, 4, 7);
  Face F3(7, 4, 6);

  Face F4(0, 4, 5);
  Face F5(5, 1, 0);

  Face F6(2, 7, 6);
  Face F7(2, 3, 7);

  Face F8(0, 6, 4);
  Face F9(0, 2, 6);

  Face F10(1, 5, 7);
  Face F11(1, 7, 3);

  //add mesh vertices
  closedTestMesh.vertices.push_back(P0);
  closedTestMesh.vertices.push_back(P1);
  closedTestMesh.vertices.push_back(P2);
  closedTestMesh.vertices.push_back(P3);
  closedTestMesh.vertices.push_back(P4);
  closedTestMesh.vertices.push_back(P5);
  closedTestMesh.vertices.push_back(P6);
  closedTestMesh.vertices.push_back(P7);

  //add mesh faces
  closedTestMesh.faces.push_back(F0);
  closedTestMesh.faces.push_back(F1);
  closedTestMesh.faces.push_back(F2);
  closedTestMesh.faces.push_back(F3);
  closedTestMesh.faces.push_back(F4);
  closedTestMesh.faces.push_back(F5);
  closedTestMesh.faces.push_back(F6);
  closedTestMesh.faces.push_back(F7);
  closedTestMesh.faces.push_back(F8);
  closedTestMesh.faces.push_back(F9);
  closedTestMesh.faces.push_back(F10);
  closedTestMesh.faces.push_back(F11);

  return closedTestMesh;

}
