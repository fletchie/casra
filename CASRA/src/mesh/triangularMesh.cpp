/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <set>
#include <math.h>
#include <iterator>
#include <numeric>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/base/vectorMath.h"

using namespace std;

#define SMALL 1.0e-20
#define DUPLICATEVERTEXTOL 30

//distance fraction after which to apply panel central node distance approximation
#define PANELDISTTOL 5.0

typedef pair<int, int> intpair;

Face::Face(long p1, long p2, long p3) : p1{p1}, p2{p2}, p3{p3}{

}

Face::Face() {

}
vector<Face> operator+(vector<Face>& a, long b) {
  vector<Face> outFaceVec;
  for (int i=0;i<a.size();i++){
    outFaceVec.push_back(Face(a[i].p1 + b, a[i].p2 + b, a[i].p3 + b));
  }
  return outFaceVec;
}


/*!
 * Method for calculating triangular Face areas and setting the attribute
 *
 * @param vertices Reference to mesh vertices
 * @return void
 */
void Face::setArea(vector<Point>& vertices){

  Point ya = vertices[p1];
  Point yb = vertices[p2];
  Point yc = vertices[p3];

  this->area = 0.5 * sqrt(pow((yb.py - ya.py) * (yc.pz - ya.pz) - (yb.pz - ya.pz) * (yc.py - ya.py), 2.0) +
                          pow((yb.pz - ya.pz) * (yc.px - ya.px) - (yb.px - ya.px) * (yc.pz - ya.pz), 2.0) +
                          pow((yb.px - ya.px) * (yc.py - ya.py) - (yb.py - ya.py) * (yc.px - ya.px), 2.0));

  return;
}

/*!
 * Method for calculating the longest side squared of a triangular Face and setting the attribute
 *
 * @param vertices Reference to mesh vertices
 * @return void
 */
void Face::setLongestSideSqrd(vector<Point>& vertices){

  Point& ya = vertices[this->p1];
  Point& yb = vertices[this->p2];
  Point& yc = vertices[this->p3];

  //calculate longest side of mesh panels
  double svx = yb.px - ya.px;
  double svy = yb.py - ya.py;
  double svz = yb.pz - ya.pz;
  double s1 = svx*svx + svy*svy + svz*svz;

  svx = yc.px - yb.px;
  svy = yc.py - yb.py;
  svz = yc.pz - yb.pz;
  double s2 = svx*svx + svy*svy + svz*svz;

  if (s2 > s1){
    s1 = s2;
  }

  svx = yc.px - ya.px;
  svy = yc.py - ya.py;
  svz = yc.pz - ya.pz;
  double s3 = svx*svx + svy*svy + svz*svz;

  if (s3 > s1){
    s1 = s3;
  }

  this->ls2 = s1;

  return;

}

/*!
 * Method for calculating the longest side of a triangular Face and setting the attribute
 *
 * @param vertices Reference to mesh vertices
 * @return void
 */
void Face::setLongestSide(vector<Point>& vertices){

  Point& ya = vertices[this->p1];
  Point& yb = vertices[this->p2];
  Point& yc = vertices[this->p3];

  //calculate longest side of mesh panels
  double svx = yb.px - ya.px;
  double svy = yb.py - ya.py;
  double svz = yb.pz - ya.pz;
  double s1 = svx*svx + svy*svy + svz*svz;

  svx = yc.px - yb.px;
  svy = yc.py - yb.py;
  svz = yc.pz - yb.pz;
  double s2 = svx*svx + svy*svy + svz*svz;

  if (s2 > s1){
    s1 = s2;
  }

  svx = yc.px - ya.px;
  svy = yc.py - ya.py;
  svz = yc.pz - ya.pz;
  double s3 = svx*svx + svy*svy + svz*svz;

  if (s3 > s1){
    s1 = s3;
  }

  this->ls = std::sqrt(s1);

  return;

}

/*!
 * Method for calculating and setting the centre of a triangular face
 *
 * @param vertices Reference to mesh vertices
 * @return void
 */
void Face::setCentre(vector<Point>& vertices){
  Point centre = (vertices[this->p1] + vertices[this->p2] + vertices[this->p3])/3.0;
  this->centre = centre;

  return;
}

/*!
 * Method for calculating and setting the normal vector of a triangular face
 *
 * @param vertices Reference to mesh vertices
 * @return void
 */
void Face::setNormal(vector<Point>& vertices){
  Point A = vertices[this->p2] - vertices[this->p1];
  Point B = vertices[this->p3] - vertices[this->p1];
  Vec fNormal(A.py * B.pz - A.pz * B.py, A.pz * B.px - A.px * B.pz, A.px * B.py - A.py * B.px);
  this->normal = fNormal;
  double nm = sqrt(fNormal.vx * fNormal.vx + fNormal.vy * fNormal.vy + fNormal.vz * fNormal.vz);

  this->normal.vx = -this->normal.vx/nm;
  this->normal.vy = -this->normal.vy/nm;
  this->normal.vz = -this->normal.vz/nm;

  return;
}

/*!
 * Method for calculating and setting the bounding box for a mesh face
 *
 * @param vertices Reference to mesh vertices
 * @return void
 */
void TriangularMesh::setBoundingBox(){

  minCoords[0] = pointDimMinimum(this->vertices, 0);
  minCoords[1] = pointDimMinimum(this->vertices, 1);
  minCoords[2] = pointDimMinimum(this->vertices, 2);

  maxCoords[0] = pointDimMaximum(this->vertices, 0);
  maxCoords[1] = pointDimMaximum(this->vertices, 1);
  maxCoords[2] = pointDimMaximum(this->vertices, 2);

  return;
}

/*!
 * TriangularMesh Method for setting mesh properties
 *
 * @return void
 */
void TriangularMesh::setMeshProperties(){
  setCentres();
  setNormals();
  setLongestSideSqrd();
  setLongestSides();
  setAreas();
  averageVertexNormals();

  return;
}

/*!
 * TriangularMesh Method for setting mesh panel areas
 *
 * @return void
 */
void TriangularMesh::setAreas(){
  const int facesSize = faces.size();
  for (int i=0;i<facesSize;i++){
    this->faces[i].setArea(this->vertices);
  }
  return;
}

/*!
 * TriangularMesh Method for setting mesh panel centres
 *
 * @return void
 */
void TriangularMesh::setCentres(){
  const int facesSize = faces.size();
  for (int i=0;i<facesSize;i++){
    this->faces[i].setCentre(this->vertices);
  }
  return;
}

/*!
 * TriangularMesh Method for setting mesh panel longest square sides
 *
 * @return void
 */
void TriangularMesh::setLongestSideSqrd(){
  const int facesSize = faces.size();
  for (int i=0;i<facesSize;i++){
    this->faces[i].setLongestSideSqrd(this->vertices);
  }
  return;
}

/*!
 * TriangularMesh Method for setting mesh panel longest sides
 *
 * @return void
 */
void TriangularMesh::setLongestSides(){
  const int facesSize = faces.size();
  for (int i=0;i<facesSize;i++){
    this->faces[i].setLongestSide(this->vertices);
  }
  return;
}

/*!
 * TriangularMesh Method for setting mesh panel normal vectors
 *
 * @return void
 */
void TriangularMesh::setNormals(){
  const int facesSize = faces.size();
  for (int i=0;i<facesSize;i++){
    this->faces[i].setNormal(this->vertices);
  }
  return;
}

/*!
 * Function for calculating the mean value of a set of points
 *
 * @param vec The set of passed points
 * @return The mean point
 */
Point pointMean(vector<Point>& vec){

  double px = 0.0;
  double py = 0.0;
  double pz = 0.0;

  double vecSize = vec.size();

  Point avPoint(0.0, 0.0, 0.0);
  for (int i=0;i<vecSize;i++){
    avPoint.px += vec[i].px;
    avPoint.py += vec[i].py;
    avPoint.pz += vec[i].pz;
  }

  avPoint.px /= vecSize;
  avPoint.py /= vecSize;
  avPoint.pz /= vecSize;

  return avPoint;

}

/*!
 * Function for calculating the mean value of a set of pointers to points
 *
 * @param vec The set of passed points (as points)
 * @return The mean point
 */
Point pointMean(vector<Point*>& vec){

  double px = 0.0;
  double py = 0.0;
  double pz = 0.0;

  double vecSize = vec.size();

  Point avPoint(0.0, 0.0, 0.0);
  for (int i=0;i<vecSize;i++){
    avPoint.px += vec[i]->px;
    avPoint.py += vec[i]->py;
    avPoint.pz += vec[i]->pz;
  }

  avPoint.px /= vecSize;
  avPoint.py /= vecSize;
  avPoint.pz /= vecSize;

  return avPoint;

}

/*!
 * Function for calculating the mean value of a set of points along a particular dimension
 *
 * @param vec The set of passed points
 * @param int The dimension along which the mean is to be calculated
 * @return The mean point coordinate
 */
double pointDimMean(vector<Point>& vec, int dim){
  double meanVal = 0.0;

  const int vecSize = vec.size();

  if (dim == 0){
    for (int i=0;i< vecSize;i++){
      meanVal += vec[i].px;
    }
  }
  else if (dim == 1){
    for (int i=0;i< vecSize;i++){
      meanVal += vec[i].py;

    }
  }
  else if (dim == 2){
    for (int i=0;i< vecSize;i++){
      meanVal += vec[i].pz;

    }
  }

  return meanVal/(double)vecSize;
}

/*!
 * Function for calculating the minimum value of a set of points along a particular dimension
 *
 * @param vec The set of passed points
 * @param int The dimension along which the minimum is to be calculated
 * @return The minimum point coordinate
 */
double pointDimMinimum(vector<Point> vec, int dim){
  double minimum = numeric_limits<double>::max();

  if (dim == 0){
    for (int i=0;i<vec.size();i++){
      if (vec[i].px < minimum) {
        minimum = vec[i].px;
      }
    }
  }
  else if (dim == 1){
    for (int i=0;i<vec.size();i++){
      if (vec[i].py < minimum) {
        minimum = vec[i].py;
      }
    }
  }
  else if (dim == 2){
    for (int i=0;i<vec.size();i++){
      if (vec[i].pz < minimum) {
        minimum = vec[i].pz;
      }
    }
  }

  return minimum;
}

/*!
 * Function for calculating the maximum value of a set of points along a particular dimension
 *
 * @param vec The set of passed points
 * @param int The dimension along which the maximum is to be calculated
 * @return The maximum point coordinate
 */
double pointDimMaximum(vector<Point> vec, int dim){

  double maximum = numeric_limits<double>::min();

  if (dim == 0){
    for (int i=0;i<vec.size();i++){
      if (vec[i].px > maximum) {
        maximum = vec[i].px;
      }
    }
  }
  else if (dim == 1){
    for (int i=0;i<vec.size();i++){
      if (vec[i].py > maximum) {
        maximum = vec[i].py;
      }
    }
  }
  else if (dim == 2){
    for (int i=0;i<vec.size();i++){
      if (vec[i].pz > maximum) {
        maximum = vec[i].pz;
      }
    }
  }

  return maximum;
}

/*!
 * TriangularMesh Method for assigning VTK cellData attributes to mesh panels
 *
 * @param cellData The 1D vector cell data (i.e. panel) attributes
 * @param key The key name
 * @return void
 */
void TriangularMesh::assignCellData(vector<double> cellData, string key){

  try {
    if (this->faces.size() != cellData.size()){
      throw 1;
    }
  }
  catch (int n) {
      cerr << "number of faces differs to number of evaporation rate nodes. Throwing exception." << endl;
      exit(1);
  }

  for (int i=0;i<this->faces.size();i++){
    this->faces[i].cellData[key] = vector<double>(1, cellData[i]);
  }

  return;
}

void Point::printPoint(){
  cout << this->px << " " << this->py << " " << this->pz << endl;
}

/*!Function for clearing a triangular mesh*/
void TriangularMesh::clear(){
  this->vertices.clear();
  this->faces.clear();
  this->vol = 0.0;
}

/*!
 * Count number of unique edges within triangular mesh
 *
 * @return number of edges, -1 if edge between same vertex exists.
 */
int TriangularMesh::edgeNum(){

  set<pair<int, int>>edgeList;

  for (int a=0;a<this->faces.size();a++){
    int p1 = this->faces[a].p1;
    int p2 = this->faces[a].p2;
    int p3 = this->faces[a].p3;

    if (p1 < p2) {
      edgeList.insert(make_pair(p1, p2));
    }
    else if (p2 < p1) {
      edgeList.insert(make_pair(p2, p1));
    }
    else{
      return -1;
    }

    if (p1 < p3) {
      edgeList.insert(make_pair(p1, p3));
    }
    else if (p3 < p1) {
      edgeList.insert(make_pair(p3, p1));
    }
    else{
      return -1;
    }

    if (p2 < p3) {
      edgeList.insert(make_pair(p2, p3));
    }
    else if (p3 < p2) {
      edgeList.insert(make_pair(p3, p2));
    }
    else{
      return -1;
    }
  }

  return edgeList.size();
}

/*!
 * Function for checking whether a mesh is watertight
 * Checks to see if each edge has exactly two neighbouring faces
 *
 * @return 0 if watertight, 1 otherwise
 */
int TriangularMesh::checkWatertight(){

  map<pair<int, int>, int>edgeList;


  //initialise edge map edges to zero
  for (int a=0;a<this->faces.size();a++){
    int p1 = this->faces[a].p1;
    int p2 = this->faces[a].p2;
    int p3 = this->faces[a].p3;

    if (p1 < p2) {
      edgeList[make_pair(p1, p2)] = 0;
    }
    else if (p2 < p1) {
      edgeList[make_pair(p2, p1)] = 0;
    }

    if (p1 < p3) {
      edgeList[make_pair(p1, p3)] = 0;
    }
    else if (p3 < p1) {
      edgeList[make_pair(p3, p1)] = 0;
    }

    if (p2 < p3) {
      edgeList[make_pair(p2, p3)] = 0;
    }
    else if (p3 < p2) {
      edgeList[make_pair(p3, p2)] = 0;
    }
  }

  //opulate edge map counts
  for (int a=0;a<this->faces.size();a++){
    int p1 = this->faces[a].p1;
    int p2 = this->faces[a].p2;
    int p3 = this->faces[a].p3;

    if (p1 < p2) {
      edgeList[make_pair(p1, p2)] += 1;
    }
    else if (p2 < p1) {
      edgeList[make_pair(p2, p1)] += 1;
    }

    if (p1 < p3) {
      edgeList[make_pair(p1, p3)] += 1;
    }
    else if (p3 < p1) {
      edgeList[make_pair(p3, p1)] += 1;
    }

    if (p2 < p3) {
      edgeList[make_pair(p2, p3)] += 1;
    }
    else if (p3 < p2) {
      edgeList[make_pair(p3, p2)] += 1;
    }
  }

  for (map<pair<int, int>, int>::iterator it = edgeList.begin(); it != edgeList.end(); it++){
    if (it->second == 1){
      return 1;
    }
  }

  return 0;
}

vector<double> averageField(vector<Face>& faces, vector<double>& field){

    /*
     * Function for smoothing the average surface flux via a median filter
     * This loss of accuracy compensates for the relatively poor conditioning of the collocation BEM
     *
     * @param1 faces 2D vector for the mesh connectivity matrix
     * @param2 area Array of panel areas
     * @param3 field The surface electric field panel fluxes
     * @returns av_field The median filtered panel fluxes
     */

    const long lf = faces.size();
    vector<double> av_field(lf);

    //perform median filtering
    #pragma omp parallel
    {
    long size = 0;
    int size2 = 0;
    double med_f = 0.0;
    vector<double> double_vec(25);
    int sit = 0;

    #pragma omp for schedule(dynamic)
    for (long i = 0; i < lf; i++) {
        Face& facei = faces[i];
        //find neighbouring panels
        sit = 0;
        for (long j = 0; j < lf; j++) {
            Face& facej = faces[j];
            if ((facei.p1 == facej.p1) || (facei.p1 == facej.p2) ||
                (facei.p1 == facej.p3) || (facei.p2 == facej.p1) ||
                (facei.p2 == facej.p2) || (facei.p2 == facej.p3) ||
                (facei.p3 == facej.p1) || (facei.p3 == facej.p2) ||
                (facei.p3 == facej.p3)) {
                double_vec[sit] = field[j];
                sit++;
            }
        }

        size = sit;
        size2 = (int)size/2;

        //sort neighbouring panel fluxes
        sort(double_vec.begin(), double_vec.begin() + sit);

        //select middle (median) flux value from neighbours list
        med_f = double_vec[size2];

        if ((size % 2) == 0) {
            med_f = (double_vec[size2] + double_vec[size2 - 1]) / 2.0;
        }

        av_field[i] = med_f;
    }
    }

    return av_field;

  }


/*!
* Check euler-characteristic to determine whether 2-manifold
* @return 0 if 2-manifold, 1 if not.
*/
int TriangularMesh::checkManifold(){

  long vNum = this->vertices.size();
  long fNum = this->faces.size();
  long eNum = this->edgeNum();

  //check Euler-Poincare characteristic for two manifold
  long eulChar = vNum + fNum - eNum;

  //cout << vNum << " " << fNum << " " << eNum << " " << eulChar << endl;

  if (eulChar == 2) {
    return 0;
  }
  else {
    return 1;
  }
}

/*!
 * TriangularMesh method for translating and isotropically rescaling the mesh vertices
 *
 * @param bottomLeftDomainCorner The translation vector
 * @param cellWidth The isotropic rescaling parameter
 * @return void
 */
void TriangularMesh::rescaleMesh(Point bottomLeftDomainCorner, double cellWidth){

  const int verticesSize = this->vertices.size();
  for (int a=0;a<verticesSize;a++){
    this->vertices[a].px = bottomLeftDomainCorner.px + this->vertices[a].px * cellWidth;
    this->vertices[a].py = bottomLeftDomainCorner.py + this->vertices[a].py * cellWidth;
    this->vertices[a].pz = bottomLeftDomainCorner.pz + this->vertices[a].pz * cellWidth;
  }
  return;
}


/*!
 * TriangularMesh method for translating and anisotropically rescaling the mesh vertices
 *
 * @param bottomLeftDomainCorner The translation vector
 * @param cellWidthX The rescaling parameter in the x-direction
 * @param cellWidthY The rescaling parameter in the y-direction
 * @param cellWidthZ The rescaling parameter in the z-direction
 * @return void
 */
void TriangularMesh::rescaleMesh(Point bottomLeftDomainCorner, double cellWidthX, double cellWidthY, double cellWidthZ){

  const int verticesSize = this->vertices.size();
  for (int a=0;a<verticesSize;a++){
    this->vertices[a].px = bottomLeftDomainCorner.px + this->vertices[a].px * cellWidthX;
    this->vertices[a].py = bottomLeftDomainCorner.py + this->vertices[a].py * cellWidthY;
    this->vertices[a].pz = bottomLeftDomainCorner.pz + this->vertices[a].pz * cellWidthZ;
  }
  return;
}

/*!
 * Calculate mesh volume (assuming mesh is closed)
 *
 * @return total mesh volume;
 */
double TriangularMesh::calculateVolume(){

  double vol = 0.0;
  const int ndim = 3;

  for (int a=0;a<this->faces.size();a++){
    int p1 = this->faces[a].p1;
    int p2 = this->faces[a].p2;
    int p3 = this->faces[a].p3;

    Point v1 = this->vertices[p1];
    Point v2 = this->vertices[p2];
    Point v3 = this->vertices[p3];

    double vArray[ndim][ndim] = {{v1.px, v1.py, v1.pz}, {v2.px, v2.py, v2.pz}, {v3.px, v3.py, v3.pz}};
    vol += 1.0/6.0 * calculateDeterminant<3>(vArray);
  }
  return fabs(vol);
}

/*!
 * TriangularMesh Method for calculating shortest distance between the panel
 * centres within a surface patch, and another mesh (passed as parameter 1)
 *
 * @param prevSurface The surface mesh from which panel centre distances are being calculated from
 * @param surfacePatch The subset of surface panel centres to consider
 * @return The list of panel evaporation volumes (same length as surfacePatch)
 */
vector<double> TriangularMesh::distanceFromSurface(TriangularMesh& prevSurface, vector<long>& surfacePatch){

  vector<double> evapVols;
  this->setCentres();

  for (int i=0;i<surfacePatch.size();i++){
    Point& testPoint = vertices[surfacePatch[i]];
    double dist = prevSurface.shortestDistanceToPoint(testPoint);
    evapVols.push_back(dist);
  }

  return evapVols;
}

/*!
 * Method for calculating shortest distance between the panel centres within a
 * surface patch, and another mesh (passed as parameter 1)
 *
 * @param prevSurface The surface mesh from which panel centre distances are being calculated from
 * @param surfacePatch The subset of surface panel centres to consider
 * @return The list of panel evaporation volumes (same length as surfacePatch)
 */
vector<double> TriangularMesh::distanceFromSurfaceFace(TriangularMesh& prevSurface, vector<long>& surfacePatch){

  vector<double> evapVols;
  this->setCentres();

  for (int i=0;i<surfacePatch.size();i++){
    Point& testPoint = faces[surfacePatch[i]].centre;
    double dist = prevSurface.shortestDistanceToPoint(testPoint);
    evapVols.push_back(dist);
  }

  return evapVols;
}

/*!
 * TriangularMesh Method for removing duplicate vertices (within a defined tolerance)
 * from a mesh via leveraging an ordered map
 *
 * @return void
 */
void TriangularMesh::removeDuplicateVertices(){

  long idx = 0;

  map<vector<double>, long> vert_index;

  vector<long> templist;
  vector<Point> verticesr;

  vector<Point> oVertices;
  vector<Face> oFaces;

  TriangularMesh outMesh;

  // Iterate over triangles
  for (long i=0;i<this->faces.size();i++){
    templist.clear();
    // Iterate over vertices within each triangle
    Point p1 = vertices[this->faces[i].p1];
    Point p2 = vertices[this->faces[i].p2];
    Point p3 = vertices[this->faces[i].p3];

    //check point 1
    vector<double> vt = {round_up(p1.px, DUPLICATEVERTEXTOL),
                         round_up(p1.py, DUPLICATEVERTEXTOL),
                         round_up(p1.pz, DUPLICATEVERTEXTOL)};
    // Check if a new unique vertex found
    if (vert_index.find(vt) == vert_index.end()) {
      vert_index[vt] = idx;
      templist.push_back(idx);
      oVertices.push_back(p1);
      idx += 1;
    }
    else{
      templist.push_back(vert_index[vt]);
    }
    //check point 2
    vt = {round_up(p2.px, DUPLICATEVERTEXTOL),
          round_up(p2.py, DUPLICATEVERTEXTOL),
          round_up(p2.pz, DUPLICATEVERTEXTOL)};
    // Check if a new unique vertex found
    if (vert_index.find(vt) == vert_index.end()) {
      vert_index[vt] = idx;
      templist.push_back(idx);
      oVertices.push_back(p2);
      idx += 1;
    }
    else{
      templist.push_back(vert_index[vt]);
    }

    //check point 3
    vt = {round_up(p3.px, DUPLICATEVERTEXTOL),
          round_up(p3.py, DUPLICATEVERTEXTOL),
          round_up(p3.pz, DUPLICATEVERTEXTOL)};

    // Check if a new unique vertex found
    if (vert_index.find(vt) == vert_index.end()) {
      vert_index[vt] = idx;
      templist.push_back(idx);
      oVertices.push_back(p3);
      idx += 1;
    }
    else{
      templist.push_back(vert_index[vt]);
    }

    Face nface(templist[0], templist[1], templist[2]);
    oFaces.push_back(nface);

  }

  this->vertices = oVertices;
  this->faces = oFaces;

}

/*!
 * Method for calculating the mesh vertex (neighbouring vertices for a vertex)
 * adjacency list
 *
 * @return Adjacency list (length equals number of faces)
 */
vector<vector<long>> TriangularMesh::buildVertexVertexAdjacencyList(){

  vector<set<long>> adjacencyListvv(this->vertices.size(), set<long>());
  vector<vector<long>> adjacencyListvvOut(this->vertices.size(), vector<long>());

  for (int f=0;f<this->faces.size();f++){
    adjacencyListvv[this->faces[f].p1].insert(this->faces[f].p2);
    adjacencyListvv[this->faces[f].p1].insert(this->faces[f].p3);
    adjacencyListvv[this->faces[f].p2].insert(this->faces[f].p1);
    adjacencyListvv[this->faces[f].p2].insert(this->faces[f].p3);
    adjacencyListvv[this->faces[f].p3].insert(this->faces[f].p1);
    adjacencyListvv[this->faces[f].p3].insert(this->faces[f].p2);
  }

  for (int f=0;f<this->faces.size();f++){
    adjacencyListvvOut.push_back(vector<long>());
    adjacencyListvvOut[f].assign(adjacencyListvv[f].begin(), adjacencyListvv[f].end());
  }

  return adjacencyListvvOut;
}

/*!
 * Method for calculating the mesh vertex-faces (neighbouring faces for
 * a vertex) adjacency list
 *
 * @return Adjacency list (length equals number of faces)
 */
vector<vector<long>> TriangularMesh::buildVertexFaceAdjacencyList(){

  vector<vector<long>> adjacencyListvf(this->vertices.size(), vector<long>());
  for (int f=0;f<this->faces.size();f++){
    adjacencyListvf[this->faces[f].p1].push_back(f);
    adjacencyListvf[this->faces[f].p2].push_back(f);
    adjacencyListvf[this->faces[f].p3].push_back(f);
  }

  return adjacencyListvf;
}

void TriangularMesh::averageVertexNormals(){

  this->setNormals();

  vector<Vec> averagedNormals(vertices.size());

  for (int p=0;p<vertices.size();p++){
    averagedNormals[p].vx = 0.0;
    averagedNormals[p].vy = 0.0;
    averagedNormals[p].vz = 0.0;
  }

  for (int f=0;f<faces.size();f++){
    int p1 = faces[f].p1;
    int p2 = faces[f].p2;
    int p3 = faces[f].p3;

    averagedNormals[p1].vx += this->faces[f].normal.vx;
    averagedNormals[p1].vy += this->faces[f].normal.vy;
    averagedNormals[p1].vz += this->faces[f].normal.vz;

    averagedNormals[p2].vx += this->faces[f].normal.vx;
    averagedNormals[p2].vy += this->faces[f].normal.vy;
    averagedNormals[p2].vz += this->faces[f].normal.vz;

    averagedNormals[p3].vx += this->faces[f].normal.vx;
    averagedNormals[p3].vy += this->faces[f].normal.vy;
    averagedNormals[p3].vz += this->faces[f].normal.vz;
  }

  for (int p=0;p<vertices.size();p++){
    double magnitude = sqrt(averagedNormals[p].vx * averagedNormals[p].vx +
                            averagedNormals[p].vy * averagedNormals[p].vy +
                            averagedNormals[p].vz * averagedNormals[p].vz);
    averagedNormals[p].vx /= magnitude;
    averagedNormals[p].vy /= magnitude;
    averagedNormals[p].vz /= magnitude;
    vertices[p].averagedNormal = averagedNormals[p];
  }
}


/*!
 * TriangularMesh Method for calculating the mesh faces (neighbouring faces
 * for a face) adjacency list
 *
 * @return Adjacency list (a 2D vector with a length equal to the number of faces)
 */
vector<vector<long>> TriangularMesh::buildFaceFaceAdjacencyList(){

  vector<vector<long>> adjacencyListvf(this->vertices.size(), vector<long>());
  vector<set<long>> adjacencyListff(this->faces.size(), set<long>());
  vector<vector<long>> adjacencyListffOut(this->faces.size(), vector<long>());

  //initially calculates neighbouring face of a vertex adjacency list
  for (int f=0;f<this->faces.size();f++){
    adjacencyListvf[this->faces[f].p1].push_back(f);
    adjacencyListvf[this->faces[f].p2].push_back(f);
    adjacencyListvf[this->faces[f].p3].push_back(f);
  }

  //using vf adjacency list, calculate neighbouring face of a face adjacency list
  for (int f=0;f<this->faces.size();f++){
    for (int l=0;l<adjacencyListvf[this->faces[f].p1].size();l++){
      adjacencyListff[f].insert(adjacencyListvf[this->faces[f].p1][l]);
    }
    for (int l=0;l<adjacencyListvf[this->faces[f].p2].size();l++){
      adjacencyListff[f].insert(adjacencyListvf[this->faces[f].p2][l]);
    }
    for (int l=0;l<adjacencyListvf[this->faces[f].p3].size();l++){
      adjacencyListff[f].insert(adjacencyListvf[this->faces[f].p3][l]);
    }
  }

  for (int f=0;f<this->faces.size();f++){
    adjacencyListffOut[f].assign(adjacencyListff[f].begin(), adjacencyListff[f].end());
  }


  return adjacencyListffOut;
}

/*!
* TriangularMesh Method for counting the number of intersections between a ray and
* triangular mesh
*
* @param Pl The ray source point
* @param Vl The ray direction vector
* @return number of intersections with the triangular mesh
*/
int TriangularMesh::intersections(Point& Pl, Vec& Vl){

  int intersects = 0;
  int p1, p2, p3;

  for (int a=0;a<this->faces.size();a++){
    p1 = this->faces[a].p1;
    p2 = this->faces[a].p2;
    p3 = this->faces[a].p3;

    intersects += this->faces[a].rayIntersection(Pl, Vl, this->vertices[p1], this->vertices[p2], this->vertices[p3]);
  }

  return intersects;

}

/*!
 * TriangularMesh Method for separating disconnected mesh components into separate meshes via
 * a iterative Depth First Search.
 *
 * @return a tuple containing the results of the separation
 * (1) a vector of separated triangle mesh instances
 * (2) a vector of the separated meshes volumes
 * (3) The index of the separated mesh with the maximum volume
 */
tuple<vector<TriangularMesh>, vector<double>, int> TriangularMesh::detachSeparateMeshComponents(){

  vector<TriangularMesh> dComponents;
  vector<int> incFaces(this->faces.size(), 1);
  vector<vector<long>> faceFaceAdjacencyList = this->buildFaceFaceAdjacencyList();

  //extract internal isocontours (voids and cavities)
  double mVol  = 0.0;
  int maxVolInd = -1;
  int mSec = 0;
  vector<int> surfConnect;
  vector<double> vols;
  while(accumulate(incFaces.begin(), incFaces.end(), 0) != 0){
    long initNode = find(incFaces.begin(), incFaces.end(), 1) - incFaces.begin();
    vector<int> connected = DFSIterative(faceFaceAdjacencyList, initNode); //perform depth first search to extract disconnected mesh components
    TriangularMesh nMesh;
    nMesh.vertices = this->vertices;
    for (int j=0;j<connected.size();j++){
      if (connected[j] == 1){
        nMesh.faces.push_back(this->faces[j]);
        incFaces[j] = 0;
      }
    }
    nMesh.removeDuplicateVertices(); //remove duplicate vertices via accelerated ordered map method
    double vol = nMesh.calculateVolume();
    vols.push_back(vol);
    if (vol > mVol){
      maxVolInd = mSec;
      surfConnect = connected;
      mVol = vol;
    }
    dComponents.push_back(nMesh);
    mSec++;
  }

  tuple<vector<TriangularMesh>, vector<double>, int> retVals = {dComponents, vols, maxVolInd};

  return retVals;

}



/*!
* TriangularMesh Method for determining the shortest distance between a
* triangular mesh and test point
*
* @param Pl The point being tested
* @return shortest distance between test point and triangular mesh
*/
double TriangularMesh::shortestDistanceToPoint(Point& Pl, double distanceTol){

  double pointDist = 1e20;
  double tPointDist;

  double panelTolSqr = distanceTol * distanceTol;

  int find;
  for (int a=0;a<this->faces.size();a++){
    Point& P1 = this->vertices[this->faces[a].p1];
    Point& P2 = this->vertices[this->faces[a].p2];
    Point& P3 = this->vertices[this->faces[a].p3];
    double dx = this->faces[a].centre.px - Pl.px;
    double dy = this->faces[a].centre.py - Pl.py;
    double dz = this->faces[a].centre.pz - Pl.pz;
    tPointDist = dx * dx + dy * dy + dz * dz;
    if (tPointDist/this->faces[a].ls2 < panelTolSqr)
      tPointDist = this->faces[a].distanceToPoint(Pl, P1, P2, P3);
    if (tPointDist < pointDist){
      find = a;
      pointDist = tPointDist;
    }
  }

  return sqrt(pointDist);
}

/*!
* Face Method for determining the distance between a triangle face and a point
*
* @param Pl The point being tested
* @param P1 face corner point 1
* @param P2 face corner point 2
* @param P3 face corner point 3
* @return distance from test point and the triangle face
*/
double Face::distanceToPoint(Point& Pl, Point& P1, Point& P2, Point& P3){

  double plx, ply, plz, tDistSq, numer, denom, invdet, tmp0, tmp1;

  double edge1[3] = {P2.px - P1.px, P2.py - P1.py, P2.pz - P1.pz};
  double edge2[3] = {P3.px - P1.px, P3.py - P1.py, P3.pz - P1.pz};
  double vec[3] = {P1.px - Pl.px, P1.py - Pl.py, P1.pz - Pl.pz};

  double d11 = edge1[0]*edge1[0] + edge1[1]*edge1[1] + edge1[2]*edge1[2];
  double d12 = edge1[0]*edge2[0] + edge1[1]*edge2[1] + edge1[2]*edge2[2];
  double d22 = edge2[0]*edge2[0] + edge2[1]*edge2[1] + edge2[2]*edge2[2];
  double d31 = vec[0] * edge1[0] + vec[1] * edge1[1] + vec[2] * edge1[2];
  double d32 = vec[0] * edge2[0] + vec[1] * edge2[1] + vec[2] * edge2[2];

  double det = (d11 * d22 - d12 * d12);

  double s = d12*d32 - d22*d31;
  double t = d12*d31 - d11*d32;

  if ((s + t) < det){
    if (s < 0.0){
      if (t < 0.0){
        if (d31 < 0.0){
          s = clamp(-d31/d11, 0.0, 1.0);
          t = 0.0;
        }
        else{
          s = 0.0;
          t = clamp(-d32/d22, 0.0, 1.0);
        }
      }
      else{
        s = 0.0;
        t = clamp(-d32/d22, 0.0, 1.0);
      }
    }
    else if (t < 0.0){
      s = clamp(-d31/d11, 0.0, 1.0);
      t = 0.0;
    }
    else{
      invdet = 1.0/det;
      s = s*invdet;
      t = t*invdet;
    }
  }
  else{
    if (s < 0.0){
      tmp0 = d12 + d31;
      tmp1 = d22 + d32;
      if (tmp1 > tmp0){
        numer = tmp1 - tmp0;
        denom = d11 - 2.0*d12 + d32;
        s = clamp(numer/denom, 0.0, 1.0);
        t = 1.0 - s;
      }
      else{
        t = clamp(-d32/d22, 0.0, 1.0);
        s = 0.0;
      }
    }
    else if (t < 0.0){
      if ((d11 + d31) > (d12 + d32)){
        numer = d22 + d32 - d12 - d31;
        denom = d11 - 2.0 * d12 + d22;
        s = clamp(numer/denom, 0.0, 1.0);
        t = 1.0 - s;
      }
      else{
        t = clamp(-d32/d22, 0.0, 1.0);
        s = 0.0;
      }
    }
    else{
      numer = d22 + d32 - d12 - d31;
      denom = d11 - 2.0*d12 + d22;
      s = clamp(numer/denom, 0.0, 1.0);
      t = 1.0 - s;
    }
  }

  plx = P1.px + s * edge1[0] + t * edge2[0];
  ply = P1.py + s * edge1[1] + t * edge2[1];
  plz = P1.pz + s * edge1[2] + t * edge2[2];

  tDistSq = (Pl.px - plx)*(Pl.px - plx) + (Pl.py - ply)*(Pl.py - ply) + (Pl.pz - plz)*(Pl.pz - plz);

  return tDistSq;

}

/*!
* TriangularMesh Method for determining whether a point falls inside the instances
* triangular mesh or not
*
* @param Pl The point being tested
* @return 0 if test point inside mesh, 1 if test point outside mesh
*/
int TriangularMesh::insideMesh(Point& Pl){

  int inside = 0;
  int outside = 0;

  int int1 = 0;
  int int2 = 0;
  int int3 = 0;

  //test vector directions for inside polygon test
  Vec Vt1(1.0, 0.0, -0.05);
  Vec Vt2(0.1, 0.3, 0.35);
  Vec Vt3(-0.1, -0.35, 0.1);

  const int facesNum = this->faces.size();

  for (int f=0;f<facesNum;f++){
    Point& P1 = this->vertices[this->faces[f].p1];
    Point& P2 = this->vertices[this->faces[f].p2];
    Point& P3 = this->vertices[this->faces[f].p3];

    int1 += this->faces[f].rayIntersection(Pl, Vt1, P1, P2, P3);
    int2 += this->faces[f].rayIntersection(Pl, Vt2, P1, P2, P3);
    int3 += this->faces[f].rayIntersection(Pl, Vt3, P1, P2, P3);
  }

  if (int1 % 2 == 0) {outside += 1;}
  if (int1 % 2 == 1) {inside += 1;}
  if (int2 % 2 == 0) {outside += 1;}
  if (int2 % 2 == 1) {inside += 1;}
  if (int3 % 2 == 0) {outside += 1;}
  if (int3 % 2 == 1) {inside += 1;}

  if (inside >= outside){
    return 0;
  }
  else{
    return 1;
  }
}


/*!
*
* Face Method for calculating whether a ray intersects a particular triangle element
*
* Implemented algorithm is the Möller–Trumbore ray-triangle intersection algorithm
* Möller, Tomas; Trumbore, Ben (1997).
* "Fast, Minimum Storage Ray-Triangle Intersection".
* Journal of Graphics Tools. 2: 21–28.
* doi:10.1080/10867651.1997.10487468
*
* @param1 Pl (Point): The ray launch position
* @param2 Vl (Vec): The ray launch direction
* @param3 P1 (Point): Triangle panel point 1
* @param4 P2 (Point): Triangle panel point 2
* @param5 P3 (Point): Triangle panel point 3
* @return: 1 if intersection, 0 if miss
*
*/
 inline int Face::rayIntersection(Point& Pl, Vec& Vl, Point& P1, Point& P2, Point& P3){

  double edge1[3] = {P2.px - P1.px, P2.py - P1.py, P2.pz - P1.pz};
  double edge2[3] = {P3.px - P1.px, P3.py - P1.py, P3.pz - P1.pz};

  double h[3] = {Vl.vy*edge2[2] - Vl.vz*edge2[1],
                 Vl.vz*edge2[0] - Vl.vx*edge2[2],
                 Vl.vx*edge2[1] - Vl.vy*edge2[0]};

  double a = edge1[0] * h[0] + edge1[1] * h[1] + edge1[2] * h[2];

  if ((a > -SMALL) && (a < SMALL)) return 0;

  double f = 1.0/a;
  double s[3] = {Pl.px - P1.px,
                 Pl.py - P1.py,
                 Pl.pz - P1.pz};

  double u = f * (s[0] * h[0] + s[1] * h[1] + s[2] * h[2]);

  if ((u < 0.0) || (u > 1.0)) return 0;

  double q[3] = {s[1] * edge1[2] - s[2] * edge1[1],
                 s[2] * edge1[0] - s[0] * edge1[2],
                 s[0] * edge1[1] - s[1] * edge1[0]};

  double v = f * (Vl.vx * q[0] + Vl.vy * q[1] + Vl.vz * q[2]);

  if ((v < 0.0) || (u + v > 1.0)) return 0;

  double t = f * (edge2[0] * q[0] + edge2[1] * q[1] + edge2[2] * q[2]);

  if (t > SMALL) {
    return 1;
  }
  else {
    return 0;
  }
}

/*!
 * Template function for calculating the determinant of an array
 *
 * @param The input square matrix from which the determinant will be calculated.
 * @return The matrix determinant value
 */
template <size_t n>
double calculateDeterminant(double mat[n][n]){

   double det = 1;

   // Row operations for i = 0, ,,,, n - 2 (n-1 not needed)
   for (int i = 0;i<n-1; i++) {
      // Partial pivot: find row r below with largest element in column i
      int r = i;
      double maxA = abs( mat[i][i] );
      for (int k = i + 1; k < n; k++) {
         double val = abs(mat[k][i]);
         if (val > maxA) {
            r = k;
            maxA = val;
         }
      }
      if (r != i) {
         for (int j = i; j < n; j++){
           swap(mat[i][j], mat[r][j]);
         }
         det = -det;
      }

      // Row operations to make upper-triangular
      double pivot = mat[i][i];
      if (abs(pivot) < SMALL) {
        return 0.0; // Singular matrix
      }
      for (int r = i + 1; r < n; r++) // On lower rows
      {
         double multiple = mat[r][i] / pivot; // Multiple of row i to clear element in ith column
         for (int j = i; j < n; j++) {
           mat[r][j] -= multiple * mat[i][j];
         }
      }
      det *= pivot; // Determinant is product of diagonal
   }

   det *= mat[n-1][n-1];

   return det;
}

/*!
 * Function for finding Barycentric coordinates for a point with respect
 * to three points defining a triangular panel
 *
 * @param pointP The point for which the Barycentric coordinates are calculated
 * @param pointA Triangle point A
 * @param pointB Triangle point B
 * @param pointC Triangle point C
 * @param u Barycentric coordinate 1
 * @param v Barycentric coordinate 2
 * @param w Barycentric coordinate 3
 */
void findBarycentricCoordinates3D(Point& pointP, Point& pointA,
                                  Point& pointB, Point& pointC,
                                  double& u, double& v, double& w){

  Point v1 = pointB - pointA;
  Point v2 = pointC - pointA;
  Point v3 = pointP - pointA;

  double d00 = dot(v1, v1);
  double d01 = dot(v1, v2);
  double d11 = dot(v2, v2);
  double d20 = dot(v3, v1);
  double d21 = dot(v3, v2);
  double denom = d00 * d11 - d01 * d01;
  v = (d11 * d20 - d01 * d21) / denom;
  w = (d00 * d21 - d01 * d20) / denom;
  u = 1.0 - v - w;

}
