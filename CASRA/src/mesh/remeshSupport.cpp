/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <map>
#include <vector>
#include <chrono>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/remeshSupport.h"
#include "CASRA/mesh/marchingCubes.h"

#include "isotropicremesher/isotropicremesher.h"
#include "isotropicremesher/isotropichalfedgemesh.h"

using namespace std;

using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::seconds;

/*!
 * Function for performing an isotropic remeshing of the surface
 *
 * @param surfaceMesh The triangularMesh instance to be remeshed
 * @param it The number of remesh iterations to perform
 * @param targetLength The target panel edge length
 *
 * @return triangularMesh the remeshed surface
 *
 */
TriangularMesh& performRemesh(TriangularMesh& surfaceMesh, int it, double targetLength){

  //generate into isotropic remeshing library input format
  vector<Vector3> vp;
  vector<vector<size_t>> vf;
  vector<bool> unlocked;

  for (int i=0;i<surfaceMesh.faces.size();i++){
    vector<size_t> tface;
    tface.push_back(surfaceMesh.faces[i].p1);
    tface.push_back(surfaceMesh.faces[i].p2);
    tface.push_back(surfaceMesh.faces[i].p3);
    vf.push_back(tface);
  }

  for (int i=0;i<surfaceMesh.vertices.size();i++){
    unlocked.push_back(surfaceMesh.vertices[i].locked);
    Vector3 tvert(surfaceMesh.vertices[i].px,
                  surfaceMesh.vertices[i].py,
                  surfaceMesh.vertices[i].pz);
    vp.push_back(tvert);
  }

  IsotropicRemesher isoRemeshSphere(&vp, &vf, &unlocked);
  isoRemeshSphere.setTargetEdgeLength(isoRemeshSphere.initialAverageEdgeLength() * targetLength);
  isoRemeshSphere.remesh(it);

  surfaceMesh.vertices.clear();
  surfaceMesh.faces.clear();

  int vc = 0;

  size_t outputIndex = 0;
  IsotropicHalfedgeMesh* halfedgeMesh = isoRemeshSphere.remeshedHalfedgeMesh();
  for (IsotropicHalfedgeMesh::Vertex *vertex = halfedgeMesh->moveToNextVertex(nullptr);
          nullptr != vertex;vertex = halfedgeMesh->moveToNextVertex(vertex)) {
      vertex->outputIndex = outputIndex++;
      Point tpoint(vertex->position[0], vertex->position[1], vertex->position[2]);
      surfaceMesh.vertices.push_back(tpoint);
      vc += 1;
  }

  int fc = 0;

  for (IsotropicHalfedgeMesh::Face *face = halfedgeMesh->moveToNextFace(nullptr);
          nullptr != face;face = halfedgeMesh->moveToNextFace(face)) {
      Face tface((long)face->halfedge->previousHalfedge->startVertex->outputIndex,
                 (long)face->halfedge->startVertex->outputIndex,
                 (long)face->halfedge->nextHalfedge->startVertex->outputIndex);
      surfaceMesh.faces.push_back(tface);
      fc += 1;
  }

  return surfaceMesh;

}


/*!
 * Function for performing adaptive remeshing. Here the submesh to be remeshed is initialised
 * in chunks within the isotropicRemesher half edge mesh. This reduces the number of panels and vertices
 * that must be looped over, accelerating the remeshing process compared to performAdaptiveRemeshR. However,
 * The function leverages a hash map, resulting in a slight loss of boundary precision.
 *
 * @param surfaceMesh The triangularMesh instance to be remeshed
 * @param remeshHeightLandmarks N List of max heights between which to remesh with a particular target length
 * @param targetLengths N List of target lengths to remesh between landmarks
 * @param it The number of remesh iterations to perform
 * @param cellWidthZ The cell width in the z direction
 * @param startShankExtZ The z-position below which the shank extension begins
 * @return triangularMesh the remeshed surface
 *
 */
TriangularMesh& performAdaptiveRemesh(TriangularMesh& surfaceMesh,
                                      vector<double> remeshHeightLandmarks,
                                      vector<double> targetLengths, int it,
                                      double cellWidthZ, double startShankExtZ){


  double v1x, v1y, v1z, v2x, v2y, v2z, v3x, v3y, v3z;
  //double utime = 0.0;
  //double rtime = 0.0;
  //double ctime = 0.0;

  const int remeshHeightLandmarksSize = remeshHeightLandmarks.size();
  for (int l=1;l< remeshHeightLandmarksSize;l++){
    remeshHeightLandmarks[l] -= 2.0 * targetLengths[l-1];
  }

  remeshHeightLandmarks.push_back(-1e10);
  const int targetLengthSize = targetLengths.size();

  for (int l=0;l<targetLengthSize;l++){

    //auto t1 = high_resolution_clock::now();

    //generate into isotropic remeshing library input format
    vector<Vector3> vp;
    vector<vector<size_t>> vf;
    vector<bool> locked;
    int rit;

    bool firstExtensionRemesh = false;

    //split mesh into region for remeshing
    vector<bool> lv(surfaceMesh.vertices.size(), true);

    double lowerBound = remeshHeightLandmarks[l+1] - 1.0 * targetLengths[l];
    rit = it;
    if (remeshHeightLandmarks[l+1] < (startShankExtZ + 2.0 * targetLengths[l])){
      lowerBound = -1e10;
      if (firstExtensionRemesh == false){
        rit = 5;
      }
    }

    double upperBound = 0.0;
    if (l > 0){
      upperBound = 1.0 * targetLengths[l-1];
    }

    upperBound += remeshHeightLandmarks[l];

    const long nverts = surfaceMesh.vertices.size();
    const long nfaces = surfaceMesh.faces.size();

    for (long i=0;i<nverts;i++){
      if ((surfaceMesh.vertices[i].pz < upperBound) && (surfaceMesh.vertices[i].pz > lowerBound)){
        lv[i] = false;
      }
    }
    int iv1;
    int iv2;
    int iv3;

    int appVert = 0;
    vector<int> appended(nverts, -1);

    vector<bool> includeVertex(nverts, false);
    vector<bool> includePanel(nfaces, false);
    vector<bool> oincludeVertex;
    oincludeVertex.reserve(nverts);
    vector<vector<vector<double>>> ntrilist;

    for (long i=0;i< nfaces;i++){

      iv1 = surfaceMesh.faces[i].p1;
      iv2 = surfaceMesh.faces[i].p2;
      iv3 = surfaceMesh.faces[i].p3;

      if (lv[iv1] == false || lv[iv2] == false || lv[iv3] == false){
        includePanel[i] = true;
        includeVertex[iv1] = true;
        includeVertex[iv2] = true;
        includeVertex[iv3] = true;
      }
    }

    oincludeVertex = includeVertex;

    for (long i=0;i< nfaces;i++){

      iv1 = surfaceMesh.faces[i].p1;
      iv2 = surfaceMesh.faces[i].p2;
      iv3 = surfaceMesh.faces[i].p3;

      if (oincludeVertex[iv1] == true || oincludeVertex[iv2] == true || oincludeVertex[iv3] == true){
        includePanel[i] = true;
        includeVertex[iv1] = true;
        includeVertex[iv2] = true;
        includeVertex[iv3] = true;
      }
    }

    for (long i=0;i<nfaces;i++){

      iv1 = surfaceMesh.faces[i].p1;
      iv2 = surfaceMesh.faces[i].p2;
      iv3 = surfaceMesh.faces[i].p3;

      if (includeVertex[iv1] == true || includeVertex[iv2] == true || includeVertex[iv3] == true){
        includePanel[i] = true;
      }
    }


    Vector3 tvert;
    int resFace = 0;
    int resVert = 0;
    int resTriList = ntrilist.size();

    for (long i = 0; i < nfaces; i++) {

        iv1 = surfaceMesh.faces[i].p1;
        iv2 = surfaceMesh.faces[i].p2;
        iv3 = surfaceMesh.faces[i].p3;

        if (includePanel[i] == true) {
            ++resFace;
            if (appended[surfaceMesh.faces[i].p1] == -1)
                ++resVert;
            if (appended[surfaceMesh.faces[i].p2] == -1)
                ++resVert;
            if (appended[surfaceMesh.faces[i].p3] == -1)
                ++resVert;
        }
        else
            ++resTriList;
    }

    vp.reserve(resVert);
    locked.reserve(resVert);
    vf.reserve(resFace);
    ntrilist.reserve(resTriList);
    vector<size_t> tface(3);

    for (long i=0;i<nfaces;i++){

      iv1 = surfaceMesh.faces[i].p1;
      iv2 = surfaceMesh.faces[i].p2;
      iv3 = surfaceMesh.faces[i].p3;

      if (includePanel[i] == true){
          if (appended[iv1] == -1) {
            tvert.setData(surfaceMesh.vertices[iv1].px, surfaceMesh.vertices[iv1].py, surfaceMesh.vertices[iv1].pz);
            vp.push_back(tvert);
            locked.push_back(lv[iv1]);
            tface[0] = appVert;
            appended[iv1] = appVert;
            appVert++;
          }
          else{
            tface[0] = appended[iv1];
          }
          if (appended[iv2] == -1) {
            tvert.setData(surfaceMesh.vertices[iv2].px, surfaceMesh.vertices[iv2].py, surfaceMesh.vertices[iv2].pz);
            vp.push_back(tvert);
            locked.push_back(lv[iv2]);
            tface[1] = appVert;
            appended[iv2] = appVert;
            appVert++;
          }
          else{
            tface[1] = appended[iv2];
          }
          if (appended[iv3] == -1) {
            tvert.setData(surfaceMesh.vertices[iv3].px, surfaceMesh.vertices[iv3].py, surfaceMesh.vertices[iv3].pz);
            vp.push_back(tvert);
            locked.push_back(lv[iv3]);
            tface[2] = appVert;
            appended[iv3] = appVert;
            appVert++;
          }
          else{
            tface[2] = appended[iv3];
          }
          vf.push_back(tface);
      }
      else{
        v1x = surfaceMesh.vertices[iv1].px;
        v1y = surfaceMesh.vertices[iv1].py;
        v1z = surfaceMesh.vertices[iv1].pz;
        v2x = surfaceMesh.vertices[iv2].px;
        v2y = surfaceMesh.vertices[iv2].py;
        v2z = surfaceMesh.vertices[iv2].pz;
        v3x = surfaceMesh.vertices[iv3].px;
        v3y = surfaceMesh.vertices[iv3].py;
        v3z = surfaceMesh.vertices[iv3].pz;

        v1x = round_up(v1x, 8);
        v1y = round_up(v1y, 8);
        v1z = round_up(v1z, 8);
        v2x = round_up(v2x, 8);
        v2y = round_up(v2y, 8);
        v2z = round_up(v2z, 8);
        v3x = round_up(v3x, 8);
        v3y = round_up(v3y, 8);
        v3z = round_up(v3z, 8);

        ntrilist.push_back({{v1x, v1y, v1z},
                            {v2x, v2y, v2z},
                            {v3x, v3y, v3z}});
      }
    }

    IsotropicRemesher isoRemeshSphere(&vp, &vf, &locked);
    IsotropicHalfedgeMesh *halfedgeMesh;

    isoRemeshSphere.setTargetEdgeLength(targetLengths[l]);
    //auto t2 = high_resolution_clock::now();
    //duration<double> ms_double = t2 - t1;
    //ctime += ms_double.count();

    //t1 = high_resolution_clock::now();
    isoRemeshSphere.remesh(rit);
    //t2 = high_resolution_clock::now();
    //ms_double = t2 - t1;
    //rtime += ms_double.count();

    //extract half edge mesh and merge with ignored panels
    //add unmeshed panels to new mesh
    halfedgeMesh = isoRemeshSphere.remeshedHalfedgeMesh();
    for (IsotropicHalfedgeMesh::Face *face = halfedgeMesh->moveToNextFace(nullptr);
            nullptr != face;face = halfedgeMesh->moveToNextFace(face)) {
        v1x = face->halfedge->previousHalfedge->startVertex->position.x();
        v1y = face->halfedge->previousHalfedge->startVertex->position.y();
        v1z = face->halfedge->previousHalfedge->startVertex->position.z();
        v2x = face->halfedge->startVertex->position.x();
        v2y = face->halfedge->startVertex->position.y();
        v2z = face->halfedge->startVertex->position.z();
        v3x = face->halfedge->nextHalfedge->startVertex->position.x();
        v3y = face->halfedge->nextHalfedge->startVertex->position.y();
        v3z = face->halfedge->nextHalfedge->startVertex->position.z();

        v1x = round_up(v1x, 8);
        v1y = round_up(v1y, 8);
        v1z = round_up(v1z, 8);
        v2x = round_up(v2x, 8);
        v2y = round_up(v2y, 8);
        v2z = round_up(v2z, 8);
        v3x = round_up(v3x, 8);
        v3y = round_up(v3y, 8);
        v3z = round_up(v3z, 8);

        ntrilist.push_back({{v1x, v1y, v1z},
                            {v2x, v2y, v2z},
                            {v3x, v3y, v3z}});

    }

    //unpack unique triangle vertices into mesh via a hash map
    //t1 = high_resolution_clock::now();
    unpackUniqueVertices(ntrilist, &surfaceMesh);
    //t2 = high_resolution_clock::now();
    //ms_double = t2 - t1;
    //utime += ms_double.count();

  }
  //cout << "unique vertex time: " << utime << endl;
  //cout << "remesh sp time: " << rtime << endl;
  //cout << "create time: " << ctime << endl;

  return surfaceMesh;

}
