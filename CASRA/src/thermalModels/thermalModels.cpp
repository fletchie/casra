/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <iostream>

#include "CASRA/thermalModels/thermalModels.h"
#include "CASRA/mesh/triangularMesh.h"

using namespace std;

BaseThermalModel::BaseThermalModel(double T0) : T0{T0} {

}

UniformTemperatureModel::UniformTemperatureModel(double T0) : BaseThermalModel{T0} {

}

vector<double> UniformTemperatureModel::calculateTemperature(TriangularMesh& surfaceMesh) {
  return vector<double>(surfaceMesh.faces.size(), T0);
}

InstantaneousHeatingModel::InstantaneousHeatingModel(double dir[3], double TMax, double T0) : TMax{TMax}, BaseThermalModel{T0} {
  double dnorm = sqrt(dir[0]*dir[0] + dir[1]*dir[1] + dir[2]*dir[2]);
  this->direction[0] = dir[0]/dnorm;
  this->direction[1] = dir[1]/dnorm;
  this->direction[2] = dir[2]/dnorm;

  if (TMax < T0){
    cerr << "WARNING: inputted TMax smaller than T0. This might produce unexpected behaviour (laser cooling)." << endl;
  }
}

vector<double> InstantaneousHeatingModel::calculateTemperature(TriangularMesh& surfaceMesh) {

  surfaceMesh.setCentres();
  surfaceMesh.setNormals();

  //propagate back from surface panel to laser source
  Vec Vl(-direction[0], -direction[1], -direction[2]);

  vector<double> TSurf(surfaceMesh.faces.size(), T0);

  const int facesSize = surfaceMesh.faces.size();

  #if defined(_OPENMP)
  #pragma omp parallel for
  #endif
  for (int f=0;f<facesSize;f++){
    Point& Pl = surfaceMesh.faces[f].centre;
    Vec& Vn = surfaceMesh.faces[f].normal;
    Pl.px = Pl.px + 1e-9 * Vn.vx;
    Pl.py = Pl.py + 1e-9 * Vn.vy;
    Pl.pz = Pl.pz + 1e-9 * Vn.vz;

    //calculate raytracing to check if surface patch shadowed by specimen surface
    int intersec = surfaceMesh.intersections(Pl, Vl);

    if (intersec == 0){
      TSurf[f] = T0 + (TMax - T0) * max(0.0, -(Vn.vx * this->direction[0] +
                                               Vn.vy * this->direction[1] +
                                               Vn.vz * this->direction[2]));
    }
  }
  return TSurf;
}
