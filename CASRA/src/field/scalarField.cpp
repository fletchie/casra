/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cmath>

#include "CASRA/field/scalarField.h"
#include "CASRA/mesh/triangularMesh.h"

using namespace std;

#define FIELDSMALL 1e-40

scalarField3D operator+(scalarField3D&& a, double b) {
  scalarField3D field = a;

  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        field[i][j][k] += b;
      }
    }
  }
  return field;
}

scalarField3D operator-(scalarField3D&& a, double b) {
  scalarField3D field = a;
  const int xDim = a.size();
  const int yDim = a[0].size();
  const int zDim = a[0][0].size();
  for (int i=0;i<xDim;i++){
    for (int j=0;j<yDim;j++){
      for (int k=0;k<zDim;k++){
        field[i][j][k] -= b;
      }
    }
  }
  return field;
}

scalarField3D operator+(scalarField3D&& a, scalarField3D&& b) {
  scalarField3D field = a;
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }

  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        field[i][j][k] += b[i][j][k];
      }
    }
  }
  return field;
}

scalarField3D operator+(scalarField3D& a, scalarField3D& b) {
  scalarField3D field = a;
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }

  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        field[i][j][k] += b[i][j][k];
      }
    }
  }
  return field;
}

scalarField3D operator-(scalarField3D&& a, scalarField3D&& b) {
  scalarField3D field = a;
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }
  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        field[i][j][k] -= b[i][j][k];
      }
    }
  }
  return field;
}

scalarField3D operator-(scalarField3D& a, scalarField3D& b) {
  scalarField3D field = a;
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }
  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        field[i][j][k] -= b[i][j][k];
      }
    }
  }
  return field;
}

void operator+=(scalarField3D& a, scalarField3D& b) {
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }
  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        a[i][j][k] += b[i][j][k];
      }
    }
  }
}

void operator+=(scalarField3D& a, scalarField3D&& b) {
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }
  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        a[i][j][k] += b[i][j][k];
      }
    }
  }
}

void operator+=(scalarField3D& a, double b) {
  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        a[i][j][k] += b;
      }
    }
  }
}

void operator-=(scalarField3D& a, double b) {

  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        a[i][j][k] -= b;
      }
    }
  }
}


void operator-=(scalarField3D& a, scalarField3D& b) {
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }
  const int xDim = a.size();
  const int yDim = a[0].size();
  const int zDim = a[0][0].size();
  for (int i=0;i<xDim;i++){
    for (int j=0;j<yDim;j++){
      for (int k=0;k<zDim;k++){
        a[i][j][k] -= b[i][j][k];
      }
    }
  }
}

void operator-=(scalarField3D& a, scalarField3D&& b) {
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }
  const int xDim = a.size();
  const int yDim = a[0].size();
  const int zDim = a[0][0].size();
  for (int i=0;i<xDim;i++){
    for (int j=0;j<yDim;j++){
      for (int k=0;k<zDim;k++){
        a[i][j][k] -= b[i][j][k];
      }
    }
  }
}

scalarField3D operator*(scalarField3D& a, scalarField3D&& b) {
  scalarField3D field = a;
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }
  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        field[i][j][k] = a[i][j][k] * b[i][j][k];
      }
    }
  }
  return field;
}

scalarField3D operator*(scalarField3D&& a, scalarField3D&& b) {
  scalarField3D field = a;
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }
  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        field[i][j][k] = a[i][j][k] * b[i][j][k];
      }
    }
  }
  return field;
}

scalarField3D operator*(scalarField3D&& a, double b) {
  scalarField3D field = a;
  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        field[i][j][k] = a[i][j][k] * b;
      }
    }
  }
  return field;
}

scalarField3D operator*(scalarField3D& a, double b) {
  scalarField3D field = a;
  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        field[i][j][k] = a[i][j][k] * b;
      }
    }
  }
  return field;
}

void operator*=(scalarField3D& a, scalarField3D& b) {
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }
  const int xDim = a.size();
  const int yDim = a[0].size();
  const int zDim = a[0][0].size();
  for (int i=0;i<xDim;i++){
    for (int j=0;j<yDim;j++){
      for (int k=0;k<zDim;k++){
        a[i][j][k] *= b[i][j][k];
      }
    }
  }
}

void operator*=(scalarField3D& a, scalarField3D&& b) {
  if ((a.size() != b.size()) || (a[0].size() != b[0].size() || a[0][0].size() != b[0][0].size())) {
    throw std::invalid_argument( "scalar field dimensions mismatch" );
  }
  const int xDim = a.size();
  const int yDim = a[0].size();
  const int zDim = a[0][0].size();
  for (int i=0;i<xDim;i++){
    for (int j=0;j<yDim;j++){
      for (int k=0;k<zDim;k++){
        a[i][j][k] *= b[i][j][k];
      }
    }
  }
}

void operator*=(scalarField3D& a, double b) {
  const int xDim = a.size();
  const int yDim = a[0].size();
  const int zDim = a[0][0].size();
  for (int i=0;i<xDim;i++){
    for (int j=0;j<yDim;j++){
      for (int k=0;k<zDim;k++){
        a[i][j][k] *= b;
      }
    }
  }
}

scalarField3D operator>(scalarField3D a, double value) {
  scalarField3D field = initialiseField(a.size(), a[0].size(), a[0][0].size(), 0.0);

  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        if (a[i][j][k] > value) {
          field[i][j][k] = 1.0;
        }
      }
    }
  }
  return field;
}

scalarField3D operator<(scalarField3D a, double value) {
  scalarField3D field = initialiseField(a.size(), a[0].size(), a[0][0].size(), 0.0);

  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        if (a[i][j][k] < value) {
          field[i][j][k] = 1.0;
        }
      }
    }
  }
  return field;
}

scalarField3D operator/(scalarField3D a, scalarField3D b) {
  scalarField3D field = a;

  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        field[i][j][k] /= b[i][j][k];
      }
    }
  }
  return field;
}

scalarField3D operator/(scalarField3D a, double b) {
  scalarField3D field = a;

  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        field[i][j][k] /= b;
      }
    }
  }
  return field;
}

void operator/=(scalarField3D& a, scalarField3D b) {

  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        a[i][j][k] /= b[i][j][k];
      }
    }
  }
}

void operator/=(scalarField3D& a, double b) {

  for (int i=0;i<a.size();i++){
    for (int j=0;j<a[0].size();j++){
      for (int k=0;k<a[0][0].size();k++){
        a[i][j][k] /= b;
      }
    }
  }
}

scalarField2D initialiseField(int xDim, int yDim, double value){
  scalarField2D field = scalarField2D(xDim, scalarField1D(yDim, value));
  return field;
}

scalarField3D initialiseField(int xDim, int yDim, int zDim, double value){
  scalarField3D field = scalarField3D(xDim, scalarField2D(yDim, scalarField1D(zDim, value)));
  return field;
}

scalarField4D initialiseField(int xDim, int yDim, int zDim, int kDim, double value){
  scalarField4D field = scalarField4D(xDim, scalarField3D(yDim, scalarField2D(zDim, scalarField1D(kDim, value))));
  return field;
}

scalarField3DInt initialiseFieldInt(int xDim, int yDim, int zDim, int value){
  scalarField3DInt field = scalarField3DInt(xDim, scalarField2DInt(yDim, scalarField1DInt(zDim, value)));
  return field;
}

scalarField4DInt initialiseFieldInt(int xDim, int yDim, int zDim, int kDim, int value){
  scalarField4DInt field = scalarField4DInt(xDim, scalarField3DInt(yDim, scalarField2DInt(zDim, scalarField1DInt(kDim, value))));
  return field;
}


/*!
 *
 * Function for initialising a signed distance field from boundaryMesh
 * @param field The to be initialised field
 * @param boundaryMesh The triangularMesh input from which the signed distance field is constructed;
 *
 */
void initialiseSDF(scalarField3D& field, TriangularMesh* boundaryMesh, Point bottomLeftDomainCorner, double cellWidth){

  double dist;
  double insideCheck;
  Point cellCentre;

  //calculate distance from mesh to cell centres
  for (int i=0;i<field.size();i++){
    cellCentre.px = bottomLeftDomainCorner.px + cellWidth * i;
    for (int j=0;j<field[0].size();j++){
      cellCentre.py = bottomLeftDomainCorner.py + cellWidth * j;
      for (int k=0;k<field[0][0].size();k++){
        cellCentre.pz = bottomLeftDomainCorner.pz + cellWidth * k;
        //cellCentre.printPoint();
        dist = boundaryMesh->shortestDistanceToPoint(cellCentre);
        field[i][j][k] = dist;
      }
    }
  }

  //calculate field sign (cell centres inside or outside boundaryMesh)
  for (int i=0;i<field.size();i++){
    cellCentre.px = bottomLeftDomainCorner.px + cellWidth * i;
    for (int j=0;j<field[0].size();j++){
      cellCentre.py = bottomLeftDomainCorner.py + cellWidth * j;
      for (int k=0;k<field[0][0].size();k++){
        cellCentre.pz = bottomLeftDomainCorner.pz + cellWidth * k;
        insideCheck = 1.0 - 2.0*(float)boundaryMesh->insideMesh(cellCentre);
        field[i][j][k] *= insideCheck;
      }
    }
  }
}


scalarField3D cornerInterpolation(scalarField3D& field){

  scalarField3D cornerField = initialiseField(field.size()-1, field[0].size()-1, field[0][0].size()-1, 0.0);

  for (int i=0;i<cornerField.size();i++){
    for (int j=0;j<cornerField[0].size();j++){
      for (int k=0;k<cornerField[0][0].size();k++){
        cornerField[i][j][k] = (field[i][j][k]+field[i+1][j][k]+
                                field[i][j+1][k]+field[i+1][j+1][k]+
                                field[i][j][k+1]+field[i+1][j][k+1]+
                                field[i][j+1][k+1]+field[i+1][j+1][k+1])/8.0;
      }
    }
  }
  return cornerField;
}

void upwindDPhi(scalarField3D& vField, scalarField3D& upwindBG, scalarField3D& upwindFG, scalarField3D& dPhi){

  for (int i=0;i<vField.size();i++){
    for (int j=0;j<vField[0].size();j++){
      for (int k=0;k<vField[0][0].size();k++){
        dPhi[i][j][k] = max(vField[i][j][k], 0.0)*upwindBG[i][j][k] +
                        min(vField[i][j][k], 0.0)*upwindFG[i][j][k];
      }
    }
  }
}

void upwindGradient(scalarField3D& dFieldXm, scalarField3D& dFieldYm,
                    scalarField3D& dFieldZm, scalarField3D& dFieldXp,
                    scalarField3D& dFieldYp, scalarField3D& dFieldZp,
                    scalarField3D& upwindBG, scalarField3D& upwindFG){
  for (int i=0;i<dFieldXm.size();i++){
    for (int j=0;j<dFieldXm[0].size();j++){
      for (int k=0;k<dFieldXm[0][0].size();k++){
        upwindFG[i][j][k] = sqrt(pow(max(dFieldXp[i][j][k], 0.0), 2.0) +
                                 pow(max(dFieldYp[i][j][k], 0.0), 2.0) +
                                 pow(max(dFieldZp[i][j][k], 0.0), 2.0) +
                                 pow(min(dFieldXm[i][j][k], 0.0), 2.0) +
                                 pow(min(dFieldYm[i][j][k], 0.0), 2.0) +
                                 pow(min(dFieldZm[i][j][k], 0.0), 2.0));
        upwindBG[i][j][k] = sqrt(pow(max(dFieldXm[i][j][k], 0.0), 2.0) +
                                 pow(max(dFieldYm[i][j][k], 0.0), 2.0) +
                                 pow(max(dFieldZm[i][j][k], 0.0), 2.0) +
                                 pow(min(dFieldXp[i][j][k], 0.0), 2.0) +
                                 pow(min(dFieldYp[i][j][k], 0.0), 2.0) +
                                 pow(min(dFieldZp[i][j][k], 0.0), 2.0));
      }
    }
  }
}

void multiplyField(scalarField3D& field1, scalarField3D& field2, scalarField3D& nField){
  for (int i=0;i<field1.size();i++){
    for (int j=0;j<field1[0].size();j++){
      for (int k=0;k<field1[0][0].size();k++){
        nField[i][j][k] = field1[i][j][k]*field2[i][j][k];
      }
    }
  }
}

void multiplyField(scalarField3D& field1, double factor, scalarField3D& nField){
  for (int i=0;i<field1.size();i++){
    for (int j=0;j<field1[0].size();j++){
      for (int k=0;k<field1[0][0].size();k++){
        nField[i][j][k] = field1[i][j][k]*factor;
      }
    }
  }
}

void smoothField(scalarField3D& field, scalarField3D& nField, double weighting, bool disableZeros){

  double dFmmm;
  double dFcmm;
  double dFpmm;
  double dFmcm;
  double dFccm;
  double dFpcm;
  double dFmpm;
  double dFcpm;
  double dFppm;

  double dFmmc;
  double dFcmc;
  double dFpmc;
  double dFmcc;
  double dFccc;
  double dFpcc;
  double dFmpc;
  double dFcpc;
  double dFppc;

  double dFmmp;
  double dFcmp;
  double dFpmp;
  double dFmcp;
  double dFccp;
  double dFpcp;
  double dFmpp;
  double dFcpp;
  double dFppp;

  int im, jm, km, ip, jp, kp;

  double invWeighting = (1.0 - weighting)/26.0;

  int xDim = field.size();
  int yDim = field[0].size();
  int zDim = field[0][0].size();

  for (int i=0;i<xDim;i++){
    for (int j=0;j<yDim;j++){
      for (int k=0;k<zDim;k++){

        if (disableZeros == true){
          if (fabs(field[i][j][k]) < FIELDSMALL)
            continue;
        }

        im = i - 1;
        if (im < 0){
          im = 0;
        }
        jm = j - 1;
        if (jm < 0){
          jm = 0;
        }
        km = k - 1;
        if (km < 0){
          km = 0;
        }

        ip = i + 1;
        if (ip == xDim){
          ip = i;
        }
        jp = j + 1;
        if (jp == yDim){
          jp = j;
        }
        kp = k + 1;
        if (kp == zDim){
          kp = k;
        }

        dFmmm = field[im][jm][km];
        dFcmm = field[i][jm][km];
        dFpmm = field[ip][jm][km];
        dFmcm = field[im][j][km];
        dFccm = field[i][j][km];
        dFpcm = field[ip][j][km];
        dFmpm = field[im][jp][km];
        dFcpm = field[i][jp][km];
        dFppm = field[ip][jp][km];

        dFmmc = field[im][jm][k];
        dFcmc = field[i][jm][k];
        dFpmc = field[ip][jm][k];
        dFmcc = field[im][j][k];
        dFccc = field[i][j][k];
        dFpcc = field[ip][j][k];
        dFmpc = field[im][jp][k];
        dFcpc = field[i][jp][k];
        dFppc = field[ip][jp][k];

        dFmmp = field[im][jm][kp];
        dFcmp = field[i][jm][kp];
        dFpmp = field[ip][jm][kp];
        dFmcp = field[im][j][kp];
        dFccp = field[i][j][kp];
        dFpcp = field[ip][j][kp];
        dFmpp = field[im][jp][kp];
        dFcpp = field[i][jp][kp];
        dFppp = field[ip][jp][kp];

        nField[i][j][k] = weighting*dFccc+invWeighting*(dFmmm+dFcmm+dFpmm +
                                                        dFmcm+dFccm+dFpcm +
                                                        dFmpm+dFcpm+dFppm +
                                                        dFmmc+dFcmc+dFpmc +
                                                        dFmcc+dFpcc +
                                                        dFmpc+dFcpc+dFppc +
                                                        dFmmp+dFcmp+dFpmp +
                                                        dFmcp+dFccp+dFpcp +
                                                        dFmpp+dFcpp+dFppp);
      }
    }
  }
}

void copyField(scalarField3D& field, scalarField3D& nField){
  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        nField[i][j][k] = field[i][j][k];
      }
    }
  }
}

void absField(scalarField3D& field, scalarField3D& nField){
  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        nField[i][j][k] = fabs(field[i][j][k]);
      }
    }
  }
}

scalarField3D absField(scalarField3D& field){
  scalarField3D nField = initialiseField(field.size(), field[0].size(), field[0][0].size(), 0.0);
  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        nField[i][j][k] = fabs(field[i][j][k]);
      }
    }
  }

  return nField;
}

void posField(scalarField3D& field, scalarField3D& nField){
  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        if (field[i][j][k] > 0.0){
          nField[i][j][k] = field[i][j][k];
        }
        else {
          nField[i][j][k] = 0.0;
        }
      }
    }
  }
}

void negField(scalarField3D& field, scalarField3D& nField){
  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        if (field[i][j][k] < 0.0){
          nField[i][j][k] = field[i][j][k];
        }
        else {
          nField[i][j][k] = 0.0;
        }
      }
    }
  }
}

void addField(scalarField3D& field, scalarField3D& vField, scalarField3D& nField, double nFactor){

  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        nField[i][j][k] = field[i][j][k] + vField[i][j][k] * nFactor;
      }
    }
  }
}

void addField(scalarField3D& field, double constant, scalarField3D& nField){

  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        nField[i][j][k] = field[i][j][k] + constant;
      }
    }
  }
}

scalarField3D maximum(scalarField3D field1, scalarField3D field2){
  scalarField3D nfield = field1;
  for (int i=0;i<field1.size();i++){
    for (int j=0;j<field1[0].size();j++){
      for (int k=0;k<field1[0][0].size();k++){
        if (field2[i][j][k] > field1[i][j][k]){
          nfield[i][j][k] = field2[i][j][k];
        }
      }
    }
  }
  return nfield;
}

scalarField3D minimum(scalarField3D field1, scalarField3D field2){
  scalarField3D nfield = field1;
  for (int i=0;i<field1.size();i++){
    for (int j=0;j<field1[0].size();j++){
      for (int k=0;k<field1[0][0].size();k++){
        if (field2[i][j][k] < field1[i][j][k]){
          nfield[i][j][k] = field2[i][j][k];
        }
      }
    }
  }
  return nfield;
}

scalarField3D maximum(scalarField3D field1, double val2){
  scalarField3D nfield = field1;
  for (int i=0;i<field1.size();i++){
    for (int j=0;j<field1[0].size();j++){
      for (int k=0;k<field1[0][0].size();k++){
        if (val2 > field1[i][j][k]){
          nfield[i][j][k] = val2;
        }
      }
    }
  }
  return nfield;
}

scalarField3D minimum(scalarField3D field1, double val2){
  scalarField3D nfield = field1;
  for (int i=0;i<field1.size();i++){
    for (int j=0;j<field1[0].size();j++){
      for (int k=0;k<field1[0][0].size();k++){
        if (val2 < field1[i][j][k]){
          nfield[i][j][k] = val2;
        }
      }
    }
  }
  return nfield;
}

double sum(scalarField3D field){
  double sumVal = 0.0;
  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        sumVal += field[i][j][k];
      }
    }
  }
  return sumVal;
}

double max(scalarField3D& field){
  double maxFieldVal = numeric_limits<double>::min();
  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        if (maxFieldVal < field[i][j][k]){
          maxFieldVal = field[i][j][k];
        }
      }
    }
  }
  return maxFieldVal;
}

double min(scalarField3D& field){
  double minFieldVal = numeric_limits<double>::max();
  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        if (minFieldVal > field[i][j][k]){
          minFieldVal = field[i][j][k];
        }
      }
    }
  }
  return minFieldVal;
}

void dotField(scalarField3D& dFieldX, scalarField3D& dFieldY, scalarField3D& dFieldZ, scalarField3D& tField){
  for (int i=0;i<dFieldX.size();i++){
    for (int j=0;j<dFieldX[0].size();j++){
      for (int k=0;k<dFieldX[0][0].size();k++){
        tField[i][j][k] = dFieldX[i][j][k]*dFieldX[i][j][k] + dFieldY[i][j][k]*dFieldY[i][j][k] + dFieldZ[i][j][k]*dFieldZ[i][j][k];
      }
    }
  }
}

void sqrtField(scalarField3D& field, scalarField3D& nField){
  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        nField[i][j][k] = sqrt(field[i][j][k]);
      }
    }
  }
}

void divideField(scalarField3D& field, scalarField3D& dField, scalarField3D& nField){

  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        nField[i][j][k] = field[i][j][k]/(dField[i][j][k]+1e-10);
      }
    }
  }
}

void centralDifferenceField(scalarField3D& field, scalarField3D& dFieldX, scalarField3D& dFieldY, scalarField3D& dFieldZ, double hx, double hy, double hz){

  double dFXm;
  double dFXp;
  double dFYm;
  double dFYp;
  double dFZm;
  double dFZp;

  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){

        dFXm = field[i][j][k];
        dFXp = field[i][j][k];
        dFYm = field[i][j][k];
        dFYp = field[i][j][k];
        dFZm = field[i][j][k];
        dFZp = field[i][j][k];

        if ((i+1 < field.size())){
          dFXp = field[i+1][j][k];
        }
        if ((i > 0)){
          dFXm = field[i-1][j][k];
        }
        if ((j+1 < field[0].size())){
          dFYp = field[i][j+1][k];
        }
        if ((j > 0)){
          dFYm = field[i][j-1][k];
        }
        if ((k+1 < field[0][0].size())){
          dFZp = field[i][j][k+1];
        }
        if ((k > 0)){
          dFZm = field[i][j][k-1];
        }

        dFieldX[i][j][k] = (dFXp - dFXm)/(2.0*hx);
        dFieldY[i][j][k] = (dFYp - dFYm)/(2.0*hy);
        dFieldZ[i][j][k] = (dFZp - dFZm)/(2.0*hz);
      }
    }
  }
}


void forwardDifferenceField(scalarField3D& field, scalarField3D& dFieldX, scalarField3D& dFieldY, scalarField3D& dFieldZ, double hx, double hy, double hz){

  double dFXp;
  double dFYp;
  double dFZp;

  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){

        dFXp = field[i][j][k];
        dFYp = field[i][j][k];
        dFZp = field[i][j][k];

        if ((i+1 < field.size())){
          dFXp = field[i+1][j][k];
        }
        if ((j+1 < field[0].size())){
          dFYp = field[i][j+1][k];
        }
        if ((k+1 < field[0][0].size())){
          dFZp = field[i][j][k+1];
        }

        dFieldX[i][j][k] = (dFXp - field[i][j][k])/hx;
        dFieldY[i][j][k] = (dFYp - field[i][j][k])/hy;
        dFieldZ[i][j][k] = (dFZp - field[i][j][k])/hz;
      }
    }
  }
}

void backwardDifferenceField(scalarField3D& field, scalarField3D& dFieldX, scalarField3D& dFieldY, scalarField3D& dFieldZ, double hx, double hy, double hz){

  double dFXm;
  double dFYm;
  double dFZm;

  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){

        dFXm = field[i][j][k];
        dFYm = field[i][j][k];
        dFZm = field[i][j][k];

        if ((i > 0)){
          dFXm = field[i-1][j][k];
        }
        if ((j > 0)){
          dFYm = field[i][j-1][k];
        }
        if ((k > 0)){
          dFZm = field[i][j][k-1];
        }

        dFieldX[i][j][k] = (field[i][j][k] - dFXm)/hx;
        dFieldY[i][j][k] = (field[i][j][k] - dFYm)/hy;
        dFieldZ[i][j][k] = (field[i][j][k] - dFZm)/hz;
      }
    }
  }
}

scalarField3D pow(scalarField3D field, double power){
  scalarField3D nfield = field;
  for (int i=0;i<field.size();i++){
    for (int j=0;j<field[0].size();j++){
      for (int k=0;k<field[0][0].size();k++){
        nfield[i][j][k] = pow(field[i][j][k], power);
      }
    }
  }
  return nfield;
}
