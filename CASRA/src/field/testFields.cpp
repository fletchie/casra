/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cmath>

#include "CASRA/field/scalarField.h"

scalarField3D buildRectangleSignField(int x, int y, int z){

  int xm = (int)x * 0.2;
  int ym = (int)x * 0.2;
  int zm = (int)x * 0.2;
  int xp = (int)x * 0.8;
  int yp = (int)x * 0.8;
  int zp = (int)x * 0.8;

  scalarField3D field = initialiseField(x, y, z, 0.0) - 1.0;

  for (int i=xm-1;i<xp;i++){
    for (int j=xm-1;j<xp;j++){
      for (int k=xm-1;k<xp;k++){
        field[i][j][k] = 1.0;
      }
    }
  }

  return field;

}

scalarField3D buildSphericalField(int x, int y, int z, double Rc){

  scalarField3D field = initialiseField(x, y, z, 0.0);

  double rad;
  double midx = (double)x/2.0;
  double midy = (double)y/2.0;
  double midz = (double)z/2.0;

  for (int i=0;i<x;i++){
    for (int j=0;j<y;j++){
      for (int k=0;k<z;k++){
        rad = sqrt(pow(i-midx, 2.0) + pow(j-midy, 2.0) + pow(k-midz, 2.0));
        field[i][j][k] = Rc-rad;
      }
    }
  }

  return field;
}
