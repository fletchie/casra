/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <string>
#include <iostream>
#include <math.h>

#include "CASRA/specimenModels/analyticModel.h"

using namespace std;

//define constants
#define PI 3.14159265359

/*!
 * Initialiser Function for detector plane in analytical evaporation models
 *
 * @param radius The detector radius
 * @param flightpath The instrument flightpath
 * @param efficiency The detection efficiency
 *
 */
AnalyticDetector::AnalyticDetector(double radius,
                                   double flightpath,
                                   double efficiency): radius{radius}, flightpath{flightpath}, efficiency{efficiency}{

}


EmitterModel::EmitterModel(){
  evapVol = 0.0;
}

/*!
 * parent class initialiser for AnalyticEmitterModels.
 * Presence enables future library extension to include other projection models
 * e.g. De Geuser et al. 2017, 10.1017/S1431927616012721
 * @param initRad The initial apex radius
 * @param initialPosition The initial apex position
 *
 */
AnalyticEmitterModel::AnalyticEmitterModel(double initRad,
                                           Point initialPosition) : R0{initRad}, apexCentre{initialPosition} {
  R = R0;
}

/*!
 * parent class initialiser for the BasEmitterModel class.
 * method outlined in Bas et al. 1995, 10.1016/0169-4332(94)00561-3
 * @param initRad The initial apex radius
 * @param initialPosition The initial apex position
 *
 */
BasEmitterModel::BasEmitterModel(vector<ICFTempPoint> imageCompressionFactor,
                                 double initRad,
                                 Point initialPosition) : AnalyticEmitterModel{initRad, initialPosition}, imagecompression{imageCompressionFactor} {

  this->dz = 0.0;
  this->ICF = imageCompressionFactor[0].ICF;
  this->ICFIt = 0;

}

/*!
 * class initialiser for the ShankBasEmitterModel class. Specifies a Bas emitter type.
 * model constrained under voltage reconstruction.
 * method outlined in Bas et al. 1995, 10.1016/0169-4332(94)00561-3
 * @param imageCompressionFactor vector of image compression factors at various positions in the detector event sequence
 * @param initRad The initial apex radius
 * @param initialPosition The initial apex position
 * @param sangles vector of shank angles at various positions in the detector event sequence
 *
 */
ShankBasEmitterModel::ShankBasEmitterModel(vector<ICFTempPoint> imageCompressionFactor,
                                           double initRad, Point initialPosition,
                                           vector<ShankTempPoint> sangles) : BasEmitterModel{imageCompressionFactor,
                                                                                             initRad, initialPosition}, shank{sangles} {
  cout << "Creating shankBasEmitterModel" << endl;
  this->shankIt = 0;
  this->alpha = sangles[0].alpha;

}

/*!
 * class initialiser for the VoltageBasEmitterModel class. Specifies a Bas emitter type.
 * model constrained under voltage reconstruction.
 * method outlined in Bas et al. 1995, 10.1016/0169-4332(94)00561-3
 * @param imageCompressionFactor vector of image compression factors at various positions in the detector event sequence
 * @param initRad The initial apex radius (value irrelevant due to VoltageBasEmitterModel::setInitialRadius)
 * @param initialPosition The initial apex position
 * @param kFactors vector of k-factors at various positions in the detector event sequence
 * @param bulkEvapField The evaporation field of the bulk material present
 *
 */
VoltageBasEmitterModel::VoltageBasEmitterModel(vector<ICFTempPoint> imageCompressionFactor,
                                               double initRad, Point initialPosition,
                                               vector<VoltageTempPoint> kFactors, double bulkEvapField) : BasEmitterModel{imageCompressionFactor,
                                                                                                                          initRad,
                                                                                                                          initialPosition}, kFactors{kFactors} {

  cout << "Creating voltageBasEmitterModel" << endl;
  this->kFactorIt = 0;
  this->kFactors = kFactors;
  this->kFactor = kFactors[0].kF;
  this->bulkEvapField = bulkEvapField;

}

/*!
 * Method for calculating the half-angle Field-Of-View (FOV) given the detector geometry
 * and current image compression.
 *
 * @param detector The detector plane
 */
void BasEmitterModel::calculateFOV(AnalyticDetector& detector){

  //rho - the distance between the projection centre within the detector plane, and the detected hit position
  //calculate FOV (section) - assuming linear projection
  //where has this equation even come from??
  this->thetaMax = atan(detector.radius/detector.flightpath);

  //invert stereographic projection from edge of detector - this projection
  //should serve to increase the actual FOV of the sample
  this->thetaMax = this->thetaMax + asin((this->ICF - 1.0)*sin(this->thetaMax));

  return;

}

/*!
 * Method for transfering an ion's detector position onto the emitter apex
 *
 * under the BAS pseudo-stereographic projection.
 * @param recIon The ranged detector event/ion being reconstructed
 * @param detector The detector plane
 * @param rNum The detector event reconstruction number
 */
void BasEmitterModel::detectorToCapTransfer(DetectionEvent& recIon, AnalyticDetector& detector, long unsigned rNum){

  if (this->ICFIt < this->imagecompression.size() - 1) {
    if (!(rNum < this->imagecompression[this->ICFIt+1].eventIndex)) {
      this->ICFIt += 1;
      this->ICF = this->imagecompression[this->ICFIt].ICF;
    }
  }

  double rmod = sqrt(pow(recIon.dx, 2) + pow(recIon.dy, 2));
  recIon.theta = atan2(rmod, detector.flightpath);
  recIon.psi = recIon.theta + asin((this->ICF-1.0) * sin(recIon.theta)); // accounts for the general stereographic projection
  recIon.phi = atan2(recIon.dy, recIon.dx);

  return;

}

/*!
 * Method for shifting the emitter apex downwards according to the volume taken up by the reconstructed ion.
 *
 * @param recIon The ranged detector event/ion being reconstructed
 * @param detector The detector plane
 */
void BasEmitterModel::capShift(DetectionEvent& recIon, AnalyticDetector& detector){

  double area = PI*(pow(this->R * sin(this->thetaMax), 2.0));
  double dshift = (recIon.dv/(detector.efficiency * area));
  this->evapVol += recIon.dv;
  this->apexCentre.pz = this->apexCentre.pz - dshift;
  this->dz = dshift;

  return;

}

/*!
 * Method for increasing the emitter apex due to blunting under evaporation.
 * Radius increase constrained by shank angle.
 *
 * @param recIon The ranged detector event/ion being reconstructed
 * @param rNum The current detection event reconstruction count
 */
void ShankBasEmitterModel::evolveRadius(DetectionEvent& rIon, long unsigned rNum){

  if (this->shankIt < this->shank.size() - 1) {
    if (!(rNum < this->shank[this->shankIt+1].eventIndex)) {
      this->shankIt += 1;
      this->alpha = this->shank[this->shankIt].alpha;
    }
  }

  //shift hemisphere down truncated cone defined by (R, alpha)
  double prevR = this->R;
  this->R = this->R + this->dz * sin(this->alpha)/(1.0 - sin(this->alpha));

  //correct apex depth position for radius increase
  this->apexCentre.pz -= (this->R - prevR);

  return;

}

/*!
 * Method for setting the initial apex radius according to the voltage curve.
 * Applies the equation R = V/(kf * F_0) where V is the pulse voltage,
 * kf is the experiment-dependent k-factor
 * and F_0 is the bulk material evaporation field.
 *
 * @param recIon The ranged detector event/ion being reconstructed
 */
void VoltageBasEmitterModel::setInitialRadius(DetectionEvent& rIon){

  //surface field relationship: R = V/k*E
  double prevR = this->R;
  this->R = (rIon.dcVoltage+rIon.pulseVoltage)/(this->kFactor * this->bulkEvapField);

  //correct apex depth position for radius increase
  this->apexCentre.pz -= this->R;

  return;

}

/*!
 * Method for increasing the emitter apex due to blunting under evaporation.
 * Radius increase constrained the voltage curve.
 * Applies the equation R = V/(kf * F_0)  where V is the pulse voltage,
 * kf is the experiment-dependent k-factor
 * and F_0 is the bulk material evaporation field.
 *
 * @param recIon The ranged detector event/ion being reconstructed
 * @param rNum The current detection event reconstruction count
 */
void VoltageBasEmitterModel::evolveRadius(DetectionEvent& rIon, long unsigned rNum){

  if (this->kFactorIt < this->kFactors.size() - 1) {
    if (!(rNum < this->kFactors[this->kFactorIt+1].eventIndex)) {
      this->kFactorIt += 1;
      this->kFactor = this->kFactors[this->kFactorIt].kF;
    }
  }

  //surface field relationship: R = V/k*E
  double prevR = this->R;
  this->R = (rIon.dcVoltage+rIon.pulseVoltage)/(this->kFactor * this->bulkEvapField);

  //correct apex depth position for radius increase
  this->apexCentre.pz -= (this->R - prevR);

  return;

}

/*!
 * Method for converting an ion's spherical coordinates on the apex
 * into Cartesian coordinates within the reconstruction space.
 *
 * @param recIon The ranged detector event/ion being reconstructed
 */
void AnalyticEmitterModel::reconPlacement(DetectionEvent& rIon){

  rIon.sx = R * cos(rIon.phi) * sin(rIon.psi) + apexCentre.px;
  rIon.sy = R * sin(rIon.phi) * sin(rIon.psi) + apexCentre.py;
  rIon.sz = R * cos(rIon.psi) + apexCentre.pz;

  return;

}
