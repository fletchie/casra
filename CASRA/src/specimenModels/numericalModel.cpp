/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cmath>

#include "CASRA/specimenModels/numericalModel.h"
#include "CASRA/mesh/triangularMesh.h"

using namespace std;

Phase::Phase(AhhreniusLawIsotropic law, string name): name{name} {
  this->setEvapLawMode = 1;
  this->ALI = law;
}

Phase::Phase(LinearLawIsotropic law, string name): name{name} {
  this->setEvapLawMode = 2;
  this->LLI = law;
}

Phase::Phase(DeformedAhhreniusLawIsotropic law, string name): name{name} {
  this->setEvapLawMode = 3;
  this->DALI = law;
}

Phase::Phase(CurvatureLawIsotropic law, string name): name{name} {
  this->setEvapLawMode = 4;
  this->CLI = law;
}

/*!
 * Phase Method for adding a phase boundary to a Phase instance, performing
 * Topology checks.
 *
 * @param phaseBoundary The phase boundary
 * @return void
 */
void Phase::addPhaseBoundary(TriangularMesh phaseBoundary){

  int waterTightTest = phaseBoundary.checkWatertight();

  if (waterTightTest != 0){
    cerr << "\t\tWARNING: loaded phase mesh not closed." << endl;
  }

  int manifoldTest = phaseBoundary.checkManifold();

  if (manifoldTest != 0){
    cerr << "\t\tWARNING: loaded phase mesh not a manifold. This is expected if mesh contains voids/cavities." << endl;
  }

  this->phaseBoundary = phaseBoundary;

  return;
}

/*!
 * TMSpecimen Method for appending a phase to the specimen model
 *
 * @param phase The phase to be appended
 */
void TMSpecimen::appendPhase(Phase phase){
  this->phases.push_back(phase);
}

/*!
 * TMSpecimen Method for appending a phase (passed as a boundary mesh) to the specimen model
 *
 * @param surfaceMesh The phase mesh to be appended
 */
void TMSpecimen::appendPhase(TriangularMesh surfaceMesh){
  Phase phase;
  phase.phaseBoundary = surfaceMesh;
  this->phases.push_back(phase);
}
