/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <string>
#include <iostream>

#include "CASRA/experimentalData/experimentalData.h"
#include "CASRA/io/rangeReader.h"

using namespace std;


ShankTempPoint::ShankTempPoint(long eventIndex, double angle) : eventIndex{eventIndex}, alpha{angle} {

}

ICFTempPoint::ICFTempPoint(long eventIndex, double icf) : eventIndex{eventIndex}, ICF{icf} {

}

VoltageTempPoint::VoltageTempPoint(long eventIndex, double kF) : eventIndex{eventIndex}, kF{kF} {

}

DetectionEvent::DetectionEvent() {
  this->isRanged = false;
}

//setter method
void APTDataSet::addDetectionEvent(DetectionEvent& newEvent){
  events.push_back(newEvent);
}

/*!
 * Method for performing a simple (naive) ranging of the ions in APTDataSet.
 * This method assigns the ion the final ranged peak within which its m-c ratio
 * is contained. This simple method only assigns each Ion within APTDataSet
 * a single peak. Also labels ion as being ranged.
 *
 * @param rData Referenced RangeData instance with which to reference the APTDataSet instance.
 *
 */
void APTDataSet::simpleRangeDataSet(RangeData& rData){

  //if range data source file exists e.g. not empty
  cout << "Ranging APTDataSet..." << endl;
  cout << "\tRangetype: Simple singular peak assignment to ions (e.g. non probabilistic assignment)." << endl;

  this->rData = rData;

  double lower;
  double upper;

  //Possible TODO: Incorporate ability to record peak overlaps from data ranging
  const int peakCount = rData.peaks.size();
  const int eventNumber = this->events.size();
  for (int b=0;b<peakCount;b++) {
    lower = rData.peaks[b].lower;
    upper = rData.peaks[b].upper;
    for (long a=0;a<eventNumber;a++) {
      if ((this->events[a].m2c >= lower) && (this->events[a].m2c < upper)) {
        this->events[a].isRanged = true;
        this->events[a].rangedPeaks.clear();
        this->events[a].rangedPeaksWeights.clear();
        this->events[a].rangedPeaks.push_back(&rData.peaks[b]);
        this->events[a].rangedPeaksWeights.push_back(1.0);
        this->events[a].dv = rData.peaks[b].ionicVol;
      }
    }
  }

  if (rData.peaks.size() > 0){
    this->ranged = true;
    cout << "\tAPTDataSet ranging complete." << endl;
  }
  else {
    this->ranged = false;
    cout << "No peaks in dataset. Data left unranged." << endl;
  }
}

APTDataSet::APTDataSet(){
  this->ranged = false;
  cout << "Created APTDataSet" << endl;
}

RangeData::RangeData(){

}

string RangeData::constructFormula(RangedPeak *rPeak){

  string formula = "";

  for (unsigned int a=0;a<rPeak->comp.size();a++) {
    string el = this->species[rPeak->comp[a]].speciesName;
    formula += el;
  }
  return formula;
}

unsigned int RangeData::findElement(string element){

  for (unsigned int a=0;a<this->species.size();a++) {
    if (this->species[a].speciesName == element) {
      return a;
    }
  }

  cout << "Element undefined. Exiting." << endl;
  exit(1);
}

void APTDataSet::filterExperimentDataOutsideFOV(double detectorRadius,
                                                vector<ICFTempPoint>& icfVals,
                                                vector<ShankTempPoint>& shankVals,
                                                vector<VoltageTempPoint>& voltageVals){

  double detectorRadiusSqrd = detectorRadius * detectorRadius;

  int ic = -1;
  int lp = 0;

  vector<ICFTempPoint>::iterator icfIt = icfVals.begin();
  vector<ShankTempPoint>::iterator shankIt = shankVals.begin();
  vector<VoltageTempPoint>::iterator voltageIt = voltageVals.begin();
  vector<DetectionEvent> filteredEvents;
  for (int i=0;i<this->events.size();i++){
    if (this->events[i].dx*this->events[i].dx + this->events[i].dy*this->events[i].dy <= detectorRadiusSqrd){
      filteredEvents.push_back(this->events[i]);
      ic++;
    }
    if (icfIt != icfVals.end()){
      if (icfIt->eventIndex == i){
        icfIt->eventIndex = ic;
        ++icfIt;
      }
    }
    if (shankIt != shankVals.end()){
      if (shankIt->eventIndex == i){
        shankIt->eventIndex = ic;
        ++shankIt;
      }
    }
    if (voltageIt != voltageVals.end()){
      if (voltageIt->eventIndex == i){
        voltageIt->eventIndex = ic;
        ++voltageIt;
      }
    }
  }

  this->events = filteredEvents;

  return;
}
