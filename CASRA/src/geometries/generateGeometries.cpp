/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <math.h>
#include <tuple>

#include "CASRA/base/vectorMath.h"
#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/mesh/marchingCubes.h"
#include "CASRA/geometries/generateGeometries.h"
#include "QuickHull.hpp"
#include "CASRA/io/modelDataio.h"

using namespace std;
using namespace quickhull;

/*!
 * Function for constructing a tip shaped mesh geometry
 * @param R0 The tip apex radius
 * @param R1 The tip base radius
 * @param length The tip length
 * @param offset The offset between the spherical cap and shank
 * @return int errorcode
 */
tuple<TriangularMesh, int> buildTipMesh(double R0, double R1, double length, double offset){

  vector<double> R;
  vector<double> Z;
  vector<Vector3<float>> points;

  double dr = fabs(R1 - R0);

  //generate point cloud representing the specimen tip surface
  for (int a=0;a<100;a++){
    R.push_back(R0 + (R1 - R0) * a/(99));
    Z.push_back(-(R[a] - R0) * length/dr);
  }

  //construct cone
  for (int r=0;r<R.size();r++){
    for (int a=0;a<50;a++){
      double ang = 2.0 * PI * a/50.0;
      float x = R[r] * cos(ang);
      float y = R[r] * sin(ang);
      float z = Z[r];

      Vector3<float> tp(x, y, z);
      points.push_back(tp);
    }
  }

  double dz = (maximum(Z) - minimum(Z)) * pow(maximum(R)/minimum(R) - 1.0, -1.0);
  double shankAngle = atan2(minimum(R), dz) * 180.0/PI;
  double joinAngle = 90.0 - 180.0/PI * atan2(R1, offset);
  cout << "Shank angle: " << shankAngle << endl;
  cout << "Join angle: " << joinAngle << endl;

  //construct cap
  for (int t=0;t<50;t++){
    for (int p=0;p<50;p++){
      double theta = t*2.0*PI/50;
      double phi = p*2.0*PI/50;
      //cout << theta << " " << phi << endl;
      float x = pow((pow(R0, 2.0) + pow(offset, 2.0)), 0.5) * cos(theta) * sin(phi);
      float y = pow((pow(R0, 2.0) + pow(offset, 2.0)), 0.5) * sin(theta) * sin(phi);
      float z = pow((pow(R0, 2.0) + pow(offset, 2.0)), 0.5) * cos(phi) + Z[0] - offset;

      Vector3<float> tp(x, y, z);

      if (z > Z[0]){
       points.push_back(tp);
      }
    }
  }

  vector<double> pts;
  for (int i=0;i<points.size();i++){
    pts.push_back(points[i].z);
  }

  double mz = minimum(pts);

  for (int i=0;i<points.size();i++){
    points[i].z -= mz;
  }

  //rescale mesh for convex hull fitting
  double maxValue = numeric_limits<double>::min();
  for (int i=0;i<points.size();i++){
    if (points[i].x > maxValue){
      maxValue = points[i].x;
    }
    if (points[i].y > maxValue){
      maxValue = points[i].y;
    }
  }
  for (int i=0;i<points.size();i++){
    points[i].x /= maxValue;
    points[i].y /= maxValue;
    points[i].z /= maxValue;
  }

  //perform convex hull algorithm on point cloud in points via quickhull
  QuickHull<float> qh;
  auto hull = qh.getConvexHull(points, true, false);
  const auto vertices = hull.getVertexBuffer();
  const auto indices = hull.getIndexBuffer();

  //define output mesh
  TriangularMesh outMesh;

  //define triangle panel points list
  vector<vector<vector<double>>> trilist;

  //decimal places to round vertices to - required for leveraging the hash map in 'unpackUniqueVertices'
  int dp = 10;

  for (int f=0;f<indices.size()/3;f++){

    //add triangle panels of generated convex hull mesh to vector
    trilist.push_back({{round_up(vertices[indices[3*f]].x, dp), round_up(vertices[indices[3*f]].y, dp), round_up(vertices[indices[3*f]].z, dp)},
                       {round_up(vertices[indices[3*f+1]].x, dp), round_up(vertices[indices[3*f+1]].y, dp), round_up(vertices[indices[3*f+1]].z, dp)},
                       {round_up(vertices[indices[3*f+2]].x, dp), round_up(vertices[indices[3*f+2]].y, dp), round_up(vertices[indices[3*f+2]].z, dp)}});

  }

  //unpack unique vertices via hash map
  unpackUniqueVertices(trilist, &outMesh);

  //reverse earlier rescaling
  outMesh.rescaleMesh(Point(0.0, 0.0, 0.0), maxValue);

  tuple<TriangularMesh, int> retData = make_tuple(outMesh, 0);

  cout << "Ran to completion" << endl;

  return retData;

}

/*!
 * Function for constructing a spherical mesh
 * e.g. for defining a precipitate phase
 * @param R The spherical mesh radius
 * @param cp The sphere centre position
 * @param length The tip length
 * @return int errorcode
 */
tuple<TriangularMesh, int> buildSphere(double R, Point cp, int n){

  vector<Vector3<float>> points;

  //generate point cloud representing the sphere
  for (int a=0;a<n;a++){
    for (int b=0;b<(int)(n/2);b++){
      double phi = 2.0 * PI * a/n;
      double theta = 2.0 * PI * b/n;
      float x = R * cos(phi) * sin(theta) + cp.px;
      float y = R * sin(phi) * sin(theta) + cp.py;
      float z = R * cos(theta) + cp.pz;

      Vector3<float> tp(x, y, z);
      points.push_back(tp);
    }
  }

  //rescale mesh for convex hull fitting
  double maxValue = numeric_limits<double>::min();
  for (int i=0;i<points.size();i++){
    if (points[i].x > maxValue){
      maxValue = points[i].x;
    }
    if (points[i].y > maxValue){
      maxValue = points[i].y;
    }
  }
  for (int i=0;i<points.size();i++){
    points[i].x /= maxValue;
    points[i].y /= maxValue;
    points[i].z /= maxValue;
  }

  //perform convex hull algorithm on point cloud in points via quickhull
  QuickHull<float> qh;
  auto hull = qh.getConvexHull(points, true, false);
  const auto vertices = hull.getVertexBuffer();
  const auto indices = hull.getIndexBuffer();

  //define output mesh
  TriangularMesh outMesh;

  //define triangle panel points list
  vector<vector<vector<double>>> trilist;

  //decimal places to round vertices to - required for leveraging the hash map in 'unpackUniqueVertices'
  int dp = 10;

  for (int f=0;f<indices.size()/3;f++){

    //add triangle panels of generated convex hull mesh to vector
    trilist.push_back({{round_up(vertices[indices[3*f]].x, dp), round_up(vertices[indices[3*f]].y, dp), round_up(vertices[indices[3*f]].z, dp)},
                       {round_up(vertices[indices[3*f+1]].x, dp), round_up(vertices[indices[3*f+1]].y, dp), round_up(vertices[indices[3*f+1]].z, dp)},
                       {round_up(vertices[indices[3*f+2]].x, dp), round_up(vertices[indices[3*f+2]].y, dp), round_up(vertices[indices[3*f+2]].z, dp)}});

  }

  //unpack unique vertices via hash map
  unpackUniqueVertices(trilist, &outMesh);

  //reverse earlier rescaling
  outMesh.rescaleMesh(Point(0.0, 0.0, 0.0), maxValue);

  tuple<TriangularMesh, int> retData = make_tuple(outMesh, 0);

  return retData;

}


/*!
 * Function for constructing a spherical mesh
 * e.g. for defining a precipitate phase
 * @param R The spherical mesh radius
 * @param cp The sphere centre position
 * @param length The tip length
 * @return int errorcode
 */
tuple<TriangularMesh, int> buildUpperShell(double outerRadius, double innerRadius, Point cp, int n){

  vector<Vector3<float>> outerShellPoints;
  vector<Vector3<float>> innerShellPoints;
  vector<Point> basePoints;
  vector<Face> baseFaces;

  //generate point cloud representing the sphere
  int mmax = (int)(n/4);
  int maxPoints = n * (mmax+1);
  int pc = 0;
  double minz = 0.0;
  for (int b=0;b<=mmax;b++){
    for (int a=0;a<n;a++){
      double phi = 2.0 * PI * a/n;
      double theta = 2.0 * PI * 0.25 * b/mmax;
      float x = outerRadius * cos(phi) * sin(theta) + cp.px;
      float y = outerRadius * sin(phi) * sin(theta) + cp.py;
      float z = outerRadius * cos(theta) + cp.pz;

      Vector3<float> tp(x, y, z);
      outerShellPoints.push_back(tp);

      if (b == mmax){
        basePoints.push_back(Point(x, y, z));
      }
      x = innerRadius * cos(phi) * sin(theta) + cp.px;
      y = innerRadius * sin(phi) * sin(theta) + cp.py;
      z = innerRadius * cos(theta) + cp.pz;

      Vector3<float> tp2(x, y, z);
      innerShellPoints.push_back(tp2);

      if (b == mmax){
        basePoints.push_back(Point(x, y, z));
        minz = z;
      }

      if (b == mmax){
        baseFaces.push_back(Face((pc + 0) % (2*n), (pc + 1) % (2*n), (pc + 2) % (2*n)));
        baseFaces.push_back(Face((pc + 1) % (2*n), (pc + 2) % (2*n), (pc + 3) % (2*n)));
        pc += 1;
        pc += 1;
      }
    }
  }

  //rescale mesh for convex hull fitting
  double maxValue = numeric_limits<double>::min();
  for (int i=0;i<outerShellPoints.size();i++){
    if (outerShellPoints[i].x > maxValue){
      maxValue = outerShellPoints[i].x;
    }
    if (outerShellPoints[i].y > maxValue){
      maxValue = outerShellPoints[i].y;
    }
  }
  for (int i=0;i<outerShellPoints.size();i++){
    outerShellPoints[i].x /= maxValue;
    outerShellPoints[i].y /= maxValue;
    outerShellPoints[i].z /= maxValue;
  }

  for (int i=0;i<innerShellPoints.size();i++){
    innerShellPoints[i].x /= maxValue;
    innerShellPoints[i].y /= maxValue;
    innerShellPoints[i].z /= maxValue;
  }

  for (int i=0;i<basePoints.size();i++){
    basePoints[i].px /= maxValue;
    basePoints[i].py /= maxValue;
    basePoints[i].pz /= maxValue;
  }
  minz /= maxValue;

  //define output mesh
  TriangularMesh outMesh;

  //define triangle panel points list
  vector<vector<vector<double>>> trilist;

  //decimal places to round vertices to - required for leveraging the hash map in 'unpackUniqueVertices'
  int dp = 4;

  //perform convex hull algorithm on point cloud in points via quickhull
  QuickHull<float> qh;
  auto hull = qh.getConvexHull(outerShellPoints, true, false);
  const auto vertices = hull.getVertexBuffer();
  const auto indices = hull.getIndexBuffer();

  for (int f=0;f<indices.size()/3;f++){

    double cpz = (vertices[indices[3*f+0]].z + vertices[indices[3*f+1]].z + vertices[indices[3*f+2]].z)/3.0;
    if (cpz > minz+1e-4){
      //add triangle panels of generated convex hull mesh to vector
      trilist.push_back({{round_up(vertices[indices[3*f]].x, dp), round_up(vertices[indices[3*f]].y, dp), round_up(vertices[indices[3*f]].z, dp)},
                         {round_up(vertices[indices[3*f+1]].x, dp), round_up(vertices[indices[3*f+1]].y, dp), round_up(vertices[indices[3*f+1]].z, dp)},
                         {round_up(vertices[indices[3*f+2]].x, dp), round_up(vertices[indices[3*f+2]].y, dp), round_up(vertices[indices[3*f+2]].z, dp)}});
    }
  }

  QuickHull<float> qh2;
  auto hull2 = qh2.getConvexHull(innerShellPoints, true, false);
  const auto vertices2 = hull2.getVertexBuffer();
  const auto indices2 = hull2.getIndexBuffer();

  //decimal places to round vertices to - required for leveraging the hash map in 'unpackUniqueVertices'

  for (int f=0;f<indices2.size()/3;f++){

    double cpz = (vertices2[indices2[3*f+0]].z + vertices2[indices2[3*f+1]].z + vertices2[indices2[3*f+2]].z)/3.0;
    //add triangle panels of generated convex hull mesh to vector
    if (cpz > minz+1e-4){
      trilist.push_back({{round_up(vertices2[indices2[3*f]].x, dp), round_up(vertices2[indices2[3*f]].y, dp), round_up(vertices2[indices2[3*f]].z, dp)},
                         {round_up(vertices2[indices2[3*f+1]].x, dp), round_up(vertices2[indices2[3*f+1]].y, dp), round_up(vertices2[indices2[3*f+1]].z, dp)},
                         {round_up(vertices2[indices2[3*f+2]].x, dp), round_up(vertices2[indices2[3*f+2]].y, dp), round_up(vertices2[indices2[3*f+2]].z, dp)}});
    }

  }

  for (int f=0;f<baseFaces.size();f++){

    //add triangle panels of generated convex hull mesh to vector
    trilist.push_back({{round_up(basePoints[baseFaces[f].p1].px, dp), round_up(basePoints[baseFaces[f].p1].py, dp), round_up(basePoints[baseFaces[f].p1].pz, dp)},
                       {round_up(basePoints[baseFaces[f].p2].px, dp), round_up(basePoints[baseFaces[f].p2].py, dp), round_up(basePoints[baseFaces[f].p2].pz, dp)},
                       {round_up(basePoints[baseFaces[f].p3].px, dp),round_up(basePoints[baseFaces[f].p3].py, dp), round_up(basePoints[baseFaces[f].p3].pz, dp)}});
  }

  //unpack unique vertices via hash map
  unpackUniqueVertices(trilist, &outMesh);

  //reverse earlier rescaling
  outMesh.rescaleMesh(Point(0.0, 0.0, 0.0), maxValue);

  tuple<TriangularMesh, int> retData = make_tuple(outMesh, 0);

  return retData;

}


/*!
 * Function for creating a cuboidal phase geometry
 *
 *     P7_____ P8
 *      /___ /|
 *   P5| |  |P6
 *     P3|__|_| P4
 *     |/___|/
 *   P1     P2
 *
 * @param P1 bottom left hand corner point cuboid
 * @param P2 cuboid base point 2 (going anticlockwise)
 * @param P3 cuboid base point 3 (going anticlockwise)
 * @param P4 cuboid base point 4 (going anticlockwise)
 * @param P5 cuboid top point 1 (going anticlockwise)
 * @param P6 cuboid top point 2 (going anticlockwise)
 * @param P7 cuboid top point 3 (going anticlockwise)
 * @param P8 top right hand corner point of cuboid
 *
 * @param return tuple containing the cuboidal phase mesh and execution status
 */
std::tuple<TriangularMesh, int> buildRectangularMesh(Point P1, Point P2, Point P3, Point P4,
                                                     Point P5, Point P6, Point P7, Point P8){

  //create the empty mesh instance
  TriangularMesh cuboidMesh;

  //create points
  //Point P0(xmin, ymin, zmin);
  //Point P1(xmax, ymin, zmin);
  //Point P2(xmin, ymax, zmin);
  //Point P3(xmax, ymax, zmin);
  //Point P4(xmin, ymin, zmax);
  //Point P5(xmax, ymin, zmax);
  //Point P6(xmin, ymax, zmax);
  //Point P7(xmax, ymax, zmax);

  //create faces - must be defined consistently anticlockwise
  Face F1(0, 1, 3);
  Face F2(0, 3, 2);

  Face F3(5, 4, 7);
  Face F4(7, 4, 6);

  Face F5(0, 4, 5);
  Face F6(5, 1, 0);

  Face F7(2, 7, 6);
  Face F8(2, 3, 7);

  Face F9(0, 6, 4);
  Face F10(0, 2, 6);

  Face F11(1, 5, 7);
  Face F12(1, 7, 3);

  //add cuboid mesh vertices
  cuboidMesh.vertices.push_back(P1);
  cuboidMesh.vertices.push_back(P2);
  cuboidMesh.vertices.push_back(P3);
  cuboidMesh.vertices.push_back(P4);
  cuboidMesh.vertices.push_back(P5);
  cuboidMesh.vertices.push_back(P6);
  cuboidMesh.vertices.push_back(P7);
  cuboidMesh.vertices.push_back(P8);

  //add cuboid mesh faces
  cuboidMesh.faces.push_back(F1);
  cuboidMesh.faces.push_back(F2);
  cuboidMesh.faces.push_back(F3);
  cuboidMesh.faces.push_back(F4);
  cuboidMesh.faces.push_back(F5);
  cuboidMesh.faces.push_back(F6);
  cuboidMesh.faces.push_back(F7);
  cuboidMesh.faces.push_back(F8);
  cuboidMesh.faces.push_back(F9);
  cuboidMesh.faces.push_back(F10);
  cuboidMesh.faces.push_back(F11);
  cuboidMesh.faces.push_back(F12);

  tuple<TriangularMesh, int> retData = make_tuple(cuboidMesh, 0);

  return retData;

}
