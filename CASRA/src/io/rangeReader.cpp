/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <vector>
#include <sstream>

#include "CASRA/io/rangeReader.h"

using namespace std;

const char* supportedRangeFileExt[] = {"rrng", ""};


/*!
 * RangeData Method for reading an APT range file (format .rrng only)
 * @param filename The rrng file location
 * @return int - (0) if successful, (1) otherwise
 */
int RangeData::readFile(string filename){

  string suffx = filename.substr(filename.find_last_of(".") + 1);

  int it = 0;
  bool supportedExt = false;

  while (strlen(supportedRangeFileExt[it]) != 0) {
    if (suffx == supportedRangeFileExt[it]) {
      supportedExt = true;
    }
    it ++;
  }

  if (supportedExt == false) {
    cout << "Inputted range file has an unidentified file extension. Please check loaded file is indeed a range file." << endl;
    cout << "Continuing with passed file: " << filename << " ..." << endl;
  }


  string textLine;
  ifstream streamedFileData(filename);

  if (!streamedFileData) {
    cout << "range file " << filename <<" failed to open. Please check directory location." << endl;
    return 1;
  }

  cout << "File successfully opened. Reading in rangeFile data..." << endl;

  string strLine;
  int readerMode = 0;
  int mline = 0;

  string attributeData;
  string elementData;

  cout << "start loading range file" << endl;

  while (getline(streamedFileData, strLine)){
    mline += 1;
    if (strLine == "[Ions]"){
      cout << "\tions called" << endl;
      readerMode = 1;
      mline = 0;
    }
    else if (strLine == "[Ranges]"){
      cout << "\tranges called" << endl;
      readerMode = 2;
      mline = 0;
    }
    else if (readerMode == 1) {
      if (mline == 1) {
        //cout << "ions: " << strLine.substr(strLine.find("=")+1) << endl;
        this->SpeciesNum = stoi(strLine.substr(strLine.find("=")+1));
      }
      else{
        //cout << strLine.substr(strLine.find("=")+1) << endl;
        attributeData = strLine.substr(strLine.find("=")+1);
        SpeciesType sType;
        sType.speciesName = attributeData;
        this->species.push_back(sType);
      }
    }
    else if (readerMode == 2) {
      if (mline == 1) {
        this->PeakNum = stoi(strLine.substr(strLine.find("=")+1));
      }
      else{
        attributeData = strLine.substr(strLine.find("=")+1);
        //cout << AttributeData << endl;
        RangedPeak rPeak;
        rPeak.charge = 1.0;
        unsigned int spos = attributeData.find(" ");
        int it = 0;

        std::vector<int> comp;
        int elNum;
        int elInd;

        string species;

        while (it >= 0) {
          spos = attributeData.find(" ");
          elementData = attributeData.substr(0, spos);
          attributeData.erase(0, spos + 1);
          it += 1;
          //cout << attributeData << endl;
          if (it == 1) {
            rPeak.lower = stod(elementData);
          }
          else if (it == 2) {
            rPeak.upper = stod(elementData);
          }
          else if (it == 3) {
            rPeak.ionicVol = stod(elementData.substr(elementData.find(":")+1)) * 1e-27;
            //cout << "yay" << endl;
          }
          else if (it > 3) {
            if ("Color" == elementData.substr(0, elementData.find(":"))){
              rPeak.colorcode = elementData.substr(elementData.find(":")+1);
              it = -10;
              break;
            }
            else {
              species = elementData.substr(0, elementData.find(":"));
              //cout << species << endl;
              elInd = this->findElement(species);
              elNum = stoi(elementData.substr(elementData.find(":")+1));

              for (int a = 0; a < elNum; a++) {
                rPeak.comp.push_back(elInd);
              }
            }
          }
        }
        this->peaks.push_back(rPeak);
      }
    }
  }

  cout << "\tRange file Loading complete" << endl;
  return 0;

}
