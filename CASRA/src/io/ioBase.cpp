/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/*!
 * Add short delay to ensure directory creation
 */

#include "CASRA/io/ioBase.h"

void delay(){
  for (int i=0;i<10000;i++);
}


int createDirectoryTree(std::string filename){

  //if passed, create output directory
  if (filename.length() == 0){
    std::cerr << "output file location must be passed" << std::endl;
    return 1;
  }

  int spos = filename.find("/");
  std::string proceeding_dir1 = filename;
  std::string proceeding_dir2 = "";

  while (spos != std::string::npos){
    spos = proceeding_dir1.find("/");
    proceeding_dir2 = filename.substr(0, spos + filename.length() - proceeding_dir1.length());
    if (proceeding_dir2.length() > 0){
      //cout << "creating directory: " << proceeding_dir2 << endl;
      std::filesystem::create_directory(proceeding_dir2);
      delay();
    }
    proceeding_dir1 = proceeding_dir1.substr(proceeding_dir1.find("/")+1);
  }

  return 0;
}
