/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>

#include "CASRA/io/APTReaders.h"
#include "CASRA/io/ioBase.h"

using namespace std;

const char* supportedAPTInputFileExt[] = {"ato", "epos", ""};

//define constants
#define NANOMETRECONV 1e-9
#define NANOMETREVOLCONV 1e-27

/*!
* method for reading an ATO file into an APTDataSet instance
 *
 * @param filename The .ato file location to read from.
 * @param start The entry number to start loading from.
 * @param stop The final number to finish loading on.
 *
 */
void APTDataSet::atoDataReader(string filename, unsigned long start, unsigned long stop){

  string textLine;
  ifstream APTDataFile(filename, ifstream::binary);

  if (!APTDataFile){
    cout << "ERROR: Cannot read file" << endl;
    exit(1);
  }
  else{
    cout << filename << " opened" << endl;
  }

  char* fileData;

  //obtain data file size
  APTDataFile.seekg(0, ifstream::end);
  int size = APTDataFile.tellg();

  //report number of bytes
  cout << "filesize (bytes): " << (int)size << endl;

  //copy binary to character array
  fileData = new char[size];
  APTDataFile.seekg(0, ifstream::beg);
  APTDataFile.read(fileData, size);
  APTDataFile.close();
  long n;

  try{
    if ((size - 8) % (4*14) != 0){
      throw 0;
    }
  }
  catch(int e){
    cout << "ERROR: Loading binary file as .ato is not the correct number of bytes." << "\n";
    cout << "This implies a non-integer number of entries." << "\n";
    exit(1);
  }

  long nions = (size-8)/(4*14);

  if (((stop < 0) || (stop > nions)) == false) {
    nions = stop;
  }

  DetectionEvent *events = new DetectionEvent[nions];

  for (long i=start;i<nions;i++){

    n = 8 + i * 4 * 14;
    float fbuffer;
    smallEndianBufferPull(&fbuffer, &fileData[n], 4);
    events[i].sx = fbuffer;
    smallEndianBufferPull(&fbuffer, &fileData[n+4*1], 4);
    events[i].sy = fbuffer;
    smallEndianBufferPull(&fbuffer, &fileData[n+4*2], 4);
    events[i].sz = fbuffer;
    smallEndianBufferPull(&fbuffer, &fileData[n+4*3], 4);
    events[i].m2c = fbuffer;
    smallEndianBufferPull(&fbuffer, &fileData[n+4*6], 4);
    events[i].dcVoltage = fbuffer;
    smallEndianBufferPull(&fbuffer, &fileData[n+4*8], 4);
    events[i].dx = fbuffer;
    smallEndianBufferPull(&fbuffer, &fileData[n+4*9], 4);
    events[i].dy = fbuffer;
    smallEndianBufferPull(&fbuffer, &fileData[n+4*10], 4);
    events[i].pulseVoltage = fbuffer;

    events[i].dx *= 0.01;
    events[i].dy *= 0.01;
    events[i].sx *= 1e10;
    events[i].sy *= 1e10;
    events[i].sz *= 1e10;
    events[i].pulseVoltage *= 1000;
    events[i].dcVoltage *= 1000;

    //add ion
    addDetectionEvent(events[i]);

  }

  printf("load completed \n");
  printf("Ions loaded: %li \n", (nions - start));

}

/*!
 * method for reading an EPOS file into an APTDataSet instance
 *
 * @param filename The .epos file location to read from.
 * @param start The entry number to start loading from.
 * @param stop The final number to finish loading on.
 *
 */
void APTDataSet::eposDataReader(string filename, unsigned long start, unsigned long stop){

  string textLine;
  ifstream APTDataFile;

  try{
    APTDataFile.open(filename, ifstream::binary);
    if (APTDataFile.is_open() == false){
      throw 0;
    }
  }
  catch(int e){
    cout << "ERROR: Cannot open APT data file. Is the directory: " << filename << " valid? \n" << endl;
    exit(EXIT_FAILURE);
  }

  char *fileData;

  //obtain data file size
  APTDataFile.seekg(0, ifstream::end);
  int size = APTDataFile.tellg();

  //report number of bytes
  cout << "filesize (bytes): " << (int)size << endl;

  //copy binary to character array
  fileData = new char[size];
  APTDataFile.seekg(0, ifstream::beg);
  APTDataFile.read(fileData, size);
  APTDataFile.close();
  long n;

  try{
    if (size % 4*11 != 0){
      throw 0;
    }
  }
  catch(int e){
    cout << "ERROR: The loaded .epos binary file is not the correct number of bytes." << "\n";
    cout << "This implies a non-integer number of entries." << "\n";
    exit(1);
  }

  long nions = size/(4*11);

  DetectionEvent *events = new DetectionEvent[nions];

  if (((stop < 0) || (stop > nions)) == false) {
    nions = stop;
  }

  for (long i=start;i<nions;i++){
    n = i * 4 * 11;
    float fbuffer;
    bigEndianBufferPull(&fbuffer, &fileData[n], 4);
    events[i].sx = fbuffer;
    bigEndianBufferPull(&fbuffer, &fileData[n+4], 4);
    events[i].sy = fbuffer;
    bigEndianBufferPull(&fbuffer, &fileData[n+2*4], 4);
    events[i].sz = fbuffer;
    bigEndianBufferPull(&fbuffer, &fileData[n+3*4], 4);
    events[i].m2c = fbuffer;
    bigEndianBufferPull(&fbuffer, &fileData[n+4*4], 4);
    events[i].tof = fbuffer;
    bigEndianBufferPull(&fbuffer, &fileData[n+5*4], 4);
    events[i].dcVoltage = fbuffer;
    bigEndianBufferPull(&fbuffer, &fileData[n+6*4], 4);
    events[i].pulseVoltage = fbuffer;
    bigEndianBufferPull(&fbuffer, &fileData[n+7*4], 4);
    events[i].dx = fbuffer;
    bigEndianBufferPull(&fbuffer, &fileData[n+8*4], 4);
    events[i].dy = fbuffer;
    bigEndianBufferPull(&events[i].pulsesSinceLastEventPulse, &fileData[n+9*4], 4);
    bigEndianBufferPull(&events[i].ionsPerPulse, &fileData[n+10*4], 4);

    events[i].dx *= 0.001;
    events[i].dy *= 0.001;
    events[i].sx *= 1e10;
    events[i].sy *= 1e10;
    events[i].sz *= 1e10;
    events[i].pulseVoltage *= 1000;
    events[i].dcVoltage *= 1000;

    //add ion
    addDetectionEvent(events[i]);

  }

  printf("load completed \n");
  printf("Ions loaded: %li \n", (nions - start));

}
