/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <math.h>

#include "CASRA/io/TIFFReader.h"
#include "CASRA/io/ioBase.h"

using namespace std;


/*!
 * Function for reading in 2D and 3D TIFF images. Supports greyscale and RGB/RGBA image types.
 *
 * @param file The input TIFF filename.
 * @return The 4D scalar field (final dimension is colour channels). For 2D images, the first dimension is one.
 */
vector<vector<vector<vector<double>>>> readTiff(string file){

  vector<vector<vector<vector<double>>>> image;

  int bPSV;
  int sPP;

  bool bigEndianness = false;
  char buffer[64];
  char byteorder[2];
  short tiffIdentifier;
  int firstIFDLoc;

  long totalImageBytes = 0;
  int firstWidth = -1;
  int firstLength = -1;

  cout << "Reading in TIFF: " << file << endl;

  ifstream imfile;
  imfile.open(file, ios::binary);

  if (imfile.good() == 0){
    cerr << "ERROR: No file exists at `" << file << "` that can be read." << endl;
    exit(1);
  }

  cout << "\treading header:" << endl;
  imfile.seekg(0, ios::beg);

  //read in first two bytes (defining byte order)
  imfile.read(byteorder, 2);

  if (string(byteorder) == "II"){
    cout << "\t\tlittle endian format detected" << endl;
    bigEndianness = false;
  }
  else if (string(byteorder) == "MM"){
    cout << "\t\tbig endian format detected" << endl;
    bigEndianness = true;
  }
  //read the next two bytes (defining as tiff file)
  imfile.read(buffer, 2);
  if (bigEndianness == false){
    smallEndianBufferPull(&tiffIdentifier, buffer, 2);
  }
  else{
    bigEndianBufferPull(&tiffIdentifier, buffer, 2);
  }

  if (tiffIdentifier != 42){
    cerr << "\tpassed file is not recognised as TIFF." << endl;
    exit(1);
  }

  //read the next four bytes (specifying the first IFD address)
  imfile.read(buffer, 4);
  if (bigEndianness == false){
      smallEndianBufferPull(&firstIFDLoc, buffer, 4);
  }
  else{
      bigEndianBufferPull(&firstIFDLoc, buffer, 4);
  }

  cout << "\t\tEndianness: " << byteorder << endl;
  cout << "\t\ttiff identifier: " << tiffIdentifier << endl;
  cout << "\t\tIFD Address: " << firstIFDLoc << endl;

  //read in IFD
  int IFDLoc = firstIFDLoc;
  cout << "\tReading in IFDs and field data:" << endl;
  while(IFDLoc != 0){

    imfile.seekg(IFDLoc, ios::beg);

    //IFD directory entries
    short directoryEntryNum;

    //IFD entry parameters
    short fieldTag;
    short fieldType;
    int typeCount;
    int valueOffset;

    //offset to next offset
    imfile.read(buffer, 2);
    if (bigEndianness == false){
        smallEndianBufferPull(&directoryEntryNum, buffer, 2);
    }
    else{
        bigEndianBufferPull(&directoryEntryNum, buffer, 2);
    }
    //cout << "num IFD directory entires: " << directoryEntryNum << endl;

    if (imfile.fail() == true){
      imfile.close();
      imfile.open(file, ios::binary);
      continue;
    }

    //IFD attributes (conforming with baseline TIFF standard)
    //see https://www.awaresystems.be/imaging/tiff/tifftags/baseline.html
    int width;
    int length;
    int rowsPerStrip = -1;
    vector<int> byteOffsets;
    vector<int> byteCounts;
    int bitsPerSampleOffset;
    short samplesPerPixel = -1;
    char bitsPerSampleCache[4];
    char stripOffsetBuffer[4];
    short stripOffsetFieldType = -1;
    short photometricInterpretationMode;
    short colourMode;
    int stripOffsetCount;

    for (int i=0;i<directoryEntryNum;i++){
      long rLoc = IFDLoc + 2 + 12 * i; //12 bytes per IFD
      //cout << "rloc: " << rLoc << endl;
      imfile.seekg(rLoc, ios::beg);
      imfile.read(buffer, 2);
      if (bigEndianness == false){
          smallEndianBufferPull(&fieldTag, buffer, 2);
      }
      else{
          bigEndianBufferPull(&fieldTag, buffer, 2);
      }

      imfile.read(buffer, 2);
      if (bigEndianness == false){
          smallEndianBufferPull(&fieldType, buffer, 2);
      }
      else{
          bigEndianBufferPull(&fieldType, buffer, 2);
      }

      imfile.read(buffer, 4);
      if (bigEndianness == false){
          smallEndianBufferPull(&typeCount, buffer, 4);
      }
      else{
          bigEndianBufferPull(&typeCount, buffer, 4);
      }

      char valOffsetBuffer[4];
      imfile.read(buffer, 4);
      if (bigEndianness == false){
          smallEndianBufferPull(&valueOffset, buffer, 4);
      }
      else{
          bigEndianBufferPull(&valueOffset, buffer, 4);
      }

      valOffsetBuffer[0] = buffer[0];
      valOffsetBuffer[1] = buffer[1];
      valOffsetBuffer[2] = buffer[2];
      valOffsetBuffer[3] = buffer[3];

      //cout << "field tag: " << fieldTag << endl;
      //cout << "field type: " << fieldType << endl;
      //cout << "type count: " << typeCount << endl;
      //cout << "value offset: " << valueOffset << endl;
      //cout << "directoryEntryNum: " << directoryEntryNum << endl;

      int valueSize;
      switch(fieldTag){
        case 254:
          //cout << "newsubfiletype" << endl;
          valueSize = 8;
          break;
        case 255:
          //cout << "subfiletype" << endl;
          valueSize = 2;
          break;
        case 256:
          //cout << "imagewidth" << endl;
          valueSize = 2;
          width = valueOffset;
          if (firstWidth < 0){
            firstWidth = width;
          }
          else if (firstWidth != width){
            cerr << "\t\twidth dimension of image slice differs from previous slices. Please check image stack in TIFF file." << endl;
            exit(1);
          }
          break;
        case 257:
          //cout << "imagelength" << endl;
          valueSize = 2;
          length = valueOffset;
          if (firstLength < 0){
            firstLength = length;
          }
          else if (firstLength != length){
            cerr << "\t\tlength dimension of image slice differs from previous slices. Please check image stack in TIFF file." << endl;
            exit(1);
          }
          break;
        case 258:
          //cout << "bits per sample" << endl;
          valueSize = 2;
          bitsPerSampleOffset = valueOffset;
          bitsPerSampleCache[0] = valOffsetBuffer[0];
          bitsPerSampleCache[1] = valOffsetBuffer[1];
          bitsPerSampleCache[2] = valOffsetBuffer[2];
          bitsPerSampleCache[3] = valOffsetBuffer[3];
          break;
        case 277:
          //cout << "samples per pixel" << endl;
          valueSize = 2;
          samplesPerPixel = valueOffset;
          break;
        case 259:
          //cout << "compression" << endl;
          valueSize = 2;
          if (valueOffset == 1){
            //cout << "no compression detected. Can load TIFF." << endl;
          }
          else {
            cout << "\t\ttiff compression detected. Cannot load TIFF." << endl;
            exit(1);
          }
          break;
        case 262:
          //cout << "photometricinterpretation" << endl;
          valueSize = 2;
          if (bigEndianness == false){
            smallEndianBufferPull(&photometricInterpretationMode, valOffsetBuffer, 2);
          }
          else{
            bigEndianBufferPull(&photometricInterpretationMode, valOffsetBuffer, 2);
          }
          if (photometricInterpretationMode == 0){
            //cout << "WhiteisZero" << endl;
            colourMode = 0;
          }
          else if (photometricInterpretationMode == 1){
            //cout << "Blackiszero" << endl;
            colourMode = 0;
          }
          else if (photometricInterpretationMode == 2){
            //cout << "RGB" << endl;
            colourMode = 1;
          }
          else{
            cout << "\t\tunsupported palette colours or transparency. Please save as greyscale or RGB." << endl;
            exit(1);
          }
          break;
        case 282:
          //cout << "Xresolution" << endl;
          valueSize = 16;
          break;
        case 283:
          //cout << "Yresolution" << endl;
          valueSize = 16;
          break;
        case 296:
          //cout << "resolution unit" << endl;
          valueSize = 2;
          break;
        case 278:
          //cout << "rows per strip" << endl;
          valueSize = 2;
          rowsPerStrip = valueOffset;
          break;
        case 273:
          //cout << "strip offset" << endl;
          //cout << "type: " << fieldType << endl;
          valueSize = 2;
          stripOffsetBuffer[0] = valOffsetBuffer[0];
          stripOffsetBuffer[1] = valOffsetBuffer[1];
          stripOffsetBuffer[2] = valOffsetBuffer[2];
          stripOffsetBuffer[3] = valOffsetBuffer[3];
          stripOffsetFieldType = fieldType;
          stripOffsetCount = typeCount;
          break;

        case 279:
          //cout << "strip byte counts" << endl;
          //cout << typeCount << endl;
          //cout << fieldType << endl;
          imfile.seekg(valueOffset, ios::beg);
          int size = 0;
          if (fieldType == 3){
            size = 2;
          }
          else if (fieldType == 4) {
            size = 4;
          }
          else {
            cout << "ERROR: Unsupported size type. Cannot read in TIFF." << endl;
            exit(1);
          }
          if (size * typeCount <= 4){
            int bytecount;
            smallEndianBufferPull(&bytecount, valOffsetBuffer, 4);
            byteCounts.push_back(bytecount);
          }
          else {
            if (fieldType == 3){
              for (int b=0;b<typeCount;b++){
                imfile.read(buffer, 4);
                short bytecount;
                if (bigEndianness == false){
                  smallEndianBufferPull(&bytecount, buffer, 2);
                }
                else{
                  bigEndianBufferPull(&bytecount, buffer, 2);
                }
                byteCounts.push_back((int)bytecount);
              }
            }
            else if (fieldType == 4){
              for (int b=0;b<typeCount;b++){
                imfile.read(buffer, 4);
                int bytecount;
                if (bigEndianness == false){
                  smallEndianBufferPull(&bytecount, buffer, 4);
                }
                else{
                  bigEndianBufferPull(&bytecount, buffer, 4);
                }
                byteCounts.push_back(bytecount);
              }
            }
          }
          break;
      }
    }

    //***Read in bits per image***//
    //* must process following the complete IFD reading as requires samples per pixel IFD entry
    short bitsPerSampleVal;
    if (samplesPerPixel < 0){
      cerr << "ERROR: No samplesPerPixel field value detected. TIFF file is an invalid format." << endl;
      exit(1);
    }
    if (rowsPerStrip < 0){
      cerr << "ERROR: No rowsPerStrip field value detected. TIFF file is an invalid format." << endl;
      exit(1);
    }
    if (samplesPerPixel > 2){
      imfile.seekg(bitsPerSampleOffset, ios::beg);
      imfile.read(buffer, 2);
      if (bigEndianness == false){
        smallEndianBufferPull(&bitsPerSampleVal, buffer, 2);
      }
      else{
        bigEndianBufferPull(&bitsPerSampleVal, buffer, 2);
      }
    }
    else {
      if (bigEndianness == false){
        smallEndianBufferPull(&bitsPerSampleVal, bitsPerSampleCache, 2);
      }
      else{
        bigEndianBufferPull(&bitsPerSampleVal, bitsPerSampleCache, 2);
      }
    }

    //***Read in strips per image***//
    //* must process following the complete IFD reading as requires stripsPerImage data
    //calculate strips per image
    int stripsPerImage = floor((length + rowsPerStrip - 1)/rowsPerStrip);
    int stripOffsetEntryByteSize = 2;

    if (stripOffsetFieldType == -1){
      cerr << "ERROR: No stripOffsetFieldType entry found. TIFF format is invalid" << endl;
      exit(1);
    }

    //select type size mode
    if (stripOffsetFieldType == 3){
      stripOffsetEntryByteSize = 2;
    }
    else if (stripOffsetFieldType == 4){
      stripOffsetEntryByteSize = 4;
    }

    //is strip offsets stored at pointer address
    if (stripOffsetEntryByteSize * stripsPerImage > 4){
      int stripOffsetValueOffset;
      if (bigEndianness == false){
        smallEndianBufferPull(&stripOffsetValueOffset, stripOffsetBuffer, 4);
      }
      else{
        bigEndianBufferPull(&stripOffsetValueOffset, stripOffsetBuffer, 4);
      }
      //read in values pointed to by value offset buffer
      imfile.seekg(stripOffsetValueOffset, ios::beg);
      if (stripOffsetEntryByteSize == 2){ //short data type
        for (int b=0;b<stripOffsetCount;b++){
          imfile.read(buffer, 2);
          short byteOffset;
          if (bigEndianness == false){
            smallEndianBufferPull(&byteOffset, buffer, 2);
          }
          else{
            bigEndianBufferPull(&byteOffset, buffer, 2);
          }
          byteOffsets.push_back((int)byteOffset);
        }
      }
      else if (stripOffsetEntryByteSize == 4){ //long data type
        for (int b=0;b<stripOffsetCount;b++){
          imfile.read(buffer, 4);
          int byteOffset;
          if (bigEndianness == false){
            smallEndianBufferPull(&byteOffset, buffer, 4);
          }
          else{
            bigEndianBufferPull(&byteOffset, buffer, 4);
          }
          byteOffsets.push_back(byteOffset);
        }
      }
    }
    else{     //else strip offsets small enough to be stored directly in offsetValue pointer
      if (stripOffsetFieldType == 3){ //short data type
        for (int b=0;b<stripOffsetCount;b++){
          short byteOffset;
          if (bigEndianness == false){
            smallEndianBufferPull(&byteOffset, stripOffsetBuffer, 2);
          }
          else{
            bigEndianBufferPull(&byteOffset, stripOffsetBuffer, 2);
          }
          byteOffsets.push_back((int)byteOffset);
        }
      }
      else if (stripOffsetFieldType == 4){ //long data type
        for (int b=0;b<stripOffsetCount;b++){
          int byteOffset;
          if (bigEndianness == false){
            smallEndianBufferPull(&byteOffset, stripOffsetBuffer, 4);
          }
          else{
            bigEndianBufferPull(&byteOffset, stripOffsetBuffer, 4);
          }
          byteOffsets.push_back(byteOffset);
        }
      }
    }

    imfile.seekg(IFDLoc + 2 + 12 * directoryEntryNum, ios::beg);

    imfile.read(buffer, 4);
    if (bigEndianness == false){
        smallEndianBufferPull(&IFDLoc, buffer, 4);
    }
    else{
        bigEndianBufferPull(&IFDLoc, buffer, 4);
    }

    //create image slice
    vector<vector<vector<double>>> imageSlice(length, vector<vector<double>>(width, vector<double>(samplesPerPixel)));

    //define x and y image indices
    int r = 0;
    int c = 0;

    //****specify the supported colour, bit, and sample modes for tiff images being read***//
    if ((colourMode == 1) && (bitsPerSampleVal == 8)  && (samplesPerPixel == 4)) {
      //load image data - RGB with alpha channel
      int bytesPerSample = (int)bitsPerSampleVal/8;
      for (int o=0;o<byteOffsets.size();o++){
        imfile.seekg(byteOffsets[o], ios::beg);
        totalImageBytes += byteCounts[o];
        for (int b=0;b<(int)(byteCounts[o]/(bytesPerSample * samplesPerPixel));b++){
          char val;
          imfile.read(&val, 1);
          double eVal = (double)val;
          eVal = (127 - eVal) * (eVal < 0) + (eVal) * (eVal >= 0); //convert unsigned int to signed double
          imageSlice[c][r][0] = eVal;
          imfile.read(&val, 1);
          eVal = (double)val;
          eVal = (127 - eVal) * (eVal < 0) + (eVal) * (eVal >= 0); //convert unsigned int to signed double
          imageSlice[c][r][1] = eVal;
          imfile.read(&val, 1);
          eVal = (double)val;
          eVal = (127 - eVal) * (eVal < 0) + (eVal) * (eVal >= 0); //convert unsigned int to signed double
          imageSlice[c][r][2] = eVal;
          imfile.read(&val, 1); //read in alpha channel
          eVal = (double)val;
          eVal = (127 - eVal) * (eVal < 0) + (eVal) * (eVal >= 0); //convert unsigned int to signed double
          imageSlice[c][r][3] = eVal;
          r++;
          if (r == width) {
            c++;
            r = 0;
          }
        }
      }
    }
    else if ((colourMode == 1) && (bitsPerSampleVal == 8) && (samplesPerPixel == 3)) {
      //load image data - RGB with no alpha channel
      int bytesPerSample = (int)bitsPerSampleVal/8;
      for (int o=0;o<byteOffsets.size();o++){
        totalImageBytes += byteCounts[o];
        imfile.seekg(byteOffsets[o], ios::beg);
        for (int b=0;b<(int)(byteCounts[o]/(bytesPerSample * samplesPerPixel));b++){
          char val;
          imfile.read(&val, 1);
          double eVal = (double)val;
          eVal = (127 - eVal) * (eVal < 0) + (eVal) * (eVal >= 0); //convert unsigned int to signed double
          imageSlice[c][r][0] = eVal;
          imfile.read(&val, 1);
          eVal = (double)val;
          eVal = (127 - eVal) * (eVal < 0) + (eVal) * (eVal >= 0); //convert unsigned int to signed double
          imageSlice[c][r][1] = eVal;
          imfile.read(&val, 1);
          eVal = (double)val;
          eVal = (127 - eVal) * (eVal < 0) + (eVal) * (eVal >= 0); //convert unsigned int to signed double
          imageSlice[c][r][2] = eVal;
          r++;
          if (r == width) {
            c++;
            r = 0;
          }
        }
      }
    }
    else if ((colourMode == 0) && (bitsPerSampleVal == 64) && (samplesPerPixel == 1)) {
      //load image data - greyscale with no alpha channel - 8 byte double entries
      int bytesPerSample = (int)bitsPerSampleVal/8;
      for (int o=0;o<byteOffsets.size();o++){
        totalImageBytes += byteCounts[o];
        imfile.seekg(byteOffsets[o], ios::beg);
        for (int b=0;b<(int)((byteCounts[o])/(bytesPerSample * samplesPerPixel));b++){ //8 -> 64/8=8 - bytes per sample
          char val[8];
          imfile.read(val, 8);
          double voxelEntry;
          if (bigEndianness == false){
              smallEndianBufferPull(&voxelEntry, val, 8);
          }
          else{
              bigEndianBufferPull(&voxelEntry, val, 8);
          }
          imageSlice[c][r][0] = voxelEntry; //$$there is the wrong number of bits per component
          r++;
          if (r == width) {
            c++;
            r = 0;
          }
        }
      }
    }
    else if ((colourMode == 0) && (bitsPerSampleVal == 32) && (samplesPerPixel == 1)) {
      //load image data - greyscale with no alpha channel - 4 byte long int entries
      int bytesPerSample = (int)bitsPerSampleVal/8;
      for (int o=0;o<byteOffsets.size();o++){
        totalImageBytes += byteCounts[o];
        imfile.seekg(byteOffsets[o], ios::beg);
        for (int b=0;b<(int)((byteCounts[o])/(bytesPerSample * samplesPerPixel));b++){ //8 -> 64/8=8 - bytes per sample
          char val[4];
          imfile.read(val, 4);
          int voxelEntry;
          if (bigEndianness == false){
              smallEndianBufferPull(&voxelEntry, val, 4);
          }
          else{
              bigEndianBufferPull(&voxelEntry, val, 4);
          }
          imageSlice[c][r][0] = (double)voxelEntry; //$$there is the wrong number of bits per component
          r++;
          if (r == width) {
            c++;
            r = 0;
          }
        }
      }
    }
    else {
      cerr << "\ttiff colour/sample/bit modes currently not supported by the CASRA tiff reader." << endl;
      exit(1);
    }

    image.push_back(imageSlice);
    bPSV = (int)bitsPerSampleVal;
    sPP = (int)samplesPerPixel;

    //zero implies no more IFDs within the loaded tiff file
    //cout << "*******next IFD*************: " << IFDLoc << endl;
  }

  int selfConsistencyByteCheck = (int)totalImageBytes/((bPSV/8) * sPP * image[0][0].size()*image[0].size()*image.size());

  if (selfConsistencyByteCheck == 1){
    cerr << "\tPassed self consistency check. Sample number of bytes in image as read in." << endl;
  }
  else {
    cerr << "\tWARNING: Failed self consistency check. Bytes read in differ to bytes in image. Continuing but be wary of result." << endl;
  }

  /*
  //for testing - output loaded tiff field to vtk file
  vector<vector<vector<double>>> outImage= vector<vector<vector<double>>>(1,
                                                                          vector<vector<double>>(image[0].size(),
                                                                          vector<double>(image[0][0].size())));
  for (int i=0;i<image[0].size();i++){
    for (int j=0;j<image[0][0].size();j++){
      outImage[0][i][j] = image[210][i][j][0];
    }
  }

  vtkObject out;
  out.setWithField(outImage);
  writeVtk(out, "testim.vtk");

  for (int i=0;i<image[0].size();i++){
    for (int j=0;j<image[0][0].size();j++){
      outImage[0][i][j] = image[0][i][j][1];
    }
  }

  out.setWithField(outImage);
  writeVtk(out, "testimy.vtk");

  for (int i=0;i<image[0].size();i++){
    for (int j=0;j<image[0][0].size();j++){
      outImage[0][i][j] = image[0][i][j][2];
    }
  }

  out.setWithField(outImage);
  writeVtk(out, "testimz.vtk");
  */


  //close TIFF file
  imfile.close();

  cout << "TIFF read complete" << endl;
  cout << "Loaded image dimensions: " << image.size() << " " << image[0].size() <<  " " << image[0][0].size() << endl;
  cout << "Loaded image channels: " << image[0][0][0].size() << endl;
  return image;

}
