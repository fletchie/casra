/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>

#include "CASRA/io/ioBase.h"
#include "CASRA/io/APTWriters.h"

using namespace std;

const char* supportedAPTOutputFileExt[] = {"ato", "epos", "pos", ""};

//define constants
#define NANOMETRECONV 1e-9
#define NANOMETREVOLCONV 1e-27

/*!
 * method for writing APT dataset within an APTDataSet instance into a POS file.
 *
 * @param filename The output file location to write the .pos file to.
 *
 */
void APTDataSet::posDataWriter(string filename){

  createDirectoryTree(filename);

  ofstream APTDataFile(filename, ofstream::binary);

  int nions = events.size();
  char buffer[4];

  for (int i = 0; i < nions; i++){

    float sx = events[i].sx * 1e9;
    float sy = events[i].sy * 1e9;
    float sz = events[i].sz * 1e9;
    float m2c = events[i].m2c;

    bigEndianBufferPush(&sx, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&sy, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&sz, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&m2c, buffer, 4);
    APTDataFile.write(buffer, 4);
  }

  APTDataFile.close();

}

/*!
 * method for writing APT dataset within an APTDataSet instance into an EPOS file.
 *
 * @param filename The output file location to write the .epos file to.
 *
 */
void APTDataSet::eposDataWriter(string filename){

  createDirectoryTree(filename);

  ofstream APTDataFile(filename, ofstream::binary);

  int nions = events.size();
  char buffer[4];

  for (int i=0;i<nions;i++){

    float sx = events[i].sx * 1e9;
    float sy = events[i].sy * 1e9;
    float sz = events[i].sz * 1e9;
    float dcVoltage = events[i].dcVoltage/1000;
    float pulseVoltage = events[i].pulseVoltage/1000;
    float dx = events[i].dx * 1000;
    float dy = events[i].dy * 1000;

    bigEndianBufferPush(&sx, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&sy, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&sz, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&events[i].m2c, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&events[i].tof, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&dcVoltage, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&pulseVoltage, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&dx, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&dy, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&events[i].pulsesSinceLastEventPulse, buffer, 4);
    APTDataFile.write(buffer, 4);
    bigEndianBufferPush(&events[i].ionsPerPulse, buffer, 4);
    APTDataFile.write(buffer, 4);

  }

  APTDataFile.close();

}

/*!
 * method for writing APT dataset within an APTDataSet instance into an ATO file.
 *
 * @param filename The output file location to write the .ato file to.
 *
 */
void APTDataSet::atoDataWriter(string filename){

  createDirectoryTree(filename);

  ofstream APTDataFile(filename, ofstream::binary);

  int nions = events.size();
  char buffer[4];

  const char version[1] = {0x03};
  APTDataFile.write(version, 1);
  APTDataFile.write(version, 1);
  APTDataFile.write(version, 1);
  APTDataFile.write(version, 1);
  APTDataFile.write(version, 1);
  APTDataFile.write(version, 1);
  APTDataFile.write(version, 1);
  APTDataFile.write(version, 1);

  for (int i=0;i<nions;i++){

    float sx = events[i].sx * 1e10;
    float sy = events[i].sy * 1e10;
    float sz = events[i].sz * 1e10;
    float dcVoltage = events[i].dcVoltage/1000;
    float pulseVoltage = events[i].pulseVoltage/1000;
    float dx = events[i].dx * 100;
    float dy = events[i].dy * 100;
    float VVolt = 0.0;
    float FourierR = 0.0;
    float FourierI = 0.0;

    smallEndianBufferPush(&sx, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&sy, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&sz, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&events[i].m2c, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&events[i].clusterId, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&events[i].pulseNumber, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&dcVoltage, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&events[i].tof, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&dx, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&dy, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&pulseVoltage, buffer, 4);
    APTDataFile.write(buffer, 4);

    smallEndianBufferPush(&VVolt, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&FourierR, buffer, 4);
    APTDataFile.write(buffer, 4);
    smallEndianBufferPush(&FourierI, buffer, 4);
    APTDataFile.write(buffer, 4);

  }

  APTDataFile.close();

}
