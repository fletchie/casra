/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cstdio>
#include <tuple>

#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/electrostatics/constantBEM.h"
#include "CASRA/electrostatics/solverParser.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/vacuumFieldSolver.h"

using namespace std;

string defaultWriteDir = "output";

const char *helpSynonyms[] = {"-h", "-help", ""};
const char* supportedVTKConfigFileExt[] = {"vtk", ""};

//initialise help strings for -h mode
string inputVTKMeshLocHelp = "File location for model surface mesh. \n\tSupported extensions: ";
string outputVTKFieldLocHelp = "File location for field solution. \n\tSupported extensions: ";
string xrangeArgHelp = "Specified x-direction range for solution grid field. \n\tformat: 'xstart;xstop;xsteps'. Must pass this command line argument in quotation marks.";
string yrangeArgHelp = "Specified y-direction range for solution grid field. \n\tformat: 'xstart;xstop;xsteps'. Must pass this command line argument in quotation marks.";
string zrangeArgHelp = "Specified z-direction range for solution grid field. \n\tformat: 'xstart;xstop;xsteps'. Must pass this command line argument in quotation marks.";

//define constants
#define NANOMETRECONV 1e-9
#define NANOMETREVOLCONV 1e-27

//define Sussman reinitialisation parameters
#define SUSSTOL 0.05
#define SUSSMAXITS 20

string defaultOutputVTKFieldLoc = "surroundingField.vtk";

/*!
 *
 * Function to calculate the vacuum field surrounding a specimen mesh and output
 * to a VTK point grid
 *
 * @param surfaceMesh The specimen surface mesh
 * @param writeFieldVTKLocation The VTK field file location
 * @param xmin The surfaceMesh bounding box x-coordinate minimum
 * @param xmax The surfaceMesh bounding box x-coordinate maximum
 * @param xsteps The x grid resolution
 * @param ymin The surfaceMesh bounding box y-coordinate minimum
 * @param ymax The surfaceMesh bounding box y-coordinate maximum
 * @param ysteps The y grid resolution
 * @param zmin The surfaceMesh bounding box z-coordinate minimum
 * @param zmax The surfaceMesh bounding box z-coordinate maximum
 * @param zsteps The z grid resolution
 * @return int - (0) if successful, (1) otherwise
 *
 */
int solveVacuumField(TriangularMesh surfaceMesh, string writeFieldVTKLocation,
                     double xmin, double xmax, int xsteps,
                     double ymin, double ymax, int ysteps,
                     double zmin, double zmax, int zsteps) {

  surfaceMesh.setMeshProperties();

  double minCoords[3] = {pointDimMinimum(surfaceMesh.vertices, 0),
                         pointDimMinimum(surfaceMesh.vertices, 1),
                         pointDimMinimum(surfaceMesh.vertices, 2)};
  double maxCoords[3] = {pointDimMaximum(surfaceMesh.vertices, 0),
                         pointDimMaximum(surfaceMesh.vertices, 1),
                         pointDimMaximum(surfaceMesh.vertices, 2)};

  //calculate surface field
  electrostaticBemSolver bemSolver(0.2);
  bemSolver.u = vector<double>(surfaceMesh.faces.size(), 1.0);
  bemSolver.calculateElectrostaticMatrix(surfaceMesh);
  bemSolver.matrixVectorProduct(bemSolver.rhs, bemSolver.Fmat, bemSolver.u);
  solveField(bemSolver.Pmat, bemSolver.rhs, bemSolver.q, bemSolver.Amat, bemSolver.AmatDimSize);

  OctreeNode top(minCoords, maxCoords,
                 &surfaceMesh,
                 &bemSolver);

  int resX = xsteps;
  int resY = ysteps;
  int resZ = zsteps;

  cout << "Evaluating surrounding field" << endl;
  scalarField4D fieldGrid = initialiseField(resX+1, resY+1, resZ+1, 3, 0.0);
  scalarField3D fieldGridZ = initialiseField(resX+1, resY+1, resZ+1, 0.0);

  double averagePanelLength = 0.0;
  int averagePanelLengthSampleSize = 0;
  for (int f=0;f<surfaceMesh.faces.size();f++){
    if (surfaceMesh.faces[f].centre.pz > (minCoords[2] + maxCoords[2]*0.9)){
      averagePanelLength += sqrt(surfaceMesh.faces[f].ls2);
      averagePanelLengthSampleSize++;
    }
  }

  averagePanelLength = averagePanelLength/averagePanelLengthSampleSize;
  double admissTol = 0.1;

  #if defined(_OPENMP)
  #pragma omp parallel for
  #endif
  for (int i=0;i<resX+1;i++){
    for (int j=0;j<resY+1;j++){
      for (int k=0;k<resZ+1;k++){
        double v1, v2, v3 = 0.0;

        int im = i * (resX+1)/resX;
        int jm = j * (resY+1)/resY;
        int km = k * (resZ+1)/resZ;

        double px = ((resX - im) * xmin + im * xmax)/(resX);
        double py = ((resY - jm) * ymin + jm * ymax)/(resY);
        double pz = ((resZ - km) * zmin + km * zmax)/(resZ);
        Point evalPoint(px, py, pz);

        double cdist = surfaceMesh.shortestDistanceToPoint(evalPoint);
        int insideCheck = surfaceMesh.insideMesh(evalPoint);

        //only evaluate points outside of specimen mesh (valid exterior laplace problem domain)
        if ((cdist > averagePanelLength * 0.75) && (insideCheck == 1)){
          top.calculateField(v1, v2, v3, evalPoint, admissTol);
          fieldGrid[i][j][k][0] = v1;
          fieldGrid[i][j][k][1] = v2;
          fieldGrid[i][j][k][2] = v3;
          fieldGridZ[i][j][k] = v3;
        }
      }
    }
  }

  Point bottomLeftDomainCorner(xmin, ymin, zmin);
  double cellWidthX = (xmax - xmin)/(resX);
  double cellWidthY = (ymax - ymin)/(resY);
  double cellWidthZ = (zmax - zmin)/(resZ);

  vtkObject out;
  out.setWithField(fieldGrid, bottomLeftDomainCorner, cellWidthX, cellWidthY, cellWidthZ);
  writeVtk(out, writeFieldVTKLocation);

  return 0;

}

/*!
 *
 * Function called on running CASRA vacuum field calculator from the command line
 * takes command line arguments
 */
int main(int argc, char* argv[])
{

  int ret = 0;

  char inputVTKMeshLocArg[] = "--inputVTKMesh";
  char outputVTKFieldLocArg[] = "--outputVTKField";
  char xrangeArg[] = "--xrange";
  char yrangeArg[] = "--yrange";
  char zrangeArg[] = "--zrange";

  string inputVTKMeshLoc = "";
  string outputVTKFieldLoc = "";
  string xrange = "";
  string yrange = "";
  string zrange = "";

  string argSymbol = "--";
  string argInputVal;

  bool helpEnabled = false;

  double xmin = 0.0;
  double ymin = 0.0;
  double zmin = 0.0;

  double xmax = 0.0;
  double ymax = 0.0;
  double zmax = 0.0;

  int xsteps = 0;
  int ysteps = 0;
  int zsteps = 0;

  //if no input detected
  if (argc == 1) {
    cout << "Welcome to CASRA's vacuum field calculation program. " <<
            "\nRun `vacuumFieldSolver -h` for help." << endl;
  }
  else if (argc == 2) { //check if help passed
    int a = 0;
    while (strlen(helpSynonyms[a]) != 0) {
      if (strcmp(argv[1], helpSynonyms[a]) == 0) {
        helpEnabled = true;
        break;
      }
      a ++;
    }
  }
  else {
    if (!(argc % 2)) {
      cout << "Incomplete number of inputs detected. Please check all arguments are defined." << endl;
      return 1;
    }
    else {
      for (int i=1; i < argc; i++){
        if (i % 2 == 1) {
          argInputVal = (string)argv[i];
          if (argSymbol == argInputVal.substr(0, 2)) {
            //command line input argument detected
            if (strcmp(inputVTKMeshLocArg, argv[i])==0) {
              cout << "Supplied argument: " << inputVTKMeshLocArg << " " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                inputVTKMeshLoc = (string)argv[i+1];
              }
              else {
                cout << "No following input argument found for: " <<
                        inputVTKMeshLocArg << ". Please check input." << endl;
                return 1;
              }
            }
            else if (strcmp(outputVTKFieldLocArg, argv[i])==0) {
              cout << "Supplied argument: " << outputVTKFieldLocArg << " " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                outputVTKFieldLoc = (string)argv[i+1];
              }
              else {
                cout << "No following input argument found for: " <<
                        outputVTKFieldLoc << ". Using default output location of " <<
                        defaultOutputVTKFieldLoc  << endl;
                return 1;
              }
            }
            else if (strcmp(xrangeArg, argv[i])==0) {
              cout << "Supplied argument: " << xrangeArg << " " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                xrange = (string)argv[i+1];
                tuple<double, double, int> extractedXRange = extractSpatialGridRange(xrange);
                xmin = get<0>(extractedXRange);
                xmax = get<1>(extractedXRange);
                xsteps = get<2>(extractedXRange);
              }
            }
            else if (strcmp(yrangeArg, argv[i])==0) {
              cout << "Supplied argument: " << yrangeArg << " " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                yrange = (string)argv[i+1];
                tuple<double, double, int> extractedYRange = extractSpatialGridRange(yrange);
                ymin = get<0>(extractedYRange);
                ymax = get<1>(extractedYRange);
                ysteps = get<2>(extractedYRange);
              }
            }
            else if (strcmp(zrangeArg, argv[i])==0) {
              cout << "Supplied argument: " << zrangeArg << " " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                zrange = (string)argv[i+1];
                tuple<double, double, int> extractedZRange = extractSpatialGridRange(zrange);
                zmin = get<0>(extractedZRange);
                zmax = get<1>(extractedZRange);
                zsteps = get<2>(extractedZRange);
              }
            }
            else {
              cout << "Unidentified input argument name detected. Please check input." << endl;
              return 1;
            }
          }
          else {
            cout << "Input argument name " << argv[i] <<
                    " not correctly marked as argument. Please check input." << endl;
            return 1;
          }
        }
      }

      if (inputVTKMeshLoc == "") {
        cout << "No model configuration file supplied. Cannot define evaporation model." << endl;
        return 1;
      }
      if (outputVTKFieldLoc == "") {
        outputVTKFieldLoc = defaultOutputVTKFieldLoc;
        cout << "No output tip configuration file supplied. Using default of '" <<
                outputVTKFieldLoc << "' within current local directory." << endl;
      }


      vtkObject in;
      int readSuccess = readVTKUnstructuredGrid(in, inputVTKMeshLoc);

      if (readSuccess != 0){
        cout << "surface mesh failed to read. Please check current directory " <<
                "and passed surface mesh file location '"<< inputVTKMeshLoc << "' is correct." << endl;
        exit(1);
      }

      TriangularMesh surfaceMesh = in.triangularMeshFromVtk();

      if (surfaceMesh.faces.size() == 0){
        cout << "Loaded surface mesh is empty. Cannot solve field." << endl;
        exit(1);
      }

      double minCoords[3] = {pointDimMinimum(surfaceMesh.vertices, 0),
                             pointDimMinimum(surfaceMesh.vertices, 1),
                             pointDimMinimum(surfaceMesh.vertices, 2)};
      double maxCoords[3] = {pointDimMaximum(surfaceMesh.vertices, 0),
                             pointDimMaximum(surfaceMesh.vertices, 1),
                             pointDimMaximum(surfaceMesh.vertices, 2)};

      if (xsteps == 0){
        xmin = minCoords[0];
        xmax = maxCoords[0];
        xsteps = 40;
        cout << "No '" << xrangeArg << "' passed. Using default range of min: " <<
                xmin << ", max: " << xmax << ", steps " << xsteps << endl;
      }
      if (ysteps == 0){
        ymin = minCoords[1];
        ymax = maxCoords[1];
        ysteps = 40;
        cout << "No '" << yrangeArg << "' passed. Using default range of min: " <<
                ymin << ", max: " << ymax << ", steps " << ysteps << endl;
      }
      if (zsteps == 0){
        zmin = minCoords[2];
        zmax = maxCoords[2];
        zsteps = 40;
        cout << "No '" << zrangeArg << "' passed. Using default range of min: " <<
                zmin << ", max: " << zmax << ", steps " << zsteps << endl;
      }

      int retVal = solveVacuumField(surfaceMesh, outputVTKFieldLoc,
                                    xmin, xmax, xsteps,
                                    ymin, ymax, ysteps,
                                    zmin, zmax, zsteps);

      if (ret > 0) {
        cout << "Execution failed." << endl;
      }
      else {
        cout << "Execution succeeded." << endl;
      }
    }
  }

  if (helpEnabled == true) {

    //Assemble model configuration file help string to append supported range file extension types
    int h = 0;
    while (strlen(supportedVTKConfigFileExt[h]) != 0) {
      inputVTKMeshLocHelp = inputVTKMeshLocHelp + (string)supportedVTKConfigFileExt[h] + ",";
      h++;
    }
    inputVTKMeshLocHelp.erase(inputVTKMeshLocHelp.end()-1,inputVTKMeshLocHelp.end());

    //Assemble write directory configuration file help string
    h = 0;
    while (strlen(supportedVTKConfigFileExt[h]) != 0) {
      outputVTKFieldLocHelp = outputVTKFieldLocHelp + (string)supportedVTKConfigFileExt[h] + ",";
      h++;
    }
    outputVTKFieldLocHelp.erase(outputVTKFieldLocHelp.end()-1,outputVTKFieldLocHelp.end());

    //output help message
    cout << "Valid input arguments: \n"
         << inputVTKMeshLocArg << " : " << inputVTKMeshLocHelp << "\n"
         << outputVTKFieldLocArg << " : " << outputVTKFieldLocHelp << "\n"
         << xrangeArg << " : " << xrangeArgHelp << "\n"
         << yrangeArg << " : " << yrangeArgHelp << "\n"
         << zrangeArg << " : " << zrangeArgHelp << endl;
  }

  return ret;

}

/*!
 * Function for extracting the vacuum field grid spatial range and resolution
 * along a particular dimension from a passed string (semicolon separated)
 *
 * @param range The grid spatial range and resolution
 * @return tuple - {minimum coord, maximum coord, sample points}
 */
tuple<double, double, int> extractSpatialGridRange(string range){
  string processedStrRange = range;
  //string processedStrPoint = range.substr(0, range.length() - 1);
  int c1 = processedStrRange.find(";");
  string min = processedStrRange.substr(0, c1);
  processedStrRange = processedStrRange.substr(c1+1, processedStrRange.length() - 1);
  int c2 = processedStrRange.find(";");
  string max = processedStrRange.substr(0, c2);
  string steps = processedStrRange.substr(c2+1);
  double dmin, dmax;
  int dsteps;
  try {
     dmin = stod(min);
     dmax = stod(max);
     dsteps = stoi(steps);
   }
   catch (const std::invalid_argument& ia) {
     cout << "spatial range '" << range <<
             "' over which to evaluate surrounding field could not be read " <<
             "correctly. Please check format matches 'start;stop;steps' where " <<
             "range is passed in quotation marks.";
     exit(1);
   }

   tuple<double, double, int> extractedRange = {dmin, dmax, dsteps};

   return extractedRange;
}
