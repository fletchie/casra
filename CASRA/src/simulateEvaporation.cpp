/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cstdio>
#include <tuple>

#include "CASRA/simulateEvaporation.h"
#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/evaporationModels/levelSetModel/lscore.h"
#include "CASRA/electrostatics/constantBEM.h"
#include "CASRA/specimenModels/numericalModel.h"
#include "CASRA/geometries/generateGeometries.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/thermalModels/thermalModels.h"

using namespace std;

string defaultWriteDir = "output";

const char *helpSynonyms[] = {"-h", "-help", ""};
const char *supportedXMLConfigFileExt[] = {"xml", ""};

//initialise help strings for -h mode
string configFileArgHelp = "File location of model configuration file (defaults to current directory). \n\tSupported extensions: ";
string writeDirArgHelp = "Directory location to write simulation results.";

//define scale constants
#define NANOMETRECONV 1e-9
#define NANOMETREVOLCONV 1e-27

//define Sussman reinitialisation parameters
#define SUSSTOL 0.1
#define SUSSMAXITS 20

//define constant BEM solver parameters
#define BEMSOLVERTOL 0.1

/*!
 *
 * Function to simulate evaporation of a given specimen model (defined within modelParams (a ModelParams object instance)).
 *
 * @param modelParams a ModelParams object instance containing all the model specific parameters required to run the evaporation simulation
 * @param writeDir The directory location to write calculated model surfaces to
 * @return (0) if successful, (1) otherwise
 *
 */
int simulateEvaporation(ModelParams modelParams, string writeDir) {

  //initialising model
  cout << "Initialising the specimen model..." << endl;
  LevelSetDomain lSDom(modelParams.eModel.gridCells[0], modelParams.eModel.gridCells[1], modelParams.eModel.gridCells[2], Point(0.0, 0.0, 0.0), 1.0);
  lSDom.assignSpecimenModel(modelParams.sModel.specimenModel, true, modelParams.eModel.gridCells[0], modelParams.eModel.gridCells[1], modelParams.eModel.gridCells[2]);
  lSDom.assignExternalModel(modelParams.esModel.systemModelMeshes, modelParams.esModel.systemModelVoltages);

  cout << "Specimen model initialisation complete." << endl;

  for (int p=0;p<modelParams.sModel.specimenModel.phases.size();p++){
    vtkObject outMesh;
    TriangularMesh& phaseMesh = modelParams.sModel.specimenModel.phases[p].phaseBoundary;
    outMesh.setWithTriangularMesh(phaseMesh);
    writeVtk(outMesh, writeDir + "/initial_model/" + modelParams.sModel.specimenModel.phases[p].name + ".vtk");
  }

  //initialising electrostatic level set iterator
  ElectrostaticConstantBEMIterator evaporate(lSDom, modelParams.eModel.timeStepFraction, BEMSOLVERTOL, modelParams.sModel.minSpecimenLength, true, modelParams.sModel.shankAngle);
  evaporate.setTerminationConditions(modelParams.cDict.stopTime, modelParams.cDict.stopIts, modelParams.cDict.stopApexHeight);

  //specify surface thermal heating model
  if (modelParams.tModel.thermalModel == "instantaneousHeatingModel"){
    InstantaneousHeatingModel tempModel(modelParams.tModel.dir, modelParams.tModel.TMax, modelParams.tModel.T0);
    evaporate.setThermalModel(tempModel);
  }
  else if (modelParams.tModel.thermalModel == "uniformTemperatureModel"){
    UniformTemperatureModel tempModel(modelParams.tModel.T0);
    evaporate.setThermalModel(tempModel);
  }
  else {
    cerr << "unrecognised thermal heating model name. Please use one of recognised laser models." << endl;
    exit(1);
  }

  //if msim directory exists, clear subdirectories
  if (modelParams.sModel.specimenModel.phases[0].fileLocation != ""){
    if (filesystem::is_regular_file(writeDir + "/" + modelParams.sModel.specimenModel.phases[0].fileLocation)){
      string surfaceLocation = modelParams.sModel.specimenModel.phases[0].fileLocation;
      reverse(surfaceLocation.begin(), surfaceLocation.end());
      string surfaceSuffix = surfaceLocation.substr(0, surfaceLocation.find("_"));
      reverse(surfaceSuffix.begin(), surfaceSuffix.end());
      string surfaceIt = surfaceSuffix.substr(0, surfaceSuffix.find(".vtk"));
      cout << "Continuation of simulation state detected" << endl;
      cout << "Continuing from iteration: " << surfaceIt << endl;
      evaporate.it = stoi(surfaceIt);
    }
  }
  else if (filesystem::is_directory(writeDir + "/msim")){
    for (const auto& entry : filesystem::directory_iterator(writeDir + "/msim"))
         filesystem::remove_all(entry.path());
  }

  //initialise output vtk object instance
  vtkObject out;

  int info;
  int terminate = 0;

  vector<string> surfacesNames;
  vector<string> surfacesTimes;

  while (terminate == 0){

    cout << "***Iteration: " << evaporate.it << endl;
    tuple<int, TriangularMesh, vector<double>, vector<double>, int> retVals = evaporate.iterate();
    vector<double> pot = get<2>(retVals);
    vector<double> flux = get<3>(retVals);
    info = get<4>(retVals);

    if (info == 0){
      cout << "***Iteration " << evaporate.it - 1 << " complete \t time: " << evaporate.T << endl;

      terminate = get<0>(retVals);

      //output model surface at current iteration
      TriangularMesh outMesh = get<1>(retVals);
      out.setWithTriangularMesh(outMesh);
      writeVtk(out, writeDir + "/msim/surfaces/surface_" + to_string(evaporate.it) + ".vtk");

      //write .vtk.series file
      writeVtkSeries(writeDir + "/msim/surfaces.vtk.series", surfacesNames, surfacesTimes);

      int itc = outMesh.faces.size();
      for (int e = 0; e < modelParams.esModel.systemModelMeshes.size(); e++) {
          TriangularMesh systemMesh = modelParams.esModel.systemModelMeshes[e];
          vector<double> epot(pot.begin() + itc, pot.begin() + itc + systemMesh.faces.size());
          vector<double> eflux(flux.begin() + itc, flux.begin() + itc + systemMesh.faces.size());
          itc += systemMesh.faces.size();
          systemMesh.assignCellData(epot, "voltage");
          systemMesh.assignCellData(eflux, "flux");
          out.setWithTriangularMesh(systemMesh);
          writeVtk(out, writeDir + "/msim/external/" + modelParams.esModel.systemModelNames[e] + "/surface_" + to_string(evaporate.it) + ".vtk");
      }

      //append outputted model surface name and iteration number for .vtk.series
      surfacesNames.push_back("surfaces/surface_" + to_string(evaporate.it) + ".vtk");
      surfacesTimes.push_back(to_string(evaporate.it));

    }

    //perform Sussman reinitialisation
    if (evaporate.it % modelParams.eModel.reinitItGap == 0 && evaporate.it > 2){
      cout << "perform reinitialization" << endl;
      lSDom.sussmanReinitialisation(SUSSTOL, SUSSMAXITS);
    }
  }

  return 0;

}

/*!
 *
 * Function called on running CASRA simulated evaporation tool from the command line
 * takes command line arguments
 */
int main(int argc, char* argv[])
{

  cout << "Welcome to CASRA's simulated specimen evaporation program. \nRun `simulateEvaporation -h` for help." << endl;

  int ret = 0;

  char configFileArg[] = "--model";
  //char writeDirArg[] = "--output";

  string modelConfigFile = "";
  string writeDir = "";

  string argSymbol = "--";
  string argInputVal;

  bool helpEnabled = false;

  //if no input detected
  if (argc == 1) {
    cout << "No model configuration file supplied. Defaulting to checking in current directory." << endl;
    modelConfigFile = ".";
  }
  if (argc == 2) { //check if help passed
    int a = 0;
    while (strlen(helpSynonyms[a]) != 0) {
      if (strcmp(argv[1], helpSynonyms[a]) == 0) {
        helpEnabled = true;
        break;
      }
      a ++;
    }
  }
  else {
    if (!(argc % 2)) {
      cout << "Incomplete number of inputs detected. Please check all arguments are defined." << endl;
      return 1;
    }
    else {
      for (int i=1; i < argc; i++){
        if (i % 2 == 1) {
          argInputVal = (string)argv[i];
          if (argSymbol == argInputVal.substr(0, 2)) {
            //command line input argument detected
            if (strcmp(configFileArg, argv[i])==0) {
              cout << "Supplied argument: " << configFileArg << " " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                modelConfigFile = (string)argv[i+1];
              }
              else {
                cout << "No following input argument found for: " << configFileArg << ". Please check input." << endl;
                return 1;
              }
            }
            else {
              cout << "Unidentified input argument name detected. Please check input." << endl;
              return 1;
            }
          }
          else {
            cout << "Input argument name " << argv[i] << " not correctly marked as argument. Please check input." << endl;
            return 1;
          }
        }
      }

      if (modelConfigFile == "") {
        cout << "No model configuration file supplied. Defaulting to checking in current directory." << endl;
        modelConfigFile = ".";
      }

      ModelParams modelParams;

      //read in model parameters from passed configFile
      if (modelConfigFile == ""){
        modelParams = initialiseModelFromXML("configuration.xml");
      }
      else {
        modelParams = initialiseModelFromXML(modelConfigFile + "/configuration.xml");
      }
      writeDir = modelConfigFile;

      //run evaporation simulator using loaded model parameters
      ret = simulateEvaporation(modelParams, writeDir);

      if (ret > 0) {
        cout << "Execution failed." << endl;
      }
      else {
        cout << "Execution succeeded." << endl;
      }
    }
  }

  if (helpEnabled == true) {

    //Assemble model configuration file help string to append supported range file extension types
    int h = 0;
    while (strlen(supportedXMLConfigFileExt[h]) != 0) {
      configFileArgHelp = configFileArgHelp + (string)supportedXMLConfigFileExt[h] + ",";
      h++;
    }
    configFileArgHelp.erase(configFileArgHelp.end()-1,configFileArgHelp.end());

    //Assemble write directory configuration file help string
    writeDirArgHelp = writeDirArgHelp;

    //output help message
    cout << "Valid input arguments: \n"
         << configFileArg << " : " << configFileArgHelp << endl;
  }

  return ret;

}
