/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cstdio>

#include "CASRA/reconstructData.h"
#include "CASRA/reconstruction/pointProjectionReconstruction/pointProjectionReconstruction.h"
#include "CASRA/reconstruction/modelDrivenReconstruction/modelDrivenReconstruction.h"
#include "CASRA/specimenModels/analyticModel.h"
#include "CASRA/experimentalData/experimentalData.h"
#include "CASRA/reconstruction/reconstructionInitialiser.h"
#include "CASRA/io/rangeReader.h"
#include "CASRA/io/APTReaders.h"
#include "CASRA/io/APTWriters.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/field/scalarField.h"

using namespace std;

const char *helpSynonyms[] = {"-h", "-help", ""};

//initialise help strings for -h mode
string aptFileArgHelp = "Location of the target APT data file to be reconstructed. \n\tSupported extensions: ";
string rangeFileArgHelp = "Location of the APT range file. \n\tSupported extensions: ";
string configFileArgHelp = "Location of the reconstruction configuration file. \n\tSupported extensions: ";
string outputaptFileArgHelp = "Location for the outputted reconstruction. \n\tSupported extensions: ";

//define constants
#define NANOMETRECONV 1e-9
#define NANOMETREVOLCONV 1e-27

int APTDataSet::readData(string outputFile, string appendFileName) {

  string suffx = outputFile.substr(outputFile.find_last_of(".") + 1);
  string prename = outputFile.substr(0, outputFile.find_last_of("."));

  outputFile = prename + appendFileName + "." + suffx;

  if (suffx == "epos") {
    this->eposDataReader(outputFile);
  }
  else if (suffx == "ato") {
    this->atoDataReader(outputFile);
  }
  else {
    cout << "Unrecognised (unsupported) filetype: " << suffx << "\n Please make sure file is either .ato or .epos" << endl;
    return 1;
  }

  return 0;

}

int APTDataSet::writeData(string outputFile, string appendFileName) {

  string suffx = outputFile.substr(outputFile.find_last_of(".") + 1);
  string prename = outputFile.substr(0, outputFile.find_last_of("."));

  outputFile = prename + appendFileName + "." + suffx;

  if (suffx == "epos") {
    this->eposDataWriter(outputFile);
  }
  else if (suffx == "pos") {
    this->posDataWriter(outputFile);
  }
  else if (suffx == "ato") {
    this->atoDataWriter(outputFile);
  }
  else {
    cout << "Unrecognised (unsupported) filetype: " << suffx << "\n Please make sure file is either .ato, .epos or .pos" << endl;
    return 1;
  }

  return 0;

}

int reconstructAPTData(string aptFile, string rangeFile, string configFile, string outputFile) {

  int readRet;
  int outRet;
  int rangeRetVal;

  APTDataSet loadedData;
  RangeData rFD;

  if (rangeFile.length() > 0) {
    rangeRetVal = rFD.readFile(rangeFile);
    if (rangeRetVal != 0) {
      return rangeRetVal;
    }
  }
  else{
    cerr << "WARNING: No range file passed. Continuing with unranged data." << endl;
  }

  readRet = loadedData.readData(aptFile, "");
  if (readRet != 0){
    return readRet;
  }

  string rType = reconType(configFile);
  cout << rType << endl;
  if (rType == "pointProjectionReconstruction"){
    pointProjectionReconstructionParams reconparams = pointProjectionReconstructionInitialiser(configFile);

    //declare Atom Probe Instrument parameters
    double detectorRadius = reconparams.iModel.detectorRadius;
    double flightpath = reconparams.iModel.flightpath;
    double detectorEfficiency = reconparams.iModel.efficiency;
    AnalyticDetector detector(detectorRadius, flightpath, detectorEfficiency);

    //initialise model geometry variables
    double initRad = reconparams.sModel.R0;
    double z0 = reconparams.sModel.initialHeight;

    //range APT dataset - assigns ionic volumes
    cout << "ranging data" << endl;
    loadedData.simpleRangeDataSet(rFD);
    cout << "ranging complete" << endl;

    //artificially reduce FOV (set artificial detector radius - filter ions outside of this)
    loadedData.filterExperimentDataOutsideFOV(detectorRadius,
                                              reconparams.pModel.imagecompression,
                                              reconparams.sModel.shank,
                                              reconparams.sModel.kFactors);

    cout << "\tInitial apex radius: " << initRad << endl;
    cout << "\tLoaded dataset size: " << loadedData.events.size() << endl;

    //initialise projection model
    if (reconparams.sModel.specimenType == "shankBasEmitterModel"){
      ShankBasEmitterModel emitter(reconparams.pModel.imagecompression, initRad,
                                   reconparams.sModel.initialPosition,
                                   reconparams.sModel.shank);
      dataReconstruction(loadedData, emitter, detector);
    }
    else if (reconparams.sModel.specimenType == "voltageBasEmitterModel"){
      VoltageBasEmitterModel emitter(reconparams.pModel.imagecompression, initRad,
                                     reconparams.sModel.initialPosition,
                                     reconparams.sModel.kFactors,
                                     reconparams.sModel.evapField);
      dataReconstruction(loadedData, emitter, detector);
    }
    else{
      cout << "ERROR: No valid emitter model found. Make sure emitter model is one of types: " <<
      "`shankBasEmitterModel`, `voltageBasEmitterModel` \n" << endl;
      return 1;
    }
  }
  else if (rType == "modelDrivenReconstruction"){

    //range APT dataset - assigns ionic volumes
    cout << "ranging data" << endl;
    loadedData.simpleRangeDataSet(rFD);
    cout << "ranging complete" << endl;

    //load model driven reconstruction parameters
    modelDrivenReconstructionParams reconparams = modelDrivenReconstructionInitialiser(configFile);

    //read the trajectory mapping
    TsvObject mappingData;
    int retCode = mappingData.readHeaderedTSV(reconparams.tMapping.location);
    if (retCode == 1){
      cerr << "ERROR: Failed to open trajectory mapping. Please check mapping tsv file exists at location: " <<
              reconparams.tMapping.location << endl;
      exit(1);
    }

    TrajectoryMapping trajectoryMap(mappingData);

    //Assign evaporation volume depths
    map<int, double> evapVolumes = determineEvapVolumeDepths(trajectoryMap,
                                                             reconparams.iModel.detectorRadius,
                                                             reconparams.iModel.efficiency);

    //determine model iteration landmark positions from floating landmark conditions
    vector<landmarkParams> processedLandmarks = determineModelLandmarkPositions(trajectoryMap,
                                                                                reconparams.landmarks,
                                                                                reconparams.iModel.detectorRadius);

    //remove ions outside of inputted detector FOV, update landmarks
    filterExperimentDataOutsideFOV(processedLandmarks,
                                   loadedData,
                                   reconparams.iModel.detectorRadius);

    cout << "Determine landmark ionic volumes" << endl;
    //calculate ionic volumes to use between landmarks
    determineLandmarkIonicVolumes(processedLandmarks, evapVolumes);

    //assign ion detector space depth positions
    cout << "\tAssign ion detector space depth positions" << endl;
    vector<double> eventDepthPositions = assignIonDepthPositions(loadedData, processedLandmarks);

    //construct interpolated detector stack
    int xDim = 300;
    int yDim = 300;

    cout << "calculate detector map" << endl;
    int startIt = processedLandmarks[0].modelIt;
    int finalIt = processedLandmarks[processedLandmarks.size()-1].modelIt;
    std::tuple<scalarField3D, scalarField3D, scalarField3D> detectorSpaceMapping = constructDetectorMappingStack(trajectoryMap, startIt, finalIt,
                                                                                                                 reconparams.iModel.detectorRadius * SCALEFACTOR,
                                                                                                                 xDim, yDim);
    cout << "detector map calculation complete" << endl;
    scalarField3D& mappingX = get<0>(detectorSpaceMapping);
    scalarField3D& mappingY = get<1>(detectorSpaceMapping);
    scalarField3D& mappingZ = get<2>(detectorSpaceMapping);

    //vtkObject outx;
    //outx.setWithField(mappingX);
    //writeVtk(outx, "fieldx.vtk");
    //vtkObject outy;
    //outy.setWithField(mappingY);
    //writeVtk(outy, "fieldy.vtk");
    //vtkObject outz;
    //outz.setWithField(mappingZ);
    //writeVtk(outz, "fieldz.vtk");

    cout << processedLandmarks[1].experimentLandmark << " " << processedLandmarks[1].modelIt << endl;

    cout << "Building the model-driven reconstruction" << endl;
    modelDrivenReconstructionProtocol(loadedData, processedLandmarks,
                                      mappingX, mappingY, mappingZ,
                                      evapVolumes, xDim, yDim, reconparams.iModel.detectorRadius);
    cout << "Model-driven reconstruction complete." << endl;

  }
  else {
    cerr << "ERROR: reconstruction protocol not supported. Please make sure reconstruction protocol " <<
            "is one of `pointProjectionReconstruction, modelDrivenReconstruction`" << endl;
    return 1;
  }

  //output reconstructed APT data
  outRet = loadedData.writeData(outputFile, "");
  if (outRet != 0){
    return outRet;
  }

  //output detector space reconstruction
  for  (long a=0;a<loadedData.events.size();a++){
    loadedData.events[a].sx = loadedData.events[a].dx/1e9;
    loadedData.events[a].sy = loadedData.events[a].dy/1e9;
    loadedData.events[a].sz = 1e-9 * 10.0 * a/loadedData.events.size();
  }

  //output reconstructed APT data in det space
  outRet = loadedData.writeData(outputFile, "_ds");
  if (outRet != 0){
    return outRet;
  }

  return 0;

}

/*!
* Function called on running CASRA from command line
* takes default command line arguments
*/
int main(int argc, char* argv[])
{

  int ret = 0;

  string defOutputAPTFile = "recon.pos";

  char aptFileArg[] = "--aptdata";
  char rangeFileArg[] = "--rangefile";
  char configFileArg[] = "--config";
  char outputaptFileArg[] = "--output";

  string aptFile = "";
  string configFile = "";
  string rangeFile = "";
  string outputAPTFile = "";

  string argSymbol = "--";
  string argInputVal;

  bool helpEnabled = false;
  //if no input detected
  if (argc == 1) {
    cout << "Welcome to CASRA's point projection reconstruction program. \nRun './reconstructData -h' for help." << endl;
  }
  else if (argc == 2) { //check if help passed
    int a = 0;
    while (strlen(helpSynonyms[a]) != 0) {
      if (strcmp(argv[1], helpSynonyms[a]) == 0) {
        helpEnabled = true;
        break;
      }
      a ++;
    }
  }
  else {
    cout << "Input arguments: " << argc << endl;
    if (!(argc % 2)) {
      cout << "ERROR: Incomplete number of inputs detected. Please check all arguments are defined." << endl;
      return 1;
    }
    else {
      for (int i=1; i < argc; i++){
        if (i % 2 == 1) {
          argInputVal = (string)argv[i];
          if (argSymbol == argInputVal.substr(0, 2)) {
            //command line input argument detected
            if (strcmp(aptFileArg, argv[i])==0) {
              cout << "Supplied argument: " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                aptFile = (string)argv[i+1];
              }
              else {
                cout << "ERROR: No following input argument found for: " << aptFileArg << ". Please check input." << endl;
                return 1;
              }
            }
            else if (strcmp(rangeFileArg, argv[i])==0) {
              cout << "Supplied argument: " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                rangeFile = (string)argv[i+1];
              }
              else {
                cout << "ERROR: No following input argument found for: " << rangeFileArg << ". Please check input." << endl;
                return 1;
              }
            }
            else if (strcmp(configFileArg, argv[i])==0) {
              cout << "Supplied argument: " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                configFile = (string)argv[i+1];
              }
              else {
                cout << "ERROR: No following input argument found for: " << configFileArg << ". Please check input." << endl;
                return 1;
              }
            }
            else if (strcmp(outputaptFileArg, argv[i])==0) {
              cout << "Supplied argument: " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                outputAPTFile = (string)argv[i+1];
              }
              else {
                cout << "ERROR: No following input argument found for: " << outputAPTFile << ". Please check input." << endl;
                return 1;
              }
            }
            else {
              cout << "ERROR: Unidentified input argument name '" << argv[i] << "' detected. Please check input. Valid arguments can be seen via -h" << endl;
              return 1;
            }
          }
          else {
            cout << "ERROR: Input argument name " << argv[i] << " not correctly marked as argument. Please check input." << endl;
            return 1;
          }
        }
      }

      if (aptFile == "") {
        cout << "ERROR: No apt data file supplied. No data to reconstruct." << endl;
        return 1;
      }
      if (configFile == "") {
        cout << "ERROR: No model configuration file supplied. Cannot define emitter model for reconstruction." << endl;
        return 1;
      }
      if (rangeFile == "") {
        cout << "WARNING: No range file supplied." << endl;
      }
      if (outputAPTFile == "") {
        outputAPTFile = defOutputAPTFile;
        cout << "No output APT data file provided. Using default value of: " << outputAPTFile << endl;
      }

      cout << aptFile << " " << configFile << " " << rangeFile << " " << outputAPTFile << endl;
      ret = reconstructAPTData(aptFile, rangeFile, configFile, outputAPTFile);

      if (ret > 0) {
        cout << "Execution failed." << endl;
      }
      else {
        cout << "Execution succeeded." << endl;
      }
    }
  }

  if (helpEnabled == true) {

    //Assemble range file help string to append supported range file extension types
    int h = 0;
    while (strlen(supportedRangeFileExt[h]) != 0) {
      rangeFileArgHelp = rangeFileArgHelp + (string)supportedRangeFileExt[h] + ",";
      h++;
    }
    rangeFileArgHelp.erase(rangeFileArgHelp.end()-1,rangeFileArgHelp.end());

    //Assemble APT file help string to append supported APT file extension types
    h = 0;
    while (strlen(supportedAPTInputFileExt[h]) != 0) {
      aptFileArgHelp = aptFileArgHelp + (string)supportedAPTInputFileExt[h] + ",";
      h++;
    }
    aptFileArgHelp.erase(aptFileArgHelp.end()-1,aptFileArgHelp.end());

    //Assemble APT output file help string to append supported APT output file extension types
    h = 0;
    while (strlen(supportedAPTOutputFileExt[h]) != 0) {
      outputaptFileArgHelp = outputaptFileArgHelp + (string)supportedAPTOutputFileExt[h] + ",";
      h++;
    }
    outputaptFileArgHelp.erase(outputaptFileArgHelp.end()-1,outputaptFileArgHelp.end());

    //Assemble configuration file help string to append supported configuration file extension types
    h = 0;
    while (strlen(supportedXMLConfigFileExt[h]) != 0) {
      configFileArgHelp = configFileArgHelp + (string)supportedXMLConfigFileExt[h] + ",";
      h++;
    }
    configFileArgHelp.erase(configFileArgHelp.end()-1,configFileArgHelp.end());

    //output help message
    cout << "Valid input arguments: \n"
         << aptFileArg << " : " << aptFileArgHelp << "\n"
         << rangeFileArg << " : " << rangeFileArgHelp << "\n"
         << configFileArg << " : " << configFileArgHelp << "\n"
         << outputaptFileArg << " : " << outputaptFileArgHelp << endl;
  }

  return ret;

}
