/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <cstdio>
#include <tuple>
#include <set>
#include <chrono>

using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::milliseconds;
using std::chrono::seconds;

#if __has_include(<filesystem>)
  #include <filesystem>
#elif __has_include(<experimental/filesystem>)
  #include <experimental/filesystem>
  namespace std {
      namespace filesystem = experimental::filesystem;
  }
#endif

#include "CASRA/calculateTrajectories.h"
#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/specimenModels/numericalModel.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/chargedOptics/chargedOptics.h"

using namespace std;

string defaultWriteDir = "output";

const char *helpSynonyms[] = {"-h", "-help", ""};
const char* supportedXMLConfigFileExt[] = {"xml", ""};

//initialise help strings for -h mode
string configFileArgHelp = "File location of model configuration file. \n\tSupported extensions: ";
string modeldirArgHelp = "Directory location for input specimen surface stack.";
string writeDirArgHelp = "Directory location to write calculated trajectory results.";

#define RELRKTOL 1e-3 //relative tolerance for Dormand Prince RK54 solver
#define PROJBEMSOLVERTOL 0.5 //tolerance for the electrostatic bemsolver when performing ion projection (determines switch between 1 point and 7 point quadrature panel integration)

/*!
 *
 * Function to calculate trajectories from an inputted model msim directory
 * @param configFile The simulation configuration file location
 * @param modelDir The model directory location
 * @param writeDir The output location to write trajectory results to (placed in 'writeDir/projection')
 * @return 0 if successful, 1 otherwise
 */
int calculateTrajectories(ModelParams modelParams, OpticsParams opticsParams, string modelDir, string writeDir) {

  if ((opticsParams.cutoffDistance <= 0.0) && (opticsParams.dparams.enabled == false)){
    cerr << "\tERROR: neither a distance threshold or detector has been set at which to terminate trajectory integration. \n\tPlease specify at least one of these within the model configuration file." << endl;
    exit(1);
  }

  //create reference to loaded specimen model
  TMSpecimen& smodel = modelParams.sModel.specimenModel;

  cout << "Building range file for synthetic data..." << endl;
  set<string> uniqueIonLabels;
  map<string, double> ionLabelVolumeMap;
  for (int p=0;p<smodel.phases.size();p++){
    if (smodel.phases[p].ghost == false){
      for (int e=0;e<smodel.phases[p].elements.size();e++){
        uniqueIonLabels.insert(smodel.phases[p].elements[e]);
        ionLabelVolumeMap[smodel.phases[p].elements[e]] = smodel.phases[p].elementIonicVolumes[e];
      }
    }
    else {
      map<string, string>::iterator it;
      for (it = smodel.phases[p].substituteElements.begin(); it != smodel.phases[p].substituteElements.end(); it++){
          string oldLabel = it->first;
          string newLabel = it->second;
          uniqueIonLabels.insert(newLabel);
          ionLabelVolumeMap[newLabel] = smodel.phases[p].substituteIonicVolumes[oldLabel];
      }
    }
  }
  cout << "Rangefile build complete." << endl;

  //if trajectories directory exists, clear subdirectories
  if (filesystem::is_directory(writeDir + "/projection")){
    for (const auto& entry : filesystem::directory_iterator(writeDir + "/projection"))
         filesystem::remove_all(entry.path());
  }

  vector<SyntheticPeak> peakSet = createPeakSet(ionLabelVolumeMap);
  string dataLoc = writeDir + "/synthetic_data/";

  rangeFileSyntheticDataWriter(dataLoc + "synthetic_data.rrng", peakSet);

  Detector* detPointer;

  if (opticsParams.projectionType == "uniform"){
    if (opticsParams.cutoffDistance > 0.0){
      detPointer = nullptr;
    }
    else if (opticsParams.dparams.enabled == true){
      cout << "create detector" << endl;
      detPointer = new Detector(opticsParams.dparams.position, opticsParams.dparams.normal, opticsParams.dparams.radius);
    }
  }
  else if (opticsParams.projectionType == "realistic"){
    cout << "create detector" << endl;
    detPointer = new Detector(opticsParams.dparams.position, opticsParams.dparams.normal, opticsParams.dparams.radius);
  }

  //load specimen surface data
  vector<string> directories;
  vector<string> fdirectories;
  vector<string> sortedDirectories;
  vector<int> fileorder;
  map<string, vector<string>> componentDirectories;
  map<string, vector<string>> sortedComponentDirectories;

  if (opticsParams.itStart > 1) {
    opticsParams.itStart -= 1;
    opticsParams.itStop -= 1;
  }

  //load in surface file locations from directory
  filesystem::directory_iterator end_itr;
  for (filesystem::directory_iterator itr(modelDir + "/msim/surfaces"); itr != end_itr;++itr){
        directories.push_back(itr->path().u8string());
  }

  //read in external system meshes if directory exists
  if (filesystem::exists(modelDir + "/msim/external")) {
      //load in external system components from directory
      for (filesystem::directory_iterator itr(modelDir + "/msim/external"); itr != end_itr; ++itr) {
          for (filesystem::directory_iterator itr2(itr->path().u8string()); itr2 != end_itr; ++itr2) {
              componentDirectories[itr->path().u8string()].push_back(itr2->path().u8string());
          }
      }
  }

  //reorder loaded surface file locations
  for (int p=0;p<directories.size();p++){
    string surfaceFileName = directories[p];
    reverse(surfaceFileName.begin(), surfaceFileName.end());
    int loc = surfaceFileName.find("_");
    if (loc == string::npos){
      cerr << "WARNING: file '" << directories[p] << "' numbering incorrect. Ignoring file." << endl;
      continue;
    }
    string end = surfaceFileName.substr(0, loc);
    reverse(end.begin(), end.end());
    loc = end.find(".");
    if (loc == string::npos){
      cerr << "WARNING: file '" << directories[p] << "' does not have suffix (cannot be identified as vtk). Ignoring file." << endl;
      continue;
    }
    string fileSuffix = end.substr(loc+1);
    if (fileSuffix != "vtk"){
      cerr << "WARNING: file '" << directories[p] << "' identified as not having .vtk suffix. Ignoring file." << endl;
      continue;
    }

    string surfaceNum = end.substr(0, loc);

    fdirectories.push_back(directories[p]);
    fileorder.push_back(stoi(surfaceNum));

  }

  cout << "sorting directories" << endl;
  vector<int> directoryFileInds(fdirectories.size(), 0);
  iota(directoryFileInds.begin(),directoryFileInds.end(),0);
  sort(directoryFileInds.begin(), directoryFileInds.end(), [&](int i,int j){return fileorder[i] < fileorder[j];});

  for (int i=0;i<directoryFileInds.size();i++){
    sortedDirectories.push_back(fdirectories[directoryFileInds[i]]);
    map<string, vector<string>>::iterator it;
    for (it = componentDirectories.begin(); it != componentDirectories.end(); it++){
        sortedComponentDirectories[it->first].push_back(it->second[directoryFileInds[i]]);
    }
  }

  cout << "sorting complete" << endl;

  vector<string> trajectoriesNames;
  vector<string> trajectoriesTimes;

  cout << "read and sorted model surface files" << endl;

  bool dataWriteHeader = true;

  TriangularMesh previousSurfaceState;
  TriangularMesh surfaceMesh;
  TriangularMesh nextSurfaceState;

  for (int l=0;l<(sortedDirectories.size() - opticsParams.itStart)/opticsParams.itStep;l++){

    int p = l * opticsParams.itStep + opticsParams.itStart;
    if (p >= opticsParams.itStop){
      break;
    }

    string surfaceLocation = sortedDirectories[p];
    reverse(surfaceLocation.begin(), surfaceLocation.end());
    string surfaceSuffix = surfaceLocation.substr(0, surfaceLocation.find("_"));
    reverse(surfaceSuffix.begin(), surfaceSuffix.end());

    cout << "Performing ion projection for model iteration: " << p+1 << endl;

    vtkObject in1;
    vtkObject in2;

    vector<double> q;
    vector<double> u;
    vector<double> avq;
    vector<double> isSurface;

    int pp = (l+1) * opticsParams.itStep + opticsParams.itStart;
    if (pp < sortedDirectories.size()){
      readVTKUnstructuredGrid(in1, sortedDirectories[pp]);
      nextSurfaceState = in1.triangularMeshFromVtk();
      nextSurfaceState.setMeshProperties();
    }
    else {
      nextSurfaceState.vertices.clear();
      nextSurfaceState.faces.clear();
    }

    if (surfaceMesh.faces.size() == 0) {
      readVTKUnstructuredGrid(in2, sortedDirectories[p]);
      surfaceMesh = in2.triangularMeshFromVtk();
      surfaceMesh.setMeshProperties();
    }

    //read in surface panel data (calculated boundary fields etc...)
    for (int i=0;i<surfaceMesh.surfaceCellFields["flux"].size();i++){
      q.push_back(surfaceMesh.surfaceCellFields["flux"][i][0]);
      avq.push_back(surfaceMesh.surfaceCellFields["average_flux"][i][0]);
      isSurface.push_back(surfaceMesh.surfaceCellFields["is_surface"][i][0]);
      u.push_back(1.0); //under the current conductor approximation, surface potential values are irrelevent
    }

    //read in external system components for electrostatic solution
    map<string, vector<string>>::iterator it;
    vector<TriangularMesh> externalComponents;
    vector<double> extq;
    vector<double> extu;
    for (it = sortedComponentDirectories.begin(); it != sortedComponentDirectories.end(); ++it) {
        vtkObject inComp;
        readVTKUnstructuredGrid(inComp, sortedComponentDirectories[it->first][p]);
        TriangularMesh componentMesh = inComp.triangularMeshFromVtk();
        externalComponents.push_back(componentMesh);
        for (int i=0;i<componentMesh.surfaceCellFields["flux"].size();i++) {
            extq.push_back(componentMesh.surfaceCellFields["flux"][i][0]);
            extu.push_back(1.0); //under the current conductor approximation, surface potential values are irrelevent
        }
    }

    surfaceMesh.setBoundingBox();
    surfaceMesh.isSurface = isSurface;

    electrostaticBemSolver bemSolver(PROJBEMSOLVERTOL);
    bemSolver.u = u;
    bemSolver.q = q;

    double minCoords[3] = {pointDimMinimum(surfaceMesh.vertices, 0),
                           pointDimMinimum(surfaceMesh.vertices, 1),
                           pointDimMinimum(surfaceMesh.vertices, 2)};

    double maxCoords[3] = {pointDimMaximum(surfaceMesh.vertices, 0),
                           pointDimMaximum(surfaceMesh.vertices, 1),
                           pointDimMaximum(surfaceMesh.vertices, 2)};

    opticsParams.dparams.position.pz -= maxCoords[2];

    //set charged optics domain dimensions
    double xmin, xmax, ymin, ymax, zmin, zmax;
    if (opticsParams.cutoffDistance > 0.0){
      xmin = minCoords[0] - opticsParams.cutoffDistance * 3.0;
      xmax = maxCoords[0] + opticsParams.cutoffDistance * 3.0;
      ymin = minCoords[1] - opticsParams.cutoffDistance * 3.0;
      ymax = maxCoords[1] + opticsParams.cutoffDistance * 3.0;
      zmin = minCoords[2] - opticsParams.cutoffDistance * 3.0;
      zmax = maxCoords[2] + opticsParams.cutoffDistance * 3.0;
    }
    else if (opticsParams.dparams.enabled == true){
      xmin = minCoords[0] - opticsParams.dparams.radius * 3.0;
      xmax = maxCoords[0] + opticsParams.dparams.radius * 3.0;
      ymin = minCoords[1] - opticsParams.dparams.radius * 3.0;
      ymax = maxCoords[1] + opticsParams.dparams.radius * 3.0;
      zmin = minCoords[2];
      zmax = maxCoords[2] + opticsParams.dparams.position.pz * 1.5;
    }

    //define charged optics trajectory calculation domain
    OpticsDomain opticsDom(xmin, xmax, ymin, ymax, zmin, zmax, detPointer, opticsParams.cutoffDistance);

    //define tuple for charged optics results
    tuple<vector<Ion>> chargedOpticsResults;

    //construct averaged normal at vertex based off neighbouring panels
    surfaceMesh.vertexFaceAdjacencyList = surfaceMesh.buildVertexFaceAdjacencyList();
    vector<long> launchSites;
    vector<int> launchPhase;
    vector<Phase*> launchGhostPhases;

    if (previousSurfaceState.faces.size() > 0)
      previousSurfaceState.vol = previousSurfaceState.surfaceCellFields["vol"][0][0];
    surfaceMesh.vol = surfaceMesh.surfaceCellFields["vol"][0][0];
    if (nextSurfaceState.faces.size() > 0)
      nextSurfaceState.vol = nextSurfaceState.surfaceCellFields["vol"][0][0];

    //calculate uniform ion projection from surface
    if (opticsParams.projectionType == "uniform"){
      auto t1 = high_resolution_clock::now();
      launchSites = determineUniformLaunchSites(surfaceMesh);
      auto t2 = high_resolution_clock::now();
      duration<double> ms_double = t2 - t1;
      cout << "\tlaunch position determination time: " << ms_double.count() << endl;

      //append external components to problem boundary
      TriangularMesh totalMesh = surfaceMesh;
      for (int c=0; c<externalComponents.size(); c++) {
          int currentVertexNum = totalMesh.vertices.size();
          int currentFacesNum = totalMesh.faces.size();
          totalMesh.vertices.insert(totalMesh.vertices.end(), externalComponents[c].vertices.begin(), externalComponents[c].vertices.end());
          totalMesh.faces.insert(totalMesh.faces.end(), externalComponents[c].faces.begin(), externalComponents[c].faces.end());
          std::for_each(totalMesh.faces.begin() + currentFacesNum, totalMesh.faces.end(), [currentVertexNum](Face& d) { d.p1 += currentVertexNum; d.p2 += currentVertexNum; d.p3 += currentVertexNum; });
      }
      bemSolver.u.insert(bemSolver.u.end(), extu.begin(), extu.end());
      bemSolver.q.insert(bemSolver.q.end(), extq.begin(), extq.end());
      totalMesh.setMeshProperties();
      totalMesh.setBoundingBox();

      cout << "\tCalculating trajectories..." << endl;
      chargedOpticsResults = opticsDom.calculateUniformChargedOptics(launchSites,
                                                                     totalMesh,
                                                                     bemSolver,
                                                                     smodel,
                                                                     opticsParams.launchDistance,
                                                                     RELRKTOL);
    }
    else if (opticsParams.projectionType == "realistic"){ //perform realistic launching of ions (ion launch flux proportional to local evaporation rate)
      vector<long> candidateLaunchSites(surfaceMesh.faces.size());
      iota(candidateLaunchSites.begin(), candidateLaunchSites.end(), 0);

      vector<double> evapVols(surfaceMesh.faces.size());
      vector<double> evapVols1 = surfaceMesh.distanceFromSurfaceFace(previousSurfaceState, candidateLaunchSites);
      vector<double> evapVols2 = surfaceMesh.distanceFromSurfaceFace(nextSurfaceState, candidateLaunchSites);

      if (previousSurfaceState.faces.size() > 0 && nextSurfaceState.faces.size() > 0)
        evapVols = (evapVols1 + evapVols2) * 0.5;
      else if (previousSurfaceState.faces.size() > 0)
        evapVols = evapVols1 * 0.5;
        else{
        evapVols = evapVols2 * 0.5;
        }

      cout << "\tEstimating number of ions to launch..." << endl;
      tuple<vector<Point>, vector<Vec>, vector<int>, vector<Phase*>> launchSiteRes = determineWeightedLaunchSites(surfaceMesh,
                                                                                                                  previousSurfaceState,
                                                                                                                  nextSurfaceState,
                                                                                                                  smodel,
                                                                                                                  evapVols);
      vector<Point> launchLocations = get<0>(launchSiteRes);
      vector<Vec> launchNormals = get<1>(launchSiteRes);
      launchPhase = get<2>(launchSiteRes);
      launchGhostPhases = get<3>(launchSiteRes);

      cout << "\tTotal calculated ions to launch: " << launchLocations.size() << endl;

      //append external components to problem boundary
      TriangularMesh totalMesh = surfaceMesh;
      for (int c=0; c<externalComponents.size(); c++) {
          int currentFacesNum = totalMesh.faces.size();
          int currentVertexNum = totalMesh.vertices.size();
          totalMesh.vertices.insert(totalMesh.vertices.end(), externalComponents[c].vertices.begin(), externalComponents[c].vertices.end());
          totalMesh.faces.insert(totalMesh.faces.end(), externalComponents[c].faces.begin(), externalComponents[c].faces.end());
          std::for_each(totalMesh.faces.begin() + currentFacesNum, totalMesh.faces.end(), [currentVertexNum](Face& d) { d.p1 += currentVertexNum; d.p2 += currentVertexNum; d.p3 += currentVertexNum; });
      }
      bemSolver.u.insert(bemSolver.u.end(), extu.begin(), extu.end());
      bemSolver.q.insert(bemSolver.q.end(), extq.begin(), extq.end());
      totalMesh.setMeshProperties();
      totalMesh.setBoundingBox();

      cout << "\tCalculating trajectories..." << endl;
      chargedOpticsResults = opticsDom.calculateWeightedChargedOptics(launchLocations,
                                                                      launchNormals,
                                                                      launchPhase,
                                                                      totalMesh,
                                                                      bemSolver,
                                                                      smodel,
                                                                      opticsParams.launchDistance,
                                                                      RELRKTOL);

      //assign pulse voltages to ions (currently a simple linear interpolation of surface potential at surfaceMesh and previousSurfaceState)
      vector<Ion>& launchedIons = get<0>(chargedOpticsResults);
      if (previousSurfaceState.faces.size() > 0 && nextSurfaceState.faces.size()> 0){
        for (int i=0;i<launchedIons.size();i++){
          double t = (double)i/(launchedIons.size()-1);
          launchedIons[i].pulseVoltage = 0.5 * (previousSurfaceState.surfaceCellFields["potential"][0][0] * (1.0-t) +
                                                surfaceMesh.surfaceCellFields["potential"][0][0] +
                                                nextSurfaceState.surfaceCellFields["potential"][0][0] * t);
        }
      }
      else if (previousSurfaceState.faces.size() > 0){
        for (int i=0;i<launchedIons.size();i++){
          double t = 0.5 * (double)i/(launchedIons.size()-1);
          launchedIons[i].pulseVoltage = (previousSurfaceState.surfaceCellFields["potential"][0][0] * (0.5-t) +
                                          surfaceMesh.surfaceCellFields["potential"][0][0] * (0.5 + t));
        }
      }
      else {
        for (int i=0;i<launchedIons.size();i++){
          double t = 0.5 * (double)i/(launchedIons.size()-1);
          launchedIons[i].pulseVoltage = (surfaceMesh.surfaceCellFields["potential"][0][0] * (1.0 - t) +
                                          nextSurfaceState.surfaceCellFields["potential"][0][0] * t);
        }
      }

      cout << "\tTrajectory calculation complete." << endl;
      previousSurfaceState = surfaceMesh;
    }
    else {
      cout << "ERROR: Unsupported ion projection mode detected: " << opticsParams.projectionType << endl;
      cout << "Please use valid projection mode: 'weighted', 'uniform'" << endl;
      exit(1);
    }

    vector<Ion>& launchedIons = get<0>(chargedOpticsResults);
    vector<Ion> filteredIons;
    vector<long> filteredLaunchSites;
    vector<int> filteredLaunchPhase;
    vector<Phase*> filteredLaunchPhaseGhost;

    if (opticsParams.projectionType == "realistic"){

      bool transfer = opticsParams.tparams.enabled;
      double kappa;

      if (opticsParams.tparams.functionParameters.size() > 0){
        kappa = opticsParams.tparams.functionParameters[0];
      }

      int intersections = 0;
      for (int i=0;i<launchedIons.size();i++){
        if (detPointer == nullptr){
          cerr << "ERROR: A detector must be defined when performing a realistic ion projection." << endl;
          exit(1);
        }
        bool intersects = detPointer->calculatedTrajectoryIntersectionPoint(launchedIons[i]);
        if (intersects == true){
          filteredIons.push_back(launchedIons[i]);
          filteredLaunchPhase.push_back(launchPhase[i]);
          filteredLaunchPhaseGhost.push_back(launchGhostPhases[i]);
          intersections++;
        }
        else if (transfer == true) {
          Vec ionVelocity(launchedIons[i].velocity.back().px, launchedIons[i].velocity.back().py, launchedIons[i].velocity.back().pz);
          bool intersects = detPointer->linearProjection(launchedIons[i].trajectory.back(), ionVelocity, kappa);
          if (intersects == true){
            filteredIons.push_back(launchedIons[i]);
            filteredLaunchPhase.push_back(launchPhase[i]);
            filteredLaunchPhaseGhost.push_back(launchGhostPhases[i]);
            intersections++;
          }
        }
      }
    }

    cout << "Iteration complete. Saving trajectory results to VTK (if write output enabled)." << endl;

    //generate hitmap by histogram binning
    if (opticsParams.outputHitmap == true && opticsParams.projectionType == "realistic"){
      int xres = opticsParams.dparams.xCells;
      int yres = opticsParams.dparams.yCells;
      scalarField3D densityHitmap = initialiseField(xres, yres, 1, 0.0);

      for (int i=0;i<filteredIons.size();i++){
        Ion& ion1 = filteredIons[i];
        Point finalPos = detPointer->convertLocalcoordinate(ion1.trajectory.back());
        if ((finalPos.px*finalPos.px + finalPos.py*finalPos.py) < (opticsParams.dparams.radius * opticsParams.dparams.radius)){
          double px = xres * (finalPos.px + opticsParams.dparams.radius)/(2.0 * opticsParams.dparams.radius);
          double py = yres * (finalPos.py + opticsParams.dparams.radius)/(2.0 * opticsParams.dparams.radius);
          densityHitmap[(int)px][(int)py][0] += 1;
        }
      }

      vtkObject hitmapOut;

      double hitmapCellWidthX = opticsParams.dparams.radius * 2.0/xres;
      double hitmapCellWidthY = opticsParams.dparams.radius * 2.0/yres;
      Point bottomLeftHitmapcorner(-opticsParams.dparams.radius + hitmapCellWidthX/2.0, -opticsParams.dparams.radius + hitmapCellWidthY/2.0, 0.0);

      hitmapOut.setWithField(densityHitmap, bottomLeftHitmapcorner, hitmapCellWidthX, hitmapCellWidthY, 1.0);
      writeVtk(hitmapOut, "projection/hitmap/density/density_hitmap" + surfaceSuffix);
    }

    if ((opticsParams.outputTrajectories == true) && (opticsParams.projectionType == "realistic")){

      vtkObject out;
      cout << "filtered ion count: " << filteredIons.size() << endl;

      for (int t=0;t<filteredIons.size();t++){
        out.addTrajectory(filteredIons[t].trajectory, filteredIons[t].velocity, 0, filteredIons[t].phaseIndex);
      }

      writeVtk(out, writeDir + "/projection/trajectories/trajectories_" + to_string(p+1) + ".vtk");
      trajectoriesNames.push_back("trajectories/trajectories_" + to_string(p+1) + ".vtk");
      trajectoriesTimes.push_back(to_string(p+1));

      cout << "Writing trajectories series file to: " << "projection/trajectories.vtk.series" << endl;
      writeVtkSeries(writeDir + "/projection/trajectories.vtk.series", trajectoriesNames, trajectoriesTimes);
    }

    else if (opticsParams.outputTrajectories == true && opticsParams.projectionType == "uniform"){

      vtkObject out;

      for (int t=0;t<launchedIons.size();t++){
        if (launchedIons[t].writeOutput == true){
          out.addTrajectory(launchedIons[t].trajectory, launchedIons[t].velocity, launchSites[t], launchedIons[t].phaseIndex);
        }
      }

      writeVtk(out, writeDir + "/projection/trajectories/trajectories_" + to_string(p+1) + ".vtk");
      trajectoriesNames.push_back("trajectories/trajectories_" + to_string(p+1) + ".vtk");
      trajectoriesTimes.push_back(to_string(p+1));

    }

    if (opticsParams.projectionType == "realistic"){
      TsvObject syntheticData;
      cout << "Writing synthetic detector data to a TSV." << endl;

      //specify column names for header
      syntheticData.columnOrder.push_back("dx");
      syntheticData.columnOrder.push_back("dy");
      syntheticData.columnOrder.push_back("ion_type");
      syntheticData.columnOrder.push_back("ionic_volume");
      syntheticData.columnOrder.push_back("sx");
      syntheticData.columnOrder.push_back("sy");
      syntheticData.columnOrder.push_back("sz");
      syntheticData.columnOrder.push_back("model_phase");

      double speciesVolume = 0.0;
      for (int i=0;i<filteredIons.size();i++){
        Ion& ion1 = filteredIons[i];
        string ionType = ion1.phase;
        Point finalPos = detPointer->convertLocalcoordinate(ion1.trajectory.back());
        ion1.finalPos = finalPos;

        //randomly assign a phase
        Phase localPhase = smodel.phases[filteredLaunchPhase[i]];

        //replace elements with ghost phase elements (if in front of phase)
        if (filteredLaunchPhaseGhost[i] != nullptr){
          map<string, string>::iterator it;
          for (int cr = 0; cr < localPhase.elements.size(); cr++){
            if (filteredLaunchPhaseGhost[i]->substituteElements.count(localPhase.elements[cr])){
              string oldElement = localPhase.elements[cr];
              localPhase.elements[cr] = filteredLaunchPhaseGhost[i]->substituteElements[oldElement];
              if (filteredLaunchPhaseGhost[i]->substituteRatios[oldElement] >= 0.0)
                localPhase.elementRatios[cr] = filteredLaunchPhaseGhost[i]->substituteRatios[oldElement];
              if (filteredLaunchPhaseGhost[i]->substituteIonicVolumes[oldElement] >= 0.0)
                localPhase.elementIonicVolumes[cr] = filteredLaunchPhaseGhost[i]->substituteIonicVolumes[oldElement];
            }
          }
          double newCumulativeMax = 0.0;
          for (int cr = 0; cr < localPhase.elements.size(); cr++){
            newCumulativeMax += localPhase.elementRatios[cr];
            localPhase.cumulativeElementRatios[cr] = newCumulativeMax;
          }
          localPhase.cumulativeMax = newCumulativeMax;
        }

        double randElementAssignment = localPhase.cumulativeMax * ((double)rand())/RAND_MAX;
        for (int cr = 0; cr < localPhase.cumulativeElementRatios.size(); cr++){
          if (randElementAssignment < localPhase.cumulativeElementRatios[cr]) {
            ionType = localPhase.elements[cr];
            speciesVolume = localPhase.elementIonicVolumes[cr];
            break;
          }
        }

        ion1.ionIdentifier = std::distance(uniqueIonLabels.begin(), uniqueIonLabels.find(ionType));
        ion1.ionicVolume = speciesVolume;
        syntheticData.outDoubles[0].push_back(finalPos.px);
        syntheticData.outDoubles[1].push_back(finalPos.py);
        syntheticData.outStrings[2].push_back(ionType);
        syntheticData.outDoubles[3].push_back(speciesVolume);
        syntheticData.outDoubles[4].push_back(ion1.launchPos.px);
        syntheticData.outDoubles[5].push_back(ion1.launchPos.py);
        syntheticData.outDoubles[6].push_back(ion1.launchPos.pz);
        syntheticData.outStrings[7].push_back(ion1.phase);
      }
      cout << "writing synthetic data to selected filetypes." << endl;
      for (int ft=0;ft<opticsParams.exportFileTypes.size();ft++){
        if (opticsParams.exportFileTypes[ft].find("tsv") != string::npos){
          cout << "\tExporting to tsv." << endl;
          syntheticData.writeTsv(dataLoc + "synthetic_data.tsv", dataWriteHeader);
        }
        if (opticsParams.exportFileTypes[ft].find("epos") != string::npos){
          cout << "\tExporting to epos." << endl;
          eposSyntheticDataWriter(dataLoc + "synthetic_data.epos", filteredIons, dataWriteHeader);
        }
        if (opticsParams.exportFileTypes[ft] == "pos"){
          cout << "\tExporting to pos." << endl;
          posSyntheticDataWriter(dataLoc + "synthetic_data.pos", filteredIons, dataWriteHeader);
        }
        if (opticsParams.exportFileTypes[ft].find("ato") != string::npos){
          cout << "\tExporting to ato." << endl;
          atoSyntheticDataWriter(dataLoc + "synthetic_data.ato", filteredIons, dataWriteHeader);
        }
      }


      dataWriteHeader = false;

    }

    previousSurfaceState = surfaceMesh;
    surfaceMesh = nextSurfaceState;

  }

  delete detPointer;

  return 0;

}

/*!
 *
 * Function called on running CASRA simulated evaporation tool from the command line
 * takes command line arguments
 */
int main(int argc, char* argv[])
{

  cout << "Welcome to CASRA's charged optics calculation program. \nRun `calculateTrajectories -h` for help." << endl;

  int ret = 0;

  char modelDirArg[] = "--model";

  string modelConfigFile = "";
  string modelDir = "";
  string writeDir = "";

  string argSymbol = "--";
  string argInputVal;

  bool helpEnabled = false;

  //if no input detected
  if (argc == 1) {
    modelDir = ".";
  }
  else if (argc == 2) { //check if help passed
    int a = 0;
    while (strlen(helpSynonyms[a]) != 0) {
      if (strcmp(argv[1], helpSynonyms[a]) == 0) {
        helpEnabled = true;
        break;
      }
      a ++;
    }
  }
  else {
    if (!(argc % 2)) {
      cout << "Incomplete number of inputs detected. Please check all arguments are defined." << endl;
      return 1;
    }
    else {
      for (int i=1; i < argc; i++){
        if (i % 2 == 1) {
          argInputVal = (string)argv[i];
          if (argSymbol == argInputVal.substr(0, 2)) {
            //command line input argument detected
            if (strcmp(modelDirArg, argv[i])==0) {
              cout << "Supplied argument: " << modelDirArg << " " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                modelDir = (string)argv[i+1];
              }
              else {
                modelDir = ".";
              }
            }
            else {
              cout << "Unidentified input argument name detected. Please check input." << endl;
              return 1;
            }
          }
          else {
            cout << "Input argument name " << argv[i] << " not correctly marked as argument. Please check input." << endl;
            return 1;
          }
        }
      }
    }
  }

  if (modelDir == "") {
    modelDir = ".";
  }

  if (helpEnabled == false){
    //read in model parameters and ion projection parameters
    ModelParams modelParams = initialiseModelFromXML(modelDir + "/configuration.xml");
    OpticsParams opticsParams = initialiseModelOpticsFromXML(modelDir + "/configuration.xml");

    //run charged optics simulator
    ret = calculateTrajectories(modelParams, opticsParams, modelDir, modelDir);

    if (ret > 0) {
      cout << "Execution failed." << endl;
    }
    else {
      cout << "Execution succeeded." << endl;
    }
  }

  if (helpEnabled == true) {

    //Assemble model configuration file help string to append supported range file extension types
    int h = 0;
    while (strlen(supportedXMLConfigFileExt[h]) != 0) {
      configFileArgHelp = configFileArgHelp + (string)supportedXMLConfigFileExt[h] + ",";
      h++;
    }
    configFileArgHelp.erase(configFileArgHelp.end()-1,configFileArgHelp.end());

    //Assemble write directory configuration file help string
    writeDirArgHelp = writeDirArgHelp;

    //output help message
    cout << "Valid input arguments: \n"
         << modelDirArg << " : " << modeldirArgHelp << endl;
  }

  return ret;

}
