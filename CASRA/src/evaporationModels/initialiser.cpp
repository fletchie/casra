/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <vector>
#include <regex>
#include <sstream>
#include <any>
#include <limits>
#include <numeric>
#include <algorithm>

#include "CASRA/evaporationModels/initialiser.h"
#include "CASRA/evaporationLaws/evaporationLaws.h"
#include "CASRA/specimenModels/numericalModel.h"
#include "CASRA/tipBuild/tipBuild.h"
#include "CASRA/io/modelDataio.h"
#include "CASRA/tipBuild/extractTomoPhases.h"

using namespace std;

const double defaultStopTime = std::numeric_limits<double>::max();
const int defaultStopIts = 100;
const double defaultStopApexHeight = std::numeric_limits<double>::min();
const double defaultTimeStepFraction = 0.5;
const double defaultMinimumSpecimenLength = 0.0;
const double defaultShankAngle = 0.0;
const double defaultIonicVolume = 1.0;
const double defaultTemperature = 80.0;

//to modify instance attributes directly must pass instance by reference (as done here)

/*!
* This function reads xml parameter configuration files and sets the passed
* SimulationParams output object instance. This function makes use of the rapidxml
* library for parsing the xml file.
*
* @param simparams The SimulationParams object instance (the output object).
* @param cfname The configuration file filename.
*
*/
ModelParams initialiseModelFromXML(string cfname, string cdirectory) {

    cout << "Reading model configuration file..." << endl;

    char *charcfname = new char[cfname.length() + 1];
    strcpy(charcfname, cfname.c_str());

    //load the xml file using the rapidxml library
    try{
      rapidxml::file<> trialLoadXmlFile(charcfname);
    }
    catch (const std::runtime_error& e){
      cerr << "ERROR: Cannot find configuration.xml within the passed model directory." << endl;
      cerr << "Please ensure the correct model directory has been passed." << endl;
      exit(1);
    }

    rapidxml::file<> xmlFile(charcfname);
    rapidxml::xml_document<> doc;

    try {
      doc.parse<0>(xmlFile.data());
    }
    catch (const rapidxml::parse_error& e){
          cerr << "\tERROR: XML parsing error prevented reading of passed file due to '" << e.what() << "'" << endl;
          cerr << "\tPlease ensure XML is in a valid format." << endl;
          exit(1);
      }

    cout << "\tsuccessfully found from xml file. Reading now..." << endl;

    rapidxml::xml_node<> *root;

    rapidxml::xml_node<> *controlNode;
    rapidxml::xml_node<> *controlAttributeNode;

    rapidxml::xml_node<> *evapModelNode;
    rapidxml::xml_node<> *evapModelAttributeNode;

    rapidxml::xml_node<> *specimenModelNode;
    rapidxml::xml_node<> *specimenPhasesNode;
    rapidxml::xml_node<> *specimenPhaseNode;
    rapidxml::xml_node<> *specimenPhasePhysicsNode;
    rapidxml::xml_node<> *specimenModelAttributeNode;

    rapidxml::xml_node<> *evapPhysicsAttributeNode;
    rapidxml::xml_node<> *phasePhysicsModelNode;

    ModelParams mParams;
    vector<int> phaseOrder;
    int simId = -1;

    root = doc.first_node();
    if (root->first_attribute("id") != nullptr){
      simId = stoi(root->first_attribute("id")->value());
      cout << "\tsimulation ID of '" << simId << "' assigned" << endl;
      mParams.simId = simId;
    }

    //load control dictionary parameters
    cout << "\tLoading simulation control parameters" << endl;

    if (root->first_node("control") == nullptr){
      cerr << "ERROR: Cannot find simulation control node within the provided configuration file. This is required to configure the simulation." << endl;
      cerr << "\tThe control node should take the following format within the configuration file:" << endl;
      cerr << "\t<control>\n\t\t<stopIts>INT</stopIts>\n\t\t<stopTime>FLOAT</stopTime>\n\t\t<stopHeight>FLOAT</stopHeight>\n\t</control>" << endl;
      exit(1);
    }
    controlNode = root->first_node("control");

    int stopIts;
    double stopTime;
    double stopApexHeight;
    double timeStepFraction;
    long reinitItGap;
    string grid;
    int gridCells[3];
    double minSpecimenLength;
    double shankAngle = 0.0;
    double extendShank = 0.0;

    string thermalModelName;

    double T0;
    double TMax;
    Point pdir;
    double dir[3];

    if (controlNode->first_node("stopIts") != nullptr){
      controlAttributeNode = controlNode->first_node("stopIts");
      stopIts = stoi(controlAttributeNode->value());
    }
    else {
      cerr << "\t\tfailed to find maximum iteration count. Using default value of " << defaultStopIts << endl;
      stopIts = defaultStopIts;
    }

    if (controlNode->first_node("stopTime") != nullptr){
      controlAttributeNode = controlNode->first_node("stopTime");
      stopTime = stod(controlAttributeNode->value());
    }
    else {
      cerr << "\t\tfailed to load simulation stop time. Using default value of " << defaultStopTime << endl;
      stopTime = defaultStopTime;
    }

    if (controlNode->first_node("stopHeight") != nullptr){
      controlAttributeNode = controlNode->first_node("stopHeight");
      stopApexHeight = stod(controlAttributeNode->value());
    }
    else {
      cerr << "\t\tfailed to find simulation apex termination height. Using default value of " << defaultStopApexHeight<< endl;
      stopApexHeight = defaultStopApexHeight;
    }

    //load evaporation model parameters
    cout << "\tLoading evaporation model settings" << endl;
    if (root->first_node("evapModel") == nullptr){
      cerr << "ERROR: Could not find a defined evapModel node within the passed simulation configuration file." << endl;
      cerr << "evapModel node should take the following format: " << endl;
      cerr << "\t<evapModel simulator='ENUM(levelSetModel)'>\n\t\t<grid>INT,INT,INT</grid>\n\t\t<timeStepFraction>FLOAT</timeStepFraction>\n\t\t<reinitItGap>INT</reinitItGap>\n\t</evapModel>" << endl;
      exit(1);
    }
    evapModelNode = root->first_node("evapModel");
    string simulator = evapModelNode->first_attribute("simulator")->value();

    if (simulator != "levelSetModel"){
      cerr << "\t\tERROR: Incorrect model type detected: '" + simulator + "'. Please make sure model type is set to 'LevelSetModel' when running LSEvaporationSimulator." << endl;
      exit(1);
    }

    if (evapModelNode->first_node("grid") != nullptr){
      evapModelAttributeNode = evapModelNode->first_node("grid");
      grid = evapModelAttributeNode->value();

      int grit = 0;
      int pos;
      string delimiter = ",";

      while ((pos = grid.find(delimiter)) != string::npos) {
          gridCells[grit] = stoi((grid.substr(0, pos)));
          grid.erase(0, pos + delimiter.length());
          grit++;
      }

      gridCells[2] = stoi(grid);
      if (grit != 2) {
        cerr << "\t\tERROR: incorrect size for grid dimensions detected (i.e. does not equal three). Make sure that three are specified, and are separated by ','" << endl;
        exit(1);
      }
    }
    else{
      cerr << "\t\tNo passed grid cell size found. Using default grid dimensions of DIMX='100',DIMY='100',DIMZ='100'." << endl;
      gridCells[0] = 100;
      gridCells[1] = 100;
      gridCells[2] = 100;
    }

    if (evapModelNode->first_node("timeStepFraction") != nullptr){
      evapModelAttributeNode = evapModelNode->first_node("timeStepFraction");
      timeStepFraction = stod(evapModelAttributeNode->value());
    }
    else {
      cerr << "\t\tCould not find passed timeStepFraction. Using default value of timeStepFraction='" << defaultTimeStepFraction << "'" << endl;
      timeStepFraction = defaultTimeStepFraction;
    }

    if (evapModelNode->first_node("reinitItGap") != nullptr){
      evapModelAttributeNode = evapModelNode->first_node("reinitItGap");
      reinitItGap = stod(evapModelAttributeNode->value());
      if (reinitItGap == 0) {
        reinitItGap = std::numeric_limits<long>::max();
      }
    }
    else {
      cerr << "\t\tfailed to find Sussman reinitItGap (parameter controlling iterations between field reinitialisation). Reinitialisation will not be performed." << endl;
      reinitItGap = std::numeric_limits<long>::max();
    }

    //load evaporation model parameters
    cout << "Loading evaporation law settings" << endl;
    if (root->first_node("specimenModel") == nullptr){
      cerr << "\tERROR: could not find the 'specimenModel' within configuration file. Please see documentation or provided examples on how to define this xml configuration node." << endl;
      exit(1);
    }

    specimenModelNode = root->first_node("specimenModel");
    specimenPhasesNode = specimenModelNode->first_node("phases");

    //iterate through phases in configuration file
    int phaseCount = 0;
    for (specimenPhaseNode = specimenPhasesNode->first_node(); specimenPhaseNode != NULL; specimenPhaseNode = specimenPhaseNode->next_sibling())
    {
      string phaseName = specimenPhaseNode->name();
      cout << "\tloading '" + phaseName << "'" << endl;

      if (specimenPhaseNode->first_attribute("order") != nullptr){
        phaseOrder.push_back(stoi(specimenPhaseNode->first_attribute("order")->value()));
      }
      else{
        cerr << "\tERROR: Phase order attribute not found for '" << phaseName << "' mesh. Please make sure this attribute is defined." << endl;
        exit(1);
      }

      specimenPhasePhysicsNode = specimenPhaseNode->first_node("evapPhysics")->first_node();

      //check if ghost phase (e.g. invisible to evaporation physics determination such as an isotopic tracer)
      if (string(specimenPhasePhysicsNode->name()) == "ghost") {
        cout << "\t\tGhost phase detected. Phase will leave evaporation physics unaffected." << endl;
        Phase loadedPhase;
        loadedPhase.name = phaseName;
        loadedPhase.ghost = true;
        mParams.sModel.specimenModel.appendPhase(loadedPhase);
      } //check if Ahhrenius isotropic law specified
      else if (string(specimenPhasePhysicsNode->name()) == "AhhreniusIsotropic") {
        cout << "\t\tIsotropic Ahhrenius law detected" << endl;
        AhhreniusLawIsotropic phaseEvapLaw = loadAhhreniusIsotropicLaw(specimenPhasePhysicsNode);
        Phase loadedPhase(phaseEvapLaw, phaseName);
        mParams.sModel.specimenModel.appendPhase(loadedPhase);
      } //else if linear isotropic law specified
      else if (string(specimenPhasePhysicsNode->name()) == "LinearIsotropic"){
        cout << "\t\tIsotropic linear law detected" << endl;
        LinearLawIsotropic phaseEvapLaw = loadLinearIsotropicLaw(specimenPhasePhysicsNode);
        Phase loadedPhase(phaseEvapLaw, phaseName);
        mParams.sModel.specimenModel.appendPhase(loadedPhase);
      } //else if deformed-Ahhrenius isotropic law specified
      else if (string(specimenPhasePhysicsNode->name()) == "DeformedAhhreniusIsotropic"){
        cout << "\t\tIsotropic deformed Ahhrenius law detected" << endl;
        DeformedAhhreniusLawIsotropic phaseEvapLaw = loadDeformedAhhreniusIsotropicLaw(specimenPhasePhysicsNode);
        Phase loadedPhase(phaseEvapLaw, phaseName);
        mParams.sModel.specimenModel.appendPhase(loadedPhase);
      } //report that undefined law has been used. Exit.
      else{
        cerr << "\tERROR: Unrecognised physics model for material phase: '" + string(specimenPhasePhysicsNode->name()) + "' detected. See documentation and included examples on how to define this." << endl;
        cerr << "\tCurrently supported phase-specific evaporation laws: 'AhhreniusIsotropic', 'linearIsotropic', 'DeformedAhhreniusIsotropic', 'ghost'." << endl;
        exit(1);
      }

      TriangularMesh phaseMesh;
      string fileLocation = "";
      vector<Point> vertices;
      vector<Face> faces;

      if (specimenPhaseNode->first_node("mesh") != nullptr){
        cout << "\t\treading in mesh for " + phaseName << endl;

        rapidxml::xml_node<> *specimenPhaseMeshNode;
        specimenPhaseMeshNode = specimenPhaseNode->first_node("mesh");

        if (specimenPhaseMeshNode->first_attribute("type") != nullptr && string(specimenPhaseMeshNode->first_attribute("type")->value()) == "TriangularSurfaceMesh"){

          if (specimenPhaseMeshNode->first_node("vertices") != nullptr){

            rapidxml::xml_node<> *specimenPhaseVerticesNode;
            specimenPhaseVerticesNode = specimenPhaseMeshNode->first_node("vertices");

            int vc = 0;
            rapidxml::xml_node<> *vertexNode;
            cout << "\t\t\treading vertices" << endl;
            for (vertexNode = specimenPhaseVerticesNode->first_node(); vertexNode != nullptr; vertexNode = vertexNode->next_sibling()){
              Point p1(stod(vertexNode->first_attribute("px")->value()), stod(vertexNode->first_attribute("py")->value()), stod(vertexNode->first_attribute("pz")->value()));
              vc += 1;
              vertices.push_back(p1);
            }
          }
          else{
            cout << "\t\tERROR: No vertices found for '" + phaseName + "'. Please check that surface mesh has been constructed using the meshBuilder tool." << endl;
            exit(1);
          }

          if (specimenPhaseMeshNode->first_node("cells") != nullptr){

            rapidxml::xml_node<> *specimenPhaseCellsNode;
            specimenPhaseCellsNode = specimenPhaseMeshNode->first_node("cells");

            int fc = 0;
            rapidxml::xml_node<> *cellNode;
            cout << "\t\t\treading cells" << endl;
            for (cellNode = specimenPhaseCellsNode->first_node(); cellNode != NULL; cellNode = cellNode->next_sibling()){
              Face f1(stol(cellNode->first_attribute("v1")->value()), stol(cellNode->first_attribute("v2")->value()), stol(cellNode->first_attribute("v3")->value()));
              fc += 1;
              faces.push_back(f1);
            }
          }

          else{
            cout << "\t\tERROR: No mesh cells found for '" + phaseName + "'. Please check that surface mesh has been constructed using the meshBuilder tool." << endl;
            exit(1);
          }

          phaseMesh.vertices = vertices;
          phaseMesh.faces = faces;
        }
        else if (specimenPhaseMeshNode->first_node("tipBuilder") != nullptr){
          int m = 0;
          rapidxml::xml_node<> *tBuildNode;
          for (tBuildNode = specimenPhaseMeshNode->first_node("tipBuilder")->first_node(); tBuildNode != NULL; tBuildNode = tBuildNode->next_sibling()){
            cout << "tip builder instruction found. Performing mesh construction ..." << endl;
            TriangularMesh tMesh = meshbuild(tBuildNode, doc, false);
            cout << "built mesh" << endl;
            tMesh.faces = tMesh.faces + phaseMesh.vertices.size();
            phaseMesh.faces.insert(phaseMesh.faces.end(), tMesh.faces.begin(), tMesh.faces.end());
            phaseMesh.vertices.insert(phaseMesh.vertices.end(), tMesh.vertices.begin(), tMesh.vertices.end());
            m++;
          }

          cout << "build complete." << endl;

          if (m == 0){
            cerr << "ERROR: No tip builder instructions found for phase: '" << phaseName << "'. Please add instructions or explicit mesh definition to continue. Exiting." << endl;
            exit(1);
          }
        }
        else if (specimenPhaseMeshNode->first_node("phaseExtractor") != nullptr){
          rapidxml::xml_node<> *tBuildNode;
          int m = 0;
          for (tBuildNode = specimenPhaseMeshNode->first_node("phaseExtractor")->first_node(); tBuildNode != NULL; tBuildNode = tBuildNode->next_sibling()){
            cout << "phase extractor instruction found. Performing mesh construction via field extraction..." << endl;
            TriangularMesh tMesh = extractPhase(tBuildNode, doc, false, phaseName);
            //append extracted mesh to output phase mesh
            tMesh.faces = tMesh.faces + phaseMesh.vertices.size();
            phaseMesh.faces.insert(phaseMesh.faces.end(), tMesh.faces.begin(), tMesh.faces.end());
            phaseMesh.vertices.insert(phaseMesh.vertices.end(), tMesh.vertices.begin(), tMesh.vertices.end());
            cout << "successfully extracted valid mesh" << endl;
            m++;
          }
          if (m == 0){
            cerr << "ERROR: No tip builder instructions found for phase: '" << phaseName << "'." << endl;
            exit(1);
          }
        }
        else if (specimenPhaseMeshNode->first_node("vtk") != nullptr){
          rapidxml::xml_node<> *externalMeshNode = specimenPhaseMeshNode->first_node("vtk");
          if (externalMeshNode->first_attribute("location") != nullptr){
            fileLocation = string(externalMeshNode->first_attribute("location")->value());
            vtkObject in;
            int retCode = readVTKUnstructuredGrid(in, fileLocation);
            if (retCode != 0){
              cerr << "ERROR: error reading in vtk file for defining phase: '" << phaseName << "'. Please check VTK has correct format (i.e. an unstructured triangular mesh). Exiting." << endl;
              exit(1);
            }
            phaseMesh = in.triangularMeshFromVtk();
          }
          else {
            cerr << "ERROR: no attached location found for vtk file defining phase: '" << phaseName << "'. Please add vtk `location='file_loc'` to continue. Exiting." << endl;
            exit(1);
          }
        }
        else{
          cout << "\t\tERROR: Only meshes of type: name='TriangularSurfaceMesh' or 'tipBuilder' instructions are currently supported. " <<
                  "If a triangular surface mesh then make sure this attributed is explicitly specified" << endl;
          exit(1);
        }
      }
      else{
        cout << "\t\tERROR: No mesh for '" + phaseName + "' found . Please construct a surface mesh using the meshBuilder tool." << endl;
        exit(1);
      }

      if (specimenPhaseNode->first_node("chemistry") != nullptr){
        rapidxml::xml_node<>* chemNode = specimenPhaseNode->first_node("chemistry");
        int m = 0;
        rapidxml::xml_node<> *tCompNode;
        if (mParams.sModel.specimenModel.phases[phaseCount].ghost == false){
          for (tCompNode = chemNode->first_node(); tCompNode != NULL; tCompNode = tCompNode->next_sibling()){
            string ionLabel = tCompNode->first_attribute("label")->value();
            double ratio = stod(tCompNode->first_attribute("ratio")->value());
            double elIonicVolume = stod(tCompNode->first_attribute("ionicVolume")->value());

            mParams.sModel.specimenModel.phases[phaseCount].elements.push_back(ionLabel);
            mParams.sModel.specimenModel.phases[phaseCount].elementRatios.push_back(ratio);
            mParams.sModel.specimenModel.phases[phaseCount].elementIonicVolumes.push_back(elIonicVolume);
          }
          //normalise composition
          double normRatios = 0.0;
          for (int cr = 0;cr < mParams.sModel.specimenModel.phases[phaseCount].elementRatios.size(); cr++){
            normRatios += mParams.sModel.specimenModel.phases[phaseCount].elementRatios[cr];
          }
          for (int cr = 0;cr < mParams.sModel.specimenModel.phases[phaseCount].elementRatios.size(); cr++){
            mParams.sModel.specimenModel.phases[phaseCount].elementRatios[cr] /= normRatios;
          }
        }
        else {
          cout << "building ghost phase" << endl;
          for (tCompNode = chemNode->first_node(); tCompNode != NULL; tCompNode = tCompNode->next_sibling()){
            if (string(tCompNode->first_attribute("operation")->value()) == "substitute"){
              string oldIonLabel = string(tCompNode->first_attribute("oldLabel")->value());
              string newIonLabel = string(tCompNode->first_attribute("newLabel")->value());
              double ratio = -1.0;
              if (tCompNode->first_attribute("newRatio") != nullptr)
                ratio = stod(tCompNode->first_attribute("newRatio")->value());

              double elIonicVolume = -1.0;
              if (tCompNode->first_attribute("newIonicVolume") != nullptr)
                elIonicVolume = stod(tCompNode->first_attribute("newIonicVolume")->value());

              mParams.sModel.specimenModel.phases[phaseCount].substituteElements[oldIonLabel] = newIonLabel;
              mParams.sModel.specimenModel.phases[phaseCount].substituteRatios[oldIonLabel] = ratio;
              mParams.sModel.specimenModel.phases[phaseCount].substituteIonicVolumes[oldIonLabel] = elIonicVolume;
            }
            else {
              cerr << "\tERROR: Unsupported ghost phase operation detected. " << endl;
              cerr << "\tCurrently only the 'substitute' operation is available (e.g. replace ion type in underlying phase with ghost phase)." << endl;
              cerr << "\tThis is useful for defining isotopic tracer structures within the simulation." << endl;
              cerr << "\tExample node: <c1 operation='substitute' oldLabel='Fe' newLabel='Fe57' newRatio='0.33' newIonicVolume='1e-26'/>" << endl;
              cerr << "\tFor incorporating addition of removal operations, feel free to contact the developers" << endl;
              exit(1);
            }
          }
        }
      }
      else {
        cerr << "\tNo chemistry xml node detected. Assuming default ionic volume of: " << defaultIonicVolume << endl;
        mParams.sModel.specimenModel.phases[phaseCount].elements.push_back(phaseName);
        mParams.sModel.specimenModel.phases[phaseCount].elementRatios.push_back(1.0);
        mParams.sModel.specimenModel.phases[phaseCount].elementIonicVolumes.push_back(defaultIonicVolume);
      }

      if (mParams.sModel.specimenModel.phases[phaseCount].elements.size() == 0){
        mParams.sModel.specimenModel.phases[phaseCount].elements.push_back(phaseName);
        mParams.sModel.specimenModel.phases[phaseCount].elementRatios.push_back(1.0);
      }

      double cumSum = 0.0;
      for (int cr = 0; cr < mParams.sModel.specimenModel.phases[phaseCount].elementRatios.size(); cr++){
        cumSum += mParams.sModel.specimenModel.phases[phaseCount].elementRatios[cr];
        mParams.sModel.specimenModel.phases[phaseCount].cumulativeElementRatios.push_back(cumSum);
      }
      mParams.sModel.specimenModel.phases[phaseCount].cumulativeMax = cumSum;

      //add phase mesh to specimen model
      mParams.sModel.specimenModel.phases[phaseCount].addPhaseBoundary(phaseMesh);
      mParams.sModel.specimenModel.phases[phaseCount].fileLocation = fileLocation;

      //update phase counter
      phaseCount += 1;
    }

    //order phases according to specified order attribute
    vector<int> phaseIndex(mParams.sModel.specimenModel.phases.size());
    vector<Phase> orderedPhases;
    iota(phaseIndex.begin(),phaseIndex.end(),0);
    sort(phaseIndex.begin(), phaseIndex.end(), [&](int i, int j){return phaseOrder[i] < phaseOrder[j];});

    for (int p=0;p<phaseIndex.size();p++){
      orderedPhases.push_back(mParams.sModel.specimenModel.phases[phaseIndex[p]]);
    }

    //load specimen model shank parameters
    mParams.sModel.specimenModel.phases = orderedPhases;
    cout << "Reading specimen model shank parameters:" << endl;
    if (specimenModelNode->first_node("shankModel") != nullptr){
      rapidxml::xml_node<> *specimenShankNode = specimenModelNode->first_node("shankModel");

      if (specimenShankNode->first_attribute("minimumShankLength") != nullptr){
        minSpecimenLength = stod(specimenShankNode->first_attribute("minimumShankLength")->value());
      }
      else {
        cerr << "\tCould not find passed minimum specimen length. Using default value of " << "minimumShankLength='" << defaultMinimumSpecimenLength << "'" << endl;
        cerr << "A complete shank model can be defined within the model configuration file as: " << endl;
        cerr << "\t<shank extension='BOOL' minimumShankLength='FLOAT' shankAngle='FLOAT'/>" << endl;
        minSpecimenLength = defaultMinimumSpecimenLength;
      }

      if (specimenShankNode->first_attribute("extension") != nullptr){
        string shankExtMode = string(specimenShankNode->first_attribute("extension")->value());
        if (shankExtMode == "true" || shankExtMode == "True" || shankExtMode == "T" || shankExtMode == "yes"){
          extendShank = true;
        }
        else {
          extendShank = false;
        }
      }
      else {
        cerr << "\tCould not find passed shank 'extension' attribute. Using default values of false (extension off)." << endl;
        extendShank = false;
      }

      if (specimenShankNode->first_attribute("shankAngle") != nullptr){
        shankAngle = stod(specimenShankNode->first_attribute("shankAngle")->value());
      }
      else {
        cerr << "\tCould not find passed shank angle. Using default value of " << "shankAngle='" << defaultShankAngle << "'" << endl;
        cerr << "A complete shank model can be defined within the model configuration file as: " << endl;
        cerr << "\t<shank extension='BOOL' minimumShankLength='FLOAT' shankAngle='FLOAT'/>" << endl;
        shankAngle = defaultShankAngle;
      }
    }
    else {
      cerr << "Could not find passed shank model. Using default values of " <<
      "minimumShankLength='" << defaultMinimumSpecimenLength << "'" << " and shankAngle='" << defaultShankAngle << "'" << endl;
      cerr << "A shank model can be added to the model configuration file as: " << endl;
      cerr << "\t<shankModel extension='BOOL' minimumShankLength='FLOAT' shankAngle='FLOAT'/>" << endl;
      minSpecimenLength = defaultMinimumSpecimenLength;
      shankAngle = defaultShankAngle;
    }

    cout << "Checking for external system:" << endl;
    if (root->first_node("externalSystem") != nullptr) {
        int itc = -1;
        rapidxml::xml_node<>* externalSystemNode = root->first_node("externalSystem");
        if (externalSystemNode->first_node("mesh") != nullptr) {
            rapidxml::xml_node<>* externalSystemMeshNode = externalSystemNode->first_node("mesh");
            for (rapidxml::xml_node<>* extSysMesh = externalSystemMeshNode->first_node(); extSysMesh != NULL; extSysMesh = extSysMesh->next_sibling()) {
                double voltage = 0.0;
                string componentName;

                if (extSysMesh->first_attribute("voltageFraction") != nullptr) {
                    voltage = stod(extSysMesh->first_attribute("voltageFraction")->value());
                }
                else {
                    cerr << "\tNo component voltage detected. Cannot define electrostatic solution. Skipping to next element." << endl;
                    continue;
                }
                if (extSysMesh->first_attribute("name") != nullptr) {
                    componentName = string(extSysMesh->first_attribute("name")->value());
                    mParams.esModel.systemModelNames.push_back(componentName);
                }
                else {
                    cerr << "\tExternal system component must have a defined name. Skipping to next element." << endl;
                    continue;
                }
                if (extSysMesh->first_attribute("location") != nullptr) {
                    string fileLocation = string(extSysMesh->first_attribute("location")->value());
                    vtkObject in;
                    int retCode = readVTKUnstructuredGrid(in, fileLocation);
                    if (retCode != 0) {
                        cerr << "ERROR: error reading in vtk file for external system (<externalSystem>). Please check passed file location." << endl;
                        exit(1);
                    }
                    mParams.esModel.systemModelMeshes.push_back(in.triangularMeshFromVtk());
                    mParams.esModel.systemModelVoltages.push_back(voltage);
                    itc++;
                }
                else {
                    cerr << "No location detected for external system component. Skipping to next component." << endl;
                    continue;
                }
                if (extSysMesh->first_attribute("scale") != nullptr) {
                    double scaleFactor = stod(extSysMesh->first_attribute("scale")->value());
                    double preMeanX = pointDimMean(mParams.esModel.systemModelMeshes[itc].vertices, 0);
                    double preMeanY = pointDimMean(mParams.esModel.systemModelMeshes[itc].vertices, 1);
                    double preMeanZ = pointDimMean(mParams.esModel.systemModelMeshes[itc].vertices, 2);
                    for (long v = 0; v < mParams.esModel.systemModelMeshes[itc].vertices.size(); v++) {
                        mParams.esModel.systemModelMeshes[itc].vertices[v].px -= preMeanX;
                        mParams.esModel.systemModelMeshes[itc].vertices[v].py -= preMeanY;
                        mParams.esModel.systemModelMeshes[itc].vertices[v].pz -= preMeanZ;
                        mParams.esModel.systemModelMeshes[itc].vertices[v].px *= scaleFactor;
                        mParams.esModel.systemModelMeshes[itc].vertices[v].py *= scaleFactor;
                        mParams.esModel.systemModelMeshes[itc].vertices[v].pz *= scaleFactor;
                    }
                }
                if (extSysMesh->first_attribute("recentre") != nullptr) {
                    Point translateFactor = extractPoint(string(extSysMesh->first_attribute("recentre")->value()));
                    for (long v = 0; v < mParams.esModel.systemModelMeshes[itc].vertices.size(); v++) {
                        mParams.esModel.systemModelMeshes[itc].vertices[v].px += translateFactor.px;
                        mParams.esModel.systemModelMeshes[itc].vertices[v].py += translateFactor.py;
                        mParams.esModel.systemModelMeshes[itc].vertices[v].pz += translateFactor.pz;
                    }
                }
                vtkObject out;
                out.setWithTriangularMesh(mParams.esModel.systemModelMeshes[itc]);
                writeVtk(out, "initial_model/" + componentName + ".vtk");
            }
        }
    }

    if (mParams.esModel.systemModelMeshes.size() == 0) {
        cerr << "\tNo external system included." << endl;
        cerr << "\tOnly specimen geometry will contribute to the electrostatic solution." << endl;
    }
    else {
        cerr << "\tLoaded external system mesh. This will be included in the electrostatic solution" << endl;
    }

    rapidxml::xml_node<> * thermalModelNode = root->first_node("thermalModel");

    if (thermalModelNode == nullptr){
      T0 = defaultTemperature;
      cerr << "No thermal model detected. Assuming 'uniformTemperatureModel' with default specimen temperature of T0='" << defaultTemperature << "'" << endl;
      cerr << "Other valid thermal models: " << endl;
      cerr << "\t <thermalModel type='uniformTemperatureModel' T0='FLOAT'/>" << endl;
      cerr << "\t <thermalModel type='instantaneousHeatingModel' T0='FLOAT' TMax='FLOAT' laserdir='(FLOAT,FLOAT,FLOAT)'/>" << endl;
      thermalModelName = "uniformTemperatureModel";
    }
    else {
      thermalModelName = string(thermalModelNode->first_attribute("type")->value());
      if (thermalModelName == "uniformTemperatureModel"){
        cout << "Uniform temperature model identified: " << endl;
        T0 = stod(thermalModelNode->first_attribute("T0")->value());
      }
      else if (thermalModelName == "instantaneousHeatingModel"){
        cout << "instantaneous thermal heating raytracing model identified: " << endl;
        T0 = stod(thermalModelNode->first_attribute("T0")->value());
        TMax = stod(thermalModelNode->first_attribute("TMax")->value());
        Point pdir = extractPoint(string(thermalModelNode->first_attribute("laserdir")->value()));
        dir[0] = pdir.px;
        dir[1] = pdir.py;
        dir[2] = pdir.pz;
      }
      else {
        cerr << "ERROR: Invalid thermal model type detected. Please use one of the following valid models: " << endl;
        cerr << "\t <thermalModel type='uniformTemperatureModel' T0='FLOAT'/>" << endl;
        cerr << "\t <thermalModel type='instantaneousHeatingModel' T0='FLOAT', TMax='FLOAT', laserdir='(FLOAT,FLOAT,FLOAT)'/>" << endl;
        exit(1);
      }
    }

    mParams.tModel.thermalModel = thermalModelName;
    copy(begin(dir), end(dir), mParams.tModel.dir);
    mParams.tModel.T0 = T0;
    mParams.tModel.TMax = TMax;

    mParams.cDict.stopIts = stopIts;
    mParams.cDict.stopTime = stopTime;
    mParams.cDict.stopApexHeight = stopApexHeight;

    mParams.eModel.evaporationModel = simulator;
    copy(begin(gridCells), end(gridCells), begin(mParams.eModel.gridCells));
    mParams.eModel.timeStepFraction = timeStepFraction;
    mParams.eModel.reinitItGap = reinitItGap;

    mParams.sModel.minSpecimenLength = minSpecimenLength;
    mParams.sModel.shankAngle = shankAngle;
    mParams.sModel.extendShank = extendShank;

    cout << "Model configuration file successfully read." << endl;

    delete[] charcfname;

    return mParams;

}

/*!
 * Function for loading the specific XML data for a phase-specific Ahhrenius law.
 * @param phasePhysicsModelNode the passed xml evaporation law data object
 * @return the processed evaporation law definer
 */
AhhreniusLawIsotropic loadAhhreniusIsotropicLaw(rapidxml::xml_node<>* phasePhysicsModelNode){

  double preFactor = stod(phasePhysicsModelNode->first_node("preFactor")->value());

  rapidxml::xml_node<>* CNodes = phasePhysicsModelNode->first_node("C");
  rapidxml::xml_node<>* CNode;

  vector<double> C;
  vector<double> Cexp;
  for (CNode = CNodes->first_node(); CNode != NULL; CNode = CNode->next_sibling()){
    C.push_back(stod(CNode->value()));
    Cexp.push_back(stod(CNode->first_attribute("exp")->value()));
  }

  if (C.size() == 0){
    cerr << "No C exponent coefficients detected for Ahhrenius evaporation law. Model undefined. Exiting." << endl;
    exit(1);
  }

  double evapField = stod(phasePhysicsModelNode->first_node("evapField")->value());
  AhhreniusLawIsotropic phaseEvapLaw(preFactor, C, Cexp, evapField);

  return phaseEvapLaw;

}

/*!
 * Function for loading the specific XML data for a phase-specific deformed-Ahhrenius law.
 * Law as defined in Aquilanti, V., Mundim, K. C., Elango, M., Kleijn, S., & Kasai, T. (2010).
 * Chemical Physics Letters, 498(1–3), 209–213. https://doi.org/10.1016/j.cplett.2010.08.035
 *
 * @param phasePhysicsModelNode the passed xml evaporation law data object
 * @return the processed evaporation law definer
 */
DeformedAhhreniusLawIsotropic loadDeformedAhhreniusIsotropicLaw(rapidxml::xml_node<>* phasePhysicsModelNode){

  double preFactor = stod(phasePhysicsModelNode->first_node("preFactor")->value());

  rapidxml::xml_node<>* CNodes = phasePhysicsModelNode->first_node("C");
  rapidxml::xml_node<>* CNode;

  vector<double> C;
  vector<double> Cexp;
  for (CNode = CNodes->first_node(); CNode != NULL; CNode = CNode->next_sibling()){
    C.push_back(stod(CNode->value()));
    Cexp.push_back(stod(CNode->first_attribute("exp")->value()));
  }

  if (C.size() == 0){
    cerr << "No C exponent coefficients detected for Deformed Ahhrenius evaporation law. Model undefined. Exiting." << endl;
    exit(1);
  }

  double evapField = stod(phasePhysicsModelNode->first_node("evapField")->value());
  double d = stod(phasePhysicsModelNode->first_node("d")->value());
  DeformedAhhreniusLawIsotropic phaseEvapLaw(preFactor, C, Cexp, evapField, d);

  return phaseEvapLaw;

}

/*!
 * Function for loading the specific XML data for a phase-specific linear law.
 * @param phasePhysicsModelNode the passed xml evaporation law data object
 * @return the processed evaporation law definer
 */
LinearLawIsotropic loadLinearIsotropicLaw(rapidxml::xml_node<>* phasePhysicsModelNode){

  double preFactor = stod(phasePhysicsModelNode->first_node("preFactor")->value());
  LinearLawIsotropic phaseEvapLaw(preFactor);

  return phaseEvapLaw;

}
