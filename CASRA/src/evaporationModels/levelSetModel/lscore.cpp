/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <stdexcept>
#include <chrono>
#include <string>
#include <numeric>

#include "CASRA/io/modelDataio.h"

#include "CASRA/evaporationModels/levelSetModel/lscore.h"
#include "CASRA/mesh/triangularMesh.h"
#include "CASRA/field/scalarField.h"
#include "CASRA/evaporationLaws/evaporationLaws.h"

#include "CASRA/mesh/remeshSupport.h"
#include "CASRA/mesh/marchingCubes.h"

#include "CASRA/electrostatics/constantBEM.h"
#include "CASRA/electrostatics/solverParser.h"

#include "isotropicremesher/isotropicremesher.h"
#include "isotropicremesher/isotropichalfedgemesh.h"

#include "CASRA/base/vectorMath.h"
#include "CASRA/base/LUdecomposition.h"

using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::seconds;

#define REMESHFRACTION 0.1
#define ADAPTIVEEXP 1.75
#define ADAPTIVECONST 1.3
#define REMESHITS 3

#define MAXVELOCITYCUTOFFFRACTION 1e-4
#define SHRINKSTEP 3

#define SIGNTOL 3.0

#define ADAPTIVEEXTENSIONVELOCITY true
#define ADAPTIVEEXTVELNARROWBAND 5
#define EXTVELSAMPLING 2

using namespace std;

LSIterator::LSIterator(LevelSetDomain* lSDomain, double cfl){
  this->cfl = cfl;
}

CurvatureIterator::CurvatureIterator(LevelSetDomain* lSDomain, double cfl): LSIterator(lSDomain, cfl){
  this->lSDomain = lSDomain;

  //set default final conditions
  this->maxT = numeric_limits<double>::max();
  this->T = 0.0;
  this->it = 0;
  this->maxIt = 100;

  this->A = 1.0;

}

CurvatureIterator::CurvatureIterator(LevelSetDomain& lSDomain, double cfl): LSIterator(&lSDomain, cfl){
  this->lSDomain = &lSDomain;

  //set default final conditions
  this->maxT = numeric_limits<double>::max();
  this->T = 0.0;
  this->it = 0;
  this->maxIt = 100;

  this->A = 1.0;

}

void CurvatureIterator::setTerminationConditions(double maxT, long maxIt){
  this->maxIt = maxIt;
  this->maxT = maxT;
}

tuple<int, TriangularMesh> CurvatureIterator::iterate(){

  TriangularMesh surfaceMesh;
  cout << "marching cubes extraction" << endl;
  marchingCubes(lSDomain->lSField, 0.0, surfaceMesh);

  double dt, maxVelField;
  int terminate = 0; //checks whether a termination condition has been reached

  centralDifferenceField(this->lSDomain->lSField, this->lSDomain->dFieldX,
                         this->lSDomain->dFieldY, this->lSDomain->dFieldZ,
                         this->lSDomain->cellWidthX,
                         this->lSDomain->cellWidthY,
                         this->lSDomain->cellWidthZ);

  this->lSDomain->mField = pow(pow(this->lSDomain->dFieldX, 2.0) +
                               pow(this->lSDomain->dFieldY, 2.0) +
                               pow(this->lSDomain->dFieldZ, 2.0), 0.5);

  this->lSDomain->dFieldX /= this->lSDomain->mField;
  this->lSDomain->dFieldY /= this->lSDomain->mField;
  this->lSDomain->dFieldZ /= this->lSDomain->mField;

  centralDifferenceField(this->lSDomain->dFieldX, this->lSDomain->ddFieldX,
                         this->lSDomain->tField, this->lSDomain->tField,
                         this->lSDomain->cellWidthX,
                         this->lSDomain->cellWidthY,
                         this->lSDomain->cellWidthZ);
  centralDifferenceField(this->lSDomain->dFieldY, this->lSDomain->tField,
                         this->lSDomain->ddFieldY, this->lSDomain->tField,
                         this->lSDomain->cellWidthX,
                         this->lSDomain->cellWidthY,
                         this->lSDomain->cellWidthZ);
  centralDifferenceField(this->lSDomain->dFieldZ, this->lSDomain->tField,
                         this->lSDomain->tField, this->lSDomain->ddFieldZ,
                         this->lSDomain->cellWidthX,
                         this->lSDomain->cellWidthY,
                         this->lSDomain->cellWidthZ);

  this->lSDomain->vField = initialiseField(this->lSDomain->xDim,
                                           this->lSDomain->yDim,
                                           this->lSDomain->zDim, 0.0);

  this->lSDomain->vField += this->lSDomain->ddFieldX;
  this->lSDomain->vField += this->lSDomain->ddFieldY;
  this->lSDomain->vField += this->lSDomain->ddFieldZ;

  //take absolute value of field
  negField(this->lSDomain->vField, this->lSDomain->vField);
  absField(this->lSDomain->vField, this->lSDomain->vField);

  cout << "grid: " << this->lSDomain->evapFieldGrid[1][1][1] << endl;
  this->lSDomain->vField *= this->lSDomain->evapFieldGrid;

  //perform smoothing of velocity field via weighted median filter
  smoothField(this->lSDomain->vField, this->lSDomain->tField, 0.5);
  smoothField(this->lSDomain->tField, this->lSDomain->vField, 0.5);

  //determine CFL adaptive timestep - check temporal termination condition
  maxVelField = max(this->lSDomain->vField);

  dt = this->cfl * min({this->lSDomain->cellWidthX, this->lSDomain->cellWidthY, this->lSDomain->cellWidthZ})/maxVelField;
  if (this->T + dt > this->maxT){
    dt = this->maxT - this->T;
    terminate = 1;
  }

  //integrate level set field equation
  forwardDifferenceField(this->lSDomain->lSField, this->lSDomain->dFieldXp, this->lSDomain->dFieldYp,
                         this->lSDomain->dFieldZp,
                         this->lSDomain->cellWidthX,
                         this->lSDomain->cellWidthY,
                         this->lSDomain->cellWidthZ);
  backwardDifferenceField(this->lSDomain->lSField, this->lSDomain->dFieldXm, this->lSDomain->dFieldYm,
                          this->lSDomain->dFieldZm,
                          this->lSDomain->cellWidthX,
                          this->lSDomain->cellWidthY,
                          this->lSDomain->cellWidthZ);

  upwindGradient(this->lSDomain->dFieldXm, this->lSDomain->dFieldYm, this->lSDomain->dFieldZm,
                 this->lSDomain->dFieldXp, this->lSDomain->dFieldYp, this->lSDomain->dFieldZp,
                 this->lSDomain->upwindBG, this->lSDomain->upwindFG);

  upwindDPhi(this->lSDomain->vField, this->lSDomain->upwindBG, this->lSDomain->upwindFG, this->lSDomain->dPhi);

  if (this->lSDomain->lockBase == true){
    for (int i=0;i<this->lSDomain->dPhi.size();i++){
      for (int j=0;j<this->lSDomain->dPhi[0].size();j++){
        for (int k=0;k<3;k++){
          this->lSDomain->dPhi[i][j][k] = 0.0;
        }
      }
    }
  }

  this->lSDomain->lSField -= this->lSDomain->dPhi * dt;
  this->T += dt;

  //check iterator termination condition
  this->it += 1;
  if (this->it >= this->maxIt){
    terminate = 1;
  }

  tuple<int, TriangularMesh> retVals = tuple<int, TriangularMesh>(terminate, surfaceMesh);

  return retVals;

}

ElectrostaticConstantBEMIterator::ElectrostaticConstantBEMIterator(LevelSetDomain* lSDomain, double cfl, double etol, double minSpecimenLength, bool extendShank, double shankAngle): LSIterator(lSDomain, cfl){

  this->lSDomain = lSDomain;

  //set default final conditions
  this->maxT = numeric_limits<double>::max();
  this->T = 0.0;
  this->beta = 10.0;
  this->it = 0;
  this->maxIt = 100;
  this->minSpecimenLength = minSpecimenLength;
  this->adaptive = extendShank;
  this->shankAngle = shankAngle * PI/180.0;

  this->A = 1.0;

  this->bemSolver = new electrostaticBemSolver(etol);

}

ElectrostaticConstantBEMIterator::ElectrostaticConstantBEMIterator(LevelSetDomain& lSDomain,
                                                                   double cfl,
                                                                   double etol,
                                                                   double minSpecimenLength,
                                                                   bool extendShank,
                                                                   double shankAngle): LSIterator(&lSDomain, cfl){

  this->lSDomain = &lSDomain;

  //set default final conditions
  this->maxT = numeric_limits<double>::max();
  this->T = 0.0;
  this->beta = 10.0;
  this->it = 0;
  this->maxIt = 100;
  this->minSpecimenLength = minSpecimenLength;
  this->adaptive = extendShank;
  this->shankAngle = shankAngle * PI/180.0;

  this->A = 1.0;

  this->bemSolver = new electrostaticBemSolver(etol);

}

ElectrostaticConstantBEMIterator::~ElectrostaticConstantBEMIterator(){

  delete this->bemSolver;

}

void ElectrostaticConstantBEMIterator::setThermalModel(UniformTemperatureModel tempModel){

  heatingModelMode = 0;
  uTM = tempModel;

}

void ElectrostaticConstantBEMIterator::setThermalModel(InstantaneousHeatingModel tempModel){

  heatingModelMode = 1;
  iHM = tempModel;

}

void ElectrostaticConstantBEMIterator::setTerminationConditions(double maxT, long maxIt, double maxApexHeight){
  this->maxIt = maxIt;
  this->maxT = maxT;
  this->maxApexHeight = maxApexHeight;
}

tuple<int, TriangularMesh, vector<double>, vector<double>, int> ElectrostaticConstantBEMIterator::iterate(){

  int info = 0;

  this->lSDomain->currentSurfaceState.setCentres();
  this->lSDomain->previousSurfaceState = this->lSDomain->currentSurfaceState;
  this->bemSolver->previousq = this->bemSolver->q;

  double minCellWidth = minimum({this->lSDomain->cellWidthX, this->lSDomain->cellWidthY, this->lSDomain->cellWidthZ});
  double maxCellWidth = maximum({this->lSDomain->cellWidthX, this->lSDomain->cellWidthY, this->lSDomain->cellWidthZ});

  double dt, maxVelField;
  int terminate = 0; //checks whether a termination condition has been reached

  //extract surface from LS field
  TriangularMesh contourMesh;
  cout << "\tmarching cubes extraction..." << endl;
  marchingCubes(this->lSDomain->lSField, 0.0, contourMesh);
  cout << "\tcontour extraction complete" << endl;
  double preminZ = pointDimMinimum(contourMesh.vertices, 2)*this->lSDomain->cellWidthZ + this->lSDomain->bottomLeftDomainCorner.pz;

  //extract internal isocontours (voids and cavities)
  tuple<vector<TriangularMesh>, vector<double>, int> separateMeshResults = contourMesh.detachSeparateMeshComponents();
  vector<TriangularMesh>& dComponents = get<0>(separateMeshResults);
  vector<double>& vols = get<1>(separateMeshResults);
  int mVolInd = get<2>(separateMeshResults);

  //if multiple components (e.g. voids) detected, remove any component with vertices outside of the specimen surface
  if (dComponents.size() > 1){
    int c = 0;
    while (c < dComponents.size()) {
      if (c != mVolInd){
        if (dComponents[mVolInd].insideMesh(dComponents[c].vertices[0]) == 1){
          cout << "removing detached surface component" << endl;
          dComponents.erase(dComponents.begin() + c);
          vols.erase(vols.begin() + c);
          continue;
        }
      }
      c++;
    }
  }

  //create reference to surface mesh
  TriangularMesh& surfaceMesh = dComponents[mVolInd];
  TriangularMesh meshCopy = dComponents[mVolInd];

  //rescale mesh obtained from the field contour extraction to true scale
  //rescaling is performed early here to calculate true specimen volume prior
  //to isotropic remeshing (resulting in a loss of volume resolution)
  meshCopy.rescaleMesh(lSDomain->bottomLeftDomainCorner,
                       lSDomain->cellWidthX,
                       lSDomain->cellWidthY,
                       lSDomain->cellWidthZ);
  surfaceMesh.vol = meshCopy.calculateVolume();

  double shankAngle = this->shankAngle;

  //estimate maximum supported simulation radius (half width of specimen base pre-extension)
  double preMinX = pointDimMinimum(meshCopy.vertices, 0);
  double preMaxX = pointDimMaximum(meshCopy.vertices, 0);
  double preMinY = pointDimMinimum(meshCopy.vertices, 1);
  double preMaxY = pointDimMaximum(meshCopy.vertices, 1);
  double prediamX = preMaxX - preMinX;
  double prediamY = preMaxY - preMinY;
  double preBaseRad = maximum({prediamX/2.0, prediamY/2.0}) * 0.66;

  // Perform shank extension
  // here additional shank is appended to the base of the extracted specimen mesh
  // to ensure a constant user-defined specimen length. This is to attempt preservation
  // of the magnification and effective apex field enhancement (k-factor) throughout the simulation.
  if (this->minSpecimenLength > 0.0 && this->adaptive == true){
    this->shankExtension(surfaceMesh);
  }

  //rescale mesh
  for (int c=0;c<dComponents.size();c++){
    dComponents[c].rescaleMesh(lSDomain->bottomLeftDomainCorner,
                               lSDomain->cellWidthX,
                               lSDomain->cellWidthY,
                               lSDomain->cellWidthZ);
  }

  cout << "isotropic remeshing" << endl;
  auto t1 = high_resolution_clock::now();

  //calculate surfaceMesh bounding box dimensions
  double minX = pointDimMinimum(surfaceMesh.vertices, 0);
  double maxX = pointDimMaximum(surfaceMesh.vertices, 0);
  double minY = pointDimMinimum(surfaceMesh.vertices, 1);
  double maxY = pointDimMaximum(surfaceMesh.vertices, 1);
  double minZ = pointDimMinimum(surfaceMesh.vertices, 2);
  double maxZ = pointDimMaximum(surfaceMesh.vertices, 2);

  double diamX = maxX - minX;
  double diamY = maxY - minY;

  double maxDiam = maximum({diamX, diamY});

  //rescale mesh for remeshing step - resolves numerical rounding issues
  for (int c=0;c<dComponents.size();c++){
    dComponents[c].rescaleMesh(Point(0.0, 0.0, 0.0), 1.0/minCellWidth);
  }
  double rescaleFraction = this->lSDomain->cellWidthZ/minCellWidth;

  //isotropic remeshing
  if (this->adaptive == false){
    surfaceMesh = performRemesh(surfaceMesh, REMESHITS, ADAPTIVECONST * rescaleFraction);

   //remesh detached components (voids) under an isotropic remesh
   if (dComponents.size() > 1){
     for (int c=0;c<dComponents.size();c++){
       if (c != mVolInd){
         dComponents[c] = performRemesh(dComponents[c], REMESHITS, ADAPTIVECONST * rescaleFraction);
       }
     }
   }
  }
  else {

    const int dComponentsSize = dComponents.size();

    vector<double> maxHeights;
    vector<double> targetLengthFractions;
    double maxPanelLength = 0.5 * maxDiam/(1.5 * PI * minCellWidth); //maximum panel length based of fraction of the maximum specimen base diameter
    double apexLength = preBaseRad; //maximum adaptive remeshing depth step
    double minRad = preBaseRad * 0.3; //minimum adaptive remeshing depth step

    TriangularMesh& previousSurfaceState = this->lSDomain->previousSurfaceState;

    //calculate appropriate adaptive remeshing step depth
    //from previous calculated surface evaporation rates

    if (previousSurfaceState.evapRates.size() > 0)
      apexLength = adaptiveRemeshingStep(previousSurfaceState, preBaseRad, minRad, maxZ);

    int m = 0;
    while (false == false){
      maxHeights.push_back(maxZ/minCellWidth - m * (apexLength/minCellWidth) + rescaleFraction * 0.1);
      targetLengthFractions.push_back(ADAPTIVECONST * pow(ADAPTIVEEXP, m));

      if (maxHeights[m] < minZ/minCellWidth){
        break;
      }

      if (targetLengthFractions[m] > maxPanelLength){
        targetLengthFractions[m] = maxPanelLength + 1e-50;
        break;
      }

      m += 1;
    }



    //perform adaptive remeshing of the specimen surface along the shank
    //auto t1 = high_resolution_clock::now();
    surfaceMesh = performAdaptiveRemesh(surfaceMesh, maxHeights,
                                        targetLengthFractions, REMESHITS,
                                        rescaleFraction, preminZ/minCellWidth);
    //auto t2 = high_resolution_clock::now();
    //duration<double> ms_double = t2 - t1;
    //cout << "adaptive remesh time: " << ms_double.count() << endl;

    //remesh detached components (voids) under an isotropic remesh
    if (dComponentsSize > 1){
      for (int c=0;c< dComponentsSize;c++){
        if (c != mVolInd){
          dComponents[c] = performRemesh(dComponents[c], REMESHITS, targetLengthFractions[0]);
        }
      }
    }
  }

  //revert mesh back to true scale
  for (int c=0;c<dComponents.size();c++){
    dComponents[c].rescaleMesh(Point(0.0, 0.0, 0.0), minCellWidth);
  }

  auto t2 = high_resolution_clock::now();
  duration<double> ms_double = t2 - t1;
  cout << "surface remeshing time: " << ms_double.count() << endl;

  this->bemSolver->u = vector<double>(surfaceMesh.faces.size(), 1.0);

  //attach the external mesh for calculating the electrostatic solution
  TriangularMesh electrostaticMesh = surfaceMesh;
  for (int e = 0; e < lSDomain->externalSystemModel.size(); e++) {
      int currentFacesNum = electrostaticMesh.faces.size();
      int currentVertexNum = electrostaticMesh.vertices.size();
      electrostaticMesh.vertices.insert(electrostaticMesh.vertices.end(), lSDomain->externalSystemModel[e].vertices.begin(), lSDomain->externalSystemModel[e].vertices.end());
      electrostaticMesh.faces.insert(electrostaticMesh.faces.end(), lSDomain->externalSystemModel[e].faces.begin(), lSDomain->externalSystemModel[e].faces.end());
      std::for_each(electrostaticMesh.faces.begin() + currentFacesNum, electrostaticMesh.faces.end(), [currentVertexNum](Face& d) { d.p1 += currentVertexNum; d.p2 += currentVertexNum; d.p3 += currentVertexNum;});
      double voltageFrac = lSDomain->externalSystemVoltages[e];
      this->bemSolver->u.resize(electrostaticMesh.faces.size(), 1.0);
      std::for_each(this->bemSolver->u.begin() + currentFacesNum, this->bemSolver->u.end(), [voltageFrac](double& d) { d = voltageFrac; });
  }

  //set mesh panel centres
  surfaceMesh.setMeshProperties();
  electrostaticMesh.setMeshProperties();

  //construct BEM problem matrices

  cout << "matrix size: " << electrostaticMesh.faces.size() << "x" << electrostaticMesh.faces.size() << endl;
  cout << "constructing BEM problem matrices" << endl;
  t1 = high_resolution_clock::now();
  this->bemSolver->calculateElectrostaticMatrix(electrostaticMesh);
  t2 = high_resolution_clock::now();
  ms_double = t2 - t1;
  cout << "matrix calculation time: " << ms_double.count() << endl;

  //set the number of specimen contours (e.g. surface + any voids) for the current iteration
  this->lSDomain->contours = dComponents.size();

  cout << "solving electrostatic field" << endl;
  t1 = high_resolution_clock::now();

  //generate an initial guess for solution vector via back-projection of
  //previous geometry - often achieves convergence in fewer iterations
  if ((this->bemSolver->previousq.size() > 0) &&
      (isnan(this->bemSolver->previousq[0]) == false) &&
      (dComponents.size() == this->lSDomain->contours)){
       this->bemSolver->q.resize(surfaceMesh.faces.size(), 0.0);
       this->initialSolutionGuess(this->bemSolver->q, surfaceMesh, this->lSDomain->previousSurfaceState, this->bemSolver->previousq);
  }

  //calculate the BEM surface electrostatics
  this->bemSolver->matrixVectorProduct(this->bemSolver->rhs, this->bemSolver->Fmat, this->bemSolver->u);
  int solverConv = solveField(this->bemSolver->Pmat, this->bemSolver->rhs, this->bemSolver->q, this->bemSolver->Amat, this->bemSolver->AmatDimSize);

  t2 = high_resolution_clock::now();
  ms_double = t2 - t1;
  cout << "GMRES solver time: " << ms_double.count() << endl;

  //solver failed to converge. Suspected error with remeshing. Perturb system and rerun iteration.
  if (solverConv != 0){
    info = 1;
    this->lSDomain->lSField -= 0.05 * minCellWidth;
    this->bemSolver->previousq = vector<double>(0);
    tuple<int, TriangularMesh, vector<double>, vector<double>, int> retVals = tuple<int, TriangularMesh, vector<double>, vector<double>, int>(terminate, surfaceMesh, this->bemSolver->u, this->bemSolver->q, info);
    return retVals;
  }

  cout << "averaging surface electric field" << endl;

  //remove external mesh electrostatic solution
  vector<double> qsol = this->bemSolver->q;
  qsol.erase(qsol.begin()+surfaceMesh.faces.size(), qsol.end());

  vector<double> qsolAv = averageField(surfaceMesh.faces, qsol) * -1.0;

  //determine local surface panel phase
  vector<int> fInd = this->lSDomain->determineLocalPhase(surfaceMesh);

  //assign evaporation fields according to the assigned specimen model
  vector<double> evapFields = this->lSDomain->assignSurfaceEvaporationFields(surfaceMesh, fInd);

  //calculate voltage ramping
  double rescaleVoltage = instantaneousSFVoltageRamp(qsolAv, evapFields, 1.0);

  //calculate temperature field
  vector<double> T = vector<double>(qsolAv.size(), 1.0);

  //calculate temperature field - laser
  cout << "Calculating specimen thermal heating." << endl;
  t1 = high_resolution_clock::now();
  switch(heatingModelMode){
    case -1:
      cerr << "thermal heating model for `ElectrostaticConstantBEMIterator` has not been set. Please set through class method `setThermalModel` (see examples)." << endl;
      exit(1);
      break;
    case 0:
      T = uTM.calculateTemperature(surfaceMesh);
      break;
    case 1:
      T = iHM.calculateTemperature(surfaceMesh);
      break;
    default:
      cerr << "unrecognised thermal heating model for `ElectrostaticConstantBEMIterator`. Please set to a recognised thermal model through class method `setThermalModel` (see examples)." << endl;
      exit(1);
      break;
  }

  t2 = high_resolution_clock::now();
  ms_double = t2 - t1;
  cout << "Thermal heating time: " << ms_double.count() << endl;
  vector<double> rsfield = qsolAv * rescaleVoltage;

  cout << "assign surface evaporation rates" << endl;
  vector<double> evapRates = this->lSDomain->assignSurfaceEvaporationRates(surfaceMesh, rsfield, T, fInd);

  //flag only surfaceMesh faces as falling on surface
  for (int f=0;f<surfaceMesh.faces.size();f++)
    surfaceMesh.faces[f].isSurface = true;

  //expand result cellData for vtk to be defined over internal surface contours
  TriangularMesh totalMesh;
  totalMesh.vertices = surfaceMesh.vertices;
  totalMesh.faces = surfaceMesh.faces;

  for (int c=0;c<dComponents.size();c++){
    if (c != mVolInd){
      vector<Face> rFaces = dComponents[c].faces + totalMesh.vertices.size();
      totalMesh.faces.insert(totalMesh.faces.end(), rFaces.begin(), rFaces.end());
      totalMesh.vertices.insert(totalMesh.vertices.end(), dComponents[c].vertices.begin(), dComponents[c].vertices.end());
    }
  }

  vector<double> evapRatesExp(totalMesh.faces.size(), 0.0);
  vector<double> avqExp(totalMesh.faces.size(), 0.0);
  vector<double> qExp(totalMesh.faces.size(), 0.0);
  vector<double>evapFieldsExp(totalMesh.faces.size(), 0.0);
  vector<double>tempExp(totalMesh.faces.size(), 0.0);
  vector<double>voltageExp(totalMesh.faces.size(), rescaleVoltage);
  vector<double>isSurfaceExp(totalMesh.faces.size(), 0.0);

  for (int i=0;i<evapRates.size();i++){
    evapRatesExp[i] = evapRates[i];
    avqExp[i] = qsolAv[i];
    qExp[i] = qsol[i];
    evapFieldsExp[i] = evapFields[i];
    tempExp[i] = T[i];
    isSurfaceExp[i] = 1.0;
  }

  totalMesh.assignCellData(evapRatesExp, "evap_rate");
  totalMesh.assignCellData(avqExp, "average_flux");
  totalMesh.assignCellData(qExp, "flux");
  totalMesh.assignCellData(evapFieldsExp, "evaporation_field");
  totalMesh.assignCellData(tempExp, "temperature");
  totalMesh.assignCellData(voltageExp, "potential");
  totalMesh.assignCellData(isSurfaceExp, "is_surface");

  totalMesh.evapRates = evapRatesExp;

  //construct extension velocity
  cout << "constructing extension velocity" << endl;
  t1 = high_resolution_clock::now();

  int shankCutoff = 0;

  if (this->adaptive == true){

    vector<double> zCentre;
    vector<int> pInd(surfaceMesh.faces.size(), 0);
    iota(pInd.begin(), pInd.end(), 0);

    double hThresh = preminZ + 0.3*(maxZ - preminZ);
    for (int i=0;i<surfaceMesh.faces.size();i++){
      zCentre.push_back(surfaceMesh.faces[i].centre.pz);
    }

    sort(pInd.begin(), pInd.end(), [&](int i,int j){return zCentre[i] < zCentre[j];});

    double lpz;
    for (int i=0;i<pInd.size();i++){
      int ip = pInd[i];
      lpz = zCentre[ip];
      if (lpz > hThresh && evapRates[ip] > MAXVELOCITYCUTOFFFRACTION) {
        break;
      }
    }
    shankCutoff = (int)((lpz - this->lSDomain->bottomLeftDomainCorner.pz)/this->lSDomain->cellWidthZ);
  }

  totalMesh.setCentres();

  if (ADAPTIVEEXTENSIONVELOCITY == true){
  //generated coarsened mesh points for adaptive extension velocity construction
  vector<vector<long>> faceFaceAdjacencyList = totalMesh.buildFaceFaceAdjacencyList();
  map<int, int> incFace;
  vector<Point> decimatedPoints;
  vector<double> decimatedEvapRates;
  for (int i=0;i<faceFaceAdjacencyList.size();i++){
    incFace[i] = 0;
  }
  for (int i=0;i<faceFaceAdjacencyList.size();i++){
    if (incFace[i] == 0){
      decimatedPoints.push_back(totalMesh.faces[i].centre);
      decimatedEvapRates.push_back(evapRatesExp[i]);
      incFace[i] = 1;
      for (int j=0;j<faceFaceAdjacencyList[i].size();j++){
        incFace[faceFaceAdjacencyList[i][j]] = 1;
      }
    }
  }

  //perform extension velocity construction using coarsened surface mesh past the narrowband region
  this->lSDomain->constructExtensionVelocityAdaptive(totalMesh, evapRatesExp, decimatedPoints, decimatedEvapRates,
                                                     this->lSDomain->lSField, ADAPTIVEEXTVELNARROWBAND, shankCutoff);
  }
  else {
    //use the regular grid extension velocity construction (slower)
    this->lSDomain->constructExtensionVelocity(totalMesh, evapRatesExp, shankCutoff);
  }

  t2 = high_resolution_clock::now();
  ms_double = t2 - t1;
  cout << "Extension velocity: " << ms_double.count() << endl;

  //perform smoothing of velocity field via weighted median filter
  cout << "smoothing velocity field" << endl;
  smoothField(this->lSDomain->vField, this->lSDomain->tField, 0.3, true); //for equiv to moving average filter coefficient -  1.0/27.0
  smoothField(this->lSDomain->tField, this->lSDomain->vField, 0.3, true); //for equiv to moving average filter coefficient -  1.0/27.0
  //this->lSDomain->vField = this->lSDomain->tField;


  maxVelField = max(this->lSDomain->vField);

  dt = this->cfl * min({this->lSDomain->cellWidthX, this->lSDomain->cellWidthY, this->lSDomain->cellWidthZ})/maxVelField;
  if (this->T + dt > this->maxT){
    dt = this->maxT - this->T;
    terminate = 1;
  }

  //integrate level set field equation
  forwardDifferenceField(this->lSDomain->lSField, this->lSDomain->dFieldXp, this->lSDomain->dFieldYp,
                         this->lSDomain->dFieldZp,
                         this->lSDomain->cellWidthX,
                         this->lSDomain->cellWidthY,
                         this->lSDomain->cellWidthZ);
  backwardDifferenceField(this->lSDomain->lSField, this->lSDomain->dFieldXm, this->lSDomain->dFieldYm,
                          this->lSDomain->dFieldZm,
                          this->lSDomain->cellWidthX,
                          this->lSDomain->cellWidthY,
                          this->lSDomain->cellWidthZ);

  upwindGradient(this->lSDomain->dFieldXm, this->lSDomain->dFieldYm, this->lSDomain->dFieldZm,
                 this->lSDomain->dFieldXp, this->lSDomain->dFieldYp, this->lSDomain->dFieldZp,
                 this->lSDomain->upwindBG, this->lSDomain->upwindFG);

  upwindDPhi(this->lSDomain->vField, this->lSDomain->upwindBG, this->lSDomain->upwindFG, this->lSDomain->dPhi);

  if (this->adaptive == true){
    for (int i=0;i<this->lSDomain->dPhi.size();i++){
      for (int j=0;j<this->lSDomain->dPhi[0].size();j++){
        for (int k=0;k<5;k++){
          this->lSDomain->dPhi[i][j][k] = 0.0;
        }
      }
    }
  }

  //vtkObject outv;
  //outv.setWithField(this->lSDomain->vField, this->lSDomain->bottomLeftDomainCorner,
  //                  this->lSDomain->cellWidthX, this->lSDomain->cellWidthY,
  //                  this->lSDomain->cellWidthZ);
  //writeVtk(outv, "vel.vtk");

  //iterate level set field
  this->lSDomain->lSField -= this->lSDomain->dPhi * dt;
  this->T += dt;

  //check iterator termination condition
  this->it += 1;
  if (this->it >= this->maxIt){
    terminate = 1;
  }

  //check max apex height termination condition
  double currentMaxApexHeight = pointDimMaximum(surfaceMesh.vertices, 2);
  if (currentMaxApexHeight <= this->maxApexHeight){
    terminate = 1;
  }

  if (this->adaptive == true){
    if ((this->lSDomain->lSField[0][0].size() * this->lSDomain->cellWidthZ + this->lSDomain->bottomLeftDomainCorner.pz - currentMaxApexHeight) > (this->lSDomain->initialGap + SHRINKSTEP)){
      cout << "shrinking level set field" << endl;
      if (this->lSDomain->lSField[0][0].size() - SHRINKSTEP > 0.0){
        this->lSDomain->resizeLSField();
        cout << "shrunk level set field. New dimensions: [" << this->lSDomain->xDim << "," << this->lSDomain->yDim << "," << this->lSDomain->zDim << "]" << endl;
      }
    }
  }

  totalMesh.vol = surfaceMesh.vol;
  this->lSDomain->currentSurfaceState = totalMesh;

  tuple<int, TriangularMesh, vector<double>, vector<double>, int> retVals = tuple<int, TriangularMesh, vector<double>, vector<double>, int>(terminate, totalMesh, this->bemSolver->u, this->bemSolver->q, info);

  return retVals;

}

/*!
 * Method for determining the distance along the z-axis between which to perform adaptive remeshing.
 * Here the evaporation rate of the previous surface state is used to determine the region this step.
 *
 * @param previousSurfaceState the previous model surface state.
 * @param maxDepthStep The upper limit for the adaptive step.
 * @param minDepthStep The lower limit for the adaptive step.
 * @param currentMaxHeight The current model specimen state maximum height
 * @return The adaptive step distance in z
 */
double ElectrostaticConstantBEMIterator::adaptiveRemeshingStep(TriangularMesh& previousSurfaceState,
                                                               double maxDepthStep, double minDepthStep,
                                                               double currentMaxHeight){

  vector<double> smoothedEvapRates = averageField(previousSurfaceState.faces, previousSurfaceState.evapRates);
  smoothedEvapRates = averageField(previousSurfaceState.faces, smoothedEvapRates);
  smoothedEvapRates = averageField(previousSurfaceState.faces, smoothedEvapRates);
  smoothedEvapRates = averageField(previousSurfaceState.faces, smoothedEvapRates);

  double zsurf = numeric_limits<double>::max();
  double prevMaxZ = numeric_limits<double>::min();
  double prevMinZ = numeric_limits<double>::max();

  const int prevFaces = previousSurfaceState.faces.size();

  for (int c=0;c<prevFaces;c++){
    if (previousSurfaceState.faces[c].centre.pz > prevMaxZ)
      prevMaxZ = previousSurfaceState.faces[c].centre.pz;
    if (previousSurfaceState.faces[c].centre.pz < prevMinZ)
      prevMinZ = previousSurfaceState.faces[c].centre.pz;
  }

  for (int c=0;c< prevFaces;c++){
    if (previousSurfaceState.evapRates[c] > REMESHFRACTION){
      if ((previousSurfaceState.faces[c].centre.pz < zsurf) &&
          (previousSurfaceState.faces[c].centre.pz > (prevMinZ + (prevMaxZ - prevMinZ) * 0.25))){
            zsurf = previousSurfaceState.faces[c].centre.pz;
      }
    }
  }

  double apexLength = currentMaxHeight - zsurf;

  if (apexLength > maxDepthStep)
    apexLength = maxDepthStep;
  else if (apexLength < minDepthStep)
    apexLength = minDepthStep;

  return apexLength;
}

/*!
 * Method for performing a shank extension at the base of the specimen surface mesh.
 * Takes vertices at base and drags them down according to the extension length and
 * shank angle.
 *
 * @param surfaceMesh The specimen surface mesh
 *
 */
void ElectrostaticConstantBEMIterator::shankExtension(TriangularMesh& surfaceMesh){

  double vmax = pointDimMaximum(surfaceMesh.vertices, 2);
  double vmin = pointDimMinimum(surfaceMesh.vertices, 2);

  if ((vmax - vmin) < this->minSpecimenLength/this->lSDomain->cellWidthZ){
    double baseShift = this->minSpecimenLength/this->lSDomain->cellWidthZ - (vmax - vmin);
    vector<Point*> basePoints;
    vector<double> angles;
    for (int i=0;i<surfaceMesh.vertices.size();i++){
      Point* lPoint = &surfaceMesh.vertices[i];
      if (lPoint->pz < (vmin + 1.5)){
        basePoints.push_back(lPoint);
        lPoint->pz -= baseShift;
      }
    }
    Point baseCentre = pointMean(basePoints);
    double baseCentreX = baseCentre.px;
    double baseCentreY = baseCentre.py;

    //also need to expand x,y for the shank
    for (int i=0;i<basePoints.size();i++){
      double angle = atan2(basePoints[i]->py - baseCentreY, basePoints[i]->px - baseCentreX);
      basePoints[i]->px += cos(angle) * baseShift * tan(shankAngle);
      basePoints[i]->py += sin(angle) * baseShift * tan(shankAngle);
    }
  }
}

/*!
 *
 * Function for resizing the level set grid once a specimen apex height is a set number of grid cells from the boundary
 * (given by the initial number of cells + SHRINKSTEP)
 *
 */
void LevelSetDomain::resizeLSField(){
  scalarField3D newField = initialiseField(this->lSField.size(), this->lSField[0].size(), this->lSField[0][0].size() - SHRINKSTEP, 0.0);
  for (int i=0;i<newField.size();i++){
    for (int j=0;j<newField[0].size();j++){
      for (int k=0;k<newField[0][0].size();k++){
        newField[i][j][k] = this->lSField[i][j][k];
      }
    }
  }
  //set new grid dimensions
  this->xDim = newField.size();
  this->yDim = newField[0].size();
  this->zDim = newField[0][0].size();
  this->lSField = newField;

  //reinitialise cached fields
  this->mField = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->tField = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->vField = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldX = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldY = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldZ = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->ddFieldX = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->ddFieldY = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->ddFieldZ = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);

  this->dFieldXp = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldYp = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldZp = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldXm = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldYm = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldZm = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);

  this->upwindBG = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->upwindFG = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dPhi = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);

}

void ElectrostaticConstantBEMIterator::initialSolutionGuess(vector<double>& nsol, TriangularMesh& newMesh,
                                                            TriangularMesh& oldMesh,
                                                            vector<double>& x){

  #if defined(_OPENMP)
  #pragma omp parallel for
  #endif
  for (int i=0;i<newMesh.faces.size();i++){
    double ndist2 = numeric_limits<double>::max();
    double tdist2;
    double ix = newMesh.faces[i].centre.px;
    double iy = newMesh.faces[i].centre.py;
    double iz = newMesh.faces[i].centre.pz;
    double ijvx, ijvy, ijvz;
    int mguess;
    for (int j=0;j<oldMesh.faces.size();j++){
      ijvx = oldMesh.faces[j].centre.px - ix;
      ijvy = oldMesh.faces[j].centre.py - iy;
      ijvz = oldMesh.faces[j].centre.pz - iz;

      tdist2 = ijvx*ijvx+ijvy*ijvy+ijvz*ijvz;
      if (tdist2 < ndist2){
        mguess = j;
        ndist2 = tdist2;
      }
    }
    nsol[i] = x[mguess];
  }

  return;
}

LevelSetDomain::LevelSetDomain(int xDim, int yDim, int zDim,
                               Point bottomLeftDomainCorner,
                               double cellWidth) : xDim{xDim},
                                                   yDim{yDim},
                                                   zDim{zDim},
                                                   bottomLeftDomainCorner{bottomLeftDomainCorner},
                                                   cellWidthX{cellWidth},
                                                   cellWidthY{cellWidth},
                                                   cellWidthZ{cellWidth}{

  //initialise level set field
  this->mField = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->lSField = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->tField = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->vField = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldX = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldY = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldZ = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->ddFieldX = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->ddFieldY = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->ddFieldZ = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);

  this->dFieldXp = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldYp = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldZp = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldXm = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldYm = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldZm = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);

  this->upwindBG = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->upwindFG = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dPhi = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);

}

/*!
 *
 * Method for performing Sussman reinitialisation on the level set field
 * restoring its signed distance form.
 * Current method is first order - a second order method would help
 * mitigate any numerical issues.
 * @param tol relative tolerance for convergence
 * @param its The maximum number of iterations to perform
 * @return if successful then number of iterations, else -1
 */
int LevelSetDomain::sussmanReinitialisation(double atol, long its){

  cout << "Level set field Reinitialisation (restoring signed distance form)" << endl;

  double minCellWidth =  min({this->cellWidthX, this->cellWidthY, this->cellWidthZ});
  this->lSField /= minCellWidth;
  scalarField3D ofield = this->lSField;

  double pzeta = 0.5;
  double dt = 0.2;
  unsigned int i = 0;

  double rCellWidthX = this->cellWidthX/minCellWidth;
  double rCellWidthY = this->cellWidthY/minCellWidth;
  double rCellWidthZ = this->cellWidthZ/minCellWidth;

  scalarField3D sign_field = ofield/pow(pow(ofield, 2.0) + pzeta, 0.5);

  scalarField3D dxfp = this->lSField;
  scalarField3D dxfm = this->lSField;
  scalarField3D dxbp = this->lSField;
  scalarField3D dxbm = this->lSField;
  scalarField3D dyfp = this->lSField;
  scalarField3D dyfm = this->lSField;
  scalarField3D dybp = this->lSField;
  scalarField3D dybm = this->lSField;
  scalarField3D dzfp = this->lSField;
  scalarField3D dzfm = this->lSField;
  scalarField3D dzbp = this->lSField;
  scalarField3D dzbm = this->lSField;
  scalarField3D G = this->lSField;

  //integrate level set field equation
  for (i=0;i<its;i++){
    forwardDifferenceField(this->lSField, this->dFieldXp, this->dFieldYp,
                           this->dFieldZp,
                           rCellWidthX,
                           rCellWidthY,
                           rCellWidthZ);
    backwardDifferenceField(this->lSField, this->dFieldXm, this->dFieldYm,
                            this->dFieldZm,
                            rCellWidthX,
                            rCellWidthY,
                            rCellWidthZ);

    maximum(dxfp, this->dFieldXp, 0.0);
    minimum(dxfm, this->dFieldXp, 0.0);
    maximum(dxbp, this->dFieldXm, 0.0);
    minimum(dxbm, this->dFieldXm, 0.0);

    maximum(dyfp, this->dFieldYp, 0.0);
    minimum(dyfm, this->dFieldYp, 0.0);
    maximum(dybp, this->dFieldYm, 0.0);
    minimum(dybm, this->dFieldYm, 0.0);

    maximum(dzfp, this->dFieldZp, 0.0);
    minimum(dzfm, this->dFieldZp, 0.0);
    maximum(dzbp, this->dFieldZm, 0.0);
    minimum(dzbm, this->dFieldZm, 0.0);

    setField(G, 0.0);
    G += (pow(rmaximum(dxbp * dxbp, dxfm * dxfm) +
              rmaximum(dybp * dybp, dyfm * dyfm) +
              rmaximum(dzbp * dzbp, dzfm * dzfm), 0.5) - 1.0) * (ofield > 0.0);

    G += (pow(rmaximum(dxbm * dxbm, dxfp * dxfp) +
              rmaximum(dybm * dybm, dyfp * dyfp) +
              rmaximum(dzbm * dzbm, dzfp * dzfp), 0.5) - 1.0) * (ofield < 0.0);

    G *= sign_field;

    //termination condition - relative tolerance
    this->lSField -= G * dt;

    double err = sum(absField(G))/(this->xDim*this->yDim*this->zDim);
    if (err < atol){
      this->lSField *= minCellWidth;
      cout << "\tIterations: " << i+1 << endl;
      return i;
    }

    cout << "\tSussman Iteration: " << i+1 << endl;

  }

  this->lSField *= minCellWidth;
  cout << "\tReinitialization ran to max number of iterations:" << i+1 << endl;
  return 0;
}

/*!
*
* Method for initialising a signed distance field from boundaryMesh within LSField attribute of the LevelSetDomain instance.
* @param boundaryMesh The triangular input mesh from which the signed distance field is constructed;
*
*/
void LevelSetDomain::initialiseSDF(TriangularMesh& boundaryMesh){

  cout << "\tInitialising SDF..." << endl;

  double dist;
  double insideCheck;

  double maximumCellWidth = max({this->cellWidthX,this->cellWidthY, this->cellWidthZ});
  double signTol = maximumCellWidth * SIGNTOL;

  boundaryMesh.setCentres();
  boundaryMesh.setLongestSideSqrd();

  scalarField3D fieldSign = initialiseField(this->lSField.size(), this->lSField[0].size(), this->lSField[0][0].size(), 0.0);

  cout << "\t\tCalculating magnitude..." << endl;

  //calculate distance from mesh to cell centres
  #if defined(_OPENMP)
  #pragma omp parallel
  #endif
  {
  Point cellCentre;
  #if defined(_OPENMP)
  #pragma omp for schedule(dynamic)
  #endif
  for (int i=0;i<this->xDim;i++){
    cellCentre.px = bottomLeftDomainCorner.px + this->cellWidthX * i;
    for (int j=0;j<this->yDim;j++){
      cellCentre.py = bottomLeftDomainCorner.py + this->cellWidthY * j;
      for (int k=0;k<this->zDim;k++){
        cellCentre.pz = bottomLeftDomainCorner.pz + this->cellWidthZ * k;
        dist = boundaryMesh.shortestDistanceToPoint(cellCentre);
        this->lSField[i][j][k] = dist;
      }
    }
  }
  }

  cout << "\t\tCalculating sign..." << endl;

  //calculate field sign (cell centres inside or outside boundaryMesh)
  #if defined(_OPENMP)
  #pragma omp parallel
  #endif
  {
  Point cellCentre2;
  #if defined(_OPENMP)
  #pragma omp for schedule(dynamic)
  #endif
  for (int i=0;i<this->xDim;i++){
    cellCentre2.px = bottomLeftDomainCorner.px + this->cellWidthX * i;
    for (int j=0;j<this->yDim;j++){
      cellCentre2.py = bottomLeftDomainCorner.py + this->cellWidthY * j;
      for (int k=0;k<this->zDim;k++){
        cellCentre2.pz = bottomLeftDomainCorner.pz + this->cellWidthZ * k;
        if (this->lSField[i][j][k] > signTol){
          if (i > 0){
            if ((fieldSign[i-1][j][k] > 0.1) || (fieldSign[i-1][j][k] < -0.1)){
              fieldSign[i][j][k] = fieldSign[i-1][j][k];
              continue;
            }
          }
          if (j > 0){
            if ((fieldSign[i][j-1][k] > 0.1) || (fieldSign[i][j-1][k] < -0.1)){
              fieldSign[i][j][k] = fieldSign[i][j-1][k];
              continue;
            }
          }
          if (k > 0){
            if ((fieldSign[i][j][k-1] > 0.1) || (fieldSign[i][j][k-1] < -0.1)){
              fieldSign[i][j][k] = fieldSign[i][j][k-1];
              continue;
            }
          }
          if (i < (xDim-1)){
            if ((fieldSign[i+1][j][k] > 0.1) || (fieldSign[i+1][j][k] < -0.1)){
              fieldSign[i][j][k] = fieldSign[i+1][j][k];
              continue;
            }
          }
          if (j < (yDim-1)){
            if ((fieldSign[i][j+1][k] > 0.1) || (fieldSign[i][j+1][k] < -0.1)){
              fieldSign[i][j][k] = fieldSign[i][j+1][k];
              continue;
            }
          }
          if (k < (zDim-1)){
            if ((fieldSign[i][j][k+1] > 0.1) || (fieldSign[i][j][k+1] < -0.1)){
              fieldSign[i][j][k] = fieldSign[i][j][k+1];
              continue;
            }
          }
        }
        insideCheck = 1.0 - 2.0*(float)boundaryMesh.insideMesh(cellCentre2);
        fieldSign[i][j][k] = insideCheck;
      }
    }
  }
  }

  #if defined(_OPENMP)
  #pragma omp parallel
  #endif
  {
  Point cellCentre2;
  #if defined(_OPENMP)
  #pragma omp for schedule(dynamic)
  #endif
  for (int i=0;i<this->xDim;i++){
    for (int j=0;j<this->yDim;j++){
      for (int k=0;k<this->zDim;k++){
        this->lSField[i][j][k] *= fieldSign[i][j][k];
      }
    }
  }
  }

  cout << "\tSDF initialisation complete" << endl;

}

void CurvatureIterator::initialiseParameterGrid(){

  Point cellCentre;
  TMSpecimen& specimenModel = this->lSDomain->getSpecimenModel();

  this->lSDomain->evapFieldGrid = initialiseField(this->lSDomain->xDim,
                                                  this->lSDomain->yDim,
                                                  this->lSDomain->zDim,
                                                  specimenModel.phases[0].CLI.materialModifier);

  //calculate distance from mesh to cell centres
  #if defined(_OPENMP)
  #pragma omp parallel for
  #endif
  for (int i=0;i<this->lSDomain->xDim;i++){
    cellCentre.px = this->lSDomain->bottomLeftDomainCorner.px + this->lSDomain->cellWidthX * i;
    for (int j=0;j<this->lSDomain->yDim;j++){
      cellCentre.py = this->lSDomain->bottomLeftDomainCorner.py + this->lSDomain->cellWidthY * j;
      for (int k=0;k<this->lSDomain->zDim;k++){
        cellCentre.pz = this->lSDomain->bottomLeftDomainCorner.pz + this->lSDomain->cellWidthZ * k;

        double field;
        int inside;
        bool set = false;
        for (int a = 1; a<specimenModel.phases.size(); a++){
          inside = specimenModel.phases[a].phaseBoundary.insideMesh(cellCentre);
          if (inside == 0){
            field = specimenModel.phases[a].CLI.materialModifier;
            set = true;
          }
        }
        if (set == true){
          this->lSDomain->evapFieldGrid[i][j][k] = field;
        }
      }
    }
  }
}

vector<int> LevelSetDomain::determineLocalPhase(TriangularMesh& surfaceMesh){

  vector<int> fInd = vector<int>(surfaceMesh.faces.size(), 0);
  vector<double> surfaceEvapField = vector<double>(surfaceMesh.faces.size(), 0.0);

  const int phaseNum = this->specimenModel.phases.size();
  const long faceNum = surfaceMesh.faces.size();

  #if defined(_OPENMP)
  #pragma omp parallel for
  #endif
  for (long l=0;l<faceNum;l++){
    int phaseIndex;
    int i = (int)round((surfaceMesh.faces[l].centre.px - this->bottomLeftDomainCorner.px)/this->cellWidthX);
    int j = (int)round((surfaceMesh.faces[l].centre.py - this->bottomLeftDomainCorner.py)/this->cellWidthY);
    int k = (int)round((surfaceMesh.faces[l].centre.pz - this->bottomLeftDomainCorner.pz)/this->cellWidthZ);
    if (i >= 0 && i < this->xDim && j >= 0 && j < this->yDim && k >= 0 && k < this->zDim){
      phaseIndex = this->phaseGrid[i][j][k];
      if (phaseIndex < 0){
        for (int p = phaseNum; p-- > 1; ){
          if (this->specimenModel.phases[p].ghost == true)
            continue;

          int inside = 1.0 - 2.0*(float)this->specimenModel.phases[p].phaseBoundary.insideMesh(surfaceMesh.faces[l].centre);
          if (inside == 1){
            phaseIndex = p;
            break;
          }
        }
      }
    }
    else {
      phaseIndex = 0;
    }
    if (phaseIndex < 0){
      phaseIndex = 0;
    }
    fInd[l] = phaseIndex;
  }

  return fInd;

}

vector<double> LevelSetDomain::assignSurfaceEvaporationFields(TriangularMesh& surfaceMesh, vector<int>& pInd){

  vector<double> surfaceEvapField = vector<double>(surfaceMesh.faces.size(), 0.0);

  const int faceCount = surfaceMesh.faces.size();
  for (int l=0;l<faceCount;l++){
    //surfaceEvapField[l] = this->specimenModel.phases[fInd[l]].F0;
    switch (this->specimenModel.phases[pInd[l]].setEvapLawMode){
      case 1:
        surfaceEvapField[l] = this->specimenModel.phases[pInd[l]].ALI.evapField;
        break;
      case 2:
        surfaceEvapField[l] = this->specimenModel.phases[pInd[l]].LLI.evapField;
        break;
      case 3:
        surfaceEvapField[l] = this->specimenModel.phases[pInd[l]].DALI.evapField;
        break;
    }
  }

  return surfaceEvapField;
}


vector<double> LevelSetDomain::assignSurfaceEvaporationRates(TriangularMesh& surfaceMesh,
                                                             vector<double>& surfaceField,
                                                             vector<double>& T,
                                                             vector<int>& pInd){

  vector<double> surfaceEvapRate = vector<double>(surfaceMesh.faces.size(), 0.0);

  //assign surface evaporation rates according to phase law
  for (int l=0;l<surfaceMesh.faces.size();l++){
    switch (this->specimenModel.phases[pInd[l]].setEvapLawMode){
      case 1:
        surfaceEvapRate[l] = this->specimenModel.phases[pInd[l]].ALI.evaporationRate(surfaceField[l], T[l]);
        break;
      case 2:
        surfaceEvapRate[l] = this->specimenModel.phases[pInd[l]].LLI.evaporationRate(surfaceField[l], T[l]);
        break;
      case 3:
        surfaceEvapRate[l] = this->specimenModel.phases[pInd[l]].DALI.evaporationRate(surfaceField[l], T[l]);
        break;
    }
  }

  return surfaceEvapRate;
}

void LevelSetDomain::assignSpecimenModel(TMSpecimen spModel){

  this->evapFieldGrid = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->phaseGrid = initialiseFieldInt(this->xDim, this->yDim, this->zDim, 0);
  this->specimenModel = spModel;

  cout << "\tConstruct phase grids..." << endl;
  for (int i = spModel.phases.size(); i-- > 1; ){
    if (spModel.phases[i].ghost == false)
      this->assignToPhaseGrid(spModel.phases[i].phaseBoundary, i);
  }
  cout << "\tPhase grids construction complete." << endl;
}

void LevelSetDomain::assignExternalModel(vector<TriangularMesh> externalModel, vector<double> externalVoltages) {

    this->externalSystemModel = externalModel;
    this->externalSystemVoltages = externalVoltages;

    cout << "\tExternal Model assigned to LS domain." << endl;

}


/*!
 * Method to assign a specimen model to a LevelSetDomain object instance.
 * Calculates the signed distance field and local phase fields
 *
 * @param spModel The specimen model instance.
 * @param lockBase Whether to lock the specimen base from evaporation or not
 * @param xDim The level set x-dimension grid cell partitioning
 * @param yDim The level set y-dimension grid cell partitioning
 * @param zDim The level set z-dimension grid cell partitioning
 *
 */
void LevelSetDomain::assignSpecimenModel(TMSpecimen spModel, bool lockBase,
                                         int xDim, int yDim, int zDim){

  //extract disconnected components
  TriangularMesh& contourMesh = spModel.phases[0].phaseBoundary;

  tuple<vector<TriangularMesh>, vector<double>, int> separateMeshResults = contourMesh.detachSeparateMeshComponents();
  vector<TriangularMesh>& dComponents = get<0>(separateMeshResults);
  vector<double>& vols = get<1>(separateMeshResults);
  int mVolInd = get<2>(separateMeshResults);

  //if multiple components (e.g. voids) detected, remove any component with vertices outside of the specimen surface
  if (dComponents.size() > 1){
    int c = 0;
    while (c < dComponents.size()) {
      if (c != mVolInd){
        if (dComponents[mVolInd].insideMesh(dComponents[c].vertices[0]) == 1){
          cout << "removing initially detached surface component from specimen model" << endl;
          dComponents.erase(dComponents.begin() + c);
          vols.erase(vols.begin() + c);
          continue;
        }
      }
      c++;
    }
  }

  TriangularMesh boundaryMesh;
  for (int c=0;c<dComponents.size();c++){
    vector<Face> rFaces = dComponents[c].faces + boundaryMesh.vertices.size();
    boundaryMesh.faces.insert(boundaryMesh.faces.end(), rFaces.begin(), rFaces.end());
    boundaryMesh.vertices.insert(boundaryMesh.vertices.end(), dComponents[c].vertices.begin(), dComponents[c].vertices.end());
  }

  spModel.phases[0].phaseBoundary = boundaryMesh;

  this->specimenModel = spModel;

  this->initialiseSDF(lockBase, xDim, yDim, zDim);
  this->evapFieldGrid = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->phaseGrid = initialiseFieldInt(this->xDim, this->yDim, this->zDim, 0);

  cout << "\tConstruct phase grids..." << endl;
  for (int i = spModel.phases.size(); i-- > 1; ){
    if (spModel.phases[i].ghost == false)
      this->assignToPhaseGrid(spModel.phases[i].phaseBoundary, i);
  }
  cout << "\tPhase grids construction complete." << endl;
}

/*!
 * Reinitialises scalar fields used in the level set method
 */
void LevelSetDomain::redefineFields(){

  this->mField = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->lSField = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->tField = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->vField = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldX = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldY = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldZ = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->ddFieldX = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->ddFieldY = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->ddFieldZ = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);

  this->dFieldXp = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldYp = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldZp = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldXm = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldYm = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dFieldZm = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);

  this->upwindBG = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->upwindFG = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);
  this->dPhi = initialiseField(this->xDim, this->yDim, this->zDim, 0.0);

}

/*!
 * LevelSetDomain class metho for handling the construction of the signed distance field
 * @param lockBase Whether to lock cells at the specimen base (z-axis) from evolution or not
 * @param xDim The desired number of grid cells along the x-axis
 * @param yDim The desired number of grid cells along the y-axis
 * @param zDim The desired number of grid cells along the z-axis
 */
void LevelSetDomain::initialiseSDF(bool lockBase, int xDim, int yDim, int zDim){

  this->lockBase = lockBase;

  if (this->specimenModel.phases.size() > 0){
    if (this->specimenModel.phases.size() != 1){
      cout << "Initialising signed distance field using specimen surface (the phase at index zero within the specimen model phase vector)" << endl;
    }

    this->xDim = xDim;
    this->yDim = yDim;
    this->zDim = zDim;

    this->redefineFields();

    TriangularMesh& tmesh = this->specimenModel.phases[0].phaseBoundary;

    double xmin = pointDimMinimum(tmesh.vertices, 0);
    double xmax = pointDimMaximum(tmesh.vertices, 0);
    double ymin = pointDimMinimum(tmesh.vertices, 1);
    double ymax = pointDimMaximum(tmesh.vertices, 1);
    double zmin = pointDimMinimum(tmesh.vertices, 2);
    double zmax = pointDimMaximum(tmesh.vertices, 2);

    this->cellWidthX = (xmax - xmin)*1.2/xDim;
    this->cellWidthY = (ymax - ymin)*1.2/yDim;
    this->cellWidthZ = (zmax - zmin)*1.2/zDim;

    double BLCx, BLCy, BLCz;

    if (this->lockBase == false){
      BLCx = xmin - (xmax - xmin) * 0.1;
      BLCy = ymin - (ymax - ymin) * 0.1;
      BLCz = zmin - (zmax - zmin) * 0.1;
    }
    else {
      BLCx = xmin - (xmax - xmin) * 0.1;
      BLCy = ymin - (ymax - ymin) * 0.1;
      BLCz = zmin - this->cellWidthZ;
    }

    this->bottomLeftDomainCorner = Point(BLCx, BLCy, BLCz);
    initialiseSDF(this->specimenModel.phases[0].phaseBoundary);
    this->initialGap = BLCz + this->cellWidthZ * zDim - zmax;
  }
}

/*!
 * LevelSetDomain class method for calculating the contribution of a particular model phases
 * to the phase grid. This phase grid is used to later accelerate surface panel calculations
 * (only for contested phase grid cells to point-in-polygon tests need to be performed on-the-fly
 * to determine the occupying phase).
 * @param phaseMesh The specimen model phase mesh to test
 * @param phaseIndex The phase index to assign within the phase grid if a grid cell falls entirely within a phase
 * @return void
 */
void LevelSetDomain::assignToPhaseGrid(TriangularMesh& phaseMesh, int phaseIndex){

  //assign model
  scalarField3DInt phaseGridCorners = initialiseFieldInt(this->xDim+1, this->yDim+1, this->zDim+1, 0);
  const int xDimC = this->xDim;
  const int yDimC = this->yDim;
  const int zDimC = this->zDim;
  const double cellWidthXC = this->cellWidthX;
  const double cellWidthYC = this->cellWidthY;
  const double cellWidthZC = this->cellWidthZ;

  //construct phase grid
  #if defined(_OPENMP)
  #pragma omp parallel
  #endif
  {
  Point cellCentre;
  #if defined(_OPENMP)
  #pragma omp for schedule(dynamic)
  #endif
  for (int i=0;i<xDimC+1;i++){ //perform corner checks
    cellCentre.px = this->bottomLeftDomainCorner.px + cellWidthXC * ((double)i-0.5);
    for (int j=0;j<yDimC+1;j++){
      cellCentre.py = this->bottomLeftDomainCorner.py + cellWidthYC * ((double)j-0.5);
      for (int k=0;k<zDimC+1;k++){
        cellCentre.pz = this->bottomLeftDomainCorner.pz + cellWidthZC * ((double)k-0.5);
        phaseGridCorners[i][j][k] = phaseMesh.insideMesh(cellCentre);
      }
    }
  }
  }

  #if defined(_OPENMP)
  #pragma omp parallel for schedule(dynamic)
  #endif
  for (int a=0;a<xDimC;a++){ //perform corner checks
    for (int b=0;b<yDimC;b++){
      for (int c=0;c<zDimC;c++){
        if ((phaseGridCorners[a][b][c] == 0) && (phaseGridCorners[a+1][b][c] == 0) &&
            (phaseGridCorners[a][b+1][c] == 0) && (phaseGridCorners[a+1][b+1][c] == 0) &&
            (phaseGridCorners[a][b][c+1] == 0) && (phaseGridCorners[a+1][b][c+1] == 0) &&
            (phaseGridCorners[a][b+1][c+1] == 0) && (phaseGridCorners[a+1][b+1][c+1] == 0)) {
              phaseGrid[a][b][c] = phaseIndex;
        }
        //else check if all corner points are outside phase
        else if ((phaseGridCorners[a][b][c] == 1) && (phaseGridCorners[a+1][b][c] == 1) &&
                 (phaseGridCorners[a][b+1][c] == 1) && (phaseGridCorners[a+1][b+1][c] == 1) &&
                 (phaseGridCorners[a][b][c+1] == 1) && (phaseGridCorners[a+1][b][c+1] == 1) &&
                 (phaseGridCorners[a][b+1][c+1] == 1) && (phaseGridCorners[a+1][b+1][c+1] == 1)) {
                   continue;
        }
        //else check if intersecting boundary
        else{
            phaseGrid[a][b][c] = -1;
        }
      }
    }
  }
}

/*!
 * LevelSetDomain class method for calculating the extension velocity to use. Does not invoke distance approximations.
 * @param surfaceMesh The current specimen surface mesh
 * @param sVel vector of specimen surface panel velocities on the boundary (length matches number of surfaceMesh panels)
 * @param shank The number of base cells along the z-axis to assign a zero velocity (freeze).
 * @return void
 */
void LevelSetDomain::constructExtensionVelocity(TriangularMesh& surfaceMesh, vector<double>& sVel, int shank){

  long panelNum = surfaceMesh.faces.size();

  #if defined(_OPENMP)
  #pragma omp parallel
  {
  #endif

  double lpx;
  double lpy;
  double lpz;
  double tdist2;
  double dist2;
  double evapVal;
  double vcx, vcy, vcz;

  #if defined(_OPENMP)
  #pragma omp for schedule(dynamic)
  #endif
  for (int i=0;i<this->xDim; i++){
    lpx = this->cellWidthX * (double)i + this->bottomLeftDomainCorner.px;
    for (int j=0;j<this->yDim; j++){
      lpy = this->cellWidthY * (double)j + this->bottomLeftDomainCorner.py;
      for (int k=shank;k<this->zDim;k++){
        lpz = this->cellWidthZ * (double)k + this->bottomLeftDomainCorner.pz;
        tdist2 = numeric_limits<double>::max();
        for (int p=0;p<panelNum;p++){
          vcx = lpx - surfaceMesh.faces[p].centre.px;
          vcy = lpy - surfaceMesh.faces[p].centre.py;
          vcz = lpz - surfaceMesh.faces[p].centre.pz;
          dist2 = vcx*vcx + vcy*vcy + vcz*vcz;
          if (dist2 < tdist2){
            evapVal = sVel[p];
            tdist2 = dist2;
          }
        }
        this->vField[i][j][k] = evapVal;
      }
    }
  }
  #if defined(_OPENMP)
  }
  #endif
}

/*!
 * LevelSetDomain class method for calculating the extension velocity to use. This method invokes distance approximation schemes
 * to further accelerate the extension velocity calculation.
 * @param surfaceMesh The current specimen surface mesh
 * @param sVel vector of specimen surface panel velocities on the boundary (length matches number of surfaceMesh panels)
 * @param decimatedPoints vector of decimated panel points defining a 'coarse' mesh passed a certain distance threshold
 * @param decimatedVel vector of corresponding velocities for decimatedPoints
 * @param field reference to the current level set field
 * @param narrowband for a magnitude field value greater than this (e.g. signed distance from the boundary), distance approximations are invoked.
 * @param shank The number of base cells along the z-axis to assign a zero velocity (freeze).
 * @return void
 */
void LevelSetDomain::constructExtensionVelocityAdaptive(TriangularMesh& surfaceMesh, vector<double>& sVel,
                                                        vector<Point>& decimatedPoints, vector<double>& decimatedVel,
                                                        scalarField3D& field, double narrowband, int shank){

  long decimatedPanelNum = decimatedPoints.size();
  long panelNum = surfaceMesh.faces.size();

  const int xDim = this->xDim;
  const int yDim = this->yDim;
  const int zDim = this->zDim;


  #if defined(_OPENMP)
  #pragma omp parallel
  {
  #endif

  double lpx;
  double lpy;
  double lpz;
  double tdist2;
  double dist2;
  double evapVal;
  double vcx, vcy, vcz;

  double maximumCellWidth = max({cellWidthX, cellWidthY, cellWidthZ});

  #if defined(_OPENMP)
  #pragma omp for schedule(dynamic)
  #endif
  for (int i=0;i<xDim; i++){
    lpx = this->cellWidthX * (double)i + this->bottomLeftDomainCorner.px;
    for (int j=0;j<yDim; j++){
      lpy = this->cellWidthY * (double)j + this->bottomLeftDomainCorner.py;
      for (int k=shank;k<zDim;k++){
        lpz = this->cellWidthZ * (double)k + this->bottomLeftDomainCorner.pz;
        tdist2 = numeric_limits<double>::max();
        if (fabs(field[i][j][k]) > narrowband * maximumCellWidth){
          if ((i % EXTVELSAMPLING != 0) || (j % EXTVELSAMPLING != 0) || (k % EXTVELSAMPLING != 0)){
            if ((i > 0) && (j > 0) && (k > shank) && (i < xDim-1) && (j < yDim-1) && (k < zDim-1))
              continue;
          }
          for (int p=0;p<decimatedPanelNum;p++){
            vcx = lpx - decimatedPoints[p].px;
            vcy = lpy - decimatedPoints[p].py;
            vcz = lpz - decimatedPoints[p].pz;
            dist2 = vcx*vcx + vcy*vcy + vcz*vcz;
            if (dist2 < tdist2){
              evapVal = decimatedVel[p];
              tdist2 = dist2;
            }
          }
        }
        else {
          for (int p=0;p<panelNum;p++){
            vcx = lpx - surfaceMesh.faces[p].centre.px;
            vcy = lpy - surfaceMesh.faces[p].centre.py;
            vcz = lpz - surfaceMesh.faces[p].centre.pz;
            dist2 = vcx*vcx + vcy*vcy + vcz*vcz;
            if (dist2 < tdist2){
              evapVal = sVel[p];
              tdist2 = dist2;
            }
          }
        }
        this->vField[i][j][k] = evapVal;
      }
    }
  }

  int x0, y0, z0, x1, y1, z1, xd, yd, zd, dx, dy, dz;
  double c00, c01, c10, c11, c0, c1;

  #if defined(_OPENMP)
  #pragma omp for schedule(dynamic)
  #endif
  for (int i=0;i<xDim; i++){
    lpx = this->cellWidthX * (double)i + this->bottomLeftDomainCorner.px;
    for (int j=0;j<yDim; j++){
      lpy = this->cellWidthY * (double)j + this->bottomLeftDomainCorner.py;
      for (int k=shank;k<zDim; k++){
        lpz = this->cellWidthZ * (double)k + this->bottomLeftDomainCorner.pz;
        if (fabs(field[i][j][k]) > narrowband * maximumCellWidth){
          dx = i % EXTVELSAMPLING;
          dy = j % EXTVELSAMPLING;
          dz = k % EXTVELSAMPLING;

          if ((dx != 0) || (dy != 0) || (dz != 0)){
            x0 = i - dx;
            if (x0 < 0)
              x0 = 0;
            y0 = j - dy;
            if (y0 < 0)
              y0 = 0;
            z0 = k - dz;
            if (z0 < shank)
              z0 = shank;

            x1 = i + EXTVELSAMPLING - dx;
            if (x1 > (xDim - 1))
              x1 = xDim -1;
            y1 = j + EXTVELSAMPLING - dy;
            if (y1 > (yDim - 1))
              y1 = yDim - 1;
            z1 = k + EXTVELSAMPLING - dz;
            if (z1 > (zDim - 1))
              z1 = zDim - 1;

            //performing trilinear interpolation
            xd = 0;
            if ((x1 - x0) > 0)
              xd = (i - x0) * 1.0/(x1 - x0);
            yd = 0;
            if ((y1 - y0) > 0)
              yd = (j - y0) * 1.0/(y1 - y0);
            zd = 0;
            if ((z1 - z0) > 0)
              zd = (k - z0) * 1.0/(z1 - z0);
            c00 = this->vField[x0][y0][z0] * (1.0 - xd) + this->vField[x1][y0][z0] * xd;
            c01 = this->vField[x0][y0][z1] * (1.0 - xd) + this->vField[x1][y0][z1] * xd;
            c10 = this->vField[x0][y1][z0] * (1.0 - xd) + this->vField[x1][y1][z0] * xd;
            c11 = this->vField[x0][y1][z1] * (1.0 - xd) + this->vField[x1][y1][z1] * xd;

            c0 = c00 * (1.0 - yd) + c10 * yd;
            c1 = c01 * (1.0 - yd) + c11 * yd;
            this->vField[i][j][k] = c0 * (1.0 - zd) + c1 * zd;
          }
        }
      }
    }
  }

  #if defined(_OPENMP)
  }
  #endif
}
