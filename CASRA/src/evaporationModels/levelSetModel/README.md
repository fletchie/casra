# LevelSetEvaporationModel

Directory containing the level-set-bem evaporation model library core source files. This model is a refactored version of that described in [Fletcher et. al 2020](https://iopscience.iop.org/article/10.1088/1361-6463/abaaa6).
