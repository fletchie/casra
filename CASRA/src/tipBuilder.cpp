/*
 * CASRA - library for Continuum Simulation and Reconstruction methods for Atom Probe Tomography
 * Copyright (C) 2021 Charles Fletcher
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Lesser Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <vector>
#include <cstdio>
#include <tuple>

#include "CASRA/tipBuilder.h"
#include "CASRA/tipBuild/tipBuild.h"

using namespace std;

const char* supportedXMLConfigFileExt[] = {"xml", ""};

const char *helpSynonyms[] = {"-h", "-help", ""};

//initialise help strings for -h mode
string configFileArgHelp = "File location of model configuration file. \n\tSupported extensions: ";
string outputConfigFileArgHelp = "File location for the output model configuration file containing the generated meshes. \n\tSupported extensions: ";

/*!
 *
 * Function called on running CASRA simulated evaporation tool from the command line
 * takes command line arguments
 */
int main(int argc, char* argv[])
{

  int ret = 0;

  char configFileArg[] = "--iconfig";
  char outputConfigFileArg[] = "--oconfig";

  string modelConfigFile = "";
  string outputConfigFile = "";

  string argSymbol = "--";
  string argInputVal;

  bool helpEnabled = false;

  //if no input detected
  if (argc == 1) {
    cout << "Welcome to CASRA's tip builder program. \nRun `tipBuilder -h` for help." << endl;
    return 0;
  }
  else if (argc == 2) { //check if help passed
    int a = 0;
    while (strlen(helpSynonyms[a]) != 0) {
      if (strcmp(argv[1], helpSynonyms[a]) == 0) {
        helpEnabled = true;
        break;
      }
      a ++;
    }
  }
  else {
    if (!(argc % 2)) {
      cout << "Incomplete number of inputs detected. Please check all arguments are defined." << endl;
      return 1;
    }
    else {
      for (int i=1; i < argc; i++){
        if (i % 2 == 1) {
          argInputVal = (string)argv[i];
          if (argSymbol == argInputVal.substr(0, 2)) {
            //command line input argument detected
            if (strcmp(configFileArg, argv[i])==0) {
              cout << "Supplied argument: " << configFileArg << " " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                modelConfigFile = (string)argv[i+1];
              }
              else {
                cout << "No following input argument found for: " << configFileArg << ". Please check input." << endl;
                return 1;
              }
            }
            else if (strcmp(outputConfigFileArg, argv[i])==0) {
              cout << "Supplied argument: " << outputConfigFileArg << " " << argv[i+1] << endl;
              if (i < (argc - 1)) {
                outputConfigFile = (string)argv[i+1];
              }
              else {
                cout << "No following input argument found for: " << outputConfigFileArg << ". Please check input." << endl;
                return 1;
              }
            }
            else {
              cout << "Unidentified input argument name detected. Please check input." << endl;
              return 1;
            }
          }
          else {
            cout << "Input argument name " << argv[i] << " not correctly marked as argument. Please check input." << endl;
            return 1;
          }
        }
      }

      if (modelConfigFile == "") {
        cout << "No input tip configuration file supplied. Cannot define evaporation model." << endl;
        return 1;
      }
      if (outputConfigFile == "") {
        outputConfigFile = "build_" + modelConfigFile;
        cout << outputConfigFile << endl;
        cout << "No output tip configuration file supplied. Using default of '" + outputConfigFile + "' within current local directory." << endl;
      }

      if (ret > 0) {
        cout << "Execution failed." << endl;
      }
      else {
        cout << "Execution succeeded." << endl;
      }
    }
  }

  if (helpEnabled == true) {

    //Assemble model configuration file help string to append supported range file extension types
    int h = 0;
    while (strlen(supportedXMLConfigFileExt[h]) != 0) {
      configFileArgHelp = configFileArgHelp + (string)supportedXMLConfigFileExt[h] + ",";
      h++;
    }
    configFileArgHelp.erase(configFileArgHelp.end()-1,configFileArgHelp.end());

    //Assemble write directory configuration file help string
    h = 0;
    while (strlen(supportedXMLConfigFileExt[h]) != 0) {
      outputConfigFileArgHelp = outputConfigFileArgHelp + (string)supportedXMLConfigFileExt[h] + ",";
      h++;
    }
    outputConfigFileArgHelp.erase(outputConfigFileArgHelp.end()-1,outputConfigFileArgHelp.end());

    //output help message
    cout << "Valid input arguments: \n"
         << configFileArg << " : " << configFileArgHelp << "\n"
         << outputConfigFileArg << " : " << outputConfigFileArgHelp << endl;
  }
  else {
    int info = tipBuild(modelConfigFile, outputConfigFile);
  }

  return ret;

}
